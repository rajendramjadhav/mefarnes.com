-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 05, 2017 at 04:54 AM
-- Server version: 5.5.52-cll-lve
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mefarnes`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(80) CHARACTER SET latin1 NOT NULL,
  `password` varchar(40) CHARACTER SET latin1 NOT NULL,
  `first_name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `email`, `password`, `first_name`, `last_name`, `created_date`, `modified_date`) VALUES
(1, 'admin@mefarnes.com', 'a69dc2d37a97a4c6ce11c9e7e89c3fb6daaa329f', 'Bikash', 'Paul', '2016-09-05 07:19:00', NULL),
(3, 'mbvarmora@gmail.com', '601f1889667efaebb33b8c12572835da3f027f78', 'Bikash', 'Paul', '2016-09-05 07:19:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `campaign_categoriy_id` bigint(20) unsigned NOT NULL,
  `title` varchar(256) NOT NULL,
  `slug` varchar(256) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `description` text NOT NULL,
  `video_type` tinyint(1) DEFAULT '1' COMMENT '1=yotubevideo ,2=vimeo video',
  `video_file_name` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `video_url` varchar(256) DEFAULT NULL,
  `campaign_location_id` varchar(255) NOT NULL,
  `fund_needed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=inactive, 1=active,-1=deleted',
  `donation_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active ,0==inactive',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `preview_mode` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=preview mode ,0=live',
  `home_page_visible` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=not visible,1=visible',
  `home_page_not_featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=not visible,1=visible',
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`,`user_id`,`campaign_categoriy_id`,`campaign_location_id`),
  KEY `fk_campaigns_users_idx` (`user_id`),
  KEY `fk_campaigns_campaign_categories1_idx` (`campaign_categoriy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=148 ;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `user_id`, `campaign_categoriy_id`, `title`, `slug`, `image`, `description`, `video_type`, `video_file_name`, `video_url`, `campaign_location_id`, `fund_needed`, `start_date_time`, `end_date_time`, `status`, `donation_status`, `create_date`, `preview_mode`, `home_page_visible`, `home_page_not_featured`, `priority`) VALUES
(53, 190, 26, '00000', '00000', '53_5834f0c131974.png', '<p>00000</p>', 1, NULL, '', 'Kenilworth, NJ, United States', '100000.00', '2016-11-23 01:28:08', '2016-12-23 23:59:59', -1, 1, '2016-11-23 08:28:08', 0, 0, 0, 0),
(55, 205, 7, 'Test', 'test', '55_583d2b436e07a.png', '<p>testing</p><p><a href="https://en.wikipedia.org/wiki/SMS_L%C3%BCtzow"><strong>SMS <em>L&uuml;tzow</em></strong></a> was the second <a href="https://en.wikipedia.org/wiki/Derfflinger-class_battlecruiser"><em>Derfflinger</em>-class</a> <a href="https://en.wikipedia.org/wiki/Battlecruiser">battlecruiser</a> built by the German Imperial Navy before <a href="https://en.wikipedia.org/wiki/World_War_I">World War I</a>. Launched on 29 November 1913, the ship was named in honor of the Prussian general <a href="https://en.wikipedia.org/wiki/Ludwig_Adolf_Wilhelm_von_L%C3%BCtzow">Ludwig Adolf Wilhelm von L&uuml;tzow</a> who fought in the <a href="https://en.wikipedia.org/wiki/Napoleonic_Wars">Napoleonic Wars</a>. Due to engine damage during trials, <em>L&uuml;tzow</em> did not join the <a href="https://en.wikipedia.org/wiki/I_Scouting_Group">I Scouting Group</a> until March 1916. She missed most of the major actions conducted by the German battlecruiser force, taking part in only one bombardment operation, at <a href="https://en.wikipedia.org/wiki/Bombardment_of_Yarmouth_and_Lowestoft">Yarmouth and Lowestoft</a>, on 24&ndash;25 April 1916. One month after becoming Admiral <a href="https://en.wikipedia.org/wiki/Franz_von_Hipper">Franz von Hipper</a>&#39;s flagship, <em>L&uuml;tzow</em> sank the British battlecruiser <a href="https://en.wikipedia.org/wiki/HMS_Invincible_%281907%29">HMS&nbsp;<em>Invincible</em></a> during the <a href="https://en.wikipedia.org/wiki/Battle_of_Jutland">Battle of Jutland</a> (31 May&nbsp;&ndash; 1 June); she is sometimes given credit for sinking the <a href="https://en.wikipedia.org/wiki/Armored_cruiser">armored cruiser</a> <a href="https://en.wikipedia.org/wiki/HMS_Defence_%281907%29">HMS&nbsp;<em>Defence</em></a> as well. Heavily damaged by around 24 heavy-caliber shell hits that flooded her <a href="https://en.wikipedia.org/wiki/Bow_%28ship%29">bow</a>, the ship was unable to make the return voyage to German ports. Her crew was evacuated and she was sunk by torpedoes fired by one of her escorts, the torpedo boat <a href="https://en.wikipedia.org/wiki/SMS_G38"><em>G38</em></a>. (<a href="https://en.wikipedia.org/wiki/SMS_L%C3%BCtzow"><strong>Full&nbsp;article...</strong></a>)</p><p><em>Part of the <strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Featured_topics/Battlecruisers_of_the_world">Battlecruisers of the world</a></strong> series, one of Wikipedia&#39;s <a href="https://en.wikipedia.org/wiki/Wikipedia:Featured_topics">featured topics</a>.</em></p><p>Recently featured:</p><ul><li><a href="https://en.wikipedia.org/wiki/Keith_Miller_with_the_Australian_cricket_team_in_England_in_1948">Keith Miller with the Australian cricket team in England in 1948</a></li><li><em><a href="https://en.wikipedia.org/wiki/Banksia_canei">Banksia canei</a></em></li><li><em><a href="https://en.wikipedia.org/wiki/Warlugulong">Warlugulong</a></em></li></ul><ul><li><strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Today%27s_featured_article/November_2016">Archive</a></strong></li><li><strong><a href="https://lists.wikimedia.org/mailman/listinfo/daily-article-l">By email</a></strong></li><li><strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Featured_articles">More featured articles...</a></strong></li></ul><p>Did you know...</p><p>&nbsp;</p><p>Sydney Eardley-Wilmot</p><ul><li>... that <strong><a href="https://en.wikipedia.org/wiki/Sydney_Eardley-Wilmot">Sydney Eardley-Wilmot</a></strong> <em>(pictured)</em> was one of four brothers who served as officers in the British military?</li><li>... that it is not known whether the Marquis Leone de Tarragon named the <strong><a href="https://en.wikipedia.org/wiki/Rosy-throated_longclaw">rosy-throated longclaw</a></strong> (<em>Macronyx ameliae</em>) for his wife or his mother?</li><li>... that horse trainer <strong><a href="https://en.wikipedia.org/wiki/Wink_Groover">Wink Groover</a></strong> and his <a href="https://en.wikipedia.org/wiki/Tennessee_Walking_Horse">Tennessee Walking Horse</a> Ace&#39;s Sensation won a <a href="https://en.wikipedia.org/wiki/Tennessee_Walking_Horse_National_Celebration#World_Grand_Championship">World Grand Championship</a> at their third attempt?</li><li>... that <strong><a href="https://en.wikipedia.org/wiki/Sairecabur">Sairecabur</a></strong> has the world&#39;s highest submillimetre telescope and is adjacent to a peak that may have been one of the world&#39;s highest volcanoes at over 7,000 metres (23,000&nbsp;ft)?</li><li>... that <a href="https://en.wikipedia.org/wiki/Auschwitz_concentration_camp">Auschwitz</a> survivor <strong><a href="https://en.wikipedia.org/wiki/Bat-Sheva_Dagan">Bat-Sheva Dagan</a></strong> writes <a href="https://en.wikipedia.org/wiki/The_Holocaust">Holocaust</a> stories for children that have <a href="https://en.wikipedia.org/wiki/Happy_ending">happy endings</a> &quot;in order not to rob them of their faith in mankind&quot;?</li><li>... that video game <em><strong><a href="https://en.wikipedia.org/wiki/MeiQ:_Labyrinth_of_Death">MeiQ: Labyrinth of Death</a></strong></em> features characters paired with robotic Guardians?</li><li>... that the chamber orchestra <strong><a href="https://en.wikipedia.org/wiki/Folkwang_Kammerorchester_Essen">Folkwang Kammerorchester Essen</a></strong>, founded in 1958, is the only orchestra to regularly perform at the historic <a href="https://en.wikipedia.org/wiki/Villa_H%C3%BCgel">Villa H&uuml;gel</a>?</li><li>... that <strong><a href="https://en.wikipedia.org/wiki/Section_127_of_the_Australian_Constitution">Section 127 of the Australian Constitution</a></strong> mandated that the <a href="https://en.wikipedia.org/wiki/Aboriginal_Australians">Aboriginal peoples</a> not be counted in &quot;reckoning the numbers of the people of the Commonwealth&quot;?</li></ul><ul><li><strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Recent_additions">Recently improved articles</a></strong></li><li><strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Your_first_article">Start a new article</a></strong></li><li><strong><a href="https://en.wikipedia.org/wiki/Template_talk:Did_you_know">Nominate an article</a></strong></li></ul>', 1, NULL, '', 'Ahmedabad, Gujarat, India', '100.00', '2016-11-29 07:16:01', '2016-12-29 23:59:59', -1, 1, '2016-11-24 21:00:45', 0, 0, 0, 0),
(56, 213, 27, 'Support Yismach: Rebuilding the Jewish People - one couple at a time', 'support-yismach:-rebuilding-the-jewish-people-one-couple-at-a-time', '56_5839b8fb55a15.png', '<p>Yismach is not an online dating site; it&rsquo;s essentially an electronic filing cabinet with search options.</p><p>Recognizing the challenges faced by overwhelmed shadchanim who often receive numerous calls a day and work with resum&eacute;s written on hundreds of pieces of paper, we put together a database open to all &ndash; everyone in shidduchim and every recognized established shadchan.</p><p>Yismach has two primary premises.&nbsp; We want to protect the dignity of those in shidduchim, and we want to maintain the element of secrecy because the segulah for a successful shidduch is secrecy.</p><p>With this in mind, we set up a system that protects the client&rsquo;s dignity by compiling information that is accessible only to established, trusted, professional shadchanim. People can feel safe that their information is not accessible on the internet.</p><p>A Wider Scope</p><p>By putting your profile on Yismach, you are in effect introducing yourself to more shadchanim who will see you there and keep you in mind. Thanks to Yismach, you can be contacted by shadchanim that you wouldn&rsquo;t have had the chance to turn to. A contrasting point: &ldquo;At Yismach, you can choose which shadchanim you want to work with. If you prefer not to work with certain people, you can make your profile inaccessible to them.&rdquo;</p><p>Yismach shadchanim aren&rsquo;t making suggestions using only profiles on the database. Every shadchan knows of hundreds of other available possibilities, so everyone who signs up could potentially be matched with any one of the people that every shadchan knows.</p><p>It&rsquo;s All About the Person</p><p>When you sign up at Yismach, Professor Neumann and his wife take the time to speak to you. In contrast, when you sign up at an interactive dating site [totally different from Yismach&rsquo;s format], the <em>shadchanit</em> was simply processing me through her computer. There was no human aspect. In order to avoid mechanical responses and at the same time expedite the dating process, Yismach holds separate events for men and women every few months where new sign-ups can briefly meet about a dozen <em>shadchanim</em>.</p><p>These events and programs cost money. Your donation is an investment in accelerating building &ldquo;bais neeman bwyisroel&rdquo; one couple at a time.</p><p>&nbsp;</p>', NULL, NULL, '', 'Jerusalem, Israel', '20000.00', '2016-12-01 05:33:21', '2016-12-31 23:59:59', 0, 1, '2016-11-29 08:16:56', 0, 0, 0, 2),
(57, 214, 5, 'Yeshiva Nachlas Yisroel Yitzchok Shortfall', 'yeshiva-nachlas-yisroel-yitzchok-shortfall', '57_583b2e83794ae.png', '<p>Yeshiva Nachlas Yisroel Yitzchok is unique in the Israeli educational landscape. It is dedicated to serving boys from Anglo families who have made Aliya. Nachlas Yisroel Yitzchok does this by providing a warm, nurturing environment. Nachlas Yisroel Yitzchok offers programs to serve various ages, populations and needs. The high school program offers a secondary education that gives solid Torah values and facilitates acclimation for Olim and Anglo students living in Israel. The &quot;Year in Israel&quot; Program is designed to maximize the first year out of high school for American students completing the 12th grade, &quot;Nachlas&quot; offers this &quot;Year in Israel&quot; for the highly motivated student seeking a high-level learning program, combined with exposure and experience with their Israeli counterparts. The Bais Medrash program - for the more mature college - level student is an intensive classic yeshiva with a built in mentorship program that allows the older student to gain his first experience as an educator, allowing him, in a fully guided structure, to teach and serve as a role model for teens.</p><p>All programs are challenging programs on a high academic level. The Yeshiva boasts knowledgeable, experienced, caring staff who educate through a strong personal relationship, through a focus on individual strengths and through positive modeling. Nachlas Yisroel Yitzchok enables these immigrant boys to acclimate smoothly to their new society, and to grow together. Today, NachJas Yisroel Yitzchok graduates have moved on to some of the top post-high-school programs in the country. They are sought after because of their honesty, their academic excellence, their pleasantness and their positive outlook. Also, they have acquired a strong sense of personal responsibility to the broader Jewish community. It is clear that these valuable qualities were developed and internalized through their education in Nachlas Yisroel Yitzchok.</p><p>This last year there is a shortfall of $40,000 needed to pay back salaries and outstanding bills. The short term need is to get to a breakeven point and long term will need additional funds to enable this expand and enable them to accept additional students in this sorely needed unique yeshiva.</p>', NULL, NULL, '', 'Ramat Beit Shemesh, Bet Shemesh, Israel', '40000.00', '2016-12-01 07:17:00', '2016-12-31 23:59:59', -1, 1, '2016-11-28 06:22:42', 1, 0, 0, 0),
(58, 216, 5, 'Yeshivas Mir Yerushalayim', 'yeshivas-mir-yerushalayim', '58_583be1c49b007.png', '<p><span style="color: #252525; font-family: ''Arial'',sans-serif; font-size: 10.5pt; mso-fareast-font-family: ''Times New Roman''; mso-bidi-language: AR-SA;">Support thousands learning Torah from early morning until late night.<span style="mso-spacerun: yes;">&nbsp;&nbsp;</span><span style="mso-spacerun: yes;">&nbsp;</span></span></p>\r\n<p><span style="color: #252525; font-family: ''Arial'',sans-serif; font-size: 10.5pt; mso-fareast-font-family: ''Times New Roman''; mso-bidi-language: AR-SA;">The Mir Yeshiva, known as the Mirrer Yeshiva or The Mir, is a yeshiva in Jerusalem, Israel. With over 7,500 single and married students, it is the largest yeshiva in Israel and one of the largest in the world. Most students are from the United States and Israel, with many from other part of the world such as UK, Belgium, France, Mexico, Switzerland, Argentina, Australia and Canada. It was under Rav Nosson Tzvi Finkel zt&rdquo;l that the yeshiva''s enrollment grew into the thousands. The large enrollment was divided into chaburas, or learning groups. Each chabura consists of the same type of student &ndash; e.g. American, European, Israeli, Hasidic, and non-Hasidic. These chaburas sit in designated areas in the Mir''s various study halls (such as Beis Yishaya, Beis Shalom, and the Merkazei), as well as in the same area in the dining room. Each chabura is subdivided by shiur (class), with one maggid shiur (lecturer) teaching an average of 40 to 60 students. The largest shiur in the yeshiva is that of Rabbi Asher Arieli, who gives shiurim in Yiddish to approximately 700 students. </span></p>\r\n<p>&nbsp;</p>\r\n<p style="background: white; margin: 6pt 0in; line-height: normal;"><span style="color: #252525; font-family: ''Arial'',sans-serif; font-size: 10.5pt; mso-fareast-font-family: ''Times New Roman''; mso-bidi-language: AR-SA;">When Rav Nosson Tzvi was alive, he traveled worldwide to raise money and bought in an excess of 500 million dollars but after his passing, the yeshiva cash flow was slowed but their costs of operation and feeding thousands so that they can maximize their time in learning remains costs not covered by the modest tuition and donations from benefactors who have supported this for years. As the new zeman starts, there is a shortfall. <span style="mso-spacerun: yes;">&nbsp;</span></span></p>\r\n<p>&nbsp;</p>', 1, NULL, 'https://vimeo.com/61545088', 'Jerusalem, Israel', '60000.00', '2016-11-28 07:51:52', '2016-12-28 23:59:59', -1, 1, '2016-11-28 14:37:27', 1, 0, 0, 0),
(60, 190, 11, 'test4', 'test4', '60_583cd4ac8734b.png', '<p>teststteststteststteststteststteststte</p>\r\n<p>ststteststteststteststteststteststteststteststt</p>\r\n<p>eststteststteststteststteststteststteststteststtestst &nbsp; teststtestst</p>\r\n<p>teststteststteststteststteststteststtestst</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', NULL, NULL, '', 'Secaucus, NJ, United States', '10000.00', '2016-11-29 01:09:27', '2016-12-29 23:59:59', -1, 1, '2016-11-29 08:09:19', 0, 0, 0, 0),
(61, 190, 19, 'xcddd', 'xcddd', '61_583cd5c0308c9.png', '<p>dddd</p>', 1, NULL, '', 'Delancey Street, New York, NY, United States', '199.00', '2016-11-29 01:11:17', '2016-12-29 23:59:59', -1, 1, '2016-11-29 08:11:17', 0, 0, 0, 0),
(63, 205, 6, 'tewstakla akdnvclad; lkadsfc lkansd;aljdshcasd', 'tewstakla-akdnvclad;-lkadsfc-lkansd;aljdshcasd', '63_583d2bebb44e2.png', '<p><a href="https://en.wikipedia.org/wiki/SMS_L%C3%BCtzow"><strong>SMS <em>L&uuml;tzow</em></strong></a> was the second <a href="https://en.wikipedia.org/wiki/Derfflinger-class_battlecruiser"><em>Derfflinger</em>-class</a> <a href="https://en.wikipedia.org/wiki/Battlecruiser">battlecruiser</a> built by the German Imperial Navy before <a href="https://en.wikipedia.org/wiki/World_War_I">World War I</a>. Launched on 29 November 1913, the ship was named in honor of the Prussian general <a href="https://en.wikipedia.org/wiki/Ludwig_Adolf_Wilhelm_von_L%C3%BCtzow">Ludwig Adolf Wilhelm von L&uuml;tzow</a> who fought in the <a href="https://en.wikipedia.org/wiki/Napoleonic_Wars">Napoleonic Wars</a>. Due to engine damage during trials, <em>L&uuml;tzow</em> did not join the <a href="https://en.wikipedia.org/wiki/I_Scouting_Group">I Scouting Group</a> until March 1916. She missed most of the major actions conducted by the German battlecruiser force, taking part in only one bombardment operation, at <a href="https://en.wikipedia.org/wiki/Bombardment_of_Yarmouth_and_Lowestoft">Yarmouth and Lowestoft</a>, on 24&ndash;25 April 1916. One month after becoming Admiral <a href="https://en.wikipedia.org/wiki/Franz_von_Hipper">Franz von Hipper</a>&#39;s flagship, <em>L&uuml;tzow</em> sank the British battlecruiser <a href="https://en.wikipedia.org/wiki/HMS_Invincible_%281907%29">HMS&nbsp;<em>Invincible</em></a> during the <a href="https://en.wikipedia.org/wiki/Battle_of_Jutland">Battle of Jutland</a> (31 May&nbsp;&ndash; 1 June); she is sometimes given credit for sinking the <a href="https://en.wikipedia.org/wiki/Armored_cruiser">armored cruiser</a> <a href="https://en.wikipedia.org/wiki/HMS_Defence_%281907%29">HMS&nbsp;<em>Defence</em></a> as well. Heavily damaged by around 24 heavy-caliber shell hits that flooded her <a href="https://en.wikipedia.org/wiki/Bow_%28ship%29">bow</a>, the ship was unable to make the return voyage to German ports. Her crew was evacuated and she was sunk by torpedoes fired by one of her escorts, the torpedo boat <a href="https://en.wikipedia.org/wiki/SMS_G38"><em>G38</em></a>. (<a href="https://en.wikipedia.org/wiki/SMS_L%C3%BCtzow"><strong>Full&nbsp;article...</strong></a>)</p><p><em>Part of the <strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Featured_topics/Battlecruisers_of_the_world">Battlecruisers of the world</a></strong> series, one of Wikipedia&#39;s <a href="https://en.wikipedia.org/wiki/Wikipedia:Featured_topics">featured topics</a>.</em></p><p>Recently featured:</p><ul><li><a href="https://en.wikipedia.org/wiki/Keith_Miller_with_the_Australian_cricket_team_in_England_in_1948">Keith Miller with the Australian cricket team in England in 1948</a></li><li><em><a href="https://en.wikipedia.org/wiki/Banksia_canei">Banksia canei</a></em></li><li><em><a href="https://en.wikipedia.org/wiki/Warlugulong">Warlugulong</a></em></li></ul><ul><li><strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Today%27s_featured_article/November_2016">Archive</a></strong></li><li><strong><a href="https://lists.wikimedia.org/mailman/listinfo/daily-article-l">By email</a></strong></li><li><strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Featured_articles">More featured articles...</a></strong></li></ul><p>Did you know...</p><p>&nbsp;</p><p>Sydney Eardley-Wilmot</p><ul><li>... that <strong><a href="https://en.wikipedia.org/wiki/Sydney_Eardley-Wilmot">Sydney Eardley-Wilmot</a></strong> <em>(pictured)</em> was one of four brothers who served as officers in the British military?</li><li>... that it is not known whether the Marquis Leone de Tarragon named the <strong><a href="https://en.wikipedia.org/wiki/Rosy-throated_longclaw">rosy-throated longclaw</a></strong> (<em>Macronyx ameliae</em>) for his wife or his mother?</li><li>... that horse trainer <strong><a href="https://en.wikipedia.org/wiki/Wink_Groover">Wink Groover</a></strong> and his <a href="https://en.wikipedia.org/wiki/Tennessee_Walking_Horse">Tennessee Walking Horse</a> Ace&#39;s Sensation won a <a href="https://en.wikipedia.org/wiki/Tennessee_Walking_Horse_National_Celebration#World_Grand_Championship">World Grand Championship</a> at their third attempt?</li><li>... that <strong><a href="https://en.wikipedia.org/wiki/Sairecabur">Sairecabur</a></strong> has the world&#39;s highest submillimetre telescope and is adjacent to a peak that may have been one of the world&#39;s highest volcanoes at over 7,000 metres (23,000&nbsp;ft)?</li><li>... that <a href="https://en.wikipedia.org/wiki/Auschwitz_concentration_camp">Auschwitz</a> survivor <strong><a href="https://en.wikipedia.org/wiki/Bat-Sheva_Dagan">Bat-Sheva Dagan</a></strong> writes <a href="https://en.wikipedia.org/wiki/The_Holocaust">Holocaust</a> stories for children that have <a href="https://en.wikipedia.org/wiki/Happy_ending">happy endings</a> &quot;in order not to rob them of their faith in mankind&quot;?</li><li>... that video game <em><strong><a href="https://en.wikipedia.org/wiki/MeiQ:_Labyrinth_of_Death">MeiQ: Labyrinth of Death</a></strong></em> features characters paired with robotic Guardians?</li><li>... that the chamber orchestra <strong><a href="https://en.wikipedia.org/wiki/Folkwang_Kammerorchester_Essen">Folkwang Kammerorchester Essen</a></strong>, founded in 1958, is the only orchestra to regularly perform at the historic <a href="https://en.wikipedia.org/wiki/Villa_H%C3%BCgel">Villa H&uuml;gel</a>?</li><li>... that <strong><a href="https://en.wikipedia.org/wiki/Section_127_of_the_Australian_Constitution">Section 127 of the Australian Constitution</a></strong> mandated that the <a href="https://en.wikipedia.org/wiki/Aboriginal_Australians">Aboriginal peoples</a> not be counted in &quot;reckoning the numbers of the people of the Commonwealth&quot;?</li></ul><ul><li><strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Recent_additions">Recently improved articles</a></strong></li><li><strong><a href="https://en.wikipedia.org/wiki/Wikipedia:Your_first_article">Start a new article</a></strong></li><li><strong><a href="https://en.wikipedia.org/wiki/Template_talk:Did_you_know">Nominate an article</a></strong></li></ul>', NULL, NULL, '', 'Ahmednagar, Maharashtra, India', '2516.00', '2016-11-29 07:30:00', '2016-12-29 23:59:59', -1, 1, '2016-11-29 14:30:00', 1, 0, 0, 0),
(65, 219, 24, 'Yerushalmi Family - Mother teacher and learning Father has medical problem cannot work', 'yerushalmi-family---mother-teacher-and-learning-father-has-medical-problem-cannot-work', NULL, '<p>I am collecting for a friend of mine - a tzadika - yiras shamayim - davens for me</p><p>&nbsp;</p>', 1, NULL, '', 'Jerusalem, Israel', '50000.00', '2016-11-29 19:10:03', '2016-12-29 23:59:59', 1, 1, '2016-11-30 02:10:03', 1, 0, 0, 0),
(66, 190, 17, 'popoopo', 'popoopo', '66_583fb827cf429.png', '<p>Yismach is not an online dating site; it&rsquo;s essentially an electronic filing cabinet with search options.</p><p>Recognizing the challenges faced by overwhelmed shadchanim who often receive numerous calls a day and work with resum&eacute;s written on hundreds of pieces of paper, we put together a database open to all &ndash; everyone in shidduchim and every recognized established shadchan.</p><p>Yismach has two primary premises.&nbsp; We want to produchim and every recognized established shadchan.</p><p>Yismach has two primary premises.&nbsp; We want to protect the dignity of those in shidduchim, and we want to maintain the element of secrecy because the segulah for a successful shidduch is secrecy.</p><p>With this in mind, we set up a system that protects the client&rsquo;s dignity by compiling information that is accessible only to established, trusted, professional shadchanim. People can feel safe that their information is not accessible on the internet.</p><p>A Wider Scope</p>', NULL, NULL, '', 'Kean University, Morris Avenue, Union, NJ, United States', '900.00', '2016-12-01 05:43:13', '2016-12-31 23:59:59', -1, 1, '2016-12-01 12:42:57', 0, 0, 0, 0),
(67, 213, 24, 'Kallah needs help furnishing first apartment', 'kallah-needs-help-furnishing-first-apartment', '67_583fce2d0120f.png', '<p>A Kallah is in need of money for matresses and basic furnishings. Future husband is in learning and intends to stay in learning and the agreement in this shidduch is that her side will pay for household furinishings and appliances. Help is urgently needed.</p>', 1, NULL, '', 'Jerusalem, Israel', '12500.00', '2016-12-01 07:10:04', '2016-12-31 23:59:59', 1, 1, '2016-12-01 14:10:04', 1, 0, 0, 0),
(68, 205, 10, 'degsfdsfg', 'degsfdsfg', '68_58400675bc178.png', '<p>dfgssdfgsdfgdsfg</p>\r\n<p>dsgf</p>\r\n<p>dfg</p>\r\n<p>dfg</p>\r\n<p>dsgf</p>', 1, NULL, '', 'Ahmedabad, Gujarat, India', '50.00', '2016-12-01 11:15:42', '2016-12-31 23:59:59', -1, 1, '2016-12-01 18:15:42', 0, 0, 0, 0),
(69, 205, 29, 'Test 312', 'test-312', '69_584257f0b7511.png', '<p>dfsdafsafsdfvsadsdf</p>\r\n<p>sdf</p>\r\n<p>sa</p>\r\n<p>fd</p>\r\n<p>sad</p>\r\n<p>s</p>\r\n<p>fasd</p>', 1, NULL, '', 'Africa', '100.00', '2016-12-03 05:27:30', '2017-01-02 23:59:59', -1, 1, '2016-12-03 12:27:30', 0, 0, 0, 0),
(70, 205, 11, 'sdfasdf', 'sdfasdf', NULL, '<p>sdfsdfsdfs</p>', 1, NULL, '', 'Aguascalientes, Mexico', '100.00', '2016-12-03 06:39:30', '2017-01-02 23:59:59', -1, 1, '2016-12-03 13:39:30', 1, 0, 0, 0),
(71, 205, 5, 'dfsg', 'dfsg', NULL, '<p>dsfgdsfgdsfg</p>', 1, NULL, '', 'Asheville, NC, United States', '100.00', '2016-12-03 09:08:59', '2017-01-02 23:59:59', -1, 1, '2016-12-03 16:08:59', 1, 0, 0, 0),
(72, 205, 10, 'dfgdsfg', 'dfgdsfg', NULL, '<p>dfgdfg</p>', 1, NULL, '', 'France', '100.00', '2016-12-03 09:19:59', '2017-01-02 23:59:59', -1, 1, '2016-12-03 16:19:59', 1, 0, 0, 0),
(73, 205, 8, 'sdfsdf', 'sdfsdf', NULL, '<p>dsfgfdsfd</p>\r\n<p>dfg</p>\r\n<p>df</p>\r\n<p>gfgsd</p>\r\n<p>sfg</p>\r\n<p>sf</p>', 1, NULL, '', 'Asda Hulme Superstore, Princess Road, Manchester, United Kingdom', '95.00', '2016-12-03 09:58:19', '2017-01-02 23:59:59', 1, 1, '2016-12-03 16:58:19', 1, 0, 0, 0),
(75, 220, 20, 'Kupat Hair: Getting your money to most needed', 'kupat-hair:-getting-your-money-to-most-needed', '75_58450c1e94e8a.png', '<p>Ten years, month after month during which millions of shekels keep flowing to their specified destinations with astonishing precision, thick volumes containing stories of salvation and stacks of sheets filled with data about families in distress receiving assistance to help put them back on their feet.</p>\r\n<p>Today, Kupat Ha''ir no longer needs to convince the public of its worthiness. Tens of thousands of witnesses located in Jewish communities across the globe testify to its greatness. Every situation of pain or distress finds its way to Kupat Ha''ir, whether the people involved are seeking support, they want to boost the efficacy of their prayers with the power of charity, or they want to submit the names of those suffering to the Gedolei Hador so that they might pray for them.</p>\r\n<p>Top-notch attorneys and accountants as well as intensive government-run inspections ensure that every single penny at Kupat Ha''ir is accounted for. In addition, Kupat Ha''ir has the absolute support and endorsement of the Gedolei Hador. In accordance with the laws of both halachah and the State, Kupat Ha''ir sticks to its founding principles: not a single managerial body receives a salary; everyone works for the sake of Heaven alone. Kupat Ha''ir provides assistance to any person, anywhere, regardless of his background. Details about needy families requesting assistance is verified with utmost discretion and instructions from rabbanim are obeyed with absolute subservience. Kupat Ha''ir is run with utmost transparency; there are no salaries and the amount of money used to run the office is negligible.</p>\r\n<p>The Gedolei Hador have all expressed praise for Kupat Ha''ir''s integrity. Their words glow like the sun: every action of Kupat Ha''ir is one hundred percent subservient to justice, integrity, halachah and ethics!&nbsp;</p>', 1, NULL, 'https://www.youtube.com/watch?v=mPLpOWRFdCg', 'Jerusalem, Israel', '75000.00', '2016-12-05 06:35:23', '2017-01-04 23:59:59', 1, 1, '2016-12-05 13:35:23', 0, 0, 0, 0),
(76, 218, 23, 'Harvesting Electron Flows To Generate Electricity Using WindEfficient, Clean, Cheap, Reliable, Renewable and Sustainable', 'harvesting-electron-flows-to-generate-electricity-using-windefficient-clean-cheap-reliable-renewable-and-sustainable', '76_58451380cdc58.png', '<p style="margin: 0in 0in 8pt 0.25in;"><span style="font-family: Calibri; font-size: medium;">Alternative Energy</span></p>\n<p>National Priority to find alternatives to fossil fuel</p>\n<ul style="list-style-type: disc; direction: ltr;">\n<li style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal;">\n<p style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal; margin-top: 0in; margin-bottom: 0pt; mso-add-space: auto; mso-list: l1 level1 lfo1;">Exponential increases in energy consumption and depletion of energy generation materials drives need to find cheaper, cleaner, and reliable alternatives.</p>\n</li>\n<li style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal;">\n<p style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal; margin-top: 0in; margin-bottom: 0pt; mso-add-space: auto; mso-list: l1 level1 lfo1;">Most alternative energy proposals are not efficient and all carry with them environmental risks.</p>\n</li>\n<li style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal;">\n<p style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal; margin-top: 0in; margin-bottom: 8pt; mso-add-space: auto; mso-list: l1 level1 lfo1;">Harnessing the power of high altitude wind can supplant energy needs of mankind for centuries.</p>\n</li>\n</ul>\n<p><span style="font-family: Calibri; font-size: medium;">Proposed Technology: Electron Wind Generator</span></p>\n<p>We propose a new method of getting electric energy from wind.</p>\n<ul style="list-style-type: disc; direction: ltr;">\n<li style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal;">\n<p style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal; margin-top: 0in; margin-bottom: 0pt; mso-add-space: auto; mso-list: l0 level1 lfo2;">A special injector injects electrons into the atmosphere.</p>\n</li>\n<li style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal;">\n<p style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal; margin-top: 0in; margin-bottom: 0pt; mso-add-space: auto; mso-list: l0 level1 lfo2;">Wind picks up the electrons and moves them in the direction of wind which is also against the direction of electric field.</p>\n</li>\n<li style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal;">\n<p style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal; margin-top: 0in; margin-bottom: 0pt; mso-add-space: auto; mso-list: l0 level1 lfo2;">At some distance from injector a unique grid acquires the electrons, thus charging and producing electricity.</p>\n</li>\n<li style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal;">\n<p style="color: #000000; font-family: ''Calibri'',sans-serif; font-size: 11pt; font-style: normal; font-weight: normal; margin-top: 0in; margin-bottom: 8pt; mso-add-space: auto; mso-list: l0 level1 lfo2;">This method does not require, as does other wind energy devices, strong columns, wind turbines, or electric generators. This proposed wind installation is cheap.</p>\n</li>\n</ul>\n<p><strong><span style="font-family: Calibri; font-size: medium;">Some Information about Wind Energy</span></strong></p>\n<p><span style="font-family: Calibri;"><span style="font-size: medium;">The power of wind engine strongly depends on wind speed (to the third power). Low altitude wind (<em>H </em>= 10 m) has the standard average speed of <strong><em>V </em></strong>= 6 m/s.<span style="mso-spacerun: yes;">&nbsp; </span>High altitude wind is powerful and practically everywhere is stable and constant.<span style="mso-spacerun: yes;">&nbsp; </span>Wind in the troposphere and stratosphere are powerful and permanent.<span style="mso-spacerun: yes;">&nbsp; </span>For example, at an altitude of 5 km, the average wind speed is about 20 M/s, at an altitude 10 - 12 km the wind may reach 40 m/s (at latitude of about 20 - 35</span><sup><span style="font-size: small;">0 </span></sup><span style="font-size: medium;">N).</span></span></p>\n<p><span style="font-family: Calibri;"><span style="font-size: medium;">There are permanent jet streams at high altitude. For example, at <em>H </em>= 12-13 km and about 25</span><sup><span style="font-size: small;">0 </span></sup><span style="font-size: medium;">N latitude, the average wind speed at its core is about 148 km/h (41 m/s). The most intensive portion has a maximum speed of 185 km/h (51 m/s) latitude 22</span><sup><span style="font-size: small;">0</span></sup><span style="font-size: medium;">, and 151 km/h (42 m/s) at latitude 35</span><sup><span style="font-size: small;">0</span></sup><span style="font-size: medium;"> in North America. On a given winter day, speeds in the jet core may exceed 370 km/h (103 m/s) for a distance of several hundred miles along the direction of the wind. Lateral wind shears in the direction normal to the jet stream may be 185 km/h per 556 km to right and 185 km/h per 185 km to the left.</span></span></p>\n<p><span style="font-family: Calibri;"><span style="font-size: medium;">The wind speed of <em>V </em>= 40 m/s at an altitude <em>H </em>= 13 km provides 64 times more energy than surface wind speeds of 6 m/s at an altitude of 10 m.<span style="mso-spacerun: yes;">&nbsp; </span>This is an enormous renewable and free energy source. (See reference: <em>Science and Technology, v.2, </em>p.265). </span></span></p>\n<p><strong><span style="font-family: Calibri; font-size: medium;">We need the money to build a prototype that can be tested for proof of concept. If it works as our computations predict, we can get funding to bring this from conception to inception to energy consumption.</span></strong></p>', 1, NULL, 'https://www.youtube.com/watch?v=o6K9aOPjFP4', 'Boston, MA, United States', '85000.00', '2016-12-05 07:15:17', '2017-01-04 23:59:59', 1, 1, '2016-12-05 14:11:52', 0, 0, 1, 4),
(77, 214, 5, 'Yeshiva Shortfall', 'yeshiva-shortfall', '77_5845153e82966.png', '<p>Yeshiva Nachlas Yisroel Yitzchok is unique in the Israeli educational landscape. It is dedicated to serving boys from Anglo families who have made Aliya. Nachlas Yisroel Yitzchok does this by providing a warm, nurturing environment. Nachlas Yisroel Yitzchok offers programs to serve various ages, populations and needs. The high school program offers a secondary education that gives solid Torah values and facilitates acclimation for Olim and Anglo students living in Israel. The "Year in Israel" Program is designed to maximize the first year out of high school for American students completing the 12th grade, "Nachlas" offers this "Year in Israel" for the highly motivated student seeking a high-level learning program, combined with exposure and experience with their Israeli counterparts. The Bais Medrash program - for the more mature college - level student is an intensive classic yeshiva with a built in mentorship program that allows the older student to gain his first experience as an educator, allowing him, in a fully guided structure, to teach and serve as a role model for teens.</p>\r\n<p>&nbsp;</p>\r\n<p>All programs are challenging programs on a high academic level. The Yeshiva boasts knowledgeable, experienced, caring staff who educate through a strong personal relationship, through a focus on individual strengths and through positive modeling. Nachlas Yisroel Yitzchok enables these immigrant boys to acclimate smoothly to their new society, and to grow together. Today, NachJas Yisroel Yitzchok graduates have moved on to some of the top post-high-school programs in the country. They are sought after because of their honesty, their academic excellence, their pleasantness and their positive outlook. Also, they have acquired a strong sense of personal responsibility to the broader Jewish community. It is clear that these valuable qualities were developed and internalized through their education in Nachlas Yisroel Yitzchok.</p>\r\n<p>&nbsp;</p>\r\n<p>This last year there is a shortfall of $40,000 needed to pay back salaries and outstanding bills. The short term need is to get to a breakeven point and long term will need additional funds to enable this expand and enable them to accept additional students in this sorely needed unique yeshiva.</p>', 1, NULL, '', 'Ramat Beit Shemesh, Bet Shemesh, Israel', '40000.00', '2016-12-05 07:19:27', '2017-01-04 23:59:59', 1, 1, '2016-12-05 14:19:27', 0, 0, 1, 3),
(78, 221, 9, 'Vaad Harabbanim founded by Gedolei Hadorr', 'vaad-harabbanim-founded-by-gedolei-hadorr', '78_5845257373889.png', '<p>Vaad Harabbanim is a Charity organization that was founded by Gedolei Hador, shlita, in order&nbsp;to supervise all charitable matters in Israel. Until its inception, there was no real control or organization to charitable donations and causes, with the very number of charitable funds being overwhelming, as well as problems like knowing if one&acute;s donation really reached its goal, how monies were spent, reviews and audits of activities, and more.</p>\r\n<p>As the economic situation worsened, it became even harder to ensure that charitable funds could be properly monitored, especially for large families in the Chareidi sector.</p>\r\n<p>Understanding the scope of the problem, as well as the large number of unsupervised charitable organizations and the growing lack of trust among donors regarding their generosity&acute;s destination, it was decided to establish a single, central organization that would be directed, operated, supervised and audited by dedicated and honest Rabbinical leaders. Each request for help would be carefully examined, criteria would be set, and every case would be followed up and reviewed to ensure honesty, transparency and that a person&acute;s hard-earned donation reached its destination in full.</p>\r\n<p>But the results surprised even the founders. Within a very short time, the Vaad&acute;s reputation preceded it, and donations increased ten-fold, with the more veteran Gabbaim saying that the Israel before the founding of the Vaad is totally different than the Israel after the organization&acute;s establishment.</p>\r\n<p>The Vaad still serves as Israel&acute;s largest charity fund, and has become the wheel of fortune and salvation for tens of thousands of tragic cases annually. The Vaad has helped countless widows, orphans, families and individuals overcome abject poverty, broken lives, broken families and dealing with fatal and near-fatal illness. In fact, the Vaad is so comprehensive and broad in its clientele base and so honest and efficient in its distribution of funds, that the Posek Hador Harav Hagaon S. Wozner, shlita, has said: "The Vaad Harabbainim is the very essence of charity in its fullest sense, unlike anything else in history."</p>\r\n<p>One of the Vaad&acute;s basic principles is absolutely transparency and trust, which has a snowball and ripple effect: It starts with the trust Gedolei Torah have in the fund, which in turn attracts donors who trust the Gedolim. Since they know that their money will reach its destination in full and that absolute discretion will be maintained vis a vis the recipient, the Vaad&acute;s reputation spreads and more and more kind and caring Jews join the long list of Vaad supporters.</p>\r\n<p>One of the most important factors in this trust is the meticulous and stringent adherence to halacha and honest business ethics. A full time accountant ensures that every cent is properly spent and accounted for, and the strictest due diligence is maintained in managing the fund. Special committees are appointed to investigate particular cases, and every detail regarding the veracity of information and case is filed and checked. Finally, only after formal approval by the Vaad Rabbis is a check issued, which must carry the signatures of two senior Gedolim.</p>\r\n<p>This highly developed trust is what distinguishes the Vaad from many other charitable funds. Yet the Vaad considers it their basic duty not only to provide for the needy, but also to ensure that the donor is respected and confident that his hard-earned money is being used for exactly the purpose it is intended - and with no overhead or administrative expenses deducted at source.</p>\r\n<p>Yearly financial reports are prepared and submitted as required by both civil and Torah law, and are available to all those interested in seeing exactly how their generous contributions were spent.</p>\r\n<p>&nbsp;</p>', 1, NULL, 'https://youtu.be/yXQcrpuc_hE', 'Jerusalem, Israel', '65000.00', '2016-12-05 08:21:33', '2017-01-04 23:59:59', 1, 1, '2016-12-05 15:21:33', 0, 0, 0, 0),
(79, 222, 7, 'United Hatzalah', 'united-hatzalah', '79_58452da5087d8.png', '<p>When emergencies occur, rapid medical treatment increases chances of survival exponentially. Using specially equipped motorcycle ambulances, United Hatzalah&rsquo;s network of more than 3,000 volunteer medics help save thousands of lives each year across Israel by providing medical treatment in three minutes or less. Their humanitarian services are free, universal and available 24 hours a day, seven days a week. Their resources are limited and when major disaster hits using triage there are some who could have survived but for more medical resorces at the site of disaster. The spate of fires in Israel where no one was injured should not lull us into false sense of security. &nbsp;</p>', 1, NULL, 'https://youtu.be/YFH74InzVoM', 'Jerusalem, Israel', '65000.00', '2016-12-29 10:27:51', '2017-01-28 23:59:59', 1, 1, '2016-12-05 16:03:15', 0, 0, 0, 5),
(80, 216, 5, 'The Mir', 'the-mir', '80_584532e77492e.png', '<p>&nbsp;</p>\r\n<p style="margin: 0in 0in 8pt; line-height: normal; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;"><span style="color: #252525; font-family: ''Arial'',sans-serif; font-size: 10.5pt; mso-fareast-font-family: ''Times New Roman''; mso-bidi-language: AR-SA;">Support thousands learning Torah from early morning until late night.<span style="mso-spacerun: yes;">&nbsp;&nbsp; </span></span></p>\r\n<p>&nbsp;</p>\r\n<p style="margin: 0in 0in 8pt; line-height: normal; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;"><span style="color: #252525; font-family: ''Arial'',sans-serif; font-size: 10.5pt; mso-fareast-font-family: ''Times New Roman''; mso-bidi-language: AR-SA;">The Mir Yeshiva, known as the Mirrer Yeshiva or The Mir, is a yeshiva in Jerusalem, Israel. With over 7,500 single and married students, it is the largest yeshiva in Israel and one of the largest in the world. Most students are from the United States and Israel, with many from other part of the world such as UK, Belgium, France, Mexico, Switzerland, Argentina, Australia and Canada. It was under Rav Nosson Tzvi Finkel zt&rdquo;l that the yeshiva''s enrollment grew into the thousands. The large enrollment was divided into chaburas, or learning groups. Each chabura consists of the same type of student &ndash; e.g. American, European, Israeli, Hasidic, and non-Hasidic. These chaburas sit in designated areas in the Mir''s various study halls (such as Beis Yishaya, Beis Shalom, and the Merkazei), as well as in the same area in the dining room. Each chabura is subdivided by shiur (class), with one maggid shiur (lecturer) teaching an average of 40 to 60 students. The largest shiur in the yeshiva is that of Rabbi Asher Arieli, who gives shiurim in Yiddish to approximately 700 students. </span></p>\r\n<p>&nbsp;</p>\r\n<p style="background: white; margin: 6pt 0in; line-height: normal;"><span style="color: #252525; font-family: ''Arial'',sans-serif; font-size: 10.5pt; mso-fareast-font-family: ''Times New Roman''; mso-bidi-language: AR-SA;">When Rav Nosson Tzvi was alive, he traveled worldwide to raise money and bought in an excess of 500 million dollars but after his passing, the yeshiva cash flow was slowed but their costs of operation and feeding thousands so that they can maximize their time in learning remains costs not covered by the modest tuition and donations from benefactors who have supported this for years. As the new zeman starts, there is a shortfall.<span style="mso-spacerun: yes;">&nbsp; </span></span></p>\r\n<p>&nbsp;</p>', 1, NULL, 'https://vimeo.com/61545088', 'Jerusalem, Israel', '95000.00', '2016-12-05 09:26:06', '2017-01-04 23:59:59', -1, 1, '2016-12-05 16:26:06', 0, 0, 0, 0),
(81, 216, 5, 'Support thousands learning Torah from early morning until late night.', 'support-thousands-learning-torah-from-early-morning-until-late-night', '81_584533bdd3c70.png', '<p>My sons learned in Mir and benfited more than money can pay back. With the previous government, budgets to Yeshivas in general were cut severely. When Rav Nosson Tzvi was alive, he traveled worldwide to raise money and bought in an excess of 500 million dollars but after his passing, the yeshiva cash flow was slowed. However they still have to cover their costs of operation and feeding thousands so that they can maximize their time in learning. For those unfamiliar with this Yeshiva:&nbsp;</p>\r\n<p style="margin: 0in 0in 8pt; line-height: normal; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto;"><span style="color: #252525; font-family: ''Arial'',sans-serif; font-size: 10.5pt; mso-fareast-font-family: ''Times New Roman''; mso-bidi-language: AR-SA;">The Mir Yeshiva, known as the Mirrer Yeshiva or The Mir, is a yeshiva in Jerusalem, Israel. With over 7,500 single and married students, it is the largest yeshiva in Israel and one of the largest in the world. Most students are from the United States and Israel, with many from other part of the world such as UK, Belgium, France, Mexico, Switzerland, Argentina, Australia and Canada. It was under Rav Nosson Tzvi Finkel zt&rdquo;l that the yeshiva''s enrollment grew into the thousands. The large enrollment was divided into chaburas, or learning groups. Each chabura consists of the same type of student &ndash; e.g. American, European, Israeli, Hasidic, and non-Hasidic. These chaburas sit in designated areas in the Mir''s various study halls (such as Beis Yishaya, Beis Shalom, and the Merkazei), as well as in the same area in the dining room. Each chabura is subdivided by shiur (class), with one maggid shiur (lecturer) teaching an average of 40 to 60 students. The largest shiur in the yeshiva is that of Rabbi Asher Arieli, who gives shiurim in Yiddish to approximately 700 students. </span></p>\r\n<p>I plead with you to give, and give generously so that this unique Yeshiva can continue its operation without having to turn away even one bochur who wants to learn there but for budget reasons they cannot take in.&nbsp;&nbsp;</p>', 1, NULL, 'https://vimeo.com/61545088', 'Jerusalem, Israel', '275000.00', '2016-12-29 10:20:02', '2017-01-28 23:59:59', 1, 1, '2016-12-05 16:30:12', 0, 0, 0, 1),
(82, 284, 9, 'Kupat Hair Israel', 'kupat-hair-israel', '82_585aa3ce70a48.png', '<p>Kupat Ha''ir assists and supports needy individuals and families and is active among the weaker population. People from all over the world contribute to Kupat Ha''ir and great Rabbanim support Kupat Ha''ir.&nbsp;</p>', 1, NULL, 'http://www.kupat.org/video/?video=95', 'Bnei Brak, Israel', '80000.00', '2016-12-21 15:40:45', '2017-01-20 23:59:59', 1, 1, '2016-12-21 17:14:24', 0, 0, 1, 0),
(87, 302, 14, 'Chofetz Chaim Heritage Foundation', 'chofetz-chaim-heritage-foundation', '87_585aa8ea32093.png', '<p>Our mission is to inspire Jews around the world to grasp the life-enhancing gift of Shmiras Haloshon and to provide easy access to a wide array of options designed to spark personal growth.</p>', 1, NULL, 'https://powerofspeech.org/proper-speech/a-small-giant-video', 'Suffern, New York 10901, United States', '45000.00', '2016-12-21 16:05:06', '2017-01-20 23:59:59', 1, 1, '2016-12-21 23:05:06', 0, 0, 1, 0),
(88, 303, 6, 'Meir Panim', 'meir-panim', '88_585aabe73420f.png', '<p>Meir Panim was established to help alleviate and diminish the harmful effects of poverty on thousands of men, women and children across Israel. The need to help disadvantaged Israeli families break out of the vicious cycle of poverty is something that cannot be ignored. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>', 1, NULL, 'https://www.meirpanim.org/wordpress/wp-content/uploads/2016/08/story_of_volunteers.mp4', 'Jerusalem, Israel', '80000.00', '2016-12-21 16:18:23', '2017-01-20 23:59:59', 1, 1, '2016-12-21 23:18:23', 0, 0, 0, 0),
(89, 304, 6, 'Ichlu Re''im Soup Kitchen', 'ichlu-re-im-soup-kitchen', '89_585ab3d4debcd.png', '<p>In the heart of Jerusalem located in a discrete location not to embarass those who need a hot meal Ichlu Reim gives full meals both duting the week and on Shabbos.</p>', 1, NULL, 'https://www.youtube.com/watch?v=7oP_RGyo6tU', 'Jerusalem, Israel', '45000.00', '2016-12-21 16:52:22', '2017-01-20 23:59:59', 1, 1, '2016-12-21 23:52:22', 0, 0, 1, 0);
INSERT INTO `campaigns` (`id`, `user_id`, `campaign_categoriy_id`, `title`, `slug`, `image`, `description`, `video_type`, `video_file_name`, `video_url`, `campaign_location_id`, `fund_needed`, `start_date_time`, `end_date_time`, `status`, `donation_status`, `create_date`, `preview_mode`, `home_page_visible`, `home_page_not_featured`, `priority`) VALUES
(90, 305, 6, 'Hineni Soup Kitchen', 'hineni-soup-kitchen', '90_585abbd0a439f.png', '<p>Jerusalem is Israel&rsquo;s poorest city where a majority of the poor depend on meals from soup kitchens. Therefore we serve at Hineni&rsquo;s soup kitchen about 200 meals a day and offer food packages and /or vouchers on a regular basis. We prefer to distribute supermarket vouchers in order to preserve a person&rsquo;s dignity and normalize their lives. This also allows them to buy according to their personal needs.</p>', 1, NULL, '', 'Jerusalem, Israel', '35000.00', '2016-12-21 20:54:41', '2017-01-20 23:59:59', 1, 1, '2016-12-22 00:27:45', 0, 0, 0, 0),
(91, 306, 24, 'Adopt-A-Wedding Program', 'adopt-a-wedding-program', '91_585ac1fcaf0c4.png', '<p style="font-size: 12.0pt; line-height: 130%; font-family: Arial;">A wedding day should be a day of joy and celebration, not anxiety and fear.</p>\r\n<p style="font-size: 12.0pt; line-height: 130%; font-family: Arial;">Getting married and hosting a wedding is a huge expense for anyone. Most families are able to find a way to manage the expense, but for poor families in Israel the cost can be prohibitive. The day that should have been the happiest occasion of their lives becomes marked by <strong>disappointment and anxiety over escalating debts</strong>. How can the impoverished afford even a modest celebration, when &ldquo;modest&rdquo; in wedding terms includes hall rental costs, gown rental costs, meal costs, flower and decoration costs, photography costs, and so many other expenses? <strong>Poverty steals beautiful moments</strong>. Laughter is turned to tears.</p>\r\n<p>&nbsp;</p>\r\n<p style="font-size: 12.0pt; line-height: 130%; font-family: Arial;">Yad Eliezer wants to enable every Jewish bride and groom in Israel to <strong>celebrate their wedding with joy</strong>.</p>\r\n<p><span style="font-size: 14.0pt; line-height: 130%; font-family: Arial;"> <strong>Fifty Subsidized Weddings Celebrations Each Month</strong> </span></p>\r\n<p style="font-size: 12.0pt; line-height: 130%; font-family: Arial;">Think you can&rsquo;t dance at two weddings? Yad Eliezer dances at up to five weddings every night! Yad Eliezer partners with wedding halls to keep expenses at a minimum through <strong>highly subsidized food and service costs</strong>. Over 15,000 weddings have been sponsored to date. Yad Eliezer&rsquo;s simple but elegant weddings welcome up to four hundred dinner guests and even larger groups for reception and dancing. <strong>No bride and groom in Israel need be downcast by poverty.</strong></p>\r\n<p style="font-size: 12.0pt; line-height: 130%; font-family: Arial;">Yad Eliezer&rsquo;s popular <strong>Gitty Perkowski Adopt-a-Wedding Program</strong> allows Jewish couples around the world to <strong>share their happy occasion</strong> with a poor couple in Israel. Choose a date from the Yad Eliezer Wedding Calendar and sponsor a wedding in Israel <strong>on the very same night as your own!</strong> Dance at your own wedding with the joy of knowing that, thanks to you, a poor couple in Israel is also dancing. Many have also sponsored weddings in the merit of a loved one suffering from illness, a single friend or relative hoping to find their soul mate, a couple praying for children, and in honor of an anniversary, a birthday, or a yartzheit.</p>\r\n<p style="font-size: 12.0pt; line-height: 130%; font-family: Arial;">&nbsp;</p>\r\n<p style="font-size: 12.0pt; line-height: 130%; font-family: Arial;">Or Adopt-a-Wedding any time <strong>just because you care</strong>.</p>', 1, NULL, 'https://www.youtube.com/watch?v=XjPURvKoS3Q', 'Jerusalem, Israel', '185000.00', '2016-12-21 17:38:22', '2017-01-20 23:59:59', 1, 1, '2016-12-22 00:38:22', 0, 0, 1, 0),
(92, 307, 21, 'One Family Fund', 'one-family-fund', '92_585ac4c755382.png', '<h1 class="entry-title">&nbsp;</h1>\r\n<div class="title">&nbsp;</div>\r\n<p>&nbsp;</p>\r\n<div class="nine columns omega keeper-0">\r\n<div id="ctl00_ctl00_ctl00_MainContent_MainContentInner_RichText_UpdatePanelRichText">\r\n<p><strong>Since 2001, OneFamily has worked in the face of terror to facilitate a resilient Israeli society.</strong></p>\r\n<p><strong>OneFamily&nbsp;</strong>empowers Israel to respond to terror by uniting all victims of terror attacks into one, national, self-supportive family of thousands.</p>\r\n<p><strong>OneFamily&nbsp;</strong>meets the diverse, intensive, and chronic needs of Israel&rsquo;s terror victims through a comprehensive and professional family-approach.</p>\r\n<p><strong>OneFamily&nbsp;</strong>develops an individualized rehabilitation program for each victim of terror based on more than a<a name="_GoBack"></a>&nbsp;decade of expertise and experience.</p>\r\n<p><strong>OneFamily&nbsp;</strong>evaluates and identifies the specific needs of victims and their families with an understanding of how far we can take them, at what pace, and at what price.</p>\r\n<p><strong>OneFamily&nbsp;</strong>provides direct services while coordinating with external entities and specialists that can provide additional assistance to victims.</p>\r\n<p><strong>OneFamily&nbsp;</strong>is devoted to being there with all victims of terror in Israel &ndash; from the moment of terror for as long as they need us&hellip; even if it is for the rest of their lives, with&nbsp;<strong>37 uniquely expert staff members, 731 volunteers, 4 assistance centers around Israel, and endless love.</strong></p>\r\n<p><strong>OneFamily</strong>&nbsp;understands the trauma of bereavement and injury from terror. Our family can&nbsp;provide the key elements that lead to successful rehabilitation.&nbsp;With this level of trauma, comes a feeling of isolation by singular experience. The perception of being alone affects every element of the recovery and rebuilding process. Our presence and resources ensure that victims and their families never need to feel alone.</p>\r\n<p><strong>OneFamily&nbsp;</strong>acknowledges and recognizes the loss, pain and sacrifice of every victim &ndash; taking part in each step of their journey to returning to life after terror.</p>\r\n<p><strong>OneFamily&nbsp;</strong>provides a unique tailor-made blend of financial assistance, therapeutic programs, legal assistance and a steady stream of personal encouragement for each victim and their family, delivered in our hallmark &ldquo;family&rdquo; atmosphere of togetherness that the victims have come to love and trust.</p>\r\n<p><strong><em>In recognition of our success and dedication, OneFamily was awarded the 2011 Presidential Citation for Volunteerism by Israeli President Shimon Peres.</em></strong></p>\r\n</div>\r\n</div>', 1, NULL, 'https://www.youtube.com/user/EngageMutual', 'Philadelphia, PA, United States', '88888.00', '2016-12-21 18:04:32', '2017-01-20 23:59:59', 1, 1, '2016-12-22 01:04:32', 0, 0, 1, 0),
(93, 308, 26, 'ZAKA', 'zaka', '93_585b8b3545f51.png', '<p>ZAKA is Israel&rsquo;s primary rescue and recovery volunteer organization, with thousands of volunteers &nbsp;on call 24/7 to respond to any terror attack, accident or disaster. ZAKA volunteers also work in specialist search and rescue units on land and sea and the UN-recognized ZAKA International Rescue Unit operates at mass casualty incidents around the world.&nbsp;</p>\r\n<p dir="LTR">The Jerusalem-based ZAKA International Rescue Unit operates a specially trained team of volunteer paramedics and search and rescue professionals who are on call 24/7, ready to respond in the fastest possible time to major international mass casualty incidents, wherever they may occur. Under the direction of Chief Operations Officer Mati Goldstein, the ZAKA International Rescue Unit works in close cooperation with Israel''s Foreign Ministry, the IDF and other government bodies.</p>\r\n<p dir="LTR">ZAKA''s assistance at major international terror attacks and natural disasters led the United Nations in 2005 to recognize ZAKA as an international volunteer humanitarian organization This UN recognition enables ZAKA to offer emergency assistance even before the official delegation has left Israel, or the host country has formally asked for help or even when the country has no diplomatic ties with Israel.</p>\r\n<p dir="LTR">ZAKA has been directly responsible that Jews throughout the world has had proper burial with their remains whether the death was caused by violence (terrorist explosion) or natural disaster (earhquake collapsing the house). Help them expand their operations. Please donate.</p>', 1, NULL, 'https://www.youtube.com/watch?v=a-EKsaG55Dc', 'Jerusalem, Israel', '180000.00', '2016-12-29 10:35:19', '2017-01-28 23:59:59', -1, 1, '2016-12-22 15:10:36', 0, 0, 0, 0),
(94, 309, 5, 'Shaar Hashamayim Yeshiva', 'shaar-hashamayim-yeshiva', '94_585b99bdb8501.png', '<p><strong title="lit. &quot;Gates of Heaven&quot;">Shaar Hashamayim Yeshiva</strong> is an Ashkenazi yeshiva in Jerusalem dedicated to the study of the Kabbalistic teachings of the <em title="Rabbi Isaac Luria zt&quot;l; Hebrew: ??&quot;? ????? ??&quot;?">Arizal</em>. It is famous for its student body of advanced Kabbalists &mdash; many of them <em>Roshei Yeshiva</em> and Torah scholars &mdash; as well as beginning and intermediate scholars who study both the revealed and concealed Torah.</p>\r\n<p>The first Roshei Yeshiva of <strong>Shaar Hashamayim Yeshiva</strong> were the founders, <a href="http://www.judaicasavings.com/prodtype.asp?CAT_ID=838" rel="Artist">Rabbi Chaim Yehuda Leib Auerbach</a> and <strong>Rabbi Shimon Horowitz</strong>. Rabbi Auerbach chose to stay with the Yeshiva even after his father, the Admor of <strong>Chernowitz-Chmielnik</strong>, died and his Chassidim asked Rabbi Auerbach to take his father''s place. He declined, leaving the position of Admor unfulfilled.</p>\r\n<p>&nbsp;</p>\r\n<p>Following Rabbi Auerbach''s death in 1954, his son, <a href="http://www.judaicasavings.com/prodtype.asp?CAT_ID=842" rel="Artist">Rabbi Eliezer Auerbach</a>, led the yeshiva for many years. After his death, another of Rabbi Auerbach''s sons, <a href="http://www.judaicasavings.com/prodtype.asp?CAT_ID=843" rel="Artist">Rabbi Refoel Dovid Auerbach</a>, assumed leadership. Subsequent Roshei Yeshiva were <strong>Rabbi Aharon Slotkin</strong> (who died in 1973), <a href="http://www.judaicasavings.com/prodtype.asp?CAT_ID=844" rel="Artist">Rabbi Yechiel Fishel Eisenbach</a> (who served from 1973 until his death in 2008), and the current Roshei Yeshiva, <a href="http://www.judaicasavings.com/prodtype.asp?CAT_ID=845" rel="Artist">Rabbi Yaakov Meir Shechter</a> and <a href="http://www.judaicasavings.com/prodtype.asp?CAT_ID=834" rel="Artist">Rabbi Gamliel Rabinowitz</a>.</p>', 1, NULL, 'https://www.youtube.com/watch?v=10X-5gtNE3w', 'Jerusalem, Israel', '88888.00', '2016-12-22 09:15:19', '2017-01-21 23:59:59', 1, 1, '2016-12-22 16:15:19', 0, 0, 1, 0),
(95, 310, 5, 'Chevron Yeshiva', 'chevron-yeshiva', '95_585bae557efd0.png', '<p class="MsoNormal">Hebron Yeshiva, also known as Yeshivas Hevron, or Knesses Yisroel, is a yeshiva devoted to high-level study of the Talmud. It originated in 1924 when the roshei yeshiva and 150 students of the Slabodka Yeshiva, known colloquially as the "mother of yeshivas", relocated to Hebron.</p>\r\n<p class="MsoNormal">A 1924 edict requiring enlistment in the military or supplementary secular studies in the yeshiva led a large number of students in the Slabodka yeshiva to relocate to Palestine. Rabbi Nosson Tzvi Finkel, also known as "Der Alter fun Slabodka" (The Elder of Slabodka), sent Rabbi Avraham Grodzinski to head this group and establish the yeshiva in Hebron. Upon Grodzinski''s return to Slabodka, the Alter transferred the mashgiach ruchani responsibilities to him, and the rosh yeshiva duties to Rabbi Yitzchok Isaac Sher, and he moved to Hebron to lead the yeshiva there together with Rabbi Moshe Mordechai Epstein. Hebron was chosen over Jerusalem to avoid the influence of the Conservative Old Yishuv.[citation needed] The Slabodka yeshiva in Europe ceased operation during the Holocaust. A branch was also established in Bnei Brak.</p>\r\n<p class="MsoNormal">1929 Hebron massacre and relocation to Jerusalem</p>\r\n<p class="MsoNormal">Hebron yeshiva students, circa 1920s. All but one of these students perished in the pogrom.</p>\r\n<p>&nbsp;</p>\r\n<p class="MsoNormal">Twenty-four students were murdered in the 1929 Hebron massacre, and the yeshiva was re-established in the Geula neighbourhood of Jerusalem. Despite a delay after the death of Rabbi Moshe Hebroni, the last of the previous generation, the yeshiva moved into a new and larger campus in the south-central Givat Mordechai neighbourhood in 1975. This yeshiva today has about 1300 students and is one of the most prestigious and influential Lithuanian yeshivohs in Israel. The current roshei yeshiva are Rabbi Dovid Cohen and Rabbi Yosef Chevroni.</p>', 1, NULL, 'https://www.youtube.com/watch?v=lCImlFx7_Ks', 'Jerusalem, Israel', '80000.00', '2016-12-22 10:30:53', '2017-01-21 23:59:59', 1, 1, '2016-12-22 16:24:30', 0, 0, 0, 0),
(96, 311, 5, 'Yeshiva Beit Matisyahu', 'yeshiva-beit-matisyahu', '96_585bb04a24a7a.png', '<p class="MsoNormal"><span dir="RTL" lang="HE" style="font-family: ''Arial'',sans-serif; mso-ascii-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-hansi-font-family: Calibri; mso-hansi-theme-font: minor-latin;">?????? (?????? ????? ????? ????? ??????? ''???? ?????'') ????? ???? ???"? (1981) ?? ??? ??? ???? ??????, ??? ???? ???? ??? ??"? ??????? ??? ???? ???? ??????. ?????? ????? ?????? ????? ??? ????-???????? ????? ???? ??? ????? ?????? ??????? ???? ???, ???? ????? ?? ?? ??? ?????? ??''???, ?????? ?????? ?? ????? "??? ????</span>".</p>\r\n<p class="MsoNormal">&nbsp;</p>\r\n<p class="MsoNormal"><span dir="RTL" lang="HE" style="font-family: ''Arial'',sans-serif; mso-ascii-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-hansi-font-family: Calibri; mso-hansi-theme-font: minor-latin;">??? ????? ???? ?? ??, ????? ?????? ??????, ??? ???? ??????, ???? ???? ?? ????? ???? ??????</span>.</p>\r\n<p class="MsoNormal">&nbsp;</p>\r\n<p class="MsoNormal"><span dir="RTL" lang="HE" style="font-family: ''Arial'',sans-serif; mso-ascii-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-hansi-font-family: Calibri; mso-hansi-theme-font: minor-latin;">???? ?????? ?? ?-400 ??????. ???? ??"???: ??? ????? ???? ?????, ??? ???? ??? ??????, ??? ????? ???? ??????, ??? ???? ???, ??? ????? ???? ?? (???? ?? ??? ??????) ??? ?? ??? ????? ??, ??? ????? ?????? ???? ??? ?????? (???? ?? ??? ??????). ?????? ???? ??? ???? ??????, ??? ?? ????'' ????? ?????? ????? ?? ??? ????? ?????, ????? ????? ??? ????. ???? ????? ???????? ??? ???? ??? ???? ????? ????. ???? ???? ??"? ??? ?????? ???? ???, ???? ????? ?? ????? ??? ?????</span>.</p>\r\n<p class="MsoNormal"><span dir="RTL" lang="HE" style="font-family: ''Arial'',sans-serif; mso-ascii-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-hansi-font-family: Calibri; mso-hansi-theme-font: minor-latin;">???? ??????</span></p>\r\n<p>&nbsp;</p>\r\n<p class="MsoNormal"><span dir="RTL" lang="HE" style="font-family: ''Arial'',sans-serif; mso-ascii-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-hansi-font-family: Calibri; mso-hansi-theme-font: minor-latin;">???? ?????? ?????? ???????? ??????? ???? ?????? ????? ?????? ????? ???????. ?????? ????? ?? ??? ???????? ?????? ????? ????? ????? ?? ???????? ??? ???? ????? ????? ?????? ?? ?????? ????? ?? ??? ?????? ??????? ?????? ????? ???? ????? ?? ???????? ??????? ????? ??????. ???? ?????? ???? ?????? ??????? ??? ??????? ???? ?????? ??? ??? ??????? ???? ?? ?? ????, ?????? ????? ??????? ???? ??? ????? ??? ?????? ????</span>.</p>', 1, NULL, 'https://www.youtube.com/watch?v=IfqTDKkKPzw', 'Bnei Brak, Israel', '88888.00', '2016-12-22 10:51:29', '2017-01-21 23:59:59', 1, 1, '2016-12-22 17:51:29', 0, 0, 0, 0),
(97, 312, 8, 'a', 'a', '97_585bd3cb699ee.png', '<p>???? ??? ???</p>', 1, NULL, '', 'aaa', '3000.00', '2016-12-22 13:22:18', '2017-01-21 23:59:59', -1, 1, '2016-12-22 20:22:18', 0, 0, 0, 0),
(114, 313, 24, 'Tomchei Israel Kallah Fund', 'tomchei-israel-kallah-fund', NULL, '<p>The &ldquo;<strong>Tomchei Israel Kallah Fund</strong>&rdquo;, under the direct supervision of <strong>Rabbi Menachem Mendel Gluckowsky</strong>, was established to assist those of Anash and Shluchim in Israel for whom feeding their large families is a daily challenge, and marrying off their children is an impossibility. With the help of our contributors worldwide, we&rsquo;ve succeeded in turning an impossible nightmare into the joyful occasion it was meant to be.</p>\r\n<p>However, the fund is constantly approached by a growing amount of unfortunate families who possess none of the means for their children to marry. The Fund is compelled to provide the very basics so that the actual engagements can occur, and to relieve the parents of the Choson and Kallah of the tremendous burden facing them.</p>\r\n<p>Consider the sorrow and shame of a bride whose marriage cannot be scheduled because she couldn&rsquo;t afford a hall for a wedding. Consider the pain and humiliation of a bride embarrassed to attend her own wedding. Consider how critical it is that support be quickly extended when weddings are halted because the couple will be nothing less than penniless and homeless upon marriage.</p>\r\n<p>&ldquo;Tomchei Israel Kallah Fund&rdquo; has established itself as a trusted and reliable Fund <strong><span style="text-decoration: underline;">whose beneficiaries are the immediate recipients of all money contributed</span></strong>. Now, as we face these families and many more causes in the direct future, you can be our partner in helping these poor people attend their own wedding with a happy heart.</p>\r\n<p>Please donate generously, and may Hashem always fill your heart with joy, and may the marriage of your loved ones always be a pleasant and a worry-free experience.</p>', 1, NULL, '', 'Brooklyn, NY, United States', '25000.00', '2016-12-22 19:36:22', '2017-01-21 23:59:59', 1, 1, '2016-12-23 02:36:22', 1, 0, 0, 0),
(122, 314, 22, 'Tomche Shabbos of Rockland County', 'tomche-shabbos-of-rockland-county', '122_585ca7053929e.png', '<p>Each Wednesday and Thursday night, dozens of people gather at our warehouse to assemble large boxes of free food, including chicken, fish, Challah, grape juice, fruits and vegetable, eggs, and canned goods. Under the cover of darkness, volunteers then drive these food packages to struggling families'' homes where they are discreetly left on their doorstep.</p>\r\n<p>Our volunteers consist of doctors, lawyers, businessmen and students, all working side by side as they prepare food packages for the poor families. Though they are not aware of the recipients'' identities, they go about their work with the same enthusiasm as if that emergency food package was destined for a brother or sister.</p>\r\n<p>Given the current financial situation, there are now more families in our community in financial distress then ever before. Numerous people have lost their jobs, many businessmen have failed, and the breadwinners of some families have been struck with illness or disability. Tomche Shabbos of Rockland County responds to these situations with programs that now include emergency food donations, job training, job placement and financial counseling.</p>\r\n<p>We remain dedicated to providing our assistance whenever these tragic situations occur. When faced with emergencies such as families with food shortages, utility shutoffs, and evictions, we can only meet these challenges by means of your continued support.Each Wednesday and Thursday night, dozens of people gather at our warehouse to assemble large boxes of free food, including chicken, fish, Challah, grape juice, fruits and vegetable, eggs, and canned goods. Under the cover of darkness, volunteers then drive these food packages to struggling families'' homes where they are discreetly left on their doorstep.</p>', 1, NULL, 'https://youtu.be/cek3erbofkU', 'Monsey, NY, United States', '8000.00', '2016-12-23 04:24:10', '2017-01-22 23:59:59', 1, 1, '2016-12-23 11:24:10', 0, 0, 0, 0),
(126, 296, 8, 'גכלחגכלגחכ לגחכגלכחגכ לחבהלב', 'גכלחגכלגחכ-לגחכגלכחגכ-לחבהלב', NULL, '<p>גכלחגכלגחכ לגחכגלכחגכ לחבהלבחהבלח קםרקן כ</p>', 1, NULL, '', 'Arizona, United States', '12.00', '2016-12-23 07:05:45', '2017-01-22 23:59:59', -1, 1, '2016-12-23 14:05:45', 1, 0, 0, 0),
(127, 315, 22, 'Tomchei Shabbos of Bergen County', 'tomchei-shabbos-of-bergen-county', '127_585d1c54624fd.png', '<p>Since 1990, Tomchei Shabbos has provided meals to needy families in Bergen county.</p>\r\n<p>During this time, close to one million meals have been distributed to Jewish families in Bergen county. Each week these households receive Shabbos meals, bringing a sense of normalcy and tranquility into the home. Tomchei Shabbos does not insist on uncovering</p>\r\n<p>uncomfortable details. The need for help is brought to our attention and confirmed with</p>\r\n<p>local organizations and rabbis. Each delivery is done with dignity, respect and the utmost discretion. Our drivers are carefully selected for each route and there are many ways to obtain a package anonymously. We fundamentally believe that how we give is just as essential as what we give.</p>\r\n<p>Selfless dedication</p>\r\n<p>Over 70 volunteers pack and distribute the Shabbos meals every week. This ensures</p>\r\n<p>that nearly 100% of our operating expenses go directly towards the purchase of food items such as eggs, vegetables, fruit, fish, chicken, Challah and other essential basics. For Pesach alone, 900 pounds of matzah and one and a half tons of chicken were distributed.</p>\r\n<p>We have not cancelled a delivery in over fifteen years.</p>\r\n<p>Short &amp; long term relief available</p>\r\n<p>Occasionally, families are faced with unexpected events that put a tremendous financial strain on them. These financial challenges can last 2 weeks, 2 months or even 2 years. Tomchei Shabbos is here to help by providing shabbos meals to community families regardless of how short or long it is required. Help is offered for as long as help is needed.</p>\r\n<p>A local organization serving local families&nbsp;Tomchei Shabbos of Bergen County fills a tremendous need in our community. It is&nbsp;entirely dependent on local support &ndash; whom else can be expected to feel the burden of our friends and neighbors? Please, open your heart and ease the pain of families that are suffering.</p>\r\n<p>Together we can ensure that everyone in our community can enjoy the Shabbos.</p>\r\n<p>You can help make a difference!</p>', 1, NULL, 'https://youtu.be/M8oWej2ayFo', 'Bergenfield, NJ, United States', '18000.00', '2016-12-23 12:42:37', '2017-01-22 23:59:59', -1, 1, '2016-12-23 19:42:37', 0, 0, 0, 0),
(128, 316, 22, 'TOMCHEI SHABBOS OF THE FIVE TOWNS AND FAR ROCKAWAY', 'tomchei-shabbos-of-the-five-towns-and-far-rockaway', '128_585d1ea013a62.png', '<p style="color: rgba(26, 26, 26, 0.701961); font-family: adobe-garamond-pro; font-size: 19px; font-weight: 600;"><span style="font-size: 13.5pt;">Tomchei Shabbos Yad Yeshaya of the Five Towns and Far Rockaway, a 501(c)(3) charitable organization,&nbsp;was founded in 1985 in memory of Yeshaya Alpert z&rsquo;l to provide food for Shabbos and Yom Tov to our needy neighbors. We currently support approximately 285 families on a weekly basis, with more receiving assistance before Pesach. Our 2015 budget is projected to be $650,000.</span></p>\r\n<p style="color: rgba(26, 26, 26, 0.701961); font-family: adobe-garamond-pro; font-size: 19px; font-weight: 600;"><span style="font-size: 13.5pt;">Even as the economy is rebounding, the effects of the recession are still felt by many local families. Many breadwinners still struggle with unemployment or under-employment. Additionally, the rising costs of food means that more families can&rsquo;t afford basic groceries, and it costs Tomchei more to provide the food that they need. Every family is carefully vetted by rabbanim and community leaders.</span></p>\r\n<p style="color: rgba(26, 26, 26, 0.701961); font-family: adobe-garamond-pro; font-size: 19px; font-weight: 600;"><span style="font-size: 13.5pt;">All of the packaging and distribution is done by volunteers. Students at TAG spend their Thursday afternoons assembling the packages. More volunteers then come to pick up the packages and discreetly deliver them to the anonymous recipients.</span></p>\r\n<p style="margin-bottom: 0px; color: rgba(26, 26, 26, 0.701961); font-family: adobe-garamond-pro; font-size: 19px; font-weight: 600;"><span style="font-size: 13.5pt;">We invite you to partner with Tomchei Shabbos and help ensure food, peace of mind, and kavod Shabbos for all of our neighbors in the Five Towns and Far Rockaway.</span></p>', 1, NULL, '', 'Far Rockaway, NY 11691', '18000.00', '2016-12-23 12:54:23', '2017-01-22 23:59:59', 1, 1, '2016-12-23 19:54:23', 0, 0, 0, 0),
(129, 317, 22, 'Tomchei Shabbos of Lakewood', 'tomchei-shabbos-of-lakewood', '129_585d21a1efbcd.png', '<div class="textlarge">Family Food Relief of New Jersey provides basic kosher food provisions for families suffering severe economic distress in a most discreet and dignified manner.</div>\r\n<div class="textsmall">The hundreds of families currently served by Family Food Relief include disadvantaged households, large families or just regular individuals struggling with a temporary crisis such as loss of employment, illness or other financial predicament.<br /><br /> Central to our mission is the core commitment never to turn away a needy family applying for assistance.</div>', 1, NULL, 'https://www.youtube.com/watch?v=UymVlbe5KUg&feature=youtu.be', ' LAKEWOOD, NJ 08701', '18000.00', '2016-12-23 13:04:29', '2017-01-22 23:59:59', 1, 1, '2016-12-23 20:04:29', 0, 0, 0, 0),
(130, 318, 22, 'Tomchei Shabbos of Dallas', 'tomchei-shabbos-of-dallas', NULL, '<p>Founded in 2010, Tomchei Shabbos (&ldquo;Supporters of Shabbat&rdquo;) of Dallas&rsquo; goal is to lovingly and anonymously providing essential Shabbat food to needy Jewish families in our community.</p>\r\n<p>Our vetting process is conducted with the utmost discretion, care and privacy. Recipients are sent credits via email which can be used only for the purchase Kosher food. The food can be ordered online and picked up without anyone knowing that the food has been paid for with Tomchei Shabbos-provided credit. Every part of our process is structured in the spirit of Mattan Beseter, allowing both the donors and donees complete anonymity.</p>\r\n<p>Tomchei Shabbos is entirely supported by the compassion, generosity and financial support of the Dallas Jewish community.</p>', 1, NULL, '', 'Dallas, TX, United States', '18000.00', '2016-12-23 13:25:05', '2017-01-22 23:59:59', 1, 1, '2016-12-23 20:25:05', 1, 0, 0, 0),
(131, 296, 12, 'test cam', 'test-cam', NULL, '<p>test</p>', 1, NULL, '', 'Pune, Maharashtra, India', '12.00', '2016-12-24 05:41:29', '2017-01-23 23:59:59', -1, 1, '2016-12-24 12:41:29', 1, 0, 0, 0),
(132, 296, 11, 'zaa', 'zaa', '132_585e27463920d.png', '<p>aaa</p>', 1, NULL, '', 'Arizona, United States', '12.00', '2016-12-24 07:33:41', '2017-01-23 23:59:59', -1, 1, '2016-12-24 14:33:41', 0, 0, 0, 0),
(133, 213, 27, 'Yismach', 'yismach', NULL, '<p class="MsoNormal"><u></u><span style="font-family: ''Arial'',sans-serif; mso-ascii-theme-font: minor-bidi; mso-hansi-theme-font: minor-bidi; mso-bidi-font-family: Arial; mso-bidi-theme-font: minor-bidi; color: #0d0d0d; mso-themecolor: text1; mso-themetint: 242;">Yismach connects you to shadchanim. Any frum individual in shidduchim can create a profile to be viewed by established, recognized, professional shadchanim worldwide.&nbsp; It has never been this easy to expedite shidduchim and connect individuals with shadchanim with a click of a button. </span></p>\r\n<p class="MsoNormal"><strong><u><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">HOW DOES IT WORK?</span></u></strong></p>\r\n<p class="MsoNormal"><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Simply, sign-up, complete a profile, upload a shidduch resume and picture, and keep your information up to date. </span><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Shadchanim will review your profile to begin the search for someone suitable. </span><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Some shadchanim will set up a face-to-face meeting, others will get to know you on the phone and others may meet you at meet-the-shadchanim parties. S</span><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">hadchanim who have already met you will be reminded about your availability and now have easy access to your current information.</span></p>\r\n<p class="MsoNormal"><strong><u><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">HOW IS YISMACH UNIQUE?</span></u></strong></p>\r\n<p class="MsoNormal"><span style="font-family: Arial, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Unlike dating sites with a closed pool of candidates, from which matches are made, Yismach is an open database for established recognized professional shadchanim worldwide</span><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">. Our state of the art technology includes features which allow shadchanim to search potential matches by age, location, religious background, haskafa, to keep notes about clients, to record past suggestions and actual dates while still perpetuating the time tested </span><span style="font-family: ''Arial'',sans-serif; mso-ascii-theme-font: minor-bidi; mso-hansi-theme-font: minor-bidi; mso-bidi-font-family: Arial; mso-bidi-theme-font: minor-bidi; color: #0d0d0d; mso-themecolor: text1; mso-themetint: 242;">old-fashioned <span style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">shidduch system</span>, where shadchanim get to know you by meeting you and staying involved until engagement. <span style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">&nbsp;</span></span></p>\r\n<p class="MsoNormal"><strong><u><span style="font-family: ''Arial'',sans-serif; mso-ascii-theme-font: minor-bidi; mso-hansi-theme-font: minor-bidi; mso-bidi-font-family: Arial; mso-bidi-theme-font: minor-bidi; color: #0d0d0d; mso-themecolor: text1; mso-themetint: 242;">WHO CAN JOIN? </span></u></strong></p>\r\n<p class="MsoNormal"><span style="font-family: ''Arial'',sans-serif; mso-ascii-theme-font: minor-bidi; mso-hansi-theme-font: minor-bidi; mso-bidi-font-family: Arial; mso-bidi-theme-font: minor-bidi; color: #0d0d0d; mso-themecolor: text1; mso-themetint: 242;">Yismach is a network. Anyone in shidduchim can put up their information. Any recognized, established, professional shadchan can have access.<span style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"> All of your information is strictly confidential visible to ONLY professional and well-established shadchanim.</span> You can let only certain shadchanim have access to your up-to-date information or be visible to all. </span></p>\r\n<p class="MsoNormal"><strong><u><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">HOW DOES YISMACH PROTECT YOUR DIGINITY?</span></u></strong></p>\r\n<p>&nbsp;</p>\r\n<p class="MsoNormal" style="line-height: normal;"><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">At Yismach we protect your dignity by ensuring confidentiality. Only established, professional shadchanim can see your information and will use discretion in relaying the relevant information only to potential matches.&nbsp; We have installed security features so that your information is not searchable on the internet.&nbsp;&nbsp;</span></p>\r\n<p class="MsoNormal" style="line-height: normal;"><span style="font-family: Arial, sans-serif; color: #0d0d0d; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">In two years we are active almost 400 got engaged and the rate of engagements are accelerating. We are growing faster than the operational budget can sustain - We need your help. Please donate generaously.</span></p>', 1, NULL, '', 'Jerusalem, Israel', '36000.00', '2016-12-24 18:40:20', '2017-01-23 23:59:59', -1, 1, '2016-12-25 01:40:20', 1, 0, 0, 0),
(134, 213, 27, 'Yismach – Rebuilding the Jewish People One Couple at a TIme', 'yismach-rebuilding-the-jewish-people-one-couple-at-a-time', '134_58600120a4785.png', '<p class="MsoNormal">It has never been this easy.</p>\r\n<p class="MsoNormal">Yismach connects those in shidduchim to established, recognized, professional shadchanim worldwide. Our state of the art technology is in the service ofl perpetuating the time tested old-fashioned shidduch system, where shadchanim get to know you by meeting you and staying involved until engagement.&nbsp;</p>\r\n<p class="MsoNormal">Yismach is a network. Anyone in shidduchim can put up their information. Any recognized, established, professional shadchan can have access. All information is strictly confidential visible to ONLY professional and well-established shadchanim. Those in shidduchim can let only certain shadchanim have access to your up-to-date information or be visible to all. And Yismach protect their dignity by ensuring confidentiality. Only established, professional shadchanim can see their information and will use discretion in relaying the relevant information only to potential matches.&nbsp; We have installed security features so that your information is not searchable on the internet.&nbsp;</p>\r\n<p class="MsoNormal">In two years, we are active almost 400 got engaged and the rate of engagements are accelerating. We are growing faster than the operational budget can sustain - We need your help. Please donate generously.</p>', 1, NULL, '', 'Jerusalem, Israel', '36000.00', '2016-12-28 18:36:56', '2017-01-27 23:59:59', 1, 1, '2016-12-25 01:50:33', 0, 1, 0, 0),
(135, 321, 22, 'Yad Yehuda of Greater Washington', 'yad-yehuda-of-greater-washington', '135_585ed1b82a3a5.png', '<p>For individuals and families in our community who are in need, our hunger relief program often means the difference between having enough to eat and doing without sufficient nutrition. We help hundreds of Jewish community members throughout Greater Washington put proper food on their tables with dignity.</p>\r\n<p>​​​Tomchei Shabbos:&nbsp;​Capital Kosher Pantry</p>\r\n<p>Provides gift cards to Shalom Kosher Supermarket so individuals can purchase food for Shabbos and Yom Tov without embarrassment or stigma.</p>\r\n<p>​Allows dozens of individuals and families in the Greater Washington Jewish community shop free of charge for the kosher food products they need most.</p>\r\n<p>​​Challah for Shabbos:&nbsp;​Simcha Food Gemach</p>\r\n<p>Bakery challahs are distributed to community members in need every Friday. Sponsored by Rosendorff Challah.</p>\r\n<p>Share the joy of your simcha by donating leftover food and preventing good food from going to waste.​ We only accept food from Vaad Harabonim-approved events. To donate, please call 301-842-7135 ext 710.</p>\r\n<p>​Produce Distribution:&nbsp;Fresh fruit and vegetables are distributed around Yom Tovim to families and individuals in need.</p>', 1, NULL, 'https://www.youtube.com/watch?v=htfx3YmkoaA', 'Silver Spring, MD, United States', '18000.00', '2016-12-24 19:49:04', '2017-01-23 23:59:59', 1, 1, '2016-12-25 02:49:04', 0, 0, 0, 0),
(136, 322, 22, 'Tomchei Shabbos of Queens', 'tomchei-shabbos-of-queens', '136_585ed7d836297.png', '<p class="MsoNormal">Tomchei Shabbos of Queens serves as "middlemen" between the hungry &amp; those willing to share on an all-volunteer basis. We spend at least 90 cents of each donated dollar on food for distribution.</p>\r\n<p class="MsoNormal">For more than thirty years, TSQ has provided food packages on a weekly basis to needy families &amp; individuals throughout Queens, NY. Services have expanded into parts of Long Island which are underserved. Presently, approximately 400 packages are delivered weekly.</p>\r\n<p class="MsoNormal">The scope of recipients varies widely. She might be a woman with children whose husband has left...or an elderly couple on a very limited pension...a family whose breadwinner has suddenly lost his or her job...families where illness saps the resources...and other cases of despair. The common denominator is need.</p>\r\n<p class="MsoNormal">Local clergy who come in close contact with problem cases.</p>\r\n<p class="MsoNormal">School principals and teachers who see children come to school having skipped breakfast.</p>\r\n<p class="MsoNormal">Social workers who deal with human needs on a daily basis.</p>\r\n<p class="MsoNormal">We are all volunteers, encompassing the spectrum of ages and professions and backgrounds. Our shared goal-the care of our extended family-those among us who are in need.</p>\r\n<p class="MsoNormal">Over ninety five percent of our funding is through the generosity of individuals and firms who wish to see direct positive results for their contributions within their community.</p>\r\n<p>&nbsp;</p>\r\n<p class="MsoNormal">No salaries are paid-----minimal operating overhead-----we purchase over 95% of the food products distributed.</p>', 1, NULL, 'https://www.youtube.com/watch?v=lw7XoVeha8Y', 'Kew Gardens, Richmond, United Kingdom', '18000.00', '2016-12-24 20:00:36', '2017-01-23 23:59:59', 1, 1, '2016-12-25 03:00:36', 0, 0, 0, 0),
(137, 323, 22, 'Tomchei Shabbos of Los Angeles', 'tomchei-shabbos-of-los-angeles', '137_585eda13946fd.png', '<p style="box-sizing: border-box; margin: 0px; font-family: Oxygen, sans-serif; color: #595959; padding-bottom: 30px; line-height: 1.714em;">We&nbsp;assist the Jewish needy of Los Angeles by providing a variety of family services with the utmost level of dignity and discretion.&nbsp;Our programs&nbsp;help those without work find jobs, start businesses &amp; earn livelihoods. They provide clothing and furniture, assist with utilities &amp; rent in emergency situations, help children obtain Jewish educations and direct those in need to the proper social service organizations. On a weekly basis&nbsp;we provide food for the week, Shabbat and the holidays. This weekly support restores spirits and allows the Sabbath to work its healing magic on families in need.</p>\r\n<p style="box-sizing: border-box; margin: 0px; font-family: Oxygen, sans-serif; color: #595959; padding-bottom: 30px; line-height: 1.714em;">There are many thousands of individuals in the greater Los Angeles area who live below the poverty line. Due to lack of sufficient funding, Tomchei LA is only able to assist a small portion of these individuals.&nbsp;Your contributions will enable Tomchei LA to assist many more of these individuals who so desperately need our help. Tomchei LA is solely donor funded. So together, let us help those who can not afford to help themselves.</p>\r\n<p style="box-sizing: border-box; margin: 0px; font-family: Oxygen, sans-serif; color: #595959; padding-bottom: 30px; line-height: 1.714em;">Touch of Kindness, Inc. d.b.a. Tomchei Shabbos is a California Non-Profit Tax Exempt 501(c) (3) organization, ID #75-3002144. Informational Returns on our organization&rsquo;s financial activities are filed annually with the Internal Revenue Service and the California Franchise Tax Board. Financial information and government compliance requirements are handled entirely voluntarily by Yosef Y. Manela, CPA 323-363-3553.</p>', 1, NULL, 'https://vimeo.com/120123912', 'Los Angeles, CA, United States', '18000.00', '2016-12-24 20:24:53', '2017-01-23 23:59:59', 1, 1, '2016-12-25 03:24:53', 0, 0, 0, 0),
(138, 324, 22, 'Tomchei Shabbos of Toronto', 'tomchei-shabbos-of-toronto', '138_585eea053e9c0.png', '<p>For over 30 years, our community has relied on Tomchei Shabbos to provide every week for our less fortunate. Tomchei Shabbos delivers hand packed boxes of Shabbos food essentials to over 225 families within the GTA. Providing quality food, timely delivery and courteous service.</p>\r\n<p>With your help, Tomchei Shabbos helps ensure that Shabbos will be a joyous day for families in our community in need. Every Wednesday night and before Yomim Tovim, a group of dedicated individuals get together on a purely volunteer basis and organize the delivery of food boxes.&nbsp;</p>', 1, NULL, 'https://vimeo.com/119513965', 'Toronto, ON, Canada', '18000.00', '2016-12-24 21:26:49', '2017-01-23 23:59:59', 1, 1, '2016-12-25 04:26:49', 0, 0, 0, 0),
(139, 325, 22, 'Tomchei Shabbos of Phoenix (Arizona)', 'tomchei-shabbos-of-phoenix-arizona', '139_585f885bbc766.png', '<p style="text-align: center;" align="center"><span style="font-size: 18pt;">We provide food for Shabbos and Yom Tov to people in the Phoenix area who are temporarily in need. </span></p>\r\n<p style="text-align: center;" align="center"><span style="font-size: 18pt;">We rely on your donations to make this possible.</span></p>', 1, NULL, '', 'Phoenix, AZ, United States', '8000.00', '2016-12-25 08:50:15', '2017-01-24 23:59:59', 1, 1, '2016-12-25 15:50:15', 0, 0, 0, 0),
(140, 326, 22, 'Tomchei Shabbos of Miami', 'tomchei-shabbos-of-miami', '140_585f979150902.png', '<p>Every Friday the organization distributes kosher food to more than seventy families throughout South Florida. Tomchei Shabbos means &ldquo;supporters of the Sabbath&rdquo; and that is just what the organization does.<br /><br />Tomchei Shabbos believes in helping the Jewish needy without making them feel needy. There is 100 percent confidentiality for recipients. The nonprofit organization is unique. All donations go directly toward the cause. There are no salaries. All work is done by volunteers.<br /><br />Donations are needed of food, money, toys, etc. Volunteers are needed who can give time for packing and delivering. All packages are put together on Thursdays in a warehouse and delivered to families in need on Friday.</p>', 1, NULL, 'https://www.youtube.com/watch?v=6eZVnRo1flQ', 'Miami, FL, United States', '18000.00', '2016-12-25 09:02:00', '2017-01-24 23:59:59', 1, 1, '2016-12-25 16:02:00', 0, 0, 0, 0),
(141, 327, 22, 'Tomchei Shabbos of Passaic-Clifton', 'tomchei-shabbos-of-passaic-clifton', '141_585fa5400cc00.png', '<p>Tomchei Shabbos of Passaic-Clifton has been serving the needy in our community for more than 25 years by discretely providing them with essential food staples and scrip used for food purchases. Tomchei Shabbos&rsquo; beneficiaries &ndash; all local community members &ndash; include working families, unemployed, single parents, widow(er)s and those suffering from debilitating illness, all of whom struggle to make ends meet. We provide a broad array of essential groceries, more than Shabbos food alone, to close to 80 families. We do our utmost to insure their privacy and dignity while maintaining their peace of mind and self-esteem.</p>', 1, NULL, 'https://youtu.be/DRq7tAn-7ZI', 'Passaic, NJ, United States', '18000.00', '2016-12-25 10:24:51', '2017-01-24 23:59:59', -1, 1, '2016-12-25 17:24:51', 0, 0, 0, 0);
INSERT INTO `campaigns` (`id`, `user_id`, `campaign_categoriy_id`, `title`, `slug`, `image`, `description`, `video_type`, `video_file_name`, `video_url`, `campaign_location_id`, `fund_needed`, `start_date_time`, `end_date_time`, `status`, `donation_status`, `create_date`, `preview_mode`, `home_page_visible`, `home_page_not_featured`, `priority`) VALUES
(142, 328, 9, 'Yad Eliezer Food and Toys for Children', 'yad-eliezer-food-and-toys-for-children', NULL, '<p><!-- [if gte mso 9]><xml>\r\n <o:OfficeDocumentSettings>\r\n  <o:AllowPNG/>\r\n </o:OfficeDocumentSettings>\r\n</xml><![endif]--></p>\r\n<p><!-- [if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves/>\r\n  <w:TrackFormatting/>\r\n  <w:PunctuationKerning/>\r\n  <w:ValidateAgainstSchemas/>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF/>\r\n  <w:LidThemeOther>EN-US</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>HE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables/>\r\n   <w:SnapToGridInCell/>\r\n   <w:WrapTextWithPunct/>\r\n   <w:UseAsianBreakRules/>\r\n   <w:DontGrowAutofit/>\r\n   <w:SplitPgBreakAndParaMark/>\r\n   <w:EnableOpenTypeKerning/>\r\n   <w:DontFlipMirrorIndents/>\r\n   <w:OverrideTableStyleHps/>\r\n  </w:Compatibility>\r\n  <m:mathPr>\r\n   <m:mathFont m:val="Cambria Math"/>\r\n   <m:brkBin m:val="before"/>\r\n   <m:brkBinSub m:val="&#45;-"/>\r\n   <m:smallFrac m:val="off"/>\r\n   <m:dispDef/>\r\n   <m:lMargin m:val="0"/>\r\n   <m:rMargin m:val="0"/>\r\n   <m:defJc m:val="centerGroup"/>\r\n   <m:wrapIndent m:val="1440"/>\r\n   <m:intLim m:val="subSup"/>\r\n   <m:naryLim m:val="undOvr"/>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!-- [if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"\r\n  DefSemiHidden="false" DefQFormat="false" DefPriority="99"\r\n  LatentStyleCount="374">\r\n  <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 5"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 6"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 7"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 8"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index 9"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 1"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 2"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 3"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 4"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 5"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 6"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 7"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 8"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="toc 9"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Normal Indent"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="footnote text"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="annotation text"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="header"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="footer"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="index heading"/>\r\n  <w:LsdException Locked="false" Priority="35" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="caption"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="table of figures"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="envelope address"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="envelope return"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="footnote reference"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="annotation reference"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="line number"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="page number"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="endnote reference"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="endnote text"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="table of authorities"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="macro"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="toa heading"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Bullet"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Number"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List 5"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Bullet 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Bullet 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Bullet 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Bullet 5"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Number 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Number 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Number 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Number 5"/>\r\n  <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Closing"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Signature"/>\r\n  <w:LsdException Locked="false" Priority="1" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="Default Paragraph Font"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Body Text"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Body Text Indent"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Continue"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Continue 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Continue 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Continue 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="List Continue 5"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Message Header"/>\r\n  <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Salutation"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Date"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Body Text First Indent"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Body Text First Indent 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Note Heading"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Body Text 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Body Text 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Body Text Indent 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Body Text Indent 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Block Text"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Hyperlink"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="FollowedHyperlink"/>\r\n  <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>\r\n  <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Document Map"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Plain Text"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="E-mail Signature"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Top of Form"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Bottom of Form"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Normal (Web)"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Acronym"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Address"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Cite"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Code"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Definition"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Keyboard"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Preformatted"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Sample"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Typewriter"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="HTML Variable"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Normal Table"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="annotation subject"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="No List"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Outline List 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Outline List 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Outline List 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Simple 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Simple 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Simple 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Classic 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Classic 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Classic 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Classic 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Colorful 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Colorful 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Colorful 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Columns 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Columns 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Columns 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Columns 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Columns 5"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Grid 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Grid 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Grid 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Grid 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Grid 5"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Grid 6"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Grid 7"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Grid 8"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table List 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table List 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table List 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table List 4"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table List 5"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table List 6"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table List 7"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table List 8"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table 3D effects 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table 3D effects 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table 3D effects 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Contemporary"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Elegant"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Professional"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Subtle 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Subtle 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Web 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Web 2"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Web 3"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Balloon Text"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Table Theme"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>\r\n  <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>\r\n  <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>\r\n  <w:LsdException Locked="false" Priority="61" Name="Light List"/>\r\n  <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>\r\n  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>\r\n  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>\r\n  <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>\r\n  <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>\r\n  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>\r\n  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>\r\n  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>\r\n  <w:LsdException Locked="false" Priority="70" Name="Dark List"/>\r\n  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>\r\n  <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>\r\n  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>\r\n  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>\r\n  <w:LsdException Locked="false" Priority="34" QFormat="true"\r\n   Name="List Paragraph"/>\r\n  <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>\r\n  <w:LsdException Locked="false" Priority="30" QFormat="true"\r\n   Name="Intense Quote"/>\r\n  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="19" QFormat="true"\r\n   Name="Subtle Emphasis"/>\r\n  <w:LsdException Locked="false" Priority="21" QFormat="true"\r\n   Name="Intense Emphasis"/>\r\n  <w:LsdException Locked="false" Priority="31" QFormat="true"\r\n   Name="Subtle Reference"/>\r\n  <w:LsdException Locked="false" Priority="32" QFormat="true"\r\n   Name="Intense Reference"/>\r\n  <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>\r\n  <w:LsdException Locked="false" Priority="37" SemiHidden="true"\r\n   UnhideWhenUsed="true" Name="Bibliography"/>\r\n  <w:LsdException Locked="false" Priority="39" SemiHidden="true"\r\n   UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>\r\n  <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>\r\n  <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>\r\n  <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>\r\n  <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>\r\n  <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>\r\n  <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>\r\n  <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>\r\n  <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>\r\n  <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="Grid Table 1 Light Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="Grid Table 6 Colorful Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="Grid Table 7 Colorful Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="Grid Table 1 Light Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="Grid Table 6 Colorful Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="Grid Table 7 Colorful Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="Grid Table 1 Light Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="Grid Table 6 Colorful Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="Grid Table 7 Colorful Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="Grid Table 1 Light Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="Grid Table 6 Colorful Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="Grid Table 7 Colorful Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="Grid Table 1 Light Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="Grid Table 6 Colorful Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="Grid Table 7 Colorful Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="Grid Table 1 Light Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="Grid Table 6 Colorful Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="Grid Table 7 Colorful Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>\r\n  <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>\r\n  <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="List Table 1 Light Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="List Table 6 Colorful Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="List Table 7 Colorful Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="List Table 1 Light Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="List Table 6 Colorful Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="List Table 7 Colorful Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="List Table 1 Light Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="List Table 6 Colorful Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="List Table 7 Colorful Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="List Table 1 Light Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="List Table 6 Colorful Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="List Table 7 Colorful Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="List Table 1 Light Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="List Table 6 Colorful Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="List Table 7 Colorful Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="46"\r\n   Name="List Table 1 Light Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="51"\r\n   Name="List Table 6 Colorful Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="52"\r\n   Name="List Table 7 Colorful Accent 6"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Mention"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Smart Hyperlink"/>\r\n  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"\r\n   Name="Hashtag"/>\r\n </w:LatentStyles>\r\n</xml><![endif]--><!-- [if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:"Table Normal";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-parent:"";\r\n	mso-padding-alt:0in 5.4pt 0in 5.4pt;\r\n	mso-para-margin-top:0in;\r\n	mso-para-margin-right:0in;\r\n	mso-para-margin-bottom:8.0pt;\r\n	mso-para-margin-left:0in;\r\n	line-height:107%;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	font-family:"Calibri",sans-serif;\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-language:HE;}\r\n</style>\r\n<![endif]--></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 15.75pt; mso-outline-level: 5;"><span style="font-size: 13.5pt; font-family: ''Trebuchet MS'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-bidi-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">As you light your Chanuka candles, remember the children with less.</span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 130%;"><span style="font-size: 12.0pt; line-height: 130%; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">As winter blows cold against the window panes, thousands of tiny golden flames illuminate the streets of Israel. But what is taking place in the homes hidden behind the warm, cheerful lights?</span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 130%;"><span style="font-size: 12.0pt; line-height: 130%; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">This Chanuka, thousands of children in Israel will still go to sleep hungry. Going to school to hear about the new toys and clothes that classmates received for Chanuka gifts is very painful when your own family doesn&rsquo;t even have enough money to provide you with a sandwich for lunch.</span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 12.75pt;"><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">Yad Eliezer illuminates the darkness for over 20,000 families in Israel today. That&rsquo;s over 80,000 children. </span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 12.75pt;"><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">Our aim is to change the face of the next generation - we provide 180 babies each month with formula, saving them from the mental and physical ravages of early malnutrition. We provide over 4,500 large food boxes monthly to families who would otherwise be lacking vital nutrition. </span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 12.75pt;"><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">As you celebrate the holiday of light, your joy will run deeper with the knowledge that you have put a smile on the face of a hungry child. </span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 12.75pt;"><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">This Chanuka, give a gift that makes a difference. Light up a life with Yad Eliezer. <strong>Let&rsquo;s celebrate together</strong>. </span></p>', 1, NULL, 'https://www.youtube.com/watch?v=2b5mmMkjx5o', 'Jerusalem', '8888.00', '2016-12-25 15:09:55', '2017-01-24 23:59:59', -1, 1, '2016-12-25 22:09:55', 1, 0, 0, 0),
(143, 328, 9, 'Yad Eliezer - A child went hungry this Chanukah', 'yad-eliezer-a-child-went-hungry-this-chanukah', '143_585feaa7e28d9.png', '<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 15.75pt; mso-outline-level: 5;"><span style="font-size: 12.0pt; line-height: 130%; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">As winter blows cold against the window panes, thousands of tiny golden flames illuminate the streets of Israel. But what is taking place in the homes hidden behind the warm, cheerful lights?</span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 130%;"><span style="font-size: 12.0pt; line-height: 130%; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">This Chanuka, thousands of children in Israel still went to sleep hungry. Going to school to hear about the new toys and clothes that classmates received for Chanuka gifts is very painful when your own family doesn&rsquo;t even have enough money to provide you with a sandwich for lunch.</span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 12.75pt;"><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">Yad Eliezer illuminates the darkness for over 20,000 families in Israel today. That&rsquo;s over 80,000 children. </span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 12.75pt;"><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">Our aim is to change the face of the next generation - we provide 180 babies each month with formula, saving them from the mental and physical ravages of early malnutrition. We provide over 4,500 large food boxes monthly to families who would otherwise be lacking vital nutrition. </span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 12.75pt;"><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">You just celebrated the holiday of light.</span></p>\r\n<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: 12.75pt;"><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">Put a smile on the face of a hungry child. </span></p>\r\n<p><span style="font-size: 12.0pt; font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; color: #666666; mso-bidi-language: AR-SA;">Give a gift that makes a difference.</span></p>', 1, NULL, 'https://www.youtube.com/watch?v=2b5mmMkjx5o&feature=youtu.be', 'Jerusalem, Israel', '88888.00', '2017-01-04 09:12:21', '2017-02-03 23:59:59', 1, 1, '2016-12-25 22:48:51', 0, 1, 0, 0),
(147, 332, 26, 'ZAKA Search and Rescue', 'zaka-search-and-rescue', '147_586a24801cfeb.png', '<p>Support the ZAKA Volunteers in Israel and around the world</p>', 1, NULL, 'https://zaka.us/galleires/Videos/0120616', 'Jerusalem, ישראל', '25000.00', '2017-01-02 09:57:56', '2017-02-01 23:59:59', 1, 1, '2017-01-02 16:57:56', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `campaign_categories`
--

CREATE TABLE IF NOT EXISTS `campaign_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET latin1 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active,-1=deleted',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `campaign_categories`
--

INSERT INTO `campaign_categories` (`id`, `name`, `status`) VALUES
(5, 'Yeshivas', 1),
(6, 'Soup Kitchen', 1),
(7, 'Health', 1),
(8, 'Non Profits', 1),
(9, 'Tzadakah', 1),
(10, 'Kivrei Tzadikim', 1),
(11, 'Kollel', 1),
(12, 'Mikvahs', 1),
(13, 'Community', 1),
(14, 'Education', 1),
(15, 'Matanas Leavyonim', 1),
(16, 'Israel', 1),
(17, 'Seminaries', 1),
(18, 'Shuls', 1),
(19, 'Tuition', 1),
(20, ' Matan Bseter', 1),
(21, 'Other', 1),
(22, 'Tomechei Shabbos', 1),
(23, ' Jewish Startups', 1),
(24, ' Hachnasat Kallah', 1),
(25, ' Gmach', 1),
(26, ' Funerals', 1),
(27, 'Shidduchim', 1),
(28, 'Bris Milah', 1),
(29, 'Seforim', 1),
(30, 'Mezuzot', 1),
(31, 'Sefer Torah', 1),
(32, 'Pidyon Shevuim', 1),
(33, 'Special ed,', 1),
(34, 'Youth at risk', 1),
(35, 'Kiruv', 1);

-- --------------------------------------------------------

--
-- Table structure for table `campaign_comments`
--

CREATE TABLE IF NOT EXISTS `campaign_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `comments` tinytext CHARACTER SET latin1 NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active,-1=deleted',
  PRIMARY KEY (`id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_donations`
--

CREATE TABLE IF NOT EXISTS `campaign_donations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Contributor User Id',
  `amount` decimal(10,2) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `donor_first_name` varchar(150) CHARACTER SET latin1 NOT NULL,
  `donor_last_name` varchar(150) CHARACTER SET latin1 NOT NULL,
  `email` varchar(150) CHARACTER SET latin1 NOT NULL,
  `country` varchar(45) CHARACTER SET latin1 NOT NULL,
  `postal_code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `is_public` tinyint(1) NOT NULL COMMENT '''0'' for private & ''1'' for public',
  `comment` tinytext CHARACTER SET latin1,
  PRIMARY KEY (`id`,`campaign_id`,`user_id`),
  KEY `fk_campaign_raised_funds_campaigns1_idx` (`campaign_id`),
  KEY `fk_campaign_raised_funds_users1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `campaign_donations`
--

INSERT INTO `campaign_donations` (`id`, `campaign_id`, `user_id`, `amount`, `create_date`, `donor_first_name`, `donor_last_name`, `email`, `country`, `postal_code`, `is_public`, `comment`) VALUES
(7, 57, 0, '5.00', '2016-11-30 07:31:43', 'Joel ', 'Newman', 'yolinewman@gmail.com', '', '07052', 0, ''),
(11, 88, 0, '5.00', '2016-12-27 12:22:54', 'Jason', 'Newman', 'Jason.o.newman@gmail.com', '', '07052', 0, ''),
(12, 134, 0, '5.00', '2016-12-29 22:20:25', 'Jason', 'Newman', 'Jason.o.newman@gmail.com', '', '07052', 0, ''),
(13, 81, 0, '5.00', '2016-12-29 22:21:40', 'Jason', 'Newman', 'Jason.o.newman@gmail.com', '', '07052', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_gallery`
--

CREATE TABLE IF NOT EXISTS `campaign_gallery` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` bigint(20) unsigned NOT NULL,
  `image_name` varchar(256) NOT NULL,
  `create_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=active ,0=inactive',
  PRIMARY KEY (`id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=317 ;

--
-- Dumping data for table `campaign_gallery`
--

INSERT INTO `campaign_gallery` (`id`, `campaign_id`, `image_name`, `create_date`, `status`) VALUES
(7, 14, '14_85869campaign-scroller-01.jpg', '2016-09-20 14:00:46', 0),
(11, 14, '14_40805organization-02.jpg', '2016-09-20 15:38:02', 1),
(14, 15, '15_27847campaign-scroller-01.jpg', '2016-09-21 13:22:52', 0),
(15, 15, '15_20956campaign-11.jpg', '2016-09-21 13:23:08', 0),
(16, 15, '15_72850campaign-01.png', '2016-09-21 13:23:44', 1),
(17, 16, '16_84433userimage_5c1917d.jpg', '2016-09-25 16:53:07', 1),
(19, 18, '18_38877f0x6mqQh4eZ2DCSyeeVTswv7ucXUQAeJR9xskg5pcmo.png', '2016-09-25 22:46:48', 0),
(20, 18, '18_91443unspecified-68.png', '2016-09-25 22:48:07', 1),
(22, 20, '20_67686wishes-for-happy-birthday-picture.jpg', '2016-09-25 23:48:51', 1),
(25, 23, '23_82923Mefarnes_Dashboard_ORG.jpg', '2016-09-28 08:05:09', 1),
(26, 24, '24_43456DSC_9542.JPG', '2016-09-28 14:41:31', 0),
(27, 24, '24_34068index.jpg', '2016-09-28 14:43:26', 0),
(28, 24, '24_62686test1.jpg', '2016-09-28 14:44:18', 1),
(29, 25, '25_57ecff68cc02a.png', '0000-00-00 00:00:00', 1),
(30, 26, '26_57ed070f8b0de.png', '0000-00-00 00:00:00', 1),
(31, 27, '27_57ee0b4e8c7d8.png', '0000-00-00 00:00:00', 0),
(32, 27, '27_57ee0b5394ddb.png', '0000-00-00 00:00:00', 1),
(33, 30, '30_57fa2d210d13d.png', '0000-00-00 00:00:00', 1),
(36, 31, '31_58021b1694d2f.png', '0000-00-00 00:00:00', 0),
(37, 32, '32_5808efed537c2.png', '0000-00-00 00:00:00', 1),
(48, 34, '34_580bac0d4cbc9.png', '0000-00-00 00:00:00', 0),
(49, 34, '34_580bbb30dc2e3.png', '0000-00-00 00:00:00', 0),
(50, 34, '34_580bbbf7822e0.png', '0000-00-00 00:00:00', 0),
(51, 34, '34_580bbe346b93d.png', '0000-00-00 00:00:00', 1),
(54, 31, '31_580ef345756bb.png', '0000-00-00 00:00:00', 0),
(55, 31, '31_58122897ced40.png', '0000-00-00 00:00:00', 1),
(56, 35, '35_58122abaa084f.png', '0000-00-00 00:00:00', 0),
(57, 35, '35_58122ad8cce5a.png', '0000-00-00 00:00:00', 0),
(60, 35, '35_58122c3a39447.png', '0000-00-00 00:00:00', 0),
(61, 36, '36_5812bf80c7a35.png', '0000-00-00 00:00:00', 1),
(62, 37, '37_5812cd56f1545.png', '0000-00-00 00:00:00', 1),
(79, 35, '35_58138e5ed33c3.png', '0000-00-00 00:00:00', 1),
(80, 38, '38_58155ad921254.png', '0000-00-00 00:00:00', 1),
(108, 39, '39_581a9eb2b732a.png', '0000-00-00 00:00:00', 1),
(112, 40, '40_5820635fc78aa.png', '0000-00-00 00:00:00', 1),
(113, 41, '41_58207bc8e0dc2.png', '0000-00-00 00:00:00', 1),
(114, 42, '42_58207dfc7021d.png', '0000-00-00 00:00:00', 1),
(119, 33, '33_58219f0ce19e9.png', '0000-00-00 00:00:00', 1),
(120, 43, '43_5821a7a0dde7c.png', '0000-00-00 00:00:00', 1),
(121, 46, '46_582eed6f73030.png', '0000-00-00 00:00:00', 1),
(129, 47, '47_583161eecc85d.png', '0000-00-00 00:00:00', 1),
(130, 48, '48_583162442e2d7.png', '0000-00-00 00:00:00', 0),
(131, 48, '48_5831626d7ef9a.png', '0000-00-00 00:00:00', 1),
(132, 49, '49_5834751460bc6.png', '0000-00-00 00:00:00', 0),
(133, 49, '49_58347559219b0.png', '0000-00-00 00:00:00', 1),
(135, 50, '50_58348d5d0316b.png', '0000-00-00 00:00:00', 1),
(136, 51, '51_583491875cac9.png', '0000-00-00 00:00:00', 0),
(138, 51, '51_5834928e9dc6e.png', '0000-00-00 00:00:00', 1),
(139, 52, '52_583494278feae.png', '0000-00-00 00:00:00', 1),
(140, 53, '53_5834f0c131974.png', '0000-00-00 00:00:00', 1),
(142, 54, '54_583506d87d160.png', '0000-00-00 00:00:00', 1),
(144, 56, '56_5839b8fb55a15.png', '0000-00-00 00:00:00', 1),
(145, 57, '57_583b2e83794ae.png', '0000-00-00 00:00:00', 0),
(150, 57, '57_583b2f8db0308.png', '0000-00-00 00:00:00', 0),
(153, 58, '58_583be1c49b007.png', '0000-00-00 00:00:00', 1),
(155, 59, '59_583c5e764f121.png', '0000-00-00 00:00:00', 1),
(156, 60, '60_583cd4ac8734b.png', '0000-00-00 00:00:00', 0),
(157, 60, '60_583cd525bbb22.png', '0000-00-00 00:00:00', 1),
(158, 61, '61_583cd5c0308c9.png', '0000-00-00 00:00:00', 1),
(162, 62, '62_583d22e3bfd03.png', '0000-00-00 00:00:00', 1),
(163, 55, '55_583d2b436e07a.png', '0000-00-00 00:00:00', 0),
(164, 55, '55_583d2b50e4379.png', '0000-00-00 00:00:00', 1),
(165, 63, '63_583d2bebb44e2.png', '0000-00-00 00:00:00', 1),
(166, 64, '64_583d79045dbfe.png', '0000-00-00 00:00:00', 1),
(168, 66, '66_583fb827cf429.png', '0000-00-00 00:00:00', 0),
(169, 66, '66_583fb83e41699.png', '0000-00-00 00:00:00', 0),
(171, 67, '67_583fce2d0120f.png', '0000-00-00 00:00:00', 1),
(172, 68, '68_58400675bc178.png', '0000-00-00 00:00:00', 1),
(174, 69, '69_584257f0b7511.png', '0000-00-00 00:00:00', 1),
(175, 70, '70_584268afd43e0.png', '0000-00-00 00:00:00', 0),
(183, 71, '71_58428bb7f3798.png', '0000-00-00 00:00:00', 1),
(185, 71, '71_58428be62ce47.png', '0000-00-00 00:00:00', 0),
(187, 72, '72_58428f4a8257a.png', '0000-00-00 00:00:00', 1),
(188, 73, '73_58429752cb930.png', '0000-00-00 00:00:00', 1),
(190, 74, '74_58443d14a17ca.png', '0000-00-00 00:00:00', 1),
(191, 75, '75_58450c1e94e8a.png', '0000-00-00 00:00:00', 1),
(192, 76, '76_58451380cdc58.png', '0000-00-00 00:00:00', 1),
(193, 77, '77_5845153e82966.png', '0000-00-00 00:00:00', 0),
(194, 77, '77_584515543818f.png', '0000-00-00 00:00:00', 1),
(195, 78, '78_5845257373889.png', '0000-00-00 00:00:00', 1),
(196, 79, '79_58452da5087d8.png', '0000-00-00 00:00:00', 1),
(197, 80, '80_584532e77492e.png', '0000-00-00 00:00:00', 0),
(198, 80, '80_584532ea2070c.png', '0000-00-00 00:00:00', 0),
(199, 80, '80_584532ec87681.png', '0000-00-00 00:00:00', 0),
(200, 80, '80_584532ed7885a.png', '0000-00-00 00:00:00', 0),
(201, 80, '80_584532ed96f72.png', '0000-00-00 00:00:00', 0),
(202, 80, '80_584532edb624e.png', '0000-00-00 00:00:00', 1),
(203, 81, '81_584533bdd3c70.png', '0000-00-00 00:00:00', 1),
(204, 82, '82_585aa3ce70a48.png', '0000-00-00 00:00:00', 1),
(205, 87, '87_585aa8ea32093.png', '0000-00-00 00:00:00', 1),
(206, 88, '88_585aabe73420f.png', '0000-00-00 00:00:00', 1),
(207, 89, '89_585ab3d4debcd.png', '0000-00-00 00:00:00', 1),
(211, 90, '90_585abbd0a439f.png', '0000-00-00 00:00:00', 1),
(212, 91, '91_585ac1fcaf0c4.png', '0000-00-00 00:00:00', 1),
(213, 92, '92_585ac4c755382.png', '0000-00-00 00:00:00', 1),
(214, 93, '93_585b8b3545f51.png', '0000-00-00 00:00:00', 1),
(215, 94, '94_585b99bdb8501.png', '0000-00-00 00:00:00', 1),
(244, 95, '95_585bae557efd0.png', '0000-00-00 00:00:00', 1),
(245, 96, '96_585bb04a24a7a.png', '0000-00-00 00:00:00', 1),
(246, 97, '97_585bd3cb699ee.png', '0000-00-00 00:00:00', 1),
(247, 108, '108_585befcd0075e.png', '0000-00-00 00:00:00', 0),
(248, 108, '108_585befd048de5.png', '0000-00-00 00:00:00', 0),
(249, 108, '108_585befd9965e8.png', '0000-00-00 00:00:00', 0),
(250, 108, '108_585befdc42cd3.png', '0000-00-00 00:00:00', 0),
(251, 108, '108_585befe0b9960.png', '0000-00-00 00:00:00', 0),
(252, 108, '108_585befe25f817.png', '0000-00-00 00:00:00', 0),
(253, 108, '108_585bf0630ff6e.png', '0000-00-00 00:00:00', 1),
(254, 109, '109_585bf74c3587a.png', '0000-00-00 00:00:00', 1),
(255, 105, '105_585bf870f2279.png', '0000-00-00 00:00:00', 0),
(256, 105, '105_585bf8751fccf.png', '0000-00-00 00:00:00', 0),
(257, 105, '105_585bf87557d12.png', '0000-00-00 00:00:00', 0),
(258, 105, '105_585bf8794c50c.png', '0000-00-00 00:00:00', 0),
(259, 105, '105_585bf87e0c3e8.png', '0000-00-00 00:00:00', 0),
(260, 105, '105_585bf880581b7.png', '0000-00-00 00:00:00', 1),
(261, 121, '121_585ca6dfb2280.png', '0000-00-00 00:00:00', 1),
(262, 122, '122_585ca7053929e.png', '0000-00-00 00:00:00', 1),
(263, 123, '123_585ca739b9f38.png', '0000-00-00 00:00:00', 1),
(264, 125, '125_585cace66fb3b.png', '0000-00-00 00:00:00', 1),
(265, 127, '127_585d1c54624fd.png', '0000-00-00 00:00:00', 1),
(266, 128, '128_585d1ea013a62.png', '0000-00-00 00:00:00', 1),
(267, 129, '129_585d21a1efbcd.png', '0000-00-00 00:00:00', 1),
(277, 132, '132_585e27463920d.png', '0000-00-00 00:00:00', 1),
(278, 133, '133_585ec2e615ab0.png', '0000-00-00 00:00:00', 1),
(279, 134, '134_585ec3933cbb5.png', '0000-00-00 00:00:00', 0),
(280, 135, '135_585ed1b82a3a5.png', '0000-00-00 00:00:00', 1),
(281, 136, '136_585ed7d836297.png', '0000-00-00 00:00:00', 1),
(282, 137, '137_585eda13946fd.png', '0000-00-00 00:00:00', 1),
(283, 138, '138_585eea053e9c0.png', '0000-00-00 00:00:00', 1),
(284, 139, '139_585f885bbc766.png', '0000-00-00 00:00:00', 1),
(285, 140, '140_585f979150902.png', '0000-00-00 00:00:00', 1),
(286, 141, '141_585fa5400cc00.png', '0000-00-00 00:00:00', 1),
(287, 142, '142_585fe20ad6a9a.png', '0000-00-00 00:00:00', 0),
(288, 142, '142_585fe20ccd618.png', '0000-00-00 00:00:00', 0),
(289, 142, '142_585fe2111efc2.png', '0000-00-00 00:00:00', 0),
(290, 142, '142_585fe21e7d5a4.png', '0000-00-00 00:00:00', 0),
(291, 142, '142_585fe22af3a83.png', '0000-00-00 00:00:00', 0),
(292, 142, '142_585fe22e79ede.png', '0000-00-00 00:00:00', 0),
(293, 142, '142_585fe28c86de4.png', '0000-00-00 00:00:00', 0),
(294, 142, '142_585fe28d954af.png', '0000-00-00 00:00:00', 0),
(295, 142, '142_585fe388003e5.png', '0000-00-00 00:00:00', 0),
(296, 142, '142_585fe38ced296.png', '0000-00-00 00:00:00', 0),
(297, 142, '142_585fe38d403c0.png', '0000-00-00 00:00:00', 0),
(298, 142, '142_585fe397c72ea.png', '0000-00-00 00:00:00', 0),
(299, 142, '142_585fe39ab04fe.png', '0000-00-00 00:00:00', 0),
(300, 142, '142_585fe39b7dc7f.png', '0000-00-00 00:00:00', 0),
(301, 142, '142_585fe404d5845.png', '0000-00-00 00:00:00', 0),
(302, 142, '142_585fe40a64805.png', '0000-00-00 00:00:00', 0),
(303, 142, '142_585fe40f77000.png', '0000-00-00 00:00:00', 0),
(304, 142, '142_585fe8bb38071.png', '0000-00-00 00:00:00', 1),
(305, 143, '143_585feaa7e28d9.png', '0000-00-00 00:00:00', 1),
(306, 134, '134_586000c7d1f47.png', '0000-00-00 00:00:00', 0),
(307, 134, '134_586000e598791.png', '0000-00-00 00:00:00', 0),
(308, 134, '134_58600120a4785.png', '0000-00-00 00:00:00', 1),
(309, 144, '144_586090cd3271c.png', '0000-00-00 00:00:00', 1),
(314, 145, '145_5860e755f3ed7.png', '0000-00-00 00:00:00', 1),
(315, 146, '146_586106ad28aa6.png', '0000-00-00 00:00:00', 1),
(316, 147, '147_586a24801cfeb.png', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `campaign_locations`
--

CREATE TABLE IF NOT EXISTS `campaign_locations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(256) CHARACTER SET latin1 NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0=inactive, 1=active,-1=deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `campaign_locations`
--

INSERT INTO `campaign_locations` (`id`, `location`, `status`) VALUES
(1, 'Kolkata', 1),
(2, 'Bangalore', 1);

-- --------------------------------------------------------

--
-- Table structure for table `campaign_reports`
--

CREATE TABLE IF NOT EXISTS `campaign_reports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `campaign_id` bigint(20) unsigned NOT NULL,
  `reason` tinytext NOT NULL,
  `report_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_shared_information`
--

CREATE TABLE IF NOT EXISTS `campaign_shared_information` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL COMMENT 'Campaign shared by',
  `shared_at` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=facebook,2=twitter,3=email',
  `total_share` bigint(20) NOT NULL,
  `shared_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`campaign_id`,`user_id`),
  KEY `fk_campaign_shared_informations_campaigns1_idx` (`campaign_id`),
  KEY `fk_campaign_shared_informations_users1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `campaign_shared_information`
--

INSERT INTO `campaign_shared_information` (`id`, `campaign_id`, `user_id`, `shared_at`, `total_share`, `shared_time`) VALUES
(49, 79, 0, 2, 0, '2016-12-22 05:24:48'),
(50, 79, 0, 2, 0, '2016-12-25 02:52:30'),
(51, 134, 0, 2, 0, '2016-12-25 03:09:36'),
(52, 134, 0, 2, 0, '2016-12-25 08:30:02'),
(53, 134, 0, 2, 0, '2016-12-25 08:30:09'),
(54, 81, 0, 1, 0, '2016-12-25 10:26:54'),
(60, 95, 0, 1, 1, '2016-12-25 14:07:21'),
(61, 95, 0, 1, 1, '2016-12-25 14:11:42'),
(62, 95, 0, 1, 1, '2016-12-25 14:12:00'),
(63, 95, 0, 1, 1, '2016-12-25 14:12:22'),
(64, 76, 0, 1, 1, '2016-12-25 14:13:12'),
(65, 134, 0, 1, 1, '2016-12-25 16:34:37'),
(66, 134, 0, 1, 1, '2016-12-25 16:48:25'),
(67, 134, 0, 1, 1, '2016-12-25 16:48:57'),
(68, 93, 0, 1, 1, '2016-12-25 16:53:44'),
(72, 134, 0, 1, 1, '2016-12-27 06:29:09'),
(73, 81, 0, 1, 1, '2016-12-27 06:30:19'),
(74, 77, 0, 1, 1, '2016-12-27 06:33:23'),
(75, 77, 0, 1, 1, '2016-12-27 06:33:48'),
(76, 77, 0, 1, 1, '2016-12-27 06:34:00'),
(77, 77, 0, 1, 1, '2016-12-27 06:34:08'),
(78, 77, 0, 1, 1, '2016-12-27 06:34:47'),
(79, 77, 0, 1, 1, '2016-12-27 06:34:55'),
(80, 81, 0, 1, 1, '2016-12-27 06:36:23'),
(81, 81, 0, 1, 1, '2016-12-27 06:36:51'),
(82, 77, 0, 1, 1, '2016-12-27 06:37:20'),
(83, 134, 0, 1, 1, '2016-12-27 06:38:11'),
(84, 134, 0, 1, 1, '2016-12-27 06:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_updates_subscribers`
--

CREATE TABLE IF NOT EXISTS `campaign_updates_subscribers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `subscribed` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=scbscribed,2=unsubscribed',
  `subscribed_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `campaign_updates_subscribers`
--

INSERT INTO `campaign_updates_subscribers` (`id`, `campaign_id`, `user_id`, `email`, `subscribed`, `subscribed_date`) VALUES
(10, 53, 177, '', 1, '2016-11-28 17:01:59');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_update_loves`
--

CREATE TABLE IF NOT EXISTS `campaign_update_loves` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_update_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `campaign_id` (`campaign_update_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `campaign_update_loves`
--

INSERT INTO `campaign_update_loves` (`id`, `campaign_update_id`, `user_id`, `create_date`) VALUES
(9, 17, 190, '2016-11-23 01:33:22');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_visits`
--

CREATE TABLE IF NOT EXISTS `campaign_visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `add_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=259 ;

--
-- Dumping data for table `campaign_visits`
--

INSERT INTO `campaign_visits` (`id`, `campaign_id`, `ip_address`, `user_id`, `add_date`) VALUES
(1, 33, '45.118.166.124', 64, '2016-10-26 06:16:24'),
(2, 30, '173.252.90.247', 0, '2016-10-26 06:17:01'),
(3, 32, '73.150.4.119', 0, '2016-10-26 11:42:55'),
(4, 34, '202.78.233.90', 5, '2016-10-26 17:07:28'),
(5, 31, '65.196.53.208', 0, '2016-10-26 17:14:00'),
(6, 32, '103.47.66.238', 0, '2016-10-26 17:59:25'),
(7, 33, '45.118.166.123', 64, '2016-10-27 06:04:00'),
(8, 33, '202.78.233.72', 0, '2016-10-27 15:43:49'),
(9, 35, '69.171.230.115', 0, '2016-10-27 16:29:00'),
(10, 31, '205.197.242.147', 0, '2016-10-28 02:30:40'),
(11, 36, '31.13.113.86', 0, '2016-10-28 03:03:11'),
(12, 37, '173.252.124.74', 0, '2016-10-28 04:01:04'),
(13, 33, '45.118.166.120', 64, '2016-10-28 05:16:16'),
(14, 32, '31.13.113.80', 0, '2016-10-30 03:10:38'),
(15, 34, '5.9.8.150', 0, '2016-11-01 07:09:00'),
(16, 33, '45.118.166.121', 64, '2016-11-01 10:03:44'),
(17, 33, '173.252.90.231', 0, '2016-11-01 10:03:49'),
(18, 33, '199.16.156.126', 0, '2016-11-02 08:44:42'),
(19, 39, '173.252.123.147', 0, '2016-11-03 02:19:49'),
(20, 32, '173.252.124.24', 0, '2016-11-07 02:09:28'),
(21, 33, '45.118.166.122', 64, '2016-11-07 06:35:50'),
(22, 40, '66.220.156.120', 0, '2016-11-07 11:40:26'),
(23, 30, '66.220.146.31', 0, '2016-11-07 12:19:14'),
(24, 41, '173.252.124.70', 0, '2016-11-07 13:05:14'),
(25, 41, '40.78.146.128', 0, '2016-11-07 13:05:45'),
(26, 41, '23.99.101.118', 0, '2016-11-07 13:05:47'),
(27, 42, '173.252.124.14', 0, '2016-11-07 13:13:53'),
(28, 41, '104.209.188.207', 0, '2016-11-08 05:57:37'),
(29, 39, '66.220.146.30', 0, '2016-11-10 08:52:18'),
(30, 33, '66.220.145.244', 0, '2016-11-10 09:43:04'),
(31, 33, '184.168.200.88', 0, '2016-11-10 09:43:23'),
(32, 34, '173.252.90.240', 0, '2016-11-10 14:03:08'),
(33, 31, '173.252.90.248', 0, '2016-11-10 14:05:54'),
(34, 39, '71.127.222.91', 0, '2016-11-10 20:01:17'),
(35, 46, '199.59.148.209', 0, '2016-11-14 08:10:48'),
(40, 46, '45.34.11.165', 0, '2016-11-18 12:59:40'),
(41, 49, '202.78.233.189', 177, '2016-11-22 16:44:20'),
(42, 49, '69.171.230.103', 0, '2016-11-22 16:44:28'),
(43, 50, '69.171.230.106', 0, '2016-11-22 18:28:01'),
(44, 50, '13.76.241.210', 0, '2016-11-22 18:28:13'),
(45, 51, '69.171.230.101', 0, '2016-11-22 18:51:31'),
(46, 52, '66.220.158.106', 0, '2016-11-22 18:53:42'),
(47, 52, '173.46.77.140', 190, '2016-11-22 20:51:41'),
(48, 53, '173.252.90.229', 0, '2016-11-23 01:28:54'),
(49, 54, '173.252.124.67', 0, '2016-11-23 03:03:56'),
(50, 52, '43.242.120.228', 0, '2016-11-24 11:25:25'),
(51, 50, '43.242.120.252', 0, '2016-11-24 13:20:22'),
(52, 53, '202.78.233.232', 0, '2016-11-24 15:47:45'),
(53, 56, '173.252.124.11', 0, '2016-11-27 14:53:54'),
(54, 57, '173.252.124.20', 0, '2016-11-27 23:21:20'),
(55, 59, '202.78.233.252', 177, '2016-11-28 16:43:21'),
(56, 59, '69.171.230.120', 0, '2016-11-28 16:43:27'),
(57, 60, '173.252.124.77', 0, '2016-11-29 01:08:22'),
(58, 56, '43.242.120.191', 0, '2016-11-29 04:17:19'),
(59, 56, '69.63.188.117', 0, '2016-11-29 04:30:28'),
(60, 56, '93.173.17.164', 218, '2016-11-29 06:50:20'),
(61, 55, '69.63.188.120', 0, '2016-11-29 07:17:54'),
(62, 56, '173.252.124.10', 0, '2016-11-29 12:40:35'),
(63, 64, '173.252.124.69', 0, '2016-11-29 12:48:16'),
(64, 57, '43.242.120.163', 0, '2016-11-29 14:15:15'),
(65, 50, '173.252.124.13', 0, '2016-11-30 00:27:09'),
(66, 52, '66.220.158.101', 0, '2016-11-30 14:51:21'),
(67, 52, '43.242.120.233', 205, '2016-11-30 15:45:16'),
(68, 56, '103.21.162.185', 0, '2016-12-01 05:30:52'),
(69, 66, '69.63.188.102', 0, '2016-12-01 05:44:36'),
(70, 68, '43.242.120.78', 205, '2016-12-01 11:16:28'),
(71, 69, '103.21.162.153', 205, '2016-12-03 05:28:39'),
(72, 69, '69.63.188.110', 0, '2016-12-03 05:28:42'),
(73, 75, '46.116.98.79', 220, '2016-12-05 06:41:46'),
(74, 75, '31.13.110.111', 0, '2016-12-05 06:41:49'),
(75, 78, '31.13.110.106', 0, '2016-12-05 08:29:52'),
(76, 79, '31.13.110.99', 0, '2016-12-05 09:04:51'),
(77, 80, '173.252.124.22', 0, '2016-12-05 09:27:42'),
(78, 50, '173.252.123.134', 0, '2016-12-07 03:11:55'),
(79, 59, '173.252.123.133', 0, '2016-12-07 03:12:12'),
(80, 64, '173.252.123.142', 0, '2016-12-07 03:12:26'),
(81, 56, '173.252.123.130', 0, '2016-12-07 03:13:49'),
(82, 81, '173.252.123.138', 0, '2016-12-07 03:14:32'),
(83, 81, '106.221.157.50', 0, '2016-12-08 05:31:27'),
(84, 79, '66.249.64.165', 0, '2016-12-10 09:17:42'),
(85, 77, '66.249.64.161', 0, '2016-12-10 09:17:54'),
(86, 77, '66.220.158.102', 0, '2016-12-10 09:18:07'),
(87, 56, '66.249.64.157', 0, '2016-12-10 09:18:20'),
(88, 75, '207.182.136.170', 0, '2016-12-12 21:58:15'),
(89, 81, '115.118.112.112', 0, '2016-12-13 06:51:20'),
(90, 79, '27.58.50.239', 0, '2016-12-15 04:55:09'),
(91, 79, '173.252.90.82', 0, '2016-12-15 04:55:13'),
(92, 75, '66.220.152.174', 0, '2016-12-18 03:58:41'),
(93, 56, '180.76.15.28', 0, '2016-12-18 13:51:27'),
(94, 79, '106.221.128.193', 261, '2016-12-21 04:09:24'),
(95, 79, '27.58.54.153', 290, '2016-12-21 12:41:03'),
(96, 81, '115.118.217.147', 0, '2016-12-21 13:46:49'),
(97, 81, '173.252.90.237', 0, '2016-12-21 13:46:53'),
(98, 79, '13.66.58.109', 0, '2016-12-21 14:48:17'),
(99, 82, '217.132.102.58', 284, '2016-12-21 15:46:33'),
(100, 82, '31.13.113.172', 0, '2016-12-21 15:46:36'),
(101, 88, '31.13.113.184', 0, '2016-12-21 16:21:30'),
(102, 77, '66.220.156.121', 0, '2016-12-21 17:21:02'),
(103, 91, '173.252.124.208', 0, '2016-12-21 18:43:02'),
(104, 78, '173.252.124.211', 0, '2016-12-21 18:44:38'),
(105, 89, '173.252.124.200', 0, '2016-12-21 18:44:51'),
(106, 87, '173.252.124.222', 0, '2016-12-21 18:45:06'),
(107, 77, '173.252.124.224', 0, '2016-12-21 18:45:16'),
(108, 90, '173.252.124.199', 0, '2016-12-21 18:45:41'),
(109, 77, '199.21.99.216', 0, '2016-12-21 20:46:55'),
(110, 79, '141.8.143.151', 0, '2016-12-21 20:57:31'),
(111, 78, '141.8.143.201', 0, '2016-12-22 01:37:23'),
(112, 81, '77.88.47.44', 0, '2016-12-22 03:38:24'),
(113, 92, '27.58.32.191', 0, '2016-12-22 04:55:42'),
(114, 79, '173.252.90.94', 0, '2016-12-22 07:51:00'),
(115, 93, '31.13.113.183', 0, '2016-12-22 08:14:14'),
(116, 96, '106.221.137.221', 0, '2016-12-22 13:16:22'),
(117, 96, '173.252.90.90', 0, '2016-12-22 13:16:29'),
(118, 97, '173.252.90.81', 0, '2016-12-22 13:23:58'),
(119, 108, '173.252.88.84', 0, '2016-12-22 15:25:37'),
(120, 109, '173.252.88.95', 0, '2016-12-22 15:55:06'),
(121, 121, '106.221.129.199', 297, '2016-12-23 04:24:09'),
(122, 121, '173.252.90.92', 0, '2016-12-23 04:24:12'),
(123, 123, '173.252.88.191', 0, '2016-12-23 04:25:43'),
(124, 81, '163.172.81.225', 0, '2016-12-23 10:42:24'),
(125, 91, '163.172.68.132', 0, '2016-12-23 20:40:34'),
(126, 89, '77.88.47.31', 0, '2016-12-24 04:05:36'),
(127, 109, '27.58.56.219', 0, '2016-12-24 04:42:47'),
(128, 79, '173.252.90.86', 0, '2016-12-24 05:00:16'),
(129, 95, '173.252.90.91', 0, '2016-12-24 05:02:27'),
(130, 76, '173.252.90.80', 0, '2016-12-24 05:12:36'),
(131, 81, '46.116.51.119', 0, '2016-12-24 18:10:17'),
(132, 94, '31.13.113.68', 0, '2016-12-24 18:11:51'),
(133, 137, '31.13.113.85', 0, '2016-12-24 20:27:13'),
(134, 138, '31.13.113.91', 0, '2016-12-24 21:35:15'),
(135, 76, '173.46.78.148', 0, '2016-12-25 02:46:58'),
(136, 79, '31.13.114.82', 0, '2016-12-25 02:52:30'),
(137, 122, '27.58.28.159', 0, '2016-12-25 07:19:20'),
(138, 77, '173.252.88.92', 0, '2016-12-25 09:44:32'),
(139, 95, '173.252.88.91', 0, '2016-12-25 09:45:55'),
(140, 95, '69.171.228.119', 0, '2016-12-25 09:46:03'),
(141, 95, '104.130.137.192', 0, '2016-12-25 09:46:07'),
(142, 79, '104.239.136.102', 0, '2016-12-25 09:50:45'),
(143, 81, '173.252.105.119', 0, '2016-12-25 10:07:56'),
(144, 81, '166.78.142.7', 0, '2016-12-25 10:07:59'),
(145, 81, '144.2.2.50', 0, '2016-12-25 10:26:54'),
(146, 134, '23.253.79.148', 0, '2016-12-25 10:39:12'),
(147, 88, '27.58.46.226', 0, '2016-12-25 13:38:44'),
(148, 79, '54.145.233.200', 0, '2016-12-25 14:30:36'),
(149, 81, '51.254.121.188', 0, '2016-12-25 15:05:38'),
(150, 143, '31.13.113.92', 0, '2016-12-25 15:50:26'),
(151, 93, '23.253.94.152', 0, '2016-12-25 16:14:03'),
(152, 143, '104.239.143.37', 0, '2016-12-25 16:18:42'),
(153, 134, '128.177.161.158', 0, '2016-12-25 16:46:31'),
(154, 134, '31.13.113.79', 0, '2016-12-25 17:22:29'),
(155, 143, '82.80.249.156', 0, '2016-12-25 17:32:47'),
(156, 88, '23.253.250.42', 0, '2016-12-25 19:44:35'),
(157, 76, '69.30.211.2', 0, '2016-12-26 01:50:11'),
(158, 143, '66.220.152.134', 0, '2016-12-26 04:58:51'),
(159, 143, '106.221.132.221', 0, '2016-12-26 05:01:12'),
(160, 145, '173.252.88.88', 0, '2016-12-26 09:48:26'),
(161, 145, '27.58.55.18', 0, '2016-12-26 11:54:44'),
(162, 146, '66.220.152.185', 0, '2016-12-26 16:52:46'),
(163, 143, '27.58.45.61', 0, '2016-12-27 03:52:34'),
(164, 93, '104.131.173.251', 0, '2016-12-27 03:57:46'),
(165, 146, '66.220.152.164', 0, '2016-12-27 04:06:51'),
(166, 146, '66.220.152.137', 0, '2016-12-27 04:36:45'),
(167, 146, '66.220.152.129', 0, '2016-12-27 05:02:39'),
(168, 146, '66.220.152.141', 0, '2016-12-27 05:08:00'),
(169, 143, '23.253.90.73', 0, '2016-12-27 05:53:02'),
(170, 146, '173.252.88.93', 0, '2016-12-27 06:08:51'),
(171, 146, '173.252.88.87', 0, '2016-12-27 06:15:04'),
(172, 146, '173.252.88.90', 0, '2016-12-27 06:21:40'),
(173, 146, '173.252.88.85', 0, '2016-12-27 06:27:31'),
(174, 77, '173.252.115.168', 0, '2016-12-27 06:32:33'),
(175, 77, '66.220.148.102', 0, '2016-12-27 06:32:33'),
(176, 77, '173.252.124.123', 0, '2016-12-27 06:32:34'),
(177, 146, '54.210.55.21', 0, '2016-12-27 06:40:19'),
(178, 146, '166.70.114.194', 0, '2016-12-27 07:00:35'),
(179, 146, '50.28.106.132', 0, '2016-12-27 07:00:37'),
(180, 81, '174.200.11.174', 0, '2016-12-27 23:12:23'),
(181, 81, '70.196.5.35', 0, '2016-12-27 23:17:42'),
(182, 81, '24.191.69.202', 0, '2016-12-27 23:21:09'),
(183, 134, '66.220.156.101', 0, '2016-12-27 23:57:20'),
(184, 81, '192.157.84.228', 0, '2016-12-28 01:53:19'),
(185, 81, '174.200.7.254', 0, '2016-12-28 03:18:54'),
(186, 81, '204.145.74.25', 0, '2016-12-28 03:27:53'),
(187, 81, '174.200.7.84', 0, '2016-12-28 03:39:10'),
(188, 81, '70.197.2.240', 0, '2016-12-28 05:17:29'),
(189, 81, '79.178.48.126', 0, '2016-12-28 05:51:51'),
(190, 141, '31.13.102.117', 0, '2016-12-28 13:21:25'),
(191, 141, '128.197.65.54', 0, '2016-12-28 18:33:46'),
(192, 81, '70.208.75.188', 0, '2016-12-28 18:51:02'),
(193, 81, '173.46.76.189', 0, '2016-12-28 19:12:01'),
(194, 141, '173.70.43.107', 0, '2016-12-28 19:19:14'),
(195, 81, '108.58.156.50', 0, '2016-12-28 19:25:15'),
(196, 141, '192.64.67.251', 0, '2016-12-28 19:32:52'),
(197, 141, '74.105.155.142', 0, '2016-12-28 20:23:47'),
(198, 76, '146.185.168.20', 0, '2016-12-28 22:35:22'),
(199, 81, '64.124.194.195', 0, '2016-12-28 22:51:58'),
(200, 87, '100.1.170.144', 0, '2016-12-28 23:28:03'),
(201, 81, '192.157.84.230', 0, '2016-12-29 00:41:05'),
(202, 87, '208.90.57.196', 0, '2016-12-29 01:23:01'),
(203, 92, '27.58.58.118', 0, '2016-12-29 06:57:09'),
(204, 92, '173.252.124.28', 0, '2016-12-29 06:57:19'),
(205, 88, '103.65.28.86', 0, '2016-12-29 12:44:57'),
(206, 82, '94.154.239.69', 0, '2016-12-29 14:06:35'),
(207, 79, '106.221.142.223', 0, '2016-12-29 14:50:48'),
(208, 92, '173.252.124.66', 0, '2016-12-29 14:58:10'),
(209, 89, '173.46.78.154', 0, '2016-12-29 15:02:01'),
(210, 89, '23.253.68.76', 0, '2016-12-29 15:02:12'),
(211, 78, '173.252.124.17', 0, '2016-12-29 15:02:57'),
(212, 90, '173.252.124.30', 0, '2016-12-29 15:03:34'),
(213, 140, '173.252.124.16', 0, '2016-12-29 15:05:50'),
(214, 93, '69.112.96.235', 0, '2016-12-29 15:54:40'),
(215, 143, '47.16.76.249', 0, '2016-12-29 15:55:56'),
(216, 92, '173.252.124.231', 0, '2016-12-29 15:58:28'),
(217, 93, '77.126.4.94', 0, '2016-12-29 16:14:24'),
(218, 88, '71.190.242.151', 0, '2016-12-29 16:21:05'),
(219, 78, '31.13.113.93', 0, '2016-12-29 16:55:54'),
(220, 81, '109.65.38.76', 0, '2016-12-29 20:54:24'),
(221, 87, '37.142.174.3', 0, '2016-12-29 20:56:05'),
(222, 79, '78.151.26.206', 0, '2016-12-29 21:32:49'),
(223, 134, '47.21.201.226', 0, '2016-12-29 21:35:21'),
(224, 134, '85.130.248.6', 0, '2016-12-29 21:42:41'),
(225, 87, '180.76.15.151', 0, '2016-12-30 06:32:31'),
(226, 143, '54.228.12.183', 0, '2016-12-30 06:35:59'),
(227, 134, '194.90.219.86', 0, '2016-12-30 08:07:33'),
(228, 134, '109.64.3.171', 0, '2016-12-30 09:45:42'),
(229, 143, '12.37.161.213', 0, '2016-12-30 16:28:32'),
(230, 87, '24.104.246.213', 0, '2016-12-30 16:59:47'),
(231, 81, '144.76.30.236', 0, '2016-12-30 20:30:21'),
(232, 143, '173.76.99.175', 0, '2016-12-30 21:43:37'),
(233, 140, '27.58.49.242', 0, '2016-12-31 05:29:14'),
(234, 81, '89.138.173.241', 0, '2016-12-31 16:19:06'),
(235, 143, '185.27.106.177', 0, '2016-12-31 21:29:31'),
(236, 81, '73.114.27.211', 0, '2016-12-31 23:52:12'),
(237, 143, '84.94.235.191', 0, '2017-01-01 07:55:31'),
(238, 93, '109.66.167.236', 0, '2017-01-01 10:58:56'),
(239, 91, '78.110.173.202', 0, '2017-01-01 11:14:02'),
(240, 93, '14.99.70.65', 0, '2017-01-01 20:48:48'),
(241, 81, '24.186.49.162', 0, '2017-01-02 03:27:10'),
(242, 94, '95.45.252.1', 0, '2017-01-02 07:42:39'),
(243, 75, '79.178.42.98', 0, '2017-01-02 08:52:17'),
(244, 93, '213.8.21.155', 0, '2017-01-02 09:46:58'),
(245, 147, '31.13.102.106', 0, '2017-01-02 09:59:46'),
(246, 88, '27.58.35.253', 0, '2017-01-02 11:40:22'),
(247, 93, '89.138.174.255', 1, '2017-01-02 14:13:07'),
(248, 147, '173.46.76.187', 0, '2017-01-02 16:31:08'),
(249, 81, '106.221.132.105', 0, '2017-01-03 16:16:47'),
(250, 134, '69.58.178.56', 0, '2017-01-03 17:30:09'),
(251, 147, '163.172.68.133', 0, '2017-01-03 18:35:35'),
(252, 81, '70.208.69.147', 0, '2017-01-04 01:35:03'),
(253, 147, '27.58.60.10', 0, '2017-01-04 05:12:56'),
(254, 128, '75.86.0.31', 0, '2017-01-04 16:51:43'),
(255, 128, '173.252.90.85', 0, '2017-01-04 16:51:51'),
(256, 128, '70.214.81.8', 0, '2017-01-04 18:47:38'),
(257, 143, '27.58.33.10', 0, '2017-01-05 06:19:36'),
(258, 143, '173.252.90.93', 0, '2017-01-05 06:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `campaings_updates`
--

CREATE TABLE IF NOT EXISTS `campaings_updates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `title` varchar(256) CHARACTER SET latin1 NOT NULL,
  `video_type` tinyint(1) DEFAULT '1' COMMENT '1=yotubevideo ,2=vimeo video',
  `video_file_name` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `user_id` (`user_id`),
  KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `campaings_updates`
--

INSERT INTO `campaings_updates` (`id`, `campaign_id`, `user_id`, `title`, `video_type`, `video_file_name`, `create_date`, `description`) VALUES
(17, 53, 190, 'kkkk', NULL, NULL, '2016-11-23 08:33:14', '<p>kkkkkkkkkkkk</p>'),
(18, 60, 190, 'dd', NULL, NULL, '2016-11-29 08:08:20', '<p>dddddd</p>');

-- --------------------------------------------------------

--
-- Table structure for table `comment_replies`
--

CREATE TABLE IF NOT EXISTS `comment_replies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `reply_text` tinytext CHARACTER SET latin1 NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active,-1=deleted',
  PRIMARY KEY (`id`),
  KEY `comment_id` (`comment_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comment_user`
--

CREATE TABLE IF NOT EXISTS `comment_user` (
  `userID` bigint(20) NOT NULL AUTO_INCREMENT,
  `oauth_provider` varchar(100) DEFAULT NULL,
  `oauth_uid` bigint(20) DEFAULT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `locale` varchar(200) DEFAULT NULL,
  `profile_url` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `comment_user`
--

INSERT INTO `comment_user` (`userID`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `profile_url`, `picture_url`, `status`) VALUES
(5, 'facebook', 1092403614177438, 'Nilesh', 'Radadiya', NULL, 'male', 'en_US', 'https://www.facebook.com/1092403614177438', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12799401_951350514949416_6828954119058521806_n.jpg?oh=1dd4b4ddc95f9c695d893ec89dfcab31&oe=5869A771', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_category`
--

CREATE TABLE IF NOT EXISTS `contact_category` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_category`
--

INSERT INTO `contact_category` (`id`, `category`) VALUES
(1, 'General Question'),
(2, 'Technical support'),
(3, 'I need help creating a campaign');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 =active , 0=inactive ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `subject`, `message`, `slug`, `create_date`, `status`) VALUES
(1, 'Campaign Reminder From Mefarnes', 'You have successfully signed up for Mefarnes the number one Jewish fund raising site in the world. You can now launch your campaigns. It will only take a few minutes and you will have instant access to thousands of donors looking to fund campaigns just like yours. Sign in to view running campaigns or create your own. Go ahead the sky is the limit.', 'campaign-reminder-from-mefarnes', '2016-09-25 18:29:19', 1),
(2, 'Campaign Reminder From Mefarnes', 'You have successfully signed up for Mefarnes the number one Jewish fund raising site in the world. You can now launch your campaigns. It will only take a few minutes and you will have instant access to thousands of donors looking to fund campaigns just like yours. Sign in to view running campaigns or create your own. Go ahead the sky is the limit.', 'campaign-reminder-from-mefarnes', '2016-09-25 18:29:19', 1),
(3, 'Campaign Reminder From Mefarnes', 'You have successfully signed up for Mefarnes the number one Jewish fund raising site in the world. You can now launch your campaigns. It will only take a few minutes and you will have instant access to thousands of donors looking to fund campaigns just like yours. Sign in to view running campaigns or create your own. Go ahead the sky is the limit.', 'campaign-reminder-from-mefarnes', '2016-09-25 18:29:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

CREATE TABLE IF NOT EXISTS `login_history` (
  `idlogin_history` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `other_data` text CHARACTER SET latin1,
  `created_date` datetime DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `utype` tinyint(1) DEFAULT '0' COMMENT ' 0 = admin, 1 = delaler',
  PRIMARY KEY (`idlogin_history`),
  KEY `fk_login_history_admin1_idx` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `login_history`
--

INSERT INTO `login_history` (`idlogin_history`, `ip`, `other_data`, `created_date`, `uid`, `utype`) VALUES
(18, '103.47.66.238', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-19 11:02:41', 1, 0),
(19, '103.47.66.238', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36', '2016-11-19 11:13:56', 1, 0),
(20, '103.47.66.238', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-19 11:16:28', 1, 0),
(21, '103.47.66.238', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36', '2016-11-19 11:35:14', 1, 0),
(22, '103.47.66.238', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-19 13:31:17', 1, 0),
(23, '103.47.66.238', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-19 13:36:01', 1, 0),
(24, '103.47.66.238', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-19 13:55:44', 1, 0),
(25, '103.47.66.238', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-19 13:59:23', 1, 0),
(26, '202.78.233.189', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-22 15:31:57', 1, 0),
(27, '43.242.120.191', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-29 04:23:57', 2, 0),
(28, '43.242.120.191', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-11-29 07:39:30', 3, 0),
(29, '103.21.162.187', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-06 06:53:50', 3, 0),
(30, '217.132.102.58', 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko', '2016-12-21 17:02:17', 1, 0),
(31, '173.46.76.155', 'Mozilla/5.0 (Linux; Android 5.0; SAMSUNG-SM-G900A Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/4.0 Chrome/44.0.2403.133 Mobile Safari/537.36', '2016-12-21 17:04:16', 1, 0),
(32, '73.150.4.119', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 00:51:11', 1, 0),
(33, '73.150.4.119', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 04:40:14', 1, 0),
(34, '217.132.102.58', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-23 04:26:30', 1, 0),
(35, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-24 18:00:05', 1, 0),
(36, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-24 18:19:01', 1, 0),
(37, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-24 18:54:02', 1, 0),
(38, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-25 15:57:34', 1, 0),
(39, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-25 16:52:25', 1, 0),
(40, '73.150.4.119', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 12:58:32', 1, 0),
(41, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-29 12:25:50', 1, 0),
(42, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-29 16:53:53', 1, 0),
(43, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-29 20:41:56', 1, 0),
(44, '46.116.51.119', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-30 07:29:31', 1, 0),
(45, '89.138.174.255', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2017-01-02 14:11:00', 1, 0),
(46, '89.138.174.255', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2017-01-04 09:33:09', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_letter_subscriptions`
--

CREATE TABLE IF NOT EXISTS `news_letter_subscriptions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `subscription_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `news_letter_subscriptions`
--

INSERT INTO `news_letter_subscriptions` (`id`, `email`, `subscription_date`) VALUES
(1, 'bikash@gmail.com', '2016-09-24 09:55:46'),
(2, 'a@a.com', '2016-09-26 06:52:55'),
(3, 'a@ab.com', '2016-09-26 06:53:02'),
(4, 'tejal.patel1007@gmail.com', '2016-09-27 21:32:50'),
(5, 'vishalkumar2448034@gmail.com', '2016-10-15 15:55:58');

-- --------------------------------------------------------

--
-- Table structure for table `organization_gallery`
--

CREATE TABLE IF NOT EXISTS `organization_gallery` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL,
  `organization_id` bigint(20) unsigned NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `upload_date` datetime NOT NULL,
  `isDeafult` bigint(20) NOT NULL,
  `isImage` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organization_id` (`organization_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `organization_gallery`
--

INSERT INTO `organization_gallery` (`id`, `title`, `organization_id`, `image_name`, `upload_date`, `isDeafult`, `isImage`) VALUES
(1, 'Nature Nomads clearing', 16, 'org-gallery-scroller-1.jpg', '2016-09-18 06:18:00', 0, 0),
(2, 'Nature Nomads clearing two', 16, 'org-gallery-scroller-3.jpg', '2016-09-18 11:28:00', 0, 0),
(3, 'Nature Nomads clearing', 16, 'org-gallery-scroller-1.jpg', '2016-09-18 06:18:00', 0, 0),
(4, 'Nature Nomads clearing three', 16, 'org-gallery-scroller-2.jpg', '2016-09-18 11:28:00', 0, 0),
(11, '', 16, '', '0000-00-00 00:00:00', 0, 0),
(12, 'test', 16, '16_12803test.jpg', '0000-00-00 00:00:00', 0, 1),
(13, '', 16, '16_9844913782150_585506064963219_1229772535886548888_n.jpg', '0000-00-00 00:00:00', 0, 1),
(14, '', 15, '15_82272wishes-for-happy-birthday-picture.jpg', '0000-00-00 00:00:00', 0, 1),
(15, '', 15, '', '0000-00-00 00:00:00', 0, 0),
(16, '', 50, '50_28240index.jpg', '0000-00-00 00:00:00', 0, 1),
(17, '', 50, '50_12422test.jpg', '0000-00-00 00:00:00', 0, 1),
(18, '', 50, '50_15192test1.jpg', '0000-00-00 00:00:00', 0, 1),
(19, '', 50, '50_88728test2.jpg', '0000-00-00 00:00:00', 0, 1),
(20, '', 50, '50_81999test4.jpg', '0000-00-00 00:00:00', 0, 1),
(21, '', 50, '50_61879test5.jpg', '0000-00-00 00:00:00', 0, 1),
(22, '', 50, 'https://www.youtube.com/watch?v=1jeflDYebLo', '0000-00-00 00:00:00', 0, 0),
(23, 'Cricket fans', 61, '61_97157photo15.jpg', '0000-00-00 00:00:00', 0, 1),
(24, '', 61, '', '0000-00-00 00:00:00', 0, 0),
(25, '', 61, '', '0000-00-00 00:00:00', 0, 0),
(26, '', 50, '50_248510f0901607c072d04d32e9271c53c8812.png', '0000-00-00 00:00:00', 0, 1),
(27, '', 52, '', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(250) NOT NULL,
  `page_name` varchar(256) NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `keyword` varchar(256) NOT NULL,
  `meta_description` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `slug`, `page_name`, `meta_title`, `keyword`, `meta_description`) VALUES
(1, 'Home', 'Home Page', '', '', ''),
(2, 'dashboard', 'Dashboard', '', '', ''),
(3, 'how-it-works', 'How It Works', '', '', ''),
(4, 'faq', 'FAQ', '', '', ''),
(5, 'contact-us', 'Contact Us', '', '', ''),
(6, 'signin', 'Login', '', '', ''),
(7, 'signup', 'Sign Up', '', '', ''),
(8, 'campaign-profile', 'Campaign Profile Page', '', '', ''),
(9, 'search', 'Search Page', '', '', ''),
(10, 'campaign-creation', 'Campaign Creation', '', '', ''),
(11, 'blog', 'Blog', '', '', ''),
(12, 'terms', 'Terms', '', '', ''),
(13, 'privacy-policy', 'Privacy Policy', '', '', ''),
(14, 'appriciate-donation', 'Appreciate Donation', '', '', ''),
(15, 'organization-profile', 'Organization Profile Page', '', '', ''),
(16, 'donate-organization', 'Organization Donation', '', '', ''),
(17, 'gmail-contact', 'Import Gmail Contact', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `site_contacts_log`
--

CREATE TABLE IF NOT EXISTS `site_contacts_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `catgory` int(11) NOT NULL DEFAULT '0',
  `message` tinytext NOT NULL,
  `create_date` datetime NOT NULL,
  `replied` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=not replied,1=replied',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `site_contacts_log`
--

INSERT INTO `site_contacts_log` (`id`, `first_name`, `last_name`, `email`, `phone`, `catgory`, `message`, `create_date`, `replied`) VALUES
(1, 'Bikash', 'Pal', 'bikashpal2008@gmail.com', '123-456-7890', 1, 'This is''s test message from contact us page', '2016-09-03 15:01:07', 0),
(2, 'Sandy', 'Roy', 'bikashpal2008@gmail.com', '123-456-7890', 1, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, co', '2016-09-03 15:07:38', 0),
(3, 'Bikash', 'Pal', 'bikashpal2008@gmail.com', '123-456-7890', 1, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, co', '2016-09-03 15:11:19', 0),
(4, 'Bikash', 'Pal', 'bikashpal2008@gmail.com', '123-456-7890', 0, 'This is message', '2016-09-04 00:00:42', 0),
(5, 'Bikash', 'Roy', 'bikash.codelogicx@gmail.com', '123-456-7890', 1, 'This is test message', '2016-09-04 03:15:37', 1),
(6, 'test', 'test', 'test@test.com', '999-999-9999', 2, 'Test test', '2016-12-28 12:48:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` text NOT NULL,
  `access_token` text NOT NULL,
  `added_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `account_id`, `access_token`, `added_date`) VALUES
(3, '2010880096', 'STAGE_196f485119540eaee553d31461ea80a7476b4c9933b85e75ffc00aa9c68a163e', '0000-00-00 00:00:00'),
(4, '1171158445', 'STAGE_428f8702f3bda086e6d562bb8f87874325885058ed423ebccdde025ca9e76172', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `test_payment`
--

CREATE TABLE IF NOT EXISTS `test_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) NOT NULL,
  `checkout_id` text NOT NULL,
  `pay_status` enum('0','1') NOT NULL DEFAULT '0',
  `added_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `test_payment`
--

INSERT INTO `test_payment` (`id`, `user_name`, `checkout_id`, `pay_status`, `added_date`) VALUES
(1, 'tes t', '1185397019', '0', '2016-12-08 23:36:30'),
(2, 'tes t', '630334439', '0', '2016-12-08 23:41:07'),
(3, 'tes t', '847963382', '1', '2016-12-08 23:44:32'),
(4, 'tes t', '1606337105', '1', '2016-12-26 02:02:10'),
(5, 'tes t', '392990986', '1', '2016-12-26 03:30:38'),
(6, 'tes t', '1562865780', '1', '2016-12-26 05:11:46'),
(7, 'tes t', '1808143168', '1', '2016-12-26 05:12:10'),
(8, 'tes t', '1061463563', '1', '2016-12-26 05:12:10'),
(9, 'tes t', '619471531', '1', '2016-12-26 05:13:09'),
(10, 'tes t', '1355312364', '1', '2016-12-26 05:21:40'),
(11, 'tes t', '345644979', '1', '2017-01-04 20:37:59'),
(12, 'tes t', '1118280274', '1', '2017-01-04 20:44:16');

-- --------------------------------------------------------

--
-- Table structure for table `twitterdata`
--

CREATE TABLE IF NOT EXISTS `twitterdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `followers_count` int(11) NOT NULL,
  `friends_count` int(11) NOT NULL,
  `favourites_count` int(11) NOT NULL,
  `profile_image_url` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `oauth_token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `profile_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Individual, 2=Organization',
  `org_account_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Myself ,2=Business,3=Non-profit,4=others',
  `org_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `first_name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `last_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(256) CHARACTER SET latin1 NOT NULL,
  `ein_no` varchar(45) CHARACTER SET latin1 NOT NULL,
  `street_name` varchar(150) CHARACTER SET latin1 NOT NULL,
  `street_no` varchar(50) CHARACTER SET latin1 NOT NULL,
  `registrar_city` varchar(256) DEFAULT NULL,
  `registrar_country` varchar(255) DEFAULT NULL,
  `city` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `country` varchar(150) CHARACTER SET latin1 NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_token` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `is_token_valid` enum('Y','N') CHARACTER SET latin1 DEFAULT 'N',
  `token_expires_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0=inactive, 1=active,-1=deleted',
  `last_login` datetime DEFAULT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active ,0=inactive',
  `we_pay_account_id` int(11) DEFAULT NULL,
  `wepay_access_token` varchar(256) DEFAULT NULL,
  `wepay_user_id` int(11) DEFAULT NULL,
  `wepay_token_type` varchar(100) DEFAULT NULL,
  `wepay_token_expires_in` varchar(100) DEFAULT NULL,
  `wepay_confirm` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=confirmed,0=pending',
  `image` varchar(255) DEFAULT 'user.png',
  `org_tag` varchar(256) DEFAULT NULL,
  `about_me` text,
  `website_url` varchar(256) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `org_slug` varchar(255) DEFAULT NULL,
  `oauth_provider` varchar(100) NOT NULL,
  `oauth_uid` bigint(20) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `locale` varchar(100) NOT NULL,
  `profile_url` varchar(500) NOT NULL,
  `picture_url` varchar(500) NOT NULL,
  `isProfile` bigint(20) NOT NULL DEFAULT '0',
  `user_image` varchar(255) NOT NULL,
  `new_wepay_account_id` text NOT NULL,
  `new_wepay_access_token` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `user_token_UNIQUE` (`user_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=333 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `profile_type`, `org_account_type`, `org_name`, `first_name`, `last_name`, `email`, `password`, `ein_no`, `street_name`, `street_no`, `registrar_city`, `registrar_country`, `city`, `country`, `create_date`, `user_token`, `is_token_valid`, `token_expires_at`, `status`, `last_login`, `newsletter`, `we_pay_account_id`, `wepay_access_token`, `wepay_user_id`, `wepay_token_type`, `wepay_token_expires_in`, `wepay_confirm`, `image`, `org_tag`, `about_me`, `website_url`, `location`, `org_slug`, `oauth_provider`, `oauth_uid`, `gender`, `locale`, `profile_url`, `picture_url`, `isProfile`, `user_image`, `new_wepay_account_id`, `new_wepay_access_token`) VALUES
(12, 1, 3, '', 'Jason', 'Newman', 'Jasonnewman2016@gmail.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, NULL, '', '2016-09-11 09:57:31', NULL, 'N', NULL, 1, NULL, 1, 421411418, 'STAGE_c0c9cb0876a6acd91b614033c6e54c1c26c971933c2569729c48f1f0d3018a06', 19875354, 'BEARER', '7776000', 1, 'user.png', NULL, NULL, NULL, '', NULL, '', 0, '', '', '', '', 0, '', '421411418', 'STAGE_c0c9cb0876a6acd91b614033c6e54c1c26c971933c2569729c48f1f0d3018a06'),
(15, 2, 3, 'Organization', '', '', 'bikash@gmail.com', '6d74494575d2ed57c210bb989bd4282e9eb4075d', '14785239', 'San Francisco, CA, United States', '', NULL, NULL, 'Cologne, Germany', '', '2016-09-11 20:55:09', '915602d46a1df2a9ad04c3f14b72b02a', 'Y', NULL, 1, NULL, 1, 975414716, 'STAGE_06aa3d5111af03e5b2ffec4603654f0a7493e924caaf33f40baa1121295cc7c6', 225227279, 'BEARER', '7776000', 0, '15_cover.png', 'test', '<p>test</p>', NULL, '', 'organization', '', 0, '', '', '', '', 1, '', '975414716', 'STAGE_06aa3d5111af03e5b2ffec4603654f0a7493e924caaf33f40baa1121295cc7c6'),
(16, 2, 2, 'Self Help', '', '', 'bikash07377@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '14785239ET6', 'Torrance, CA, United States', '', NULL, NULL, 'Kolkata, West Bengal, India', '', '2016-09-11 21:03:47', NULL, 'N', NULL, 1, NULL, 1, 253804624, 'STAGE_ff994db454205d824da78ae45437a8b947f8a210cc6bdec814c0165fad2c908d', 5768497, 'BEARER', '7776000', 1, '16_cover.png', 'Let’s invest some time for our nature take us to save our future!', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vehicula magna eu nunc varius, in euismod urna porta. Phasellus vulputate mi libero, bibendum porttitor risus bibendum sed. Donec imperdiet, est vel vehicula dapibus, mauris lectus facilisis risus, sit amet accumsan tellus ligula eu urna. Donec quis hendrerit massa. Morbi ullamcorper tortor enim. Vestibulum vitae enim rhoncus, mattis diam in, tincidunt sapien. Vestibulum eget erat ut arcu tincidunt pharetra. Integer at mauris quam. Vestibulum neque lorem, porttitor a elit molestie, hendrerit aliquet augue. Cras vel sollicitudin ex.</p>\r\n<p>Cras odio ante, tristique aliquet suscipit ac, auctor eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse potenti. In turpis mauris, placerat quis sapien ut, lobortis mattis metus.</p>', NULL, 'Maryland, Washington', 'self-help', '', 0, '', '', '', '', 1, '', '253804624', 'STAGE_ff994db454205d824da78ae45437a8b947f8a210cc6bdec814c0165fad2c908d'),
(40, 1, 1, '', 'Bikash', 'Pal', 'bbikash.codelogicx@gmail.com', '6d74494575d2ed57c210bb989bd4282e9eb4075d', '', '', '', NULL, NULL, 'Kolkata, West Bengal, India', '', '2016-09-26 22:38:29', '345c33ec298fa4267d920079c39850b2', 'Y', NULL, 0, NULL, 1, 1557380932, 'STAGE_063a1ee450fec124568fbc93eb7817f0b59cf9848bb161da70802a8e79fd7cb8', 110545997, 'BEARER', '0', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '1557380932', 'STAGE_063a1ee450fec124568fbc93eb7817f0b59cf9848bb161da70802a8e79fd7cb8'),
(50, 2, 3, 'Neal', '', '', 'neal@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '121212', 'Florida, United States', '', NULL, NULL, 'Florida, United States', '', '2016-09-29 14:37:41', 'cb1ea3ea1b18e1ac35d031a9241b7c04', 'Y', NULL, 1, NULL, 1, 1669871415, 'STAGE_65f777c656ff457cd78e9285d40b008d6bc3be883113420230891c726ab291a0', 243610091, 'BEARER', '7776000', 0, 'user.png', 'this is testing text by tejal', '<p>this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal this is testing text by tejal</p>', NULL, NULL, 'neal', '', 0, '', '', '', '', 1, '', '1669871415', 'STAGE_65f777c656ff457cd78e9285d40b008d6bc3be883113420230891c726ab291a0'),
(52, 2, 3, 'Self Help 2', '', '', 'bikash@codelogicx.com', '6d74494575d2ed57c210bb989bd4282e9eb4075d', '14785239ET6', 'Garland, TX, United States', '', NULL, NULL, 'Kolkata, West Bengal, India', '', '2016-09-30 22:27:17', 'f247d0d6cf7dd8f21850d313f28cb8c6', 'Y', NULL, 1, NULL, 1, 2086605080, 'STAGE_006abd24f4d7a3ecf8be2f7fd3565774aa58d849e3a843d01ad441699ac24a94', 145258075, 'BEARER', '7775980', 0, '52_cover.png', 'this is test moto', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence</p>', NULL, NULL, 'self-help-2', '', 0, '', '', '', '', 1, '', '2086605080', 'STAGE_006abd24f4d7a3ecf8be2f7fd3565774aa58d849e3a843d01ad441699ac24a94'),
(57, 1, 1, '', 'Bikash', 'Pal', 'bikashpal2008@gmail.com', '', '', '', '', NULL, NULL, NULL, '', '2016-10-03 17:01:36', NULL, 'N', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, 'facebook', 1453635004662744, 'male', 'en_US', 'https://www.facebook.com/1453635004662744', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/10259957_1269294376430142_1680003229355965580_n.jpg?oh=3d181d7597da7b631de883bab0dbbe13&oe=58A8D0EB', 0, '', '', ''),
(59, 2, 1, 'a', '', '', 'a@a.com', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '', '', '', NULL, NULL, 'a', '', '2016-10-09 08:33:15', 'f8794904f72a7f6a349c96426720fff0', 'Y', NULL, 0, NULL, 1, 1586676984, 'STAGE_16a6b78b691d3f15fa77c4c8663832900cf25eb9f1c89e5838c95acff8949b84', 20214416, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'a', '', 0, '', '', '', '', 0, '', '1586676984', 'STAGE_16a6b78b691d3f15fa77c4c8663832900cf25eb9f1c89e5838c95acff8949b84'),
(61, 2, 4, 'Codeyiizen', '', '', 'maneesh@codeyiizen.com', '719855e8f4ebd94341277b0b0d50b75c5187133f', '1234567890', 'India', '', NULL, NULL, 'India', '', '2016-10-11 15:54:39', '7bd0cddfe4d1bcfeb91a402996efc007', 'Y', NULL, 1, NULL, 1, 228520666, 'STAGE_50423cc9c6c1de0208490e104c0aece25796d955915bbbec3255e814d648167f', 23817458, 'BEARER', '7775964', 0, '61_cover.png', 'Motto', '<p>gfdfgsdfgsdfgsggdfgsdfggsdfgsdfgs sdf gf sdfg sdfg sdf gsdf gsd fgsdfg </p>', NULL, NULL, 'codeyiizen', '', 0, '', '', '', '', 1, '', '228520666', 'STAGE_50423cc9c6c1de0208490e104c0aece25796d955915bbbec3255e814d648167f'),
(89, 2, 3, 'Codeyiizen', '', '', 'test@codeyiizen.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '123456', 'Delhi, India', '', NULL, NULL, 'Delhi, India', '', '2016-11-17 20:39:34', '49e826cd29363f27c505249315728427', 'Y', NULL, 1, NULL, 1, 49830103, 'STAGE_7c15680a9643a411199a0027c03c3c285dfed248596e9caa0758ded643eb1711', 178446040, 'BEARER', '7775959', 0, 'user.png', NULL, NULL, NULL, NULL, 'codeyiizen', '', 0, '', '', '', '', 0, '', '49830103', 'STAGE_7c15680a9643a411199a0027c03c3c285dfed248596e9caa0758ded643eb1711'),
(123, 2, 3, 'Test', '', '', 'pappu.shiwjee1985@gmail.com', '94ba69fdd6ac7c1576e4b079514aa04004822824', '123', 'India', '', NULL, NULL, 'India', '', '2016-11-18 15:23:26', '4d3f137f07bfa6092629855532d7a6ce', 'Y', NULL, -1, NULL, 1, 312501359, 'STAGE_20919013ccba2b5c2e8a418306a3606fe9fb85d2edc134f75d3ac0b6ebfa3f57', 240103177, 'BEARER', '7770067', 0, 'user.png', NULL, NULL, NULL, NULL, 'test', '', 0, '', '', '', '', 0, '', '312501359', 'STAGE_20919013ccba2b5c2e8a418306a3606fe9fb85d2edc134f75d3ac0b6ebfa3f57'),
(152, 2, 3, 'Yismach', '', '', 'Rafi@yismach.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '11111', '136 Pennington Avenue, Passaic, NJ, United States', '', NULL, NULL, 'Passaic, NJ, United States', '', '2016-11-21 20:33:25', 'd9a0ca20272c33dab7957916a2d6acaf', 'Y', NULL, 0, NULL, 1, 1818594267, 'STAGE_8516f8fffb42a00218a6b4170d4cdd9ff298d80ac24ce235a9a8a1605305edfa', 237811606, 'BEARER', '0', 0, 'user.png', NULL, NULL, NULL, NULL, 'yismach', '', 0, '', '', '', '', 0, '', '1818594267', 'STAGE_8516f8fffb42a00218a6b4170d4cdd9ff298d80ac24ce235a9a8a1605305edfa'),
(177, 1, 1, '', 'Bikash', 'Paul', 'dipakpal1993@gmail.com', '6d74494575d2ed57c210bb989bd4282e9eb4075d', '', '', '', NULL, NULL, 'Kolkata, West Bengal, India', '', '2016-11-22 23:38:43', NULL, 'N', NULL, 1, NULL, 1, 2007771695, 'PRODUCTION_b2feceaa66f7d1e3a60836f73a5fb84d9db81ae0e8e15b6ffb594841fd3a99f3', 144172334, 'BEARER', '7776000', 1, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '177_58347a757ab76.png', '2007771695', 'PRODUCTION_b2feceaa66f7d1e3a60836f73a5fb84d9db81ae0e8e15b6ffb594841fd3a99f3'),
(189, 1, 1, '', 'Rafi', 'Newman', 'jason.o.newman@gmail.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'West Orange, NJ, United States', '', '2016-11-23 01:19:04', '21865c5bb953d489c21ea15264163e38', 'Y', NULL, 0, NULL, 1, 527446164, 'PRODUCTION_a5efec77238a89d7c9a0ebfabd7eddaa6632fdf8855e88c7a64b4542a556447f', 19329595, 'BEARER', '0', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '527446164', 'PRODUCTION_a5efec77238a89d7c9a0ebfabd7eddaa6632fdf8855e88c7a64b4542a556447f'),
(190, 2, 2, 'Nbworks', '', '', 'info@nbworks.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '136 Pennington Avenue, Passaic, NJ, United States', '', NULL, NULL, 'Passaic, NJ, United States', '', '2016-11-23 01:22:13', 'dd1172a998ee1f9f33fdafafa4c9eb77', 'Y', NULL, 1, NULL, 1, 1733898502, 'PRODUCTION_1ef865d51e4eced18af30cd9a10b686f290d110ea25c0238e72dbd509e748fd3', 3104277, 'BEARER', '7775984', 1, 'user.png', NULL, NULL, NULL, NULL, 'nbworks', '', 0, '', '', '', '', 0, '', '1733898502', 'PRODUCTION_1ef865d51e4eced18af30cd9a10b686f290d110ea25c0238e72dbd509e748fd3'),
(191, 1, 1, '', 'Bikash', 'Pal', 'bikash.codelogicx@gmail.com', '6d74494575d2ed57c210bb989bd4282e9eb4075d', '', '', '', NULL, NULL, 'Kolkata, West Bengal, India', '', '2016-11-23 01:23:24', '78681ba27f6b7143f9d28adbdac35641', 'Y', NULL, 0, NULL, 1, 303255201, 'PRODUCTION_5d80138f77ce801dcfc310cff934ce8abda48a0619714c0a06349f931433e628', 25135952, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '303255201', 'PRODUCTION_5d80138f77ce801dcfc310cff934ce8abda48a0619714c0a06349f931433e628'),
(205, 1, 1, '', 'Manish', 'Varmora', 'mbvarmora@gmail.com', '46b4416f87798b3dee10bdacd307b1654b145117', '', '', '', NULL, NULL, 'Ahmedabad, Gujarat, India', '', '2016-11-23 11:06:10', 'cedb4a280144c94be7e727672e4cb573', 'Y', NULL, 0, NULL, 1, 605000458, 'PRODUCTION_5c8a7f91d26ee9eb77952bb931ecea97c5544026d88caf07579284b3aa54fba4', 132706032, 'BEARER', '7774179', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '605000458', 'PRODUCTION_5c8a7f91d26ee9eb77952bb931ecea97c5544026d88caf07579284b3aa54fba4'),
(209, 2, 1, 'aa', '', '', 'aa@aa.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'Avenue of the Americas, New York, NY, United States', '', '2016-11-24 19:52:23', '622a69f32f5efb148eefb0f5d4fed90f', 'Y', NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'aa', '', 0, '', '', '', '', 0, '', '', ''),
(211, 2, 2, 'Kajs', '', '', 'jsjsj@jdjsj.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', 'E', '', NULL, NULL, 'Dkd', '', '2016-11-24 23:11:47', 'ec9f3f7a01ca6b1bbc38a1a81c120ae0', 'Y', NULL, 0, NULL, 1, 641997441, 'PRODUCTION_65e5fb87747c0e5ae384bfe022870101340d8663eb12c9bc7914cd2a3f12ec1e', 75352113, 'BEARER', '7775991', 0, 'user.png', NULL, NULL, NULL, NULL, 'kajs', '', 0, '', '', '', '', 0, '', '641997441', 'PRODUCTION_65e5fb87747c0e5ae384bfe022870101340d8663eb12c9bc7914cd2a3f12ec1e'),
(212, 1, 1, '', 'Jjsj', 'Jn', 'njk@kdks.com', '4e4c240fc8770257e898347f437aace8ba015316', '', '', '', NULL, NULL, 'JJ Fish & Chicken, East Adams Street, Chicago, IL, United States', '', '2016-11-24 23:12:31', '42e9576de25dd7fd3274f905e8432a09', 'Y', NULL, 0, NULL, 1, 555673393, 'PRODUCTION_cdff70b49756cbb0196299a9c67a57704f72940c8457e93d0c4c773cc588971b', 155424902, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '555673393', 'PRODUCTION_cdff70b49756cbb0196299a9c67a57704f72940c8457e93d0c4c773cc588971b'),
(213, 2, 3, 'Yismach', '', '', 'shmuel@yismach.com', 'ccb1eb3e656d9c6434298bffd5254d6361007f7f', ' 81-1024073', 'West Orange, NJ, United States', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-11-26 23:07:16', NULL, 'N', NULL, 1, NULL, 1, 1360889832, 'PRODUCTION_0ec249ce8c4c0a56956a03db728b3259ebe7b011aac1579927ba32ba831a04e5', 269925329, 'BEARER', '7775930', 1, 'user.png', NULL, NULL, NULL, NULL, 'yismach', '', 0, '', '', '', '', 0, '', '1360889832', 'PRODUCTION_0ec249ce8c4c0a56956a03db728b3259ebe7b011aac1579927ba32ba831a04e5'),
(214, 2, 3, 'Yeshiva Nachlas Yisroel Yitzchok ', '', '', 'neumannpsych@gmail.com', 'ccb1eb3e656d9c6434298bffd5254d6361007f7f', '010681969', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-11-27 14:46:05', NULL, 'N', NULL, 1, NULL, 1, 1643366080, 'PRODUCTION_27bdd8260db278f20d75b42866d7f2d18ad6b78b6d302bc79a04abc01b3a7190', 201003688, 'BEARER', '7775857', 1, 'user.png', NULL, NULL, NULL, NULL, 'yeshiva-nachlas-yisroel-yitzchok-', '', 0, '', '', '', '', 0, '', '1643366080', 'PRODUCTION_27bdd8260db278f20d75b42866d7f2d18ad6b78b6d302bc79a04abc01b3a7190'),
(215, 2, 2, 'Strategic Solutions Technology Group', '', '', 'strategictecho@gmail.com', 'e1da29a72accb3bedbe9b0dbb1c50e561c5c4747', '', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-11-27 23:29:35', '9efa07bc425882f86f3ab4791c59718f', 'Y', NULL, 0, NULL, 1, 1680420672, 'PRODUCTION_3e468c133718f86e1221321bf44f8eeb62088ac87a6d88a4ec8015fe20873dd0', 98760784, 'BEARER', '7775980', 0, 'user.png', NULL, NULL, NULL, NULL, 'strategic-solutions-technology-group', '', 0, '', '', '', '', 0, '', '1680420672', 'PRODUCTION_3e468c133718f86e1221321bf44f8eeb62088ac87a6d88a4ec8015fe20873dd0'),
(216, 2, 3, 'Yeshivas Mir Yerushalayim', '', '', 'strategic@013net.net', '23c27c01d8af7b59d170101d992a52136c9224d2', '123456789', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-11-28 14:13:47', 'aa06e347df8b738eca5c6624f2a688de', 'N', NULL, 1, NULL, 1, 1175388158, 'PRODUCTION_57a2c1bd68de509cbe31e16f0c7e32a14e5544982b21bc79c8a00fb0c55c2164', 34762555, 'BEARER', '7774992', 1, 'user.png', NULL, NULL, NULL, NULL, 'yeshivas-mir-yerushalayim', '', 0, '', '', '', '', 0, '', '1676363636', 'PRODUCTION_d65a6f6941b33c62d60c213ca22c28c53ba19e18a90f9573e254fa92eca75fdd'),
(217, 2, 3, 'abc', '', '', 'abcd@efgh.com', '6d74494575d2ed57c210bb989bd4282e9eb4075d', 'ENIM-9876', 'Washington, United States', '', NULL, NULL, 'Kolkata, West Bengal, India', '', '2016-11-29 00:00:27', '7318cb3e054e6cbd47cd12955b8b0cd7', 'Y', NULL, 0, NULL, 1, 393152493, 'PRODUCTION_28c3dbe88d5578e99d48d1e4ad5bc49dd907e77f3d54de76aaefc678f24ad1be', 33968171, 'BEARER', '7775969', 0, 'user.png', NULL, NULL, NULL, NULL, 'abc', '', 0, '', '', '', '', 0, '', '393152493', 'PRODUCTION_28c3dbe88d5578e99d48d1e4ad5bc49dd907e77f3d54de76aaefc678f24ad1be'),
(218, 2, 2, 'Strategic Solutions Technology Group', '', '', 'strategictechno@gmail.com', 'e1da29a72accb3bedbe9b0dbb1c50e561c5c4747', '', 'Passaic, NJ, United States', '', NULL, NULL, 'Passaic, NJ, United States', '', '2016-11-29 12:48:17', '8376c0df44ecb433e25feb798f77e6a0', 'N', NULL, 1, NULL, 1, 648141599, 'PRODUCTION_f36e10d1ea255da01a9ec131f051e4c59841f1ac0a401b1e95bae3e7968347dd', 78206324, 'BEARER', '7775977', 1, 'user.png', NULL, NULL, NULL, NULL, 'strategic-solutions-technology-group', '', 0, '', '', '', '', 0, '', '752668116', 'PRODUCTION_53caf8ec4e7878f7016b7c1bdad7b720cf7bc324db57755d405f77cd1caa8de8'),
(219, 1, 1, '', 'Menucha', 'Shapiro', 'debmenu@gmail.com', 'e50ad7e6acf70fa484f2567d401db031886c9e70', '', '', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-11-30 01:57:46', NULL, 'N', NULL, 1, NULL, 1, 882503234, 'PRODUCTION_0f4e781bd39415f42cfa9af81a86b38998c626bc004c98b410772448c1b68534', 87925355, 'BEARER', '7776000', 1, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '882503234', 'PRODUCTION_0f4e781bd39415f42cfa9af81a86b38998c626bc004c98b410772448c1b68534'),
(220, 2, 3, 'Kupat Hair', '', '', 'domemountain@gmail.com', 'ccb1eb3e656d9c6434298bffd5254d6361007f7f', 'domemountain@gmail.com', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-05 13:25:43', NULL, 'N', NULL, 1, NULL, 1, 1317006258, 'PRODUCTION_20e512ea417044c373909ab3e3e6006140ee3dfde17cc10e056cd8332fffd3c0', 4160540, 'BEARER', '7775975', 1, 'user.png', NULL, NULL, NULL, NULL, 'kupat-hair', '', 0, '', '', '', '', 0, '', '1317006258', 'PRODUCTION_20e512ea417044c373909ab3e3e6006140ee3dfde17cc10e056cd8332fffd3c0'),
(221, 2, 3, 'Vaad Harabanim l’Inyanei Tzedaka b’Eretz Hakodesh', '', '', 'yeshahomestead@gmail.com', '73ce48fbfee852b1de0ffccc2bf64094b5957a6b', '20-1523797', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-05 15:09:02', NULL, 'N', NULL, 1, NULL, 1, 564806550, 'PRODUCTION_db0102a6ead2156e461bcaa5047a8c4963aa20fc420e30794988ec8d541168ee', 176447871, 'BEARER', '7775945', 1, 'user.png', NULL, NULL, NULL, NULL, 'vaad-harabanim-l’inyanei-tzedaka-b’eretz-hakodesh', '', 0, '', '', '', '', 0, '', '564806550', 'PRODUCTION_db0102a6ead2156e461bcaa5047a8c4963aa20fc420e30794988ec8d541168ee'),
(222, 2, 3, 'United Hatzalah', '', '', 'vibrantcounseling@gmail.com', 'de587ec3310b9627a08826ffcf0914373c1c2e4d', '11-3533002', 'New York, NY 10022', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-05 15:55:27', NULL, 'N', NULL, 1, NULL, 1, 1364046984, 'PRODUCTION_98319a217a71eeae1c67f9922c99dfbc8bfcc74bfadceffac77447774fdf9541', 250133537, 'BEARER', '7775932', 1, 'user.png', NULL, NULL, NULL, NULL, 'united-hatzalah', '', 0, '', '', '', '', 0, '', '1364046984', 'PRODUCTION_98319a217a71eeae1c67f9922c99dfbc8bfcc74bfadceffac77447774fdf9541'),
(224, 1, 1, '', 'raj', 'jadhav', 'raj@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'pune', '', '2016-12-11 18:11:57', '6511bf01b3491a15b8e0b23348a052b4', 'Y', NULL, 1, NULL, 1, 343389614, 'PRODUCTION_9ceab7ec8be8aa902713e9d18054e35026b77e346f73fecf5f334e16bd8b3a58', 533042, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '343389614', 'PRODUCTION_9ceab7ec8be8aa902713e9d18054e35026b77e346f73fecf5f334e16bd8b3a58'),
(225, 1, 1, '', 'llll', 'lll', 'lalal@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Paris, France', '', '2016-12-12 15:21:20', 'cbe9a91a742923572da3bc4b4f337f67', 'Y', NULL, 0, NULL, 1, 1796191429, 'PRODUCTION_e2ffc14487a127545ba3fd3a8c31fff8d228809799db84b51b362337316097f5', 147883474, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '1796191429', 'PRODUCTION_e2ffc14487a127545ba3fd3a8c31fff8d228809799db84b51b362337316097f5'),
(227, 1, 1, '', 'raj', 'jadhav', 'rajmjadhav1984@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-12 15:59:19', '97db7e1cd9cd5ba932c44dfc5bcb0a9f', 'Y', NULL, 0, NULL, 1, 195223329, 'PRODUCTION_6ed8c24da727e935d935b88edcc479ddc101f94036689ac8038fdd5ded2229df', 205960706, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '195223329', 'PRODUCTION_6ed8c24da727e935d935b88edcc479ddc101f94036689ac8038fdd5ded2229df'),
(228, 1, 1, '', 'kkkkk', 'lllll', 'lalaccl@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-12 19:45:59', 'b4cff26957e13ba42351702d392626d5', 'Y', NULL, 0, NULL, 1, 1531173958, 'PRODUCTION_469b53a945c6972cfd8ed3431c8fcf7b61eee22202f54f97699ebe36b8eadbda', 189802338, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '1531173958', 'PRODUCTION_469b53a945c6972cfd8ed3431c8fcf7b61eee22202f54f97699ebe36b8eadbda'),
(229, 1, 1, '', 'raj', 'jadhav', 'raj2@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-13 14:54:15', 'cc08474d96ba7a3ae02c83c9a8fb554b', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(230, 1, 1, '', 'rajj', 'jadhav', 'raj3@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-13 14:56:08', '3fbe542511f5f46bea3d40d6f3014168', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(231, 1, 1, '', 'rajl', 'jadhav', 'raj4@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-13 15:00:22', '1e1beceae34ee048d5be383f2c9bf566', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(232, 1, 1, '', 'raje', 'jjadhv', 'raj6@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-13 15:04:01', 'ffc0b38b98fc00a0e36f714f7b2c42d6', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(233, 1, 1, '', 'raj', 'jadhav', 'raj7@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-13 16:35:07', 'da91531ef3dd6dff2a2e12631fe76c96', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(234, 1, 1, '', 'aa', 'aa', 'rafi10293@gmail.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'South Orange, NJ, United States', '', '2016-12-13 19:48:56', '41e823d6beae4cf806efaa8b3642108b', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(235, 1, 1, '', 'rafi', 'newman', 'rafinewman1@gmail.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'West Orange, NJ, United States', '', '2016-12-13 19:49:45', '9a04f873ab85893abf7ad0eecf5e46d5', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(236, 2, 3, 'One Family Fund', '', '', 'shadchanitjh@gmail.com', '0a0ef968ffc4d24807862ad9d4cc32f8cab132c2', '11-3585917', 'Philadelphia, PA 19182-2732', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-14 02:23:01', '08261c252d25860c1a5a537fd2a1abda', 'Y', NULL, 0, NULL, 1, 176882626, 'PRODUCTION_0adc410ad0bfddc2fe61833fd536c08eb144f55f6efce8bc43247bf32dc5b145', 41660104, 'BEARER', '7775944', 0, 'user.png', NULL, NULL, NULL, NULL, 'one-family-fund', '', 0, '', '', '', '', 0, '', '176882626', 'PRODUCTION_0adc410ad0bfddc2fe61833fd536c08eb144f55f6efce8bc43247bf32dc5b145'),
(237, 2, 3, 'Kol Kallah Bridal Fund', '', '', 'KolKallah@gmail.com', 'aff23f6e1e6c673bc5fbb387a1e80fbdb31e0a3f', '11-3459952', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-14 22:18:27', 'd6c52bdac031a4fa0a7d6f1794acfca5', 'Y', NULL, 0, NULL, 1, 1235827529, 'PRODUCTION_618b522dcf295602a9a148709860c2f11676bb9946fe7ad28bb8c866415cf11b', 33830297, 'BEARER', '7775953', 0, 'user.png', NULL, NULL, NULL, NULL, 'kol-kallah-bridal-fund', '', 0, '', '', '', '', 0, '', '1235827529', 'PRODUCTION_618b522dcf295602a9a148709860c2f11676bb9946fe7ad28bb8c866415cf11b'),
(238, 1, 1, '', 'aaaa', 'a', 'aaaaaa@afh.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 's', '', '2016-12-15 11:35:30', '60a2535a37d322fc7e566f480c70d1a7', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(239, 2, 3, 'test', '', '', 'test1@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-15 11:58:05', '831810f2c39bcbdae053263c0ef29b94', 'Y', NULL, 0, NULL, 1, 2146195651, 'PRODUCTION_a0ebf1217e073f2af7dcf748e854f6bcef6325e415615920f0e69dafe1b644da', 61488, 'BEARER', '7775951', 0, 'user.png', NULL, NULL, NULL, NULL, 'test', '', 0, '', '', '', '', 0, '', '2146195651', 'PRODUCTION_a0ebf1217e073f2af7dcf748e854f6bcef6325e415615920f0e69dafe1b644da'),
(240, 2, 3, 'l', '', '', 'l@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-15 21:51:24', 'e8f02e9f3f156bc74ee4908f1ab9df6e', 'Y', NULL, 0, NULL, 1, 1774072251, 'PRODUCTION_d2a42e6966d10ad814ed1aa854a9a100f4c848b0be87479c98ea1b3bd196b8f5', 642984, 'BEARER', '7775985', 0, 'user.png', NULL, NULL, NULL, NULL, 'l', '', 0, '', '', '', '', 0, '', '1774072251', 'PRODUCTION_d2a42e6966d10ad814ed1aa854a9a100f4c848b0be87479c98ea1b3bd196b8f5'),
(241, 2, 3, 'k', '', '', 'k@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-15 22:47:03', '666b9c1c8cf7d272559aeb5fea812971', 'Y', NULL, 1, NULL, 1, 1616975724, 'PRODUCTION_af094bf30817878fc803a94774e1d3bcba5ae32412c3e7c4dc2e2abcf2017fde', 36785, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'k', '', 0, '', '', '', '', 0, '', '1616975724', 'PRODUCTION_af094bf30817878fc803a94774e1d3bcba5ae32412c3e7c4dc2e2abcf2017fde'),
(242, 2, 3, 'lk', '', '', 'lalalalalalal1qq@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-15 22:49:46', 'e70b8eeaf99fa3bf33aeca6626ee156d', 'Y', NULL, 1, NULL, 1, 310179677, 'PRODUCTION_cb3bf4f485e4ec7afb7ae23854f9e23f823ee0636df39f55b488f4075ad409f5', 168396243, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'lk', '', 0, '', '', '', '', 0, '', '310179677', 'PRODUCTION_cb3bf4f485e4ec7afb7ae23854f9e23f823ee0636df39f55b488f4075ad409f5'),
(243, 2, 3, 'am', '', '', 'amlal@gmail.clm', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-15 22:59:55', 'bd92ed5b54ed1f532fbbd446397f44bb', 'Y', NULL, 1, NULL, 1, 692321242, 'PRODUCTION_800be86209012500889efe6c0ad45e8587682a5d28473d0af18a474705c40e2b', 184956417, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'am', '', 0, '', '', '', '', 0, '', '692321242', 'PRODUCTION_800be86209012500889efe6c0ad45e8587682a5d28473d0af18a474705c40e2b'),
(244, 2, 3, 'lll', '', '', 'lalaldjdjd@kkkkk.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Paris, France', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-15 23:02:07', '9a1ee8e8f435d51bd87e5935401248d5', 'Y', NULL, 1, NULL, 1, 1850294393, 'PRODUCTION_2d2387f3767363e1e9670bacc8c63cfc0ef17bd81595e5883fb91d2afc43a1b9', 108839742, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'lll', '', 0, '', '', '', '', 0, '', '1850294393', 'PRODUCTION_2d2387f3767363e1e9670bacc8c63cfc0ef17bd81595e5883fb91d2afc43a1b9'),
(245, 2, 3, 'lp', '', '', 'lp@h.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Paris, France', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-15 23:05:06', '2ffa48571d19b7c08387d03d02b7309d', 'Y', NULL, 1, NULL, 1, 1576831400, 'PRODUCTION_a605182b62647202605edb5e2e054ddab4a7cb4fd438fe377ed89eccd851cb70', 82517200, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'lp', '', 0, '', '', '', '', 0, '', '1576831400', 'PRODUCTION_a605182b62647202605edb5e2e054ddab4a7cb4fd438fe377ed89eccd851cb70'),
(246, 2, 2, 'a', '', '', 's@as.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', 'Little Falls, NJ, United States', '', NULL, NULL, 's', '', '2016-12-16 11:27:19', '85465d7018320034391dca957a9f2cf7', 'Y', NULL, 1, NULL, 1, 1028589574, 'PRODUCTION_c69feb4d056cf25afce31d81b5d3683093007f7ce9b45a7d1b2feffb8850b56e', 74252438, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'a', '', 0, '', '', '', '', 0, '', '1028589574', 'PRODUCTION_c69feb4d056cf25afce31d81b5d3683093007f7ce9b45a7d1b2feffb8850b56e'),
(247, 2, 4, 'a', '', '', 'aaa@aaa.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '133 Bloomfield Avenue, Verona, NJ, United States', '', NULL, NULL, 'Amsterdam Avenue, New York, NY, United States', '', '2016-12-16 11:30:54', '38a7038dc51851b8d8082e7f36213223', 'Y', NULL, 1, NULL, 1, 1253996650, 'PRODUCTION_796c4612bc7e6abb135ce2f59184ec0bc61eddf0fca507e15c2c28feaf2d784d', 9245484, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'a', '', 0, '', '', '', '', 0, '', '1253996650', 'PRODUCTION_796c4612bc7e6abb135ce2f59184ec0bc61eddf0fca507e15c2c28feaf2d784d'),
(248, 2, 4, 'a', '', '', 'aaa@alaa.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', 'KK Dental Edison NJ, Plainfield Avenue, Edison, NJ, United States', '', NULL, NULL, 'Kenilworth, NJ, United States', '', '2016-12-16 11:31:42', 'fa58768e2b6415ce5fe51d930dc98867', 'Y', NULL, 1, NULL, 1, 1229412243, 'PRODUCTION_19f2a91a64d23c2ddc2dede8f2d02e2eb40300259a5202414178b3d92d438027', 258739850, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'a', '', 0, '', '', '', '', 0, '', '1229412243', 'PRODUCTION_19f2a91a64d23c2ddc2dede8f2d02e2eb40300259a5202414178b3d92d438027'),
(249, 2, 3, 'lll', '', '', 'sldlksdm@gaml.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1333', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-16 16:59:41', '8b2c7e4192aaff0b2ac41468068b681d', 'Y', NULL, 1, NULL, 1, 1766660052, 'PRODUCTION_a7e3760eac85a6c5ae09671419d711a101e9ecef970bda8fb0ba69df3ac88529', 170833255, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'lll', '', 0, '', '', '', '', 0, '', '1766660052', 'PRODUCTION_a7e3760eac85a6c5ae09671419d711a101e9ecef970bda8fb0ba69df3ac88529'),
(250, 2, 3, 'efw', '', '', 'lsmslsl@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pinheiros, São Paulo - State of São Paulo, Brazil', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-16 17:02:50', '393dd050ace3bcbaabb28caf6ac5dc35', 'Y', NULL, 1, NULL, 1, 1554933538, 'PRODUCTION_a3eef657fb0b474fd9f98649411bbf4a9e556c2b4e45ffa366623a27152755d3', 33982328, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'efw', '', 0, '', '', '', '', 0, '', '1554933538', 'PRODUCTION_a3eef657fb0b474fd9f98649411bbf4a9e556c2b4e45ffa366623a27152755d3'),
(251, 2, 3, 'sldk', '', '', 'kdkdkdk@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-16 17:56:41', 'b25e5e61d32f082c6fb653ab65199603', 'Y', NULL, 1, NULL, 1, 1690158624, 'PRODUCTION_d0902279c3d317600b62c7ebb541417e62bde3822cbc3acef79132ce1dd409bc', 67486033, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'sldk', '', 0, '', '', '', '', 0, '', '1690158624', 'PRODUCTION_d0902279c3d317600b62c7ebb541417e62bde3822cbc3acef79132ce1dd409bc'),
(252, 1, 1, '', 'lalal', 'lala', 'lsala@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-16 17:58:38', '93a0aae53dbc3e32709ecacdad9e99e3', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(253, 1, 1, '', 'sss', 'asa', 'ksksklq@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-20 12:31:54', '7a164b0b2a85d7f9fae98570fe1703a1', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(254, 1, 1, '', 's', 's', 'ssss@aaaa.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'SD, United States', '', '2016-12-20 19:36:14', 'ee64222217614ac116c16fd8cc49abae', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(255, 2, 3, 'aaaa', '', '', 'aaaaaa@asdsd.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '55555', '595 New Jersey 10, West Orange, NJ, United States', '', NULL, NULL, 'SDF Management Group, West 35th Street, New York, NY, United States', '', '2016-12-20 19:42:31', '63adf4873a6b5b48ea4c01f81b9d220a', 'Y', NULL, 1, NULL, 1, 402539576, 'PRODUCTION_5dd8ada1f54561e7cc94950fadc6ca5a72ba0b31c191a89e8cdd3723736ac764', 89471793, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'aaaa', '', 0, '', '', '', '', 0, '', '402539576', 'PRODUCTION_5dd8ada1f54561e7cc94950fadc6ca5a72ba0b31c191a89e8cdd3723736ac764'),
(256, 2, 3, 'Zaka USA', '', '', 'zaka123@gmail.com', '08001236d4d283db1f7eb51105514cb3fc0b3142', '46-0567613', 'New York, NY 10004, United States', '', NULL, NULL, 'Passaic, NJ, United States', '', '2016-12-21 05:27:53', '4ac70d501256377f137f965c7e194376', 'Y', NULL, 0, NULL, 1, 1132207882, 'PRODUCTION_d60b7a14b3b394e6d928d03669f2a89d2ea1d499787904b6832993a5cb065691', 171657008, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'zaka-usa', '', 0, '', '', '', '', 0, '', '1132207882', 'PRODUCTION_d60b7a14b3b394e6d928d03669f2a89d2ea1d499787904b6832993a5cb065691'),
(257, 2, 3, 'aaaaa', '', '', 'aaaasas@asdsd.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '33333', 'Queens, NY, United States', '', NULL, NULL, 'DD Thai Cuisine, West Passaic Street, Rochelle Park, NJ, United States', '', '2016-12-21 07:10:05', '20cbc2f03c4a1a95a7243705eaa39db9', 'Y', NULL, 1, NULL, 1, 1691836082, 'PRODUCTION_2fc026e5556d57012844031e67677635cc931b9362d9932f00472c96cb90746c', 233368243, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'aaaaa', '', 0, '', '', '', '', 0, '', '1691836082', 'PRODUCTION_2fc026e5556d57012844031e67677635cc931b9362d9932f00472c96cb90746c'),
(258, 1, 1, '', 'lll', 'lll', 'llll@alx.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'Lyndhurst, NJ, United States', '', '2016-12-21 07:16:11', '07670f68d956e4e15f1d409b55e4bbae', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(259, 1, 1, '', 'Rafi', 'vggygy', 'gbhhyuhuh@gmail.vom', '63a0b8fa3b62ec89537f0172868c441a2e2cdabd', '', '', '', NULL, NULL, 'West Orange, NJ, United States', '', '2016-12-21 07:19:25', '6bbdb42874bba9fc2829e5e514cb8539', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(260, 2, 2, 'assds', '', '', 'sgksjgkfjg@gmail.com', '11b3711c1bdd66107f9414fa5718cd027e4e5d14', '', 'Livingston, NJ, United States', '', NULL, NULL, 'Staten Island, NY, United States', '', '2016-12-21 10:16:18', '6aea124f2017370e6fe60e7541df33c0', 'Y', NULL, 1, NULL, 1, 1819264997, 'PRODUCTION_ae044675d5ebe24a73d6286d3c241461ccb9dee4144075caa06c1a29c4e3b1f7', 110590859, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'assds', '', 0, '', '', '', '', 0, '', '1819264997', 'PRODUCTION_ae044675d5ebe24a73d6286d3c241461ccb9dee4144075caa06c1a29c4e3b1f7'),
(261, 2, 3, 'test', '', '', 'teeeeeeets@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 11:08:19', '03429318bde48e72f5166fc65ab9c760', 'Y', NULL, 1, NULL, 1, 887346985, 'PRODUCTION_6d11cf6d9c59d7a240a2eabeb1552974d7659ca66e7acc15e18d1b9cfe831c91', 41449080, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'test', '', 0, '', '', '', '', 0, '', '887346985', 'PRODUCTION_6d11cf6d9c59d7a240a2eabeb1552974d7659ca66e7acc15e18d1b9cfe831c91'),
(262, 2, 3, 'pap', '', '', 'lkjha@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 11:32:18', '10de963e5cdde3dfcae6f80bc50193de', 'Y', NULL, 1, NULL, 1, 969340649, 'PRODUCTION_6539c247011d50ac355b50818397234161891199215f6943e9dde8cd66cc62e7', 234393256, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'pap', '', 0, '', '', '', '', 0, '', '969340649', 'PRODUCTION_6539c247011d50ac355b50818397234161891199215f6943e9dde8cd66cc62e7'),
(269, 2, 3, 'rajsss', '', '', 'test1as@fgmfail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pennsylvania, United States', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 12:03:17', 'bbae21a35ad6716d0d71b2599fd008b0', 'Y', NULL, 1, NULL, 1, 651779945, 'PRODUCTION_3aee3c3aab3eec6d7c51f0126f3263a24946ea83475f4487d5066758cd2cd953', 232145492, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'rajsss', '', 0, '', '', '', '', 0, '', '651779945', 'PRODUCTION_3aee3c3aab3eec6d7c51f0126f3263a24946ea83475f4487d5066758cd2cd953'),
(270, 2, 1, 'rajjj', '', '', 'sjkkhjdsk@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 12:22:52', '045b8b4b90bef21d5b800f18eee2dda3', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'rajjj', '', 0, '', '', '', '', 0, '', '', ''),
(271, 2, 2, 'asdk', '', '', 'lkdfsflk@esflk.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', 'lll Auto Repairs, President Street, Brooklyn, NY, United States', '', NULL, NULL, 'Dfds Transport US Inc, Walnut Avenue, Clark, NJ, United States', '', '2016-12-21 12:26:10', '6dcbfa4aed04b047f38dc800c7e878a7', 'Y', NULL, 1, NULL, 1, 1318398232, 'PRODUCTION_f77cc9719f47317e796f1211aec796f3bd717d18121894ac78c674d55b81fb01', 38770988, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'asdk', '', 0, '', '', '', '', 0, '', '1318398232', 'PRODUCTION_f77cc9719f47317e796f1211aec796f3bd717d18121894ac78c674d55b81fb01'),
(272, 2, 3, 'sdfhj', '', '', 'hsdhj@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pennsylvania, United States', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 12:26:21', 'a603e8c21c163cfce457e9c8cc193f38', 'Y', NULL, 1, NULL, 1, 881741988, 'PRODUCTION_ca51a1bd73ebe07a95f1876bb7667dd892abaadf9dc90b7d1f45497d0e33746e', 114702383, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'sdfhj', '', 0, '', '', '', '', 0, '', '881741988', 'PRODUCTION_ca51a1bd73ebe07a95f1876bb7667dd892abaadf9dc90b7d1f45497d0e33746e'),
(273, 2, 3, 'lkdfdlf', '', '', 'lkdfdsfl@dfslk.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '0000', 'LLL International Inc, Queens, NY, United States', '', NULL, NULL, 'Denver Fire Department Federal Credit Union, Federal Boulevard, Denver, CO, United States', '', '2016-12-21 12:26:54', '239fc33178ac731350fe8e0d6c2ac18e', 'Y', NULL, 1, NULL, 1, 397068906, 'PRODUCTION_ee1b4cd4477aff46efcadea4fd647b0ccaad833b7b5723b03b9f595a9a107312', 175037636, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'lkdfdlf', '', 0, '', '', '', '', 0, '', '397068906', 'PRODUCTION_ee1b4cd4477aff46efcadea4fd647b0ccaad833b7b5723b03b9f595a9a107312'),
(274, 1, 1, '', 'raaaj', 'jadhav', 'jskbsdjk@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'pune', '', '2016-12-21 12:27:59', '70ff2a247c8ac2a789abb90f0660fca5', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(275, 1, 1, '', 'lklkd', 'lkdflskf', 'dfgfgl@dfslfk.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'SDF Management Group, West 35th Street, New York, NY, United States', '', '2016-12-21 12:28:07', '2b9c77db81afc2cef22d133aa84b1fc0', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(276, 2, 3, 'sdsf', '', '', 'sdsdfsd@h.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Paris, France', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 12:43:32', '1b5f5e4138d1faa02a658c936a2ae4aa', 'Y', NULL, 1, NULL, 1, 661402144, 'PRODUCTION_c73bf2ba585c38cadb99278fe8b29bc1c0c018bd80cd089b47121db9f67680ad', 113266937, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'sdsf', '', 0, '', '', '', '', 0, '', '661402144', 'PRODUCTION_c73bf2ba585c38cadb99278fe8b29bc1c0c018bd80cd089b47121db9f67680ad'),
(277, 1, 1, '', 'asd', 'asd', 'kdkdddss@gail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Rio de Janeiro - State of Rio de Janeiro, Brazil', '', '2016-12-21 12:45:37', '87880b917b69d6b6952b9c66f8fe68bd', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(278, 2, 3, 'kjdkfdf kj', '', '', 'ss@as.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '111', 'City of Orange, NJ, United States', '', NULL, NULL, 'Staten Island, NY, United States', '', '2016-12-21 12:46:12', 'af45d2b55bfc1adab5b9ede56f18560a', 'Y', NULL, 1, NULL, 1, 348815321, 'PRODUCTION_ad4c7a0ccedfb627703b2daacf903c354f44e8cd38093b80ae4b7e31a8289ede', 101250607, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'kjdkfdf-kj', '', 0, '', '', '', '', 0, '', '348815321', 'PRODUCTION_ad4c7a0ccedfb627703b2daacf903c354f44e8cd38093b80ae4b7e31a8289ede'),
(279, 2, 3, 'sd', '', '', 'amsdflal@gmafsil.clm', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pennsylvania, United States', '', NULL, NULL, 'Arizona, United States', '', '2016-12-21 12:46:38', '45e0dc13872c344cc6ed5a750d69a109', 'Y', NULL, 1, NULL, 1, 1449252677, 'PRODUCTION_e44bc78f2e998024596e48dd11d9e97cdb1204b12208c5c5288b02d485b88c08', 137152053, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'sd', '', 0, '', '', '', '', 0, '', '1449252677', 'PRODUCTION_e44bc78f2e998024596e48dd11d9e97cdb1204b12208c5c5288b02d485b88c08'),
(280, 1, 1, '', 'klkj', 'kjkj', 'sdf@sdfk.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'SD, United States', '', '2016-12-21 12:47:47', '090af7077deb9482dfe757a7282194a5', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(281, 2, 3, 'sdksjd', '', '', 'kjdsmjsj@gk.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Pune, Maharashtra, India', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 13:13:32', '2dcc999b00a083331d4f53148e6a1845', 'Y', NULL, 1, NULL, 1, 1593044883, 'PRODUCTION_d08aef5401a1303159076d18576fac41069af4beb4bc166bed472dd19fc2204c', 152418722, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'sdksjd', '', 0, '', '', '', '', 0, '', '1593044883', 'PRODUCTION_d08aef5401a1303159076d18576fac41069af4beb4bc166bed472dd19fc2204c'),
(282, 2, 3, 'lllerter', '', '', 'erter@hm.com', '8cb2237d0679ca88db6464eac60da96345513964', '1234', 'Québec, Canada', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 13:33:42', '08259fc039507caa76243bc88aec92b4', 'Y', NULL, 1, NULL, 1, 444511810, 'PRODUCTION_b1543eda8012086d2af1ec065405c621c8b4783d1538bbf5d8505e80dac1c96a', 137485289, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'lllerter', '', 0, '', '', '', '', 0, '', '444511810', 'PRODUCTION_b1543eda8012086d2af1ec065405c621c8b4783d1538bbf5d8505e80dac1c96a'),
(283, 1, 1, '', 'lld', 'ldld', 'jfjf@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'SD, United States', '', '2016-12-21 14:26:10', 'dff06e0c1f409ca8025dbeda10f6f8a9', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(284, 2, 3, 'Kupat Hair Israel', '', '', 'neumann.psych@gmail.com', '41ad5c4d0ae6d37567a457062e3b966f99a18a45', '1987741350', 'Bnei Brak 51552, Tel Aviv-Yafo, Israel', '', NULL, NULL, 'Bnei Brak 51552, Tel Aviv-Yafo, Israel', '', '2016-12-21 17:00:09', '14a02f94a436861a5d7be918761c0f84', 'Y', NULL, 1, NULL, 1, 2060741660, 'PRODUCTION_5274e3eb3e5283821497287a71092f815e984cf5299fc008df51d9ea6527ff48', 112642626, 'BEARER', '7776000', 0, 'user.png', NULL, NULL, NULL, NULL, 'kupat-hair-israel', '', 0, '', '', '', '', 0, '', '2060741660', 'PRODUCTION_5274e3eb3e5283821497287a71092f815e984cf5299fc008df51d9ea6527ff48'),
(289, 2, 3, 'hhhhkkk', '', '', 'rassdflk@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '1234', 'Paris, France', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 18:04:38', '3dce039b90e25c8d4a34051486884b0e', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'hhhhkkk', '', 0, '', '', '', '', 0, '', '', ''),
(296, 2, 3, 'sd', '', '', 'raj1@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '3244', 'San Francisco, CA, United States', '', NULL, NULL, 'pune', '', '2016-12-21 20:20:13', 'fe85841448a0ed72e5924607c0b61811', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'sd', '', 0, '', '', '', '', 0, '', '', ''),
(300, 1, 1, '', 'sdjsdk', 'skdjfk', 'hdhdhd@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'Pune, Maharashtra, India', '', '2016-12-21 21:13:33', '0e9c5ed0d36eb78dbb9847313ecc00b1', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(301, 1, 1, '', 'ksdksdk', 'ksdksdk', 'lmmnccbc@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'pune', '', '2016-12-21 21:14:22', '8734b3cf0b196f16de5e8486222e011e', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(302, 2, 3, 'CHOFETZ CHAIM HERITAGE FOUNDaHERITAGE', '', '', 'neumann..psych@gmail.com', '6774fd1d5227afd8368a3f7a1f02361ee729bc48', '11-2642599', 'Suffern, NY, United States', '', NULL, NULL, 'Suffern, New York 10901, United States', '', '2016-12-21 22:48:39', '2aa5c39eb6c5c4c1650358ed0f29df6c', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'chofetz-chaim-heritage-foundaheritage', '', 0, '', '', '', '', 0, '', '', ''),
(303, 2, 3, 'Meir Panim', '', '', 'neu.mann.psych@gmail.com', 'b45542a14753f4ff05d388df5b4d4d3657be5f69', '20-1582478', 'Brooklyn, NY, United States', '', NULL, NULL, 'Brooklyn, NY, United States', '', '2016-12-21 23:11:39', '46b2355c6425cabc0aa51daddaf7e920', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'meir-panim', '', 0, '', '', '', '', 0, '', '911585631', 'PRODUCTION_a1cd28ede9ba59f0618be5f55d3ad3f96f5b34b003a8c6f7e770874ef74a6b9d'),
(304, 2, 3, 'Ichlu Re''im Soup Kitchen', '', '', 'n.eumannpsych@gmail.com', 'f36d257ac038c67ae1a8f2deb1e08efa6f626e9b', '02-5022313', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-21 23:46:16', '48e52ccc37b5d8f2f4faaaa627b3c949', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'ichlu-re''im-soup-kitchen', '', 0, '', '', '', '', 0, '', '1585245825', 'PRODUCTION_3511e73397a34711d69b7142b8e4e954d36ec7bf50339fcbbfc3aea148bebb46');
INSERT INTO `users` (`id`, `profile_type`, `org_account_type`, `org_name`, `first_name`, `last_name`, `email`, `password`, `ein_no`, `street_name`, `street_no`, `registrar_city`, `registrar_country`, `city`, `country`, `create_date`, `user_token`, `is_token_valid`, `token_expires_at`, `status`, `last_login`, `newsletter`, `we_pay_account_id`, `wepay_access_token`, `wepay_user_id`, `wepay_token_type`, `wepay_token_expires_in`, `wepay_confirm`, `image`, `org_tag`, `about_me`, `website_url`, `location`, `org_slug`, `oauth_provider`, `oauth_uid`, `gender`, `locale`, `profile_url`, `picture_url`, `isProfile`, `user_image`, `new_wepay_account_id`, `new_wepay_access_token`) VALUES
(305, 2, 3, 'Hineni Soup Kitchen', '', '', 'ne.umann.psych@gmail.com', '026e4030f6166c33da9a130e12d60a3f5384bc17', '9722 624 3407 ', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-21 23:57:41', 'f6f2d522d8b0b5eecba1e5411109e1b0', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'hineni-soup-kitchen', '', 0, '', '', '', '', 0, '', '748205450', 'PRODUCTION_69858511a6529b6415be48766f9a0938c769931a2757ac2ff09514cadcf127fb'),
(306, 2, 3, 'yadeliezer adopt a wedding', '', '', 'neu.m.annpsych@gmail.com', 'a0ac4c577e89f78034168a268d00862921dd9d55', '11-3459952', 'Staten Island, NY, United States', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-22 00:32:35', 'efb00735c40c77796892a2b7687c6ea0', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'yadeliezer-adopt-a-wedding', '', 0, '', '', '', '', 0, '', '1033974391', 'PRODUCTION_f0596a9bb719f9d17cf6ae77727b9dc0114e4bd602c9577823f0ef8ed0ea4c95'),
(307, 2, 3, 'One Family Fund', '', '', 'n.e.umannpsych@gmail.com', '8be6c869fc0a246f9bc361027b862ce7ccd03253', '11-3585917', 'Philadelphia, PA, United States', '', NULL, NULL, 'Philadelphia, PA, United States', '', '2016-12-22 00:59:17', 'a95db6525f8a4a78d26fde76c357a20f', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'one-family-fund', '', 0, '', '', '', '', 0, '', '1308914497', 'PRODUCTION_57f962e3e5f7d311e8a049c7a422bfa7528e2c00a7f3b03f9e037da5b1f7c123'),
(308, 2, 1, 'ZAKA', '', '', 'neumannpsy.c.h@gmail.com', 'de9749f9cc8a9f9ea81ad8e2c65138730b432676', '', '', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-22 15:04:16', '21ea7b47e3a9d0fc0d02f1c8d8d433a2', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'zaka', '', 0, '', '', '', '', 0, '', '', ''),
(309, 2, 3, 'Shaar Hashamayim Yeshiva', '', '', 'neum.ann.psyc.h@gmail.com', '57409355757f10af40f012719a5e26bd2b6bec38', '0000000000', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-22 16:07:32', '727ebabebbd2640d483e4a980d4ba9fa', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'shaar-hashamayim-yeshiva', '', 0, '', '', '', '', 0, '', '2145753926', 'PRODUCTION_69bb088d7e04c8a78c415862826a170966f0d56d26e503166adba76e961c3ce8'),
(310, 2, 3, ' Yeshiva Chevron', '', '', 'neuma.nn.psyc.h@gmail.com', '197a381c290f5cc13973c5e964d2e7a7e9580072', '0000000001', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-22 16:18:57', '7edd19f8467fe1529db752b89c0d1f3c', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, '-yeshiva-chevron', '', 0, '', '', '', '', 0, '', '621655719', 'PRODUCTION_dc179e4b420d0055c3a2b2cb23d8752aa1186c2da63ae4fea2e854a401eef31e'),
(311, 2, 3, 'Yeshiva Beit Matisyahu', '', '', 'neuman.n.psyc.h@gmail.com', '2b07bc7f672e087521209aa9a9df08880de7cde5', '00000002', 'Bnei Brak, Israel', '', NULL, NULL, 'Bnei Brak, Israel', '', '2016-12-22 17:46:12', '2faee6c04ef50ca6dbb883ae5092c106', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'yeshiva-beit-matisyahu', '', 0, '', '', '', '', 0, '', '1752050149', 'PRODUCTION_643c3271caf10c10a9718ed18418316bb30a9795d5521692c53faacc02940de5'),
(312, 1, 1, '', 'aa', 'aaa', 'aaaaa@aaaaa.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'AAAA Secure Driving School, Victory Boulevard, Staten Island, NY, United States', '', '2016-12-22 20:20:27', '2145f0fd052956a60ae80c101789bb29', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(313, 2, 3, 'Tomchei Israel Kallah Fund', '', '', 'neum.a.nnpsych@gmail.com', '54787047a7f8222e4861c7383f72de9dccf199b4', '0000000003', 'Brooklyn, NY, United States', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-23 02:02:35', '9eba01fe4c1805305b1e46211a9984e9', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-israel-kallah-fund', '', 0, '', '', '', '', 0, '', '1372647226', 'PRODUCTION_3e0e87dea7b143f6176df764b2faae6bbac80ebfb2e1f6c2dd4c64d2b4df9e7a'),
(314, 2, 3, 'Tomchei Shabbos of Rockland County', '', '', 'neu.mann.psyc.h@gmail.com', '03608d141596eb3b16304c60265cf339886c1b28', '13-3694712', 'Monsey, NY, United States', '', NULL, NULL, 'Monsey, NY, United States', '', '2016-12-23 02:49:06', '1e2f613b9a28cb14bef2ba8b3ad388bd', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-rockland-county', '', 0, '', '', '', '', 0, '', '1799995983', 'PRODUCTION_2c130758569fd67a995ae2326e0a146878638f62ba392a7f669b46d3049ececc'),
(315, 2, 3, 'Tomchei Shabbos of Bergen County', '', '', 'neum.ann.psych@gmail.com', '849990f4fe8321c1c997a771f88c63e3e22401f3', '22-3074230', 'BERGENFIELD, NJ 07621', '', NULL, NULL, 'admin@mefarnes.com', '', '2016-12-23 19:36:35', '91a127b151368e459947810bf49eb3e4', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-bergen-county', '', 0, '', '', '', '', 0, '', '1050173194', 'PRODUCTION_a9c6ffbd5c12196bced44d5e7b5107f01219d2424c72a628956e53576afdd514'),
(316, 2, 3, 'Tomchei Shabbos of Far Rockaway', '', '', 'neuma.nn.psych@gmail.com', '849990f4fe8321c1c997a771f88c63e3e22401f3', '0000000004', 'Far Rockaway, NY 11691', '', NULL, NULL, 'Far Rockaway, NY 11691', '', '2016-12-23 19:46:28', '32e528e89f212d538f4c8179704d4a89', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-far-rockaway', '', 0, '', '', '', '', 0, '', '88431247', 'PRODUCTION_76a4acfffb83f849fac9ffba57c9a0827bf717a7c968116f50a77e8c307e45ea'),
(317, 2, 3, 'Tomchei Shabbos of Lakewood', '', '', 'neuman.n.psych@gmail.com', '6b67afe0a68d4737334cfe0ef619ef6bb5b96878', '04-3731340', 'LAKEWOOD, NJ 08701', '', NULL, NULL, ' LAKEWOOD, NJ 08701', '', '2016-12-23 19:56:51', '8699ac3c1d3f8de0cb2e485f3b665151', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-lakewood', '', 0, '', '', '', '', 0, '', '', ''),
(318, 2, 3, 'Tomchei Shabbos of Dallas', '', '', 'neumann.p.sych@gmail.com', '86ae17bb19533817f6c23ac9fa7e460abb05286d', '27-2971156 ', 'DALLAS, TX 75251', '', NULL, NULL, 'DALLAS, TX 75251', '', '2016-12-23 20:09:40', '02ab3dc46f791909391879fb3649394f', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-dallas', '', 0, '', '', '', '', 0, '', '1565198919', 'PRODUCTION_e415c6e005df82f4fb935a017c35c96e2111208f69c7af1e300c527741955d7b'),
(320, 1, 1, '', 'raj', 'jadhav', 'rajendramjadhav@gmail.com', '0a6807c0856b137fb44ce239587e4f34e011b005', '', '', '', NULL, NULL, 'pun e', '', '2016-12-24 20:11:04', 'a8a112ebc5d26c496cd3681585de2b27', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(321, 2, 3, 'Tomchei Shabbos of Greater Washington (Maryland and Greater Washington Area)', '', '', 'neumann.ps.ych@gmail.com', '0a0ef968ffc4d24807862ad9d4cc32f8cab132c2', '22-3949731', 'Silver Spring, MD 20902', '', NULL, NULL, 'Silver Spring, MD 20902', '', '2016-12-25 02:43:43', 'd49f804acb26854643723b816238f4f6', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-greater-washington-(maryland-and-greater-washington-area)', '', 0, '', '', '', '', 0, '', '1988961479', 'PRODUCTION_c958dc98b2d349e6e2aacec2c1741a9a71a50577b3ed6a87be65822de73ba9ba'),
(322, 2, 3, 'Tomchei Shabbos of Queens', '', '', 'neumann.psy.ch@gmail.com', '57409355757f10af40f012719a5e26bd2b6bec38', '0000000006', 'Kew Gardens, Queens, NY, United States', '', NULL, NULL, 'Kew Gardens, NY 11415, USA', '', '2016-12-25 02:54:58', 'b122fce8aedd027a902b146077906dfd', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-queens', '', 0, '', '', '', '', 0, '', '355854936', 'PRODUCTION_57cef6468eea03b68b2328b4f57db27adbca10f7a79fef7568eebef06d35927c'),
(323, 2, 3, 'Tomchei Shabbos of Los Angeles', '', '', 'neumann.psyc.h@gmail.com', '9b0288f43a1e2280bbb34b6d9df0494c07a51b6a', '75-3002144 ', 'Los Angeles, CA, United States', '', NULL, NULL, 'Los Angeles, CA, United States', '', '2016-12-25 03:19:13', 'f95e289a3b1b6f46b69b31f7a3cec27e', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-los-angeles', '', 0, '', '', '', '', 0, '', '1490235288', 'PRODUCTION_778d2a73ac1f9a464d13c8f525669bfd0c4f24329810000586830210e8c07592'),
(324, 2, 3, 'Tomchei Shabbos of Toronto', '', '', 'n.eumannpsy.ch@gmail.com', '63d2cce5285185ad64ee4e778d5a594316b62ba0', '859298440RR0001', 'North York, ON M6B 3R7, Canada', '', NULL, NULL, ' North York, ON M6B 3R7, Canada', '', '2016-12-25 04:18:55', '2b55fac0c8c40e7cbfa7b6caf1f4a69d', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-toronto', '', 0, '', '', '', '', 0, '', '1236593025', 'PRODUCTION_afefb4c018f80c817f9b8434b7aadfe24815aff3802be4aed875a9030c61518a'),
(325, 2, 3, 'Tomchei Shabbos of Phoenix (Arizona)', '', '', 'ne.umannpsy.ch@gmail.com', '81934ac89b779ecb8efdd13fa7d6b3aae15d632e', '27-3099147 ', 'Phoenix, AZ, United States', '', NULL, NULL, 'Phoenix, AZ', '', '2016-12-25 15:45:52', '8c23dd376a110b5df685c09efbefa7a2', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-phoenix-(arizona)', '', 0, '', '', '', '', 0, '', '1435277050', 'PRODUCTION_2510d6bdef191acb338cd364e7cc1712020276679d1cd5762369f5657e9d53b3'),
(326, 2, 3, 'Tomchei Shabbos of Miami', '', '', 'neu.mannpsy.ch@gmail.com', '137b70b2cf1ad32ff1a119d50b7916ecc04c00ba', '0000000007', 'Hallandale Florida, Northwest 1st Avenue, Hallandale Beach, FL 33009, United States', '', NULL, NULL, 'Miami, FL, United States', '', '2016-12-25 15:52:51', '14b43344dbba1ef66c80209f954e1859', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-miami', '', 0, '', '', '', '', 0, '', '134274752', 'PRODUCTION_1950b917a0c022e4f863b777b77ca50a5e30968015d0c769cb279d74061a74d5'),
(327, 2, 3, 'Tomchei Shabbos of Passaic', '', '', 'neum.annpsy.ch@gmail.com', '21e0643377b8e40e92a0bbdcfcf0650669372668', '22-3598564', 'Passaic, NJ, United States', '', NULL, NULL, 'Passaic, NJ, United States', '', '2016-12-25 17:20:54', '40b962e7b2596cf1cd311919f3775acf', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-passaic', '', 0, '', '', '', '', 0, '', '1191358991', 'PRODUCTION_4fe78cd888dd443ee82e4dbf3a69474aebcd3c8bb5c28e6e45cf3bcfb0b9a84c'),
(328, 2, 3, 'Yad Eliezer', '', '', 'neumannpsyc.h@gmail.com', 'cc99fcc5d82831bf9477e8b6ceb98eab4f72f3f9', '00000000010', 'Jerusalem, Israel', '', NULL, NULL, 'Jerusalem, Israel', '', '2016-12-25 22:05:12', '5e0ef2a098a11d4cfd45312f8cc9ee02', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'yad-eliezer', '', 0, '', '', '', '', 0, '', '1065965354', 'PRODUCTION_80041f42a87763a73b98c0c032e2e5ce3bec7e4f8f357eb967fe7a0bb503789f'),
(329, 1, 1, '', 'aaaa', 'aaaa', 'aaaaaaa@saaaaa.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '', '', '', NULL, NULL, 'Star Tavern, High Street, City of Orange, NJ, United States', '', '2016-12-26 10:35:55', '7fd5e0b23968d27a1d11f12a83fb3871', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, NULL, '', 0, '', '', '', '', 0, '', '', ''),
(330, 2, 3, 'Tomchei Shabbos of Staten Island', '', '', 'neuman.npsy.ch@gmail.com', '406e22b3cd0d8566d2f5d0183b5b0bffab46b74f', '22-3901876 ', 'Staten Island, NY, United States', '', NULL, NULL, 'Staten Island, NY 10314-5026', '', '2016-12-26 14:51:27', '744339d7475c4381dc6a0f200578a338', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'tomchei-shabbos-of-staten-island', '', 0, '', '', '', '', 0, '', '702448013', 'PRODUCTION_9ec50737482f9f503fe8b327442e121671fff20ea4b6bc1b852c44e6bf9f5ba1'),
(331, 2, 3, 'aaaaaa', '', '', 'aassmmmmm@gmail.com', '5abe22691c97ba4d6311e144377fa979751fa4b3', '111111', 'Denville, NJ, United States', '', NULL, NULL, 'Avenue of the Americas, New York, NY, United States', '', '2016-12-26 19:00:12', '6d374d850f8a2f066afe538b96328a3b', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'aaaaaa', '', 0, '', '', '', '', 0, '', '1556924861', 'PRODUCTION_8df61a69edb25256a82ece391ccde025a1815150c1be2a66444c5d00d6451923'),
(332, 2, 3, 'ZAKA Search and Rescue', '', '', 'david@zaka.org.il', 'df16c2a066edbede96a9c1b27c9093e6da6e1fde', '46-0567613', '11 Broadway, # 518 NY, United States', '', NULL, NULL, 'Jerusalem, ?????', '', '2017-01-02 16:51:42', 'c9bc40166e46984cc42db9fdb62e3c8b', 'Y', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 'user.png', NULL, NULL, NULL, NULL, 'zaka-search-and-rescue', '', 0, '', '', '', '', 0, '', '753975132', 'PRODUCTION_b9e01b3caab91d95147127b780cc1245c02f72866b49325ba55cebc7d574633b');

-- --------------------------------------------------------

--
-- Table structure for table `user_email_contacts`
--

CREATE TABLE IF NOT EXISTS `user_email_contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contact_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=gmail,2=microsoft.3=yahoo,4=hotmail',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1086 ;

--
-- Dumping data for table `user_email_contacts`
--

INSERT INTO `user_email_contacts` (`id`, `user_id`, `email`, `create_date`, `contact_type`) VALUES
(1084, 177, 'enduetech.bikash@gmail.com', '2016-11-28 23:27:31', 1),
(1085, 177, 'bikashpal2008@gmail.com', '2016-11-28 23:27:31', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD CONSTRAINT `fk_campaigns_campaign_categories1` FOREIGN KEY (`campaign_categoriy_id`) REFERENCES `campaign_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_campaigns_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `campaign_comments`
--
ALTER TABLE `campaign_comments`
  ADD CONSTRAINT `campaign_comments_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campaign_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `campaign_donations`
--
ALTER TABLE `campaign_donations`
  ADD CONSTRAINT `fk_campaign_raised_funds_campaigns1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `campaign_reports`
--
ALTER TABLE `campaign_reports`
  ADD CONSTRAINT `campaign_reports_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campaign_reports_ibfk_2` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `campaign_shared_information`
--
ALTER TABLE `campaign_shared_information`
  ADD CONSTRAINT `fk_campaign_shared_informations_campaigns1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `campaign_updates_subscribers`
--
ALTER TABLE `campaign_updates_subscribers`
  ADD CONSTRAINT `campaign_updates_subscribers_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campaign_updates_subscribers_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `campaign_update_loves`
--
ALTER TABLE `campaign_update_loves`
  ADD CONSTRAINT `campaign_update_loves_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campaign_update_loves_ibfk_3` FOREIGN KEY (`campaign_update_id`) REFERENCES `campaings_updates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `campaings_updates`
--
ALTER TABLE `campaings_updates`
  ADD CONSTRAINT `campaings_updates_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campaings_updates_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comment_replies`
--
ALTER TABLE `comment_replies`
  ADD CONSTRAINT `comment_replies_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `campaign_comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_replies_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `organization_gallery`
--
ALTER TABLE `organization_gallery`
  ADD CONSTRAINT `organization_gallery_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_email_contacts`
--
ALTER TABLE `user_email_contacts`
  ADD CONSTRAINT `user_email_contacts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
