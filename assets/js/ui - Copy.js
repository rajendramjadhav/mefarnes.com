// JavaScript Document

$(document).ready(function() {
	// home page main slider
	if($(".main-slider").length) { $('.main-slider').flexslider({animation: "slide", directionNav: false, slideshowSpeed: 10000}); }
	
	// fancy selectbox
	if($(".fancy-selectbox")) { $(".fancy-selectbox").selectbox(); }
	
	// profile page tabs
	if($(".page.profile .tab-area").length) { initProfileTabs(); }
	
	// donate page tabs
	if($(".page.donate .tab-area").length) { initDonateTabs(); }
	
	// signup page sections
	if($(".page.signup").length) { initSignupSections(); }
	
	// create campaign days selector
	if($("#day-slider").length) {
		$( "#day-slider" ).slider({ range: true, min: 1, max: 60, values: [0, 30],
			slide: function( event, ui ) { if(ui.values[1] == "1") { var days = " Day"; } else { var days = " Days"; } $( ".days" ).html( ui.values[1] + days ); }
		});
		
		$( ".days" ).html( "30 Days" );
	}
	
	// signup page sections
	if($(".page.create-campaign .tab-area").length) { initCreateCampaignSections(); }
	
	// dashboard page tabs
	if($(".page.dashboard .tab-area").length) { initDashboardTabs(); }
	
	// enable tooltips
	$('[data-toggle="tooltip"]').tooltip();
	
	// init toggle button (on/off)
	if($(".toggle-btn").length) { initToggleBtn(); }
	
	// dashboard page inner tabs
	if($(".page.dashboard .inner-tabs").length) { initDashboardInnerTabs(); }
});

function initProfileTabs() {
	$(".page.profile .tab-area .tabs li a").click(function() {
		var tab = $($(this).data("toggle"));
		$(".page.profile .tab-area .tabs li").removeClass("active");
		$(this).parent().addClass("active");
		$(".page.profile .tab-area .tab-contents").hide();
		tab.fadeIn(500);
	});
}

function initDonateTabs() {
	$(".page.donate .continue-step-1").click(function() {
		$(".page.donate #tab1").hide();
		$(".page.donate #tab2").fadeIn(500);
		$(".page.donate .tabs li.one").addClass("done").removeClass("active");
		$(".page.donate .tabs li.two").addClass("active");
	});
	
	$(".page.donate .back-step-2").click(function() {
		$(".page.donate #tab1").fadeIn(500);
		$(".page.donate #tab2").hide();
		$(".page.donate .tabs li.one").addClass("active").removeClass("done");
		$(".page.donate .tabs li.two").removeClass("active");
	});
	
	$(".page.donate .continue-step-2").click(function() {
		$(".page.donate #tab2").hide();
		$(".page.donate #tab3").fadeIn(500);
		$(".page.donate .tabs li.two").addClass("done").removeClass("active");
		$(".page.donate .tabs li.three").addClass("active");
	});
	
	$(".page.donate .back-step-3").click(function() {
		$(".page.donate #tab2").fadeIn(500);
		$(".page.donate #tab3").hide();
		$(".page.donate .tabs li.two").addClass("active").removeClass("done");
		$(".page.donate .tabs li.three").removeClass("active");
	});
}

function initSignupSections() {
	$(".page.signup .individual-signup").click(function() {
		$(".page.signup .sectionMain").hide();
		$(".page.signup .section_individual_1").fadeIn(500);
	});
	
	$(".page.signup .organization-signup").click(function() {
		$(".page.signup .sectionMain").hide();
		$(".page.signup .section_organization_1").fadeIn(500);
	});
	
	$(".page.signup .individual-signup-fb, .page.signup .individual-signup-mail").click(function() {
		$(".page.signup .section_individual_1").hide();
		$(".page.signup .section_individual_2").fadeIn(500);
	});
	
	$(".page.signup .continue-step-1").click(function() {
		$(".page.signup .section_organization_1").hide();
		$(".page.signup .section_organization_2").fadeIn(500);
	});
	
	$(".page.signup .continue-step-2").click(function() {
		$(".page.signup .section_organization_2").hide();
		$(".page.signup .section_organization_3").fadeIn(500);
	});
}

function initCreateCampaignSections() {
	$(".page.create-campaign .continue-section-1").click(function() {
            
		$(".page.create-campaign .step-1").hide();
		$(".page.create-campaign .step-2").fadeIn(500);
		$(".page.create-campaign .tabs li.one").removeClass("active");
		$(".page.create-campaign .tabs li.two").addClass("active");
		$("html, body").delay(100).animate({ scrollTop: ($('.page.create-campaign .tabs').offset().top - 120) }, 500);
	});
	
	$(".page.create-campaign .continue-section-2").click(function() {
		$(".page.create-campaign .step-2").hide();
		$(".page.create-campaign .step-3").fadeIn(500);
		$(".page.create-campaign .tabs li.two").removeClass("active");
		$(".page.create-campaign .tabs li.three").addClass("active");
		$("html, body").delay(100).animate({ scrollTop: ($('.page.create-campaign .tabs').offset().top - 120) }, 500);
	});
}

function initDashboardTabs() {
	$(".page.dashboard .tab-area .tabs li a").click(function() {
		var tab = $("." + $(this).data("toggle"));
		$(".page.dashboard .tab-area .tabs li").removeClass("active");
		$(this).parent().addClass("active");
		$(".page.dashboard .tab-area .tab-contents").hide();
		tab.fadeIn(500);
	});
}

function initToggleBtn() {
	$(".toggle-btn").click(function() {
		if($(this).hasClass("off")) {
			$(this).removeClass("off").addClass("on").html($(this).data("toggle-text"));
			$(this).data("toggle-text", "off");
		} else {
			$(this).removeClass("on").addClass("off").html($(this).data("toggle-text"));
			$(this).data("toggle-text", "on");
		}
	});
}

function initDashboardInnerTabs() {
	$(".page.dashboard .inner-tabs li a").click(function() {
		var tab = $("." + $(this).data("toggle"));
		$(".page.dashboard .inner-tabs li").removeClass("active");
		$(this).parent().addClass("active");
		$(".page.dashboard .inner-tab-contents").hide();
		tab.fadeIn(500);
	});
}