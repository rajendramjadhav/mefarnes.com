// JavaScript Document
var winXS = false, winSM = false, winMD = false, winLG = false;

$(document).ready(function() {
	initWindows();
	
	// section links (navigate to page sections)
	if($(".section-link").length) {
		$(".section-link").click(function() {
			var top = $(this).data("link") == "top" ? 0 : $($(this).data("link")).offset().top - 100;
			
			$('html, body').animate({ scrollTop:top }, 1000);
			return false;
		});
	}
	
	// home page main slider
	if($(".main-slider").length) { $('.main-slider').flexslider({animation: "slide", directionNav: false, slideshowSpeed: 10000}); }
	
	// fancy selectbox
	if($(".fancy-selectbox")) { $(".fancy-selectbox").selectbox(); }
	
	// profile page
	if($(".page.profile .pic-slider").length) { 
		$('.pic-slider').flickity({
			contain: true,
			cellAlign: 'left',
			imagesLoaded: true,
			adaptiveHeight: true,
			pageDots: false,
			draggable: false
		});
	}
	
	if($(".page.profile .tab-area").length) { initProfileTabs(); }
	
	// donate page tabs
	if($(".page.donate .tab-area").length) { initDonateTabs(); }
	
	// signup page sections
	if($(".page.signup").length) { initSignupSections(); }
	
	// create campaign days selector
	if($("#day-slider").length) {
		$( "#day-slider" ).slider({ range: true, min: 1, max: 60, values: [0, 30],
			slide: function( event, ui ) { if(ui.values[1] == "1") { var days = " Day"; } else { var days = " Days"; } $( ".days" ).html( ui.values[1] + days ); }
		});
		
		$( ".days" ).html( "30 Days" );
	}
	
	// signup page sections
	if($(".page.create-campaign .tab-area").length) { initCreateCampaignSections(); }
	
	// dashboard page tabs
	if($(".page.dashboard .tab-area").length) { initDashboardTabs(); }
	
	// enable tooltips
	$('[data-toggle="tooltip"]').tooltip();
	
	// init toggle button (on/off)
	if($(".toggle-btn").length) { initToggleBtn(); }
	
	// dashboard page inner tabs
	if($(".page.dashboard .inner-tabs").length) { initDashboardInnerTabs(); }
	
	// up counter
	if($(".counter").length) { $('.counter').counterUp({ delay:10, time:1000 }); }
	
	// faq page
	if($(".faq-main").length) { initFAQPage(); }
	
	// date picker
	if($(".date-input").length) { initDatePicker(); }
	
	// date time picker
	if($(".date-time-input").length) { initDateTimePicker(); }
	
	$(".scroll-up").click(function() {
		$('html, body').animate({ scrollTop:0 }, 500);
		return false;
	});
	
	// organization profile page campaigns slider
	if($(".page.profile-org .campaigns-slider").length) {
		if($(".campaigns-slider").length) {
			var totalSlides = $(".campaigns-slider").find(".slides").children().length;
			var sliderItemWidth = $(".campaigns-slider").find(".slides li").width();
			var sliderItemFullWidth = $(".campaigns-slider").find(".slides li").outerWidth();
			$(".campaigns-slider").find(".slides li").width(sliderItemWidth);
			var sliderWidth = sliderItemFullWidth * totalSlides;
			$(".campaigns-slider").find(".slides").width(sliderWidth);
		}
	}
	
	// organization profile page gallery slider
	if($(".page.profile-org .gallery-slider").length) {
		$('.gallery-slider').flickity({
			cellAlign: 'center',
			wrapAround: true,
			imagesLoaded: true,
			adaptiveHeight: true,
			pageDots: false,
			draggable: false
		});
	}
	
	// create organization profile page
	if($(".page.organization-profile .tab-area").length) { initCreateOrgProfileTabs(); }
	
	// window resize handler
	$(window).resize(function() {
		initWindows();
	});
});

$(window).scroll(function() {
	// change header
	if($(".scroll-up").length) {
		if( $(this).scrollTop() > 100 ) {
			$(".scroll-up").fadeIn(500);
		} else {
			$(".scroll-up").fadeOut(500);
		}
	}	
});

$(document).on("click", function(e) {
	// hide date picker on clicked outside
	var elem = $(e.target);
	
	if(!elem.hasClass("hasDatepicker") && !elem.hasClass("ui-datepicker") && !elem.hasClass("ui-icon") && !elem.hasClass("ui-datepicker-next") && !elem.hasClass("ui-datepicker-prev") && !$(elem).parents(".ui-datepicker").length) { $('.hasDatepicker').datepicker('hide'); }
});

function initWindows() {
	winXS = false, winSM = false, winMD = false, winLG = false;
	
	// window size
	if ($(window).width() <= 767) {
		winXS = true;
	} else if ($(window).width() >= 768 && $(window).width() <= 991) {
		winSM = true;
	} else if ($(window).width() >= 992 && $(window).width() <= 1199) {
		winMD = true;
	} else if ($(window).width() >= 1200) {
		winLG = true;
	}
}

function initProfileTabs() {
	$(".page.profile .tab-area .tabs li a").click(function() {
		var tab = $($(this).data("toggle"));
		$(".page.profile .tab-area .tabs li").removeClass("active");
		$(this).parent().addClass("active");
		$(".page.profile .tab-area .tab-contents").hide();
		tab.fadeIn(100, function() {
			$('.pic-slider').flickity('resize');
		});
	});
	
	if(winXS) {
		$(".page.profile .tab-area .tab-btn-xs").click(function() {
			var thisObj = $(this);
			var tab = $($(this).data("toggle"));
			$(".page.profile .tab-btn-xs").removeClass("active");
			$(this).addClass("active");
			if(tab.is(":hidden")) {
				$(".page.profile .tab-area .tab-contents").slideUp(300);
				tab.slideDown(300, function() {
					var top = thisObj.offset().top - 100;
					$('html, body').animate({ scrollTop:top }, 300, function() {
						$('.pic-slider').flickity('resize');
					});
				});				
			}
		});
	}
}

function initDonateTabs() {
	$(".page.donate .continue-step-1").click(function() {
		$(".page.donate #tab1").hide();
		$(".page.donate #tab2").fadeIn(500);
		$(".page.donate .tabs li.one").addClass("done").removeClass("active");
		$(".page.donate .tabs li.two").addClass("active");
	});
	
	$(".page.donate .back-step-2").click(function() {
		$(".page.donate #tab1").fadeIn(500);
		$(".page.donate #tab2").hide();
		$(".page.donate .tabs li.one").addClass("active").removeClass("done");
		$(".page.donate .tabs li.two").removeClass("active");
	});
	
	$(".page.donate .continue-step-2").click(function() {
		$(".page.donate #tab2").hide();
		$(".page.donate #tab3").fadeIn(500);
		$(".page.donate .tabs li.two").addClass("done").removeClass("active");
		$(".page.donate .tabs li.three").addClass("active");
	});
	
	$(".page.donate .back-step-3").click(function() {
		$(".page.donate #tab2").fadeIn(500);
		$(".page.donate #tab3").hide();
		$(".page.donate .tabs li.two").addClass("active").removeClass("done");
		$(".page.donate .tabs li.three").removeClass("active");
	});
	
	// show / hide donation period based on contribution type
	if($("#donation_type_recurring").is(":checked")) {
		$(".donation-period").show();
	} else {
		$(".donation-period").hide();
	}
}

function initSignupSections() {
	$(".page.signup .individual-signup").click(function() {
		$(".page.signup .sectionMain").hide();
		$(".page.signup .section_individual_1").fadeIn(500);
	});
	
	$(".page.signup .organization-signup").click(function() {
		$(".page.signup .sectionMain").hide();
		$(".page.signup .section_organization_1").fadeIn(500);
	});
	
	$(".page.signup .individual-signup-fb, .page.signup .individual-signup-mail").click(function() {
		$(".page.signup .section_individual_1").hide();
		$(".page.signup .section_individual_2").fadeIn(500);
	});
	
	$(".page.signup .continue-step-1").click(function() {
		$(".page.signup .section_organization_1").hide();
		$(".page.signup .section_organization_2").fadeIn(500);
	});
	
	$(".page.signup .continue-step-2").click(function() {
		$(".page.signup .section_organization_2").hide();
		$(".page.signup .section_organization_3").fadeIn(500);
	});
}

function initCreateCampaignSections() {
	$(".page.create-campaign .continue-section-1").click(function() {
		$(".page.create-campaign .step-1").hide();
		$(".page.create-campaign .step-2").fadeIn(500);
		$(".page.create-campaign .tabs li.one").removeClass("active");
		$(".page.create-campaign .tabs li.two").addClass("active");
		$("html, body").delay(100).animate({ scrollTop: ($('.page.create-campaign .tabs').offset().top - 120) }, 500);
	});
	
	$(".page.create-campaign .continue-section-2").click(function() {
		$(".page.create-campaign .step-2").hide();
		$(".page.create-campaign .step-3").fadeIn(500);
		$(".page.create-campaign .tabs li.two").removeClass("active");
		$(".page.create-campaign .tabs li.three").addClass("active");
		$("html, body").delay(100).animate({ scrollTop: ($('.page.create-campaign .tabs').offset().top - 120) }, 500);
	});
}

function initDashboardTabs() {
	$(".page.dashboard .tab-area .tabs li a").click(function() {
		var tab = $("." + $(this).data("toggle"));
		$(".page.dashboard .tab-area .tabs li").removeClass("active");
		$(this).parent().addClass("active");
		$(".page.dashboard .tab-area .tab-contents").hide();
		tab.fadeIn(500);
	});
}

function initToggleBtn() {
	$(".toggle-btn").click(function() {
		if($(this).hasClass("off")) {
			$(this).removeClass("off").addClass("on").html($(this).data("toggle-text"));
			$(this).data("toggle-text", "off");
		} else {
			$(this).removeClass("on").addClass("off").html($(this).data("toggle-text"));
			$(this).data("toggle-text", "on");
		}
	});
}

function initDashboardInnerTabs() {
	$(".page.dashboard .inner-tabs li a").click(function() {
		var tab = $("." + $(this).data("toggle"));
		$(".page.dashboard .inner-tabs li").removeClass("active");
		$(this).parent().addClass("active");
		$(".page.dashboard .inner-tab-contents").hide();
		tab.fadeIn(500);
	});
}

function initFAQPage() {
	$('.faq-main .view-all').click(function(event){
		event.preventDefault();
		$(this).closest('.question-group').find('.panel-collapse:not(".in")').collapse('show');
	});
}

function showPopup(popup) {
	if($(".popup").is(":visible")) {
		$(".popup").fadeOut();
	} else {
		$(".overlay").fadeIn(500);
	}
	
	$(popup).css("top", ($(window).height() - $(popup).height()) / 2 + $(window).scrollTop() + "px").fadeIn(500);
	
	$(popup).find("a.close").click(function() {
		$(".overlay, .popup").fadeOut(500);
	});
}

function initDatePicker() {
	$(".date-input").datepicker({
		showOn: "button",
		buttonText: '<i class="fa fa-calendar-check-o"></i>',
		dateFormat: "M d, yy",
		beforeShow: function (input, inst) {
			var top = $(input).offset().top + $(input).height();
			var left = $(input).offset().left;
			
			setTimeout(function () {
				inst.dpDiv.css({ top: top, left: left });
			}, 0);
		}
	});
}

function initDateTimePicker() {
	$(".date-time-input").datetimepicker({
		showOn: "button",
		buttonText: '<i class="fa fa-calendar-check-o"></i>',
		controlType: 'select',
		dateFormat: "d MM, ",
		timeFormat: "hh:mm TT",
		beforeShow: function (input, inst) {
			var top = $(input).offset().top + $(input).height();
			var left = $(input).offset().left;
			
			setTimeout(function () {
				inst.dpDiv.css({ top: top, left: left });
			}, 0);
		}
	});
}

function Slidebox(obj, slider, slideTo) {
	if($(obj).hasClass("disabled")) {
		return false;
	}
	
	var sliderStr = slider;
	var slider = $(slider).find($('.slides'));
	var totalSlides = slider.children().length;
	var sliderItemFullWidth = $(slider).find($('.slides li')).outerWidth();
	var sliderWidth = Math.floor((totalSlides * sliderItemFullWidth) - slider.parent().width() - 2);
	
	if( !slider.is(":animated") ) {
		if( slideTo == 'next' ) {
			slider.animate({ marginLeft: '-=' + sliderItemFullWidth }, 500, "easeOutExpo", function() {
				$("a.prev" + sliderStr).removeClass("disabled");
				
				var leftPos = slider.css("margin-left").replace("px", "");
				
				if( leftPos <= -sliderWidth ) {
					$("a.next" + sliderStr).addClass("disabled");
				}
			});
		} else if( slideTo == 'prev' ) {
			slider.animate({ marginLeft: '+=' + sliderItemFullWidth }, 500, "easeOutExpo", function() {
				$("a.next" + sliderStr).removeClass("disabled");
				
				var leftPos = slider.css("margin-left").replace("px", "");
				
				if( leftPos == 0 ) {
					$("a.prev" + sliderStr).addClass("disabled");
				}
			});
		}
	}
}

function initCreateOrgProfileTabs() {
	$(".page.organization-profile .continue-section-1").click(function() {
		$(".page.organization-profile .step-1").hide();
		$(".page.organization-profile .step-2").fadeIn(500);
		$(".page.organization-profile .tabs li.one").removeClass("active");
		$(".page.organization-profile .tabs li.two").addClass("active");
		$("html, body").delay(100).animate({ scrollTop: ($('.page.organization-profile .tabs').offset().top - 120) }, 500);
	});
}