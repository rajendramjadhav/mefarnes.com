<?php



function setMessage($msg,$task = '')
{
    $_this =& get_instance();
    $_this->session->set_flashdata('message',json_encode($msg));
    if($task)
        $_this->session->set_flashdata('task',$task);
}

function getMessage()
{
    $_this =& get_instance();
    $data = $_this->session->flashdata('message');
    return $data;
}

function getTask()
{
    $_this =& get_instance();
    $data = $_this->session->flashdata('task');
    return $data;
}

function setMessageSuccess($text)
{
	setMessage([
				'title' => 'Successful',
				'text' => $text,
				'timer' => 2000,
				'type' => 'success',
				'showConfirmButton' =>false
				]);
}

function setMessageError($text)
{
	setMessage([
	           'title' => 'Opps !!',
				'text' => $text,
				'timer' => 2000,
				'type' => 'error',
				'showConfirmButton' =>false
	]);
}