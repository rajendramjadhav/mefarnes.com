<?php

function encodeData($data)
{
    return urlencode(base64_encode($data));
}

function decodeData($data)
{
    return base64_decode(urldecode($data));
}