<?php

function send_mail_to_user($subject, $message, $email) {
    $headers = 'From: admin@mefarnes.com' . "\r\n";
    $headers.= "MIME-Version: 1.0\r\n";
    $headers.= "Content-Type: text/html; charset=utf-8\r\n";
    $headers.= "X-Priority: 1\r\n";
    mail($email, $subject, $message, $headers, "-fadmin@mefarnes.com");
}

function token() {
    return md5(uniqid(rand(), true));
}

function videoType($url) {
    $_this = & get_instance();
    if (strpos($url, 'youtube') > 0) {

        $link = $url;
        $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
        if (empty($video_id[1]))
            $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

        $video_id = explode("&", $video_id[1]); // Deleting any other params

        return $return = array('videoType' => '1', 'videoId' => $video_id[0]);
        return $video_id = $video_id[0];
    } elseif (strpos($url, 'vimeo') > 0) {
        $video_id = (int) substr(parse_url($url, PHP_URL_PATH), 1);
        return $return = array('videoType' => '2', 'videoId' => $video_id);
    } else {
        return $return = array('videoType' => NULL, 'videoId' => NULL);
    }
}

function time_elapsed_string($ptime) {
    $etime = time() - $ptime;

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array(365 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    $a_plural = array('year' => 'years',
        'month' => 'months',
        'day' => 'days',
        'hour' => 'hours',
        'minute' => 'minutes',
        'second' => 'seconds'
    );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

function format_num($num, $precision = 2) {
    if ($num >= 1000 && $num < 1000000) {
        $n_format = number_format($num / 1000, $precision) . 'K';
    } else if ($num >= 1000000 && $num < 1000000000) {
        $n_format = number_format($num / 1000000, $precision) . 'M';
    } else if ($num >= 1000000000) {
        $n_format = number_format($num / 1000000000, $precision) . 'B';
    } else {
        $n_format = $num;
    }
    return $n_format;
}

function send_mail_through_sendgrid($params) {

    $pass = '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'; // not the key, but the token

    $url = 'https://api.sendgrid.com/';

    $request = $url . 'api/mail.send.json';
    $headr = array();
// set authorization header
    $headr[] = 'Authorization: Bearer ' . $pass;

    $session = curl_init($request);
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $params);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// add authorization header
    curl_setopt($session, CURLOPT_HTTPHEADER, $headr);

    $response = curl_exec($session);
    curl_close($session);
}


function generate_pagination($_config)
{
		$_this =& get_instance();	
		
		$_this->load->library('pagination');

		$config['full_tag_open'] = '<ul class="pagination pull-right" >';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = '&laquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = '&raquo;';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		foreach($_config as $name => $value)
		{
			$config[$name] = $value;
		}

		$_this->pagination->initialize($config);

		return $_this->pagination->create_links(); 
}

function getDateFormat($date)
{
    return date('F d, Y', strtotime($date));
}

function curl_file_get_contents($url)
{
 $curl = curl_init();
 $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';

 curl_setopt($curl,CURLOPT_URL,$url);   //The URL to fetch. This can also be set when initializing a session with curl_init().
 curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);    //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
 curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,5);   //The number of seconds to wait while trying to connect.    

 curl_setopt($curl, CURLOPT_USERAGENT, $userAgent); //The contents of the "User-Agent: " header to be used in a HTTP request.
 curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);  //To follow any "Location: " header that the server sends as part of the HTTP header.
 curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE); //To automatically set the Referer: field in requests where it follows a Location: redirect.
 curl_setopt($curl, CURLOPT_TIMEOUT, 10);   //The maximum number of seconds to allow cURL functions to execute.
 curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); //To stop cURL from verifying the peer's certificate.
 curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

 $contents = curl_exec($curl);
 curl_close($curl);
 return $contents;
}

