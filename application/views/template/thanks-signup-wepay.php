<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!--<![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title></title>
        <!--[if (gte mso 9)|(IE)]>
        <style type="text/css">
                table {border-collapse: collapse;}
        </style>
        <![endif]-->
    </head>

    <body style="Margin-top: 0 !important; Margin-bottom: 0 !important; Margin-left: 0 !important; Margin-right: 0 !important; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; background-color: #ffffff;">
        <center class="wrapper" style="width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
            <div class="webkit" style="max-width: 600px; Margin-top: 0; Margin-bottom: 0; Margin-left: auto; Margin-right: auto;">
                <!--[if (gte mso 9)|(IE)]>
                <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; ">
                <tr>
                <td>
                <![endif]-->
                <table class="outer" align="center" style="border-spacing: 0; width: 100%; max-width: 600px; Margin-top: 0; Margin-bottom: 0; Margin-left:auto; Margin-right:auto;">
                    <tr>
                        <td class="one-column" style="padding-top:40px; padding-bottom:40px; padding-left:60px; padding-right:60px; font-family:'Open Sans', sans-serif; font-weight:300;">
                            <p style="Margin-top:0; Margin-bottom:20px; Margin-left:0; Margin-right:0; text-align:left; font-size:30px; color:#fc6c06;">Thank You, <?= $user->first_name . '' . $user->org_name ?></p><br/>
                            <p style="Margin-top:0; Margin-bottom:10px; Margin-left:0; Margin-right:0; text-align:left; font-size:16px; color:#454545; line-height:30px;">for signing up to Mefarnes!</p>
                            <p style="Margin-top:0; Margin-bottom:10px; Margin-left:0; Margin-right:0; text-align:left; font-size:16px; color:#454545; line-height:30px;"> Donations are processed though Wepay, the same processor that is used by Gofundme.</p>
                            <p style="Margin-top:0; Margin-bottom:30px; Margin-left:0; Margin-right:0; text-align:left; font-size:16px; color:#454545; line-height:30px;">Please confirm your account in order to receive any funds you raise.</p><br/>
                        </td>
                    </tr>
                </table>
                <!--[if (gte mso 9)|(IE)]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>
        </center>
    </body>

</html>