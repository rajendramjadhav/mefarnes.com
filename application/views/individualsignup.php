<style>
	.blue:hover{
		background: #0064d2!important;
		color : white;
		
	}
</style>
<div id="content" class="page signup">
    <div class="container">
        <div class="row">
        	<!-- individual signup panel : section 2 (form) -->
            <section class="individual-signup-mail">
                <h2>Signup as Individual</h2>
                <!--<h3>Lorem ipsum eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>--->
                <div class="signup-form-box">
                    <div class="col-left green"><i class="fa fa-envelope"></i></div>
                    <div class="col-right">
                        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                        <?php //echo form_open(current_url()); ?> 
                        <form name="frm_individualsignup" id="frm_individualsignup" method="post" action="<?php echo base_url(); ?>home/saveindividualuser">
                        <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                            <div class="signup-form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="txtfname">First Name <span>[ *Please Enter First Name ]</span></label>
                                        <input type="text" id="txtfname" name="txtfname" required="" pattern="[a-zA-Z0-9]+" title="Please enter alpha numeric character only"  x-moz-errormessage="Please enter alpha numeric character only" value="<?php echo set_value('txtfname'); ?>" />
                                    </div>
                                    <div class="col-md-6">
                                        <label for="txtlname">Last Name <span>[ *Please Enter Last Name ]</span></label>
                                        <input type="text" id="txtlname" name="txtlname" required="" pattern="[a-zA-Z0-9]+" title="Please enter alpha numeric character only"  x-moz-errormessage="Please enter alpha numeric character only" value="<?php echo set_value('txtlname'); ?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtemail">Your Email Address <span>[ *Please Enter Email Address ]</span></label>
                                        <input type="email" id="txtemail" name="txtemail" required="" value="<?php echo set_value('txtemail'); ?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtcity">City <span>[ *Please Enter City ]</span></label>
                                        <input type="text" id="txtcity" name="txtcity" required="" value="<?php echo set_value('txtcity'); ?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtpassword">Set Password <span>[ *Please Enter Password ]</span></label>
                                        <input type="password" id="txtpassword" name="txtpassword" required="" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="UpperCase, LowerCase, Number/SpecialChar and min 8 Chars"  x-moz-errormessage="UpperCase, LowerCase, Number/SpecialChar and min 8 Chars" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="checkbox" id="chkbox" name="opt_status" checked="checked" class="fancy-checkbox" /><label for="chkbox">Receive our weekly newsletter and other occasional updates</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                <!--                                    <a href="javascript:void(0);" class="button blue"><span>Sign me up!</span></a>-->
                                        <input type="submit" style="background: #fff;transition: height 0.2s linear 0s;" class="button blue" value="Sign me up!" name="signup">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>You are agreeing with all <a target="_blank" href="<?= base_url() . 'terms' ?>" class="text-default">terms</a> and <a target="_blank" href="<?= base_url() . 'privacy' ?>" class="text-default">privacy policy</a>.</label>
                                    </div>
                                </div>
                            </div>
                        </form>
            
                    </div>
                </div>
            </section>
            <!-- individual signup panel : section 2 (form) -->
        </div>
    </div>
</div> 
<script src="https://maps.googleapis.com/maps/api/js??v=3.13&libraries=places&key=AIzaSyBgS4A2KbRY159HA-HDIRxaoanaYQscQy8" type="text/javascript"></script>
<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('txtcity');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('city');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('full_address');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>