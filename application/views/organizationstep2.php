<div id="content" class="page signup">
    <div class="container">
        <div class="row">
        	<form name="frm_signup2" id="frm_signup2" method="post" action="<?php echo base_url(); ?>auth/saveorgsignupstep2">
            <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                <section class="">
                    <h2>Signup as Organization</h2>
                    <!--<h3>Lorem ipsum eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>-->
                    <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                    <div class="signup-form-box">
                        <div class="col-left orange"><i class="fa fa-envelope"></i></div>
                        <div class="col-right">
                            <div class="signup-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Organization Account Type</h4>
                                    </div>
                                </div>
                                <div class="row">
                                	
                                    <div class="col-md-12 radios">
                                        <!--<span><input type="radio" name="org_acc_type" id="org_acc_type_myself" class="fancy-radio" /> <label for="org_acc_type_myself">My Self</label></span>-->
                                        <span style="float: left; width: 25%;"><input type="radio" name="org_account_type" id="org_acc_type_business" value="2" class="fancy-radio" onclick="hideDivnonprofit1(this.value)"/> <label for="org_acc_type_business">Business</label></span>
                                        <span style="float: left; width: 25%;"><input type="radio" name="org_account_type" id="org_acc_type_non_profit" value="3" checked="checked" class="fancy-radio" onclick="hideDivnonprofit2(this.value)"/> <label for="org_acc_type_non_profit">Non-Profit</label></span>
                                        <span style="float: left; width: 25%;"><input type="radio" name="org_account_type" id="org_acc_type_others" value="4" class="fancy-radio" onclick="hideDivnonprofit3(this.value)" /> <label for="org_acc_type_others">Other</label></span>
                                    </div>
                                    
                                </div>
                               
                                <div class="row" id="hidenonprofit">
                                    <div class="col-md-12">
                                        <div class="jumbotron">
                                            <h4>501(c)(3) Nonprofit Registration <a href="#"><i class="fa fa-question-circle"></i></a></h4>
                                            <h6>Is this non-profit a registered 501(c)(3) in the United States?</h6>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="txtein_org">EIN No  <span>[ *Please Enter EIN number ]</span></label>
                                                    <input type="text" id="ein_no" name="ein_no" />
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="hidenonprofit1">
                                    <div class="col-md-12">
                                        <label>For verification reasons, Please provide valide EIN number.</label>
                                    </div>
                                </div>
                                
                              
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--<a href="javascript:void(0);" class="button blue continue-step-2"><span>Continue</span></a>-->
                                        <button type="submit" name="save" class="button blue continue-step-2" onclick="return checkeinno()">
                                        	<span>Continue</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>	
            </form>
        </div>
    </div>
</div> 

<script>
	function hideDivnonprofit1(str)
	{
		//alert(str);
		var check = str;
		if(check == 2){
			$('#hidenonprofit').hide();
			$('#hidenonprofit1').hide();
			//alert('Please Choose Only Non Profit');
			return false;
		}
		
	}
	function hideDivnonprofit2(str)
	{
		//alert(str);
		var check = str;
		if(check == 3){
			$('#hidenonprofit').show();
			$('#hidenonprofit1').show();
			//alert('Please Choose Only Non Profit');
			return false;
		}
		
	}
	function hideDivnonprofit3(str)
	{
		//alert(str);
		var check = str;
		if(check == 4){
			$('#hidenonprofit').hide();
			$('#hidenonprofit1').hide();
			//alert('Please Choose Only Non Profit');
			return false;
		}
		
	}
</script>
<script>
  function checkeinno(){
    var checkaccounttype = $('input[name=org_account_type]:checked').val();
    var ein_no = $('#ein_no').val();
    
    //alert(checkaccounttype );
       if(checkaccounttype == '3'){
        if(ein_no == ''){
          alert('Please Enter Ein No');
          return false;
           }
      }
}
 
</script>