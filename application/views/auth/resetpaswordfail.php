<!-- content -->
<div id="content" class="page signup">
    <div class="container">

        <div class="row">

            <!-- login form -->

            <section class="login">

                <h2>Reset Password</h2>

                <!--<h3>Lorem ipsum eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>---->

                <div class="signup-form-box">

                    

                    <div class="col-right">
                       
                        <div class="signup-form"> 
                        <?php 
                            if ($this->session->userdata('success')) {
                                echo '<div class="alert alert-success alert-dismissable fade in">';
                                echo $this->session->userdata('success');
                                $this->session->unset_userdata('success');
                                echo '</div>';
                            } else {
                                echo '<div class="alert alert-danger alert-dismissable fade in">';
                                echo $this->session->userdata('error');
                                echo '</div>';   
                           }
                            
                        ?>                        
                             
                        </div>                        

                    </div>

                </div>

            </section>

            <!-- login form -->

        </div>

    </div>

</div>

<!-- /content -->

<div class="overlay"></div>

<div class="popup forgot-password">

    <h2>Don’t worry we help you! <a href="javascript:void(0);" class="close"><i class="icon-close-x-lg-white"></i></a></h2>

    <div class="content">

        <div class="alert" id="password-reset-alert"></div>

        <h3>Enter your register email address</h3>



        <div class="form">

            <div class="form-row">

                

                <label for="txtemail">Email Address</label>

                <input type="email" id="emailReset" name="emailReset" />

            </div>

            <div class="buttons">

<!--				<a href="javascript:void(0);" onClick="showPopup('.forgot-password-link')" class="button blue"><span>Submit</span></a>-->

                <a href="javascript:void(0);" onClick="validateEmail();" class="button blue"><span>Submit</span></a>

            </div>

        </div>

    </div>

</div>



<div class="popup forgot-password-link">

    <h2>Thank you! <a href="javascript:void(0);" class="close"><i class="icon-close-x-lg-white"></i></a></h2>

    <div class="content">

        <h3>Please check your resigter email, we have sent you reset password link to you email.</h3>

        <p><a href="#">Resend Link</a></p>

    </div>

</div>



<script>

    function validateEmail()

    {

        $("#password-reset-alert").html('');

        if ($('#emailReset').val() == '')

        {

            alert("Please enter your email address !");

            $('#emailReset').focus();

        }

        else {

            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

            if (pattern.test($('#emailReset').val()) == false)

            {

                alert("Please enter valid email address !");

                $('#emailReset').focus();

            }

            else {

                 

                jQuery.ajax({

                    type: "POST",

                    url: "<?php echo base_url(); ?>" + "auth/forgotPassword",

                    dataType: 'json',

                    data: {email: $('#emailReset').val()},

                    success: function (res) {

                        if (res)

                        {

                            if (res.code == 200)

                            {

                                showPopup('.forgot-password-link');

                            }

                            else

                            {

                                $("#password-reset-alert").addClass('alert-danger');

                                $("#password-reset-alert").html('This email address is not registered with us.');

                            }

                        }

                    }

                });

            }

            //showPopup('.forgot-password-link');

        }

    }
    $("body").on('click','.blue-submit',function(){
    	$(".submit-button").click();
    });
</script>    