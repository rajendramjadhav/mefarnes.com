<!-- content -->
<style>
a.wepay-widget-button.wepay-green {
    background-color: #FFF !important;
    background-image: none !important;
    box-shadow: none !important;
    border-radius: 0 !important;
    border: 2px solid #999;
    text-shadow: none !important;
    color: #0064d2 !important;
    font-family: 'Lato',Helvetica,Arial !important;
    font-size: 15px !important;
    font-weight: 700 !important;
    padding: 13px 25px !important;
    margin: 0 0 20px 0 !important;
    text-align: center !important;
    text-decoration: none !important;
    border-color: #0064d2 !important;
   
}
a.wepay-widget-button.wepay-green:hover {
    background-color: #0064d2 !important;
    color: #FFF !important;

   
    
}
</style>
<div id="content" class="page create-campaign">
    <div class="container">
        <div class="row">
             <h2 style="color:#99cc00">Account Activation</h2>
            <h3>In order to activate your account, You must link your mefarnes account to Wepay - the same processor for gofundme.com. Please click on the button below you get started.
                <br><br>
                 <a  class="wepay-widget-button wepay-green" id="start_oauth2">Click here to create your WePay account</a>

<script src="https://static.wepay.com/min/js/wepay.v2.js" type="text/javascript"></script>
<script type="text/javascript">

WePay.set_endpoint("production"); // stage or production

WePay.OAuth2.button_init(document.getElementById('start_oauth2'), {
    "client_id":"74601",
     "scope":["manage_accounts","collect_payments","view_user","send_money","preapprove_payments"],
    "user_name":"<?php if(isset($_SESSION['userdata']->org_name) && $_SESSION['userdata']->org_name!=''){
                             echo $_SESSION['userdata']->org_name;
                 }else{ echo $_SESSION['userdata']->first_name.' '.$_SESSION['userdata']->last_name; } ?>",
    "user_email":"<?php echo $_SESSION['userdata']->email; ?>",
    "redirect_uri":"https://mefarnes.com/home/success",
    "top":100, // control the positioning of the popup with the top and left params
    "left":100,
    "state":"robot", // this is an optional parameter that lets you persist some state value through the flow
    "callback":function(data) { 
		/** This callback gets fired after the user clicks "grant access" in the popup and the popup closes. The data object will include the code which you can pass to your server to make the /oauth2/token call **/
		if (data.code.length !== 0) {
                       
                        window.location.href = "<?= base_url() . 'home/paymentthank/' ?>"+data.code; 
			// send the data to the server
		} else {
			// an error has occurred and will be in data.error
		}
	}
});

</script>
        </div>
    </div>
</div>
<!-- /content -->