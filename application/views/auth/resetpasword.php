<!-- content -->
<div id="content" class="page signup">
    <div class="container">

        <div class="row">

            <!-- login form -->

            <section class="login">

                <h2>Reset Password</h2>

                <!--<h3>Lorem ipsum eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>---->

                <div class="signup-form-box">

                <div class="col-left green"><i class="fa fa-key"></i></div>
                    <div class="col-right">
                       <?php echo $this->session->flashdata('success'); ?>
                        <?php // echo $msg; ?>
                        <form method="post" action="<?php echo base_url('auth/savePassword');  ?>" id="resetPassword">
                        <div class="signup-form"> 
                            <input type="hidden" name="resetCode" value="<?php echo $resetCode; ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txtemail">New Password </label>
                                    <input type="password" id="new_password" name="new_password"  required pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="UpperCase, LowerCase, Number/SpecialChar and min 8 Chars"  x-moz-errormessage="UpperCase, LowerCase, Number/SpecialChar and min 8 Chars" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txtemail">Confirm Password </label>
                                    <input type="password" id="conf_password" name="conf_password" required pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="UpperCase, LowerCase, Number/SpecialChar and min 8 Chars"  x-moz-errormessage="UpperCase, LowerCase, Number/SpecialChar and min 8 Chars" />
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-12">
                                 <a href="#" onclick="submitForm();" class="button blue blue-submit"><span>Save</span></a>
                                </div>
                            </div>
                           
                        </div>                        

                    </div>

                </div>

            </section>

            <!-- login form -->

        </div>

    </div>

</div>

<!-- /content -->

<script type="text/javascript">
    function submitForm () {
        var pass = document.getElementById('new_password').value;
        var conf_pass = document.getElementById('conf_password').value;        
       if (pass != '' && conf_pass != '') {
            if (pass == conf_pass) {
                document.getElementById("resetPassword").submit();
            } else{
                alert('Please enter correct password');
            }
        }
        else{
            alert("All fields are required.....!");
        }
       
        
    }
</script>