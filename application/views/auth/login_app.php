<!-- content -->
<div id="content" class="page signup">
    <div class="container">

        <div class="row">

            <!-- login form -->

            <section class="login">

                <h2>Welcome to Mefarnes</h2>

                <h3>Lorem ipsum eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>

                <div class="signup-form-box">

                    <div class="col-left green"><i class="fa fa-user"></i></div>

                    <div class="col-right">

                        <?php echo $this->session->flashdata('success'); ?>

                        <?php echo form_open(base_url('auth/login')); ?> 

                        <div class="signup-form">

                            <div class="row">

                                <div class="col-md-12">

                                    <label for="txtfname" class="error">Email Address <span></span></label>

                                    <input type="email" id="email" name="email" required="" class="error" value="<?php echo set_value('email');?>" />

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <label for="txtemail">Password <span></span></label>

                                    <input type="password" id="password" name="password" required="" />

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <input type="checkbox" id="chkbox" checked="checked" class="fancy-checkbox" /><label for="chkbox">Remember me next time.</label>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

<!--									<a href="javascript:void(0);" class="button blue"><span>Log In</span></a>-->

                                    <input type="submit" class="button blue" value="Login" name="signup">

                                </div>

                            </div>

                            <div class="clearfix mb10">

                                <a href="javascript:void(0);" onClick="showPopup('.forgot-password')">Forgot Password?</a>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <label>New User? to join Mefarnes Sign Up <a href="<?php echo base_url() . 'signup' ?>">Here</a>.</label>

                                </div>

                            </div>

                            <div class="or"><span>OR</span></div>

                            <div class="facebook-btn-container">
								<?php if (@$user_profile):  // call var_dump($user_profile) to view all data ?>
                                    <div class="row">
                                        <div class="col-lg-12 text-center">
                                            <img class="img-thumbnail" data-src="holder.js/140x140" alt="140x140" src="https://graph.facebook.com/<?=$user_profile['id']?>/picture?type=large" style="width: 140px; height: 140px;">
                                            <h2><?=$user_profile['name']?></h2>
                                            <a href="<?=$user_profile['link']?>" class="btn btn-lg btn-default btn-block" role="button">View Profile</a>
                                            <a href="<?= $logout_url ?>" class="btn btn-lg btn-primary btn-block" role="button">Logout</a>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <h2 class="form-signin-heading">Login with Facebook</h2>
                                    <a href="<?= $login_url ?>" class="facebook-btn button" role="button">Login</a>
                                <?php endif; ?>
                                <!--<a href="" class="facebook-btn button"><span><i class="fa fa-facebook"></i>Log In by Facebook</span></a>-->

                            </div>

                        </div>

                        </form>

                    </div>

                </div>

            </section>

            <!-- login form -->

        </div>

    </div>

</div>

<!-- /content -->

<div class="overlay"></div>

<div class="popup forgot-password">

    <h2>Don’t worry we help you! <a href="javascript:void(0);" class="close"><i class="icon-close-x-lg-white"></i></a></h2>

    <div class="content">

        <div class="alert" id="password-reset-alert"></div>

        <h3>Enter your register email address</h3>



        <div class="form">

            <div class="form-row">

                

                <label for="txtemail">Email Address</label>

                <input type="email" id="emailReset" name="emailReset" />

            </div>

            <div class="buttons">

<!--				<a href="javascript:void(0);" onClick="showPopup('.forgot-password-link')" class="button blue"><span>Submit</span></a>-->

                <a href="javascript:void(0);" onClick="validateEmail();" class="button blue"><span>Submit</span></a>

            </div>

        </div>

    </div>

</div>



<div class="popup forgot-password-link">

    <h2>Thank you! <a href="javascript:void(0);" class="close"><i class="icon-close-x-lg-white"></i></a></h2>

    <div class="content">

        <h3>Please check your resigter email, we have sent you reset password link to you email.</h3>

        <p><a href="#">Resend Link</a></p>

    </div>

</div>



<script>

    function validateEmail()

    {

        $("#password-reset-alert").html('');

        if ($('#emailReset').val() == '')

        {

            alert("Please enter your email address !");

            $('#emailReset').focus();

        }

        else {

            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

            if (pattern.test($('#emailReset').val()) == false)

            {

                alert("Please enter valid email address !");

                $('#emailReset').focus();

            }

            else {

                 

                jQuery.ajax({

                    type: "POST",

                    url: "<?php echo base_url(); ?>" + "auth/forgotPassword",

                    dataType: 'json',

                    data: {email: $('#emailReset').val()},

                    success: function (res) {

                        if (res)

                        {

                            if (res.code == 200)

                            {

                                showPopup('.forgot-password-link');

                            }

                            else

                            {

                                $("#password-reset-alert").addClass('alert-danger');

                                $("#password-reset-alert").html('This email address is not registered with us.');

                            }

                        }

                    }

                });

            }

            //showPopup('.forgot-password-link');

        }

    }

</script>    