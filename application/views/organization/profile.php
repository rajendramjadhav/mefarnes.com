<link rel="stylesheet" type="text/css" href="<?= base_url() . 'assets/' ?>css/flickity.min.css">
<script type="text/javascript" src="<?= base_url() . 'assets/' ?>js/waypoints.min.js"></script>
<script type="text/javascript" src="<?= base_url() . 'assets/' ?>js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="<?= base_url() . 'assets/' ?>js/flickity.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() . 'assets/' ?>js/jquery.counterup.min.js"></script>
<!-- content -->
<div id="content" class="page profile-org" >
    <div class="banner" style="background:transparent url(<?= base_url() . 'uploads/' ?>organization/cover_images/<?= $org->image ?>) center center no-repeat;">
        <div class="contents">
            <h2><?= $org->org_name ?></h2>
            <p><?= $org->org_tag ?></p>
            <p><?= $org->location ?></p>
            <div class="buttons"><a href="<?= base_url() . 'donate-organization/' . $org->org_slug ?>" class="donate-button"><span>Donate Now!</span></a></div>
        </div>
    </div>

    <!-- tab panel -->
    <div class="tab-panel">
        <div class="container">
            <div class="row">
                <ul class="tabs">
                    <li><a href="javascript:void(0);" data-link="#overview" class="section-link">Overview</a></li>
                    <li><a href="javascript:void(0);" data-link="#campaigns" class="section-link">Campaigns</a></li>
                    <li><a href="javascript:void(0);" data-link="#network" class="section-link">Our Network</a></li>
                    <li><a href="javascript:void(0);" data-link="#gallery" class="section-link">Gallery</a></li>
                    <li><a href="javascript:void(0);" data-link="#contact" class="section-link">Contact</a></li>
                </ul>
                <div class="buttons">
                    <a href="#" class="fb-share-button"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="tw-share-button"><i class="fa fa-twitter"></i></a>
                    <a href="<?= base_url() . 'donate-organization/' . $org->org_slug ?>" class="donate-button"><span>Donate Now!</span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /tab panel -->

    <div id="overview" class="overview">
        <div class="container">
            <div class="row">
                <h3 class="underline left">Overview</h3>
                <div class="row">
                    <div class="col-sm-8">
                        <p><?= $org->about_me ?></p>
                    </div>
                    <div class="col-sm-4 text-right">
                        <div class="box"><img src="<?= base_url() . 'assets/' ?>images/data/video-1.jpg" alt="" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /overview -->

    <!-- campaign scroller -->
    <?php
    if (!empty($campaigns)) {
        ?>
        <div id="campaigns" class="campaign-scroller">
            <div class="container">
                <div class="row">
                    <div class="col-xs-9">
                        <h3 class="underline left">Campaigns</h3>
                    </div>
                    <div class="col-xs-3">
                        <div class="buttons-container">
                            <a href="javascript:void(0);" onClick="Slidebox(this, '.campaigns-slider', 'prev')" class="campaigns-slider prev disabled"><i class="fa fa-angle-left"></i></a>
                            <a href="javascript:void(0);" onClick="Slidebox(this, '.campaigns-slider', 'next')" class="campaigns-slider next"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="campaigns-slider">
                            <div class="slide-container">
                                <ul class="slides">
                                    <?php
                                    foreach ($campaigns as $campaign):
                                        ?>
                                        <li>
                                            <div class="campaign-box">
                                                <figure>
                                                    <?php
                                                    $image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/cover_images/thumb/' . $campaign->image)) ? base_url() . 'uploads/campaign/cover_images/thumb/' . $campaign->image : base_url() . 'uploads/campaign/cover_images/' . $campaign->image;
                                                    ?>
                                                    <img src="<?= $image_file ?>" alt="" />
                                                    <figcaption><?= $campaign->title ?></figcaption>
                                                </figure>

                                                <div class="contents">
                                                    <div class="col-one">Raised<span>$<?= format_num($campaign->fund_needed) ?></span></div>
                                                    <div class="col-two">Donors<span><?= $campaign->donor ?></span></div>
                                                </div>

                                                <div class="buttons">
                                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                                    <a href="<?= base_url() . 'campaign/' . $campaign->slug ?>" class="donate">Donate Now!</a>
                                                </div>
                                            </div>
                                        </li>

                                    <?php endforeach; ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }else { ?>
        <div id="campaigns" class="alert alert-info campaign-scroller">No Campaign created yet!</div>
    <?php } ?>
    <!-- /campaign scroller -->


    <!-- our network -->
    <!--    <div id="network" class="our-network">
            <div class="container">
                <div class="row">
                    <h3 class="underline">Our Network</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="box facebook">
                                <div class="icon"><i class="fa fa-facebook"></i></div>
                                <p>Facebook Friends</p>
                                <h4>530</h4>
                                <hr />
                                <p><a href="#">Share Our Profile</a></p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box twitter">
                                <div class="icon"><i class="fa fa-twitter"></i></div>
                                <p>Twitter Friends</p>
                                <h4>657</h4>
                                <hr />
                                <p><a href="#">Share Our Profile</a></p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box mail">
                                <div class="icon"><i class="fa fa-envelope-o"></i></div>
                                <p>Email Friends</p>
                                <h4>457</h4>
                                <hr />
                                <p><a href="#">Share Our Profile</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    <!-- /our network -->

    <!-- gallery slider -->
    <?php if (!empty($gallery_images)) { ?>
        <div id="gallery" class="gallery-slider">
            <?php
            foreach ($gallery_images as $gallery_image):
                // echo is_file(base_url() . ORG_GALLERY_IMAGE.$gallery_image->image_name); echo '<br/>';
                ?>
                <div class="carousel-cell">
                    <figure><img src="<?= base_url() . ORG_GALLERY_IMAGE . $gallery_image->image_name ?>" alt="" /></figure>
                    <div class="content"><span><?= $gallery_image->title ?></span> <a href="#"><i class="fa fa-expand"></i></a></div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php }else { ?>
        <div id="gallery" class="alert alert-info">No Image uploaded yet!</div>
    <?php } ?>
    <!-- /gallery slider -->

    <!-- contact us -->
    <div id="contact" class="contact-us">
        <h3 class="underline">Contact Us</h3>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="box">
                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                        <div class="content">
                            <strong>Address</strong><br /><?= $org->street_name ?>,<br /><?= $org->city ?>, <?= $org->country ?>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="box">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <div class="content pt15">
                            <strong>Contact Number</strong><br />01-234-56789
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="box">
                        <div class="icon"><i class="fa fa-envelope"></i></div>
                        <div class="content pt15">
                            <strong>Send Email</strong><br /><a href="mailto:<?= $org->email ?>"><?= $org->email ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /contact us -->

    <div class="scroll-up"><i class="icon-long-arrow-up"></i></div>
</div>
<!-- /content -->