<!-- content -->
<div id="content" class="page donate organization">
    <div class="banner" style="background:transparent url(<?= base_url() . 'uploads/' ?>organization/cover_images/<?= $org->image ?>) center center no-repeat;">
        <div class="contents">
            <h2><?= $org->org_name ?></h2>
            <p><?= $org->org_tag ?></p>
            <p><?= $org->location ?></p>
        </div>
    </div>

    <div class="container">
        <h2 class="desc">Hello! Josh, Please follow below steps to make your donation successful.</h2>
        <div class="tab-area">
            <ul class="tabs">
                <li class="one active">Contribution <span><i class="icon-check"></i></span></li>
                <li class="two">Personal Details <span><i class="icon-check"></i></span></li>
                <li class="three">Payment Option <span><i class="icon-check"></i></span></li>
            </ul>
            <div class="tab-data">
                <div id="tab1" class="tab-contents">
                    <div class="row">
                        <div class="col-sm-5 col-md-4"><img src="<?= base_url() ?>assets/images/donate-contribute.jpg" alt="" /></div>
                        <div class="col-sm-7 col-md-8">
                            <div class="form">
                                <div class="info">&nbsp;</div>
                                <h2>Contribution Type</h2>
                                <div class="radios clearfix">
                                    <span><input type="radio" name="donation_type" id="donation_type_recurring" checked="checked" class="fancy-radio" onChange="$('.donation-period').show();" /> <label for="donation_type_recurring">Recurring Donation</label></span>
                                    <span><input type="radio" name="donation_type" id="donation_type_once" class="fancy-radio" onChange="$('.donation-period').hide();" /> <label for="donation_type_once">One-Time Donation</label></span>
                                </div>

                                <div class="donation-period">
                                    <h2>Donation Period</h2>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="select-box">
                                                <select class="fancy-selectbox">
                                                    <option>Monthly</option>
                                                    <option>Yearly</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="select-box">
                                                <select class="fancy-selectbox">
                                                    <option>6 Months</option>
                                                    <option>12 Months</option>
                                                </select>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <h2>Contribution Amount</h2>
                                <div class="textbox">
                                    <span>$</span>
                                    <input type="text" value="100.00" />
                                </div>
                                <div class="buttons">
                                    <a href="javascript:void(0);" class="button blue continue-step-1"><span>Continue</span></a>
                                </div>
                                <div class="text-right">By continuing, you agree with the Mafernas <a href="#">terms</a> and <a href="#">privacy policy</a>.</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tab2" class="tab-contents">
                    <div class="row">
                        <div class="col-sm-5 col-md-4"><img src="images/donate-personal.jpg" alt="" /></div>
                        <div class="col-sm-7 col-md-8">
                            <div class="form">
                                <div class="info">Your Contribution : <span>$100</span></div>
                                <h2>Personal Details</h2>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="error" for="txtfname">Your First Name <span>[ *Please Enter First Name ]</span></label>
                                        <input type="text" id="txtfname" class="error" />
                                    </div>
                                    <div class="col-md-6">
                                        <label for="txtlname">Your Last Name <span>[ *Please Enter Last Name ]</span></label>
                                        <input type="text" id="txtlname" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtemail">Your Email Address <span>[ *Please Enter Email Address ]</span></label>
                                        <input type="text" id="txtemail" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="select-box">
                                            <select class="fancy-selectbox">
                                                <option>Country</option>
                                                <option>Country One</option>
                                                <option>Country Two</option>
                                                <option>Country Three</option>
                                                <option>Country Four</option>
                                            </select>
                                        </span>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="txtpostcode">Postal Code <span>[ *Please Enter Postal Code ]</span></label>
                                        <input type="text" id="txtpostcode" />
                                    </div>
                                </div>

                                <h2>Privacy Option</h2>

                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="select-box">
                                            <select class="fancy-selectbox">
                                                <option>Make donation public</option>
                                                <option>Make donation private</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="buttons">
                                    <a href="javascript:void(0);" class="button back-step-2"><span>Back</span></a>
                                    <a href="javascript:void(0);" class="button blue continue-step-2"><span>Continue</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tab3" class="tab-contents">
                    <div class="row">
                        <div class="col-sm-5 col-md-4"><img src="images/donate-payment.jpg" alt="" /></div>
                        <div class="col-sm-7 col-md-8">
                            <div class="form">
                                <div class="info">Your Contribution : <span>$100</span></div>
                                <h2>Payment Option</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="cardnumber">Credit or Debit Number <span>[ *Please Enter Card Number ]</span></label>
                                        <input type="text" id="cardnumber" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="select-box">
                                            <select class="fancy-selectbox">
                                                <option>Expiration Month</option>
                                                <option>Jan</option>
                                                <option>Feb</option>
                                                <option>Mar</option>
                                                <option>Apr</option>
                                            </select>
                                        </span>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="select-box">
                                            <select class="fancy-selectbox">
                                                <option>Expiration Year</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                                <option>2017</option>
                                                <option>2018</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 relpos">
                                        <label for="ccv">Security Code <span>[ *Please Enter Security Code ]</span></label>
                                        <input type="text" id="ccv" />
                                        <span class="ccv"><i class="icon-ccv"></i></span>
                                    </div>
                                    <div class="col-md-12">
                                        <span class="form-info">Enter 3-digits on back of card. AmEx uses front 4-digit code.</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea rows="2" cols="10">Leave a comment</textarea>
                                    </div>
                                </div>
                                <div class="buttons">
                                    <a href="javascript:void(0);" class="button back-step-3"><span>Back</span></a>
                                    <a href="javascript:void(0);" class="button blue finalize-steps"><span>I’m Done</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /content -->