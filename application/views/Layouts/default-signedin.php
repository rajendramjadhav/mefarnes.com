<?php $pagename = $this->uri->segment(2); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $title ?></title>
        <meta property="og:title" content="<?= $meta_title?>" />
        <meta name="Description" content="<?= $meta_description?>">
        <meta name="keywords" content="<?= $keyword?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:300,400,600,700">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery-ui.css" />
        
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/flexslider.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/flickity.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/circle.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lightbox/src/css/lightbox.css">
        
        <?php if($pagename !="step2" && $pagename !="step-two"){?>
        	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/styles.css" />
        <?php } ?>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <!--<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.cropit.js"></script>-->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.flexslider-min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/flickity.pkgd.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.selectbox.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/waypoints.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.counterup.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/ui.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.mask.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.inputmask.bundle.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/lightbox/src/js/lightbox.js"></script>
        <link rel="shortcut icon" href="<?= base_url() ?>assets/images/logo.ico" />
    </head>

    <body>
        <!-- header -->
        <?php include 'partials/top-header-after-login.php'; ?>
        <!-- /header -->

        <!-- content -->
        <?= $content ?>
        <!-- /content -->

        <?php include 'partials/footer.php'; ?>
    </body>
</html>
