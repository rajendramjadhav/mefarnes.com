<!-- footer -->
<div class="footer-top">
    <h2>Changing the world one donation at a time!!</h2>
    <h4>One Small step in this world, One GIANT step in the next!</h4>
    <p class="buttons">
        <a href="#" class="big-button facebook"><span><i class="fa fa-facebook"></i> <b>Support a Campaign</b></span></a>
        <a href="#" class="big-button"><span><i class="fa fa-bullhorn"></i> <b>Start your Campaign</b></span></a>
    </p>
</div>
<footer>
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3>Quick Links</h3>
                            <div class="clearfix">
                                <ul class="links">
                                    <li><a href="<?= base_url().'how-it-works'?>">How it Works</a></li>
                                    <li><a href="#">Success Stories</a></li>
                                    <li><a href="<?= base_url().'faq'?>">Common Questions</a></li>
                                    <li><a href="<?= base_url().'contact-us'?>">Ask a Question</a></li>
                                    <li><a href="<?= base_url().'signup'?>">Sign Up Now</a></li>
                                </ul>
                                <!--
                                <ul class="links">
                                    <li><a href="#">Watch the Video</a></li>
                                    <li><a href="#">Mefares Reviews</a></li>
                                    <li><a href="#">Pricing &amp; Fees</a></li>
                                    <li><a href="#">Fundraising Ideas</a></li>
                                    <li><a href="#">We're Hiring!</a></li>
                                </ul> -->
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h3>Contact Info</h3>
                            <div class="contact">
                                <p><i class="fa fa-map-marker"></i>
                                    <span>136 Pennington Ave<br>
                                        Passaic, New Jersey<br>
                                        07055</span></p>
                                <p><i class="fa fa-phone"></i>
                                    <span>(617) 861-9235</span></p>
                                <p><i class="fa fa-phone"></i>
                                    <span>+972 523-444-890</span></p>
                                <p><i class="fa fa-envelope"></i>
                                    <span><a href="mailto:admin@mefarnes.com">admin@mefarnes.com</a></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <h3>Subscribe To Our Newsletter</h3>
                    <p>Get Latest updates by us on your email.</p>
                    <div class="newsletter-form">
                        <form method="post">
                            <span class="cover"><input type="email" placeholder="Enter your valid email here..." /></span>
                            <button type="submit"><span>Subscribe Now!</span></button>
                        </form>
                    </div>
                    <div class="social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">&copy; 2016 Mefarnes, All Rights Reserved.</div>
                <div class="col-sm-6">
                    <div class="links">
                        <a href="#">Blog</a> |
                        <a href="#">Legal</a> |
                        <a href="<?= base_url().'terms'?>">Terms</a> |
                        <a href="#">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /footer -->