<header>

    <!-- nav -->

    <nav class="navbar navbar-default navbar-fixed-top">

        <div class="container">

            <div class="row">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                        <span class="sr-only">Toggle navigation</span>

                        <i class="fa fa-bars"></i>

                    </button>

                    <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/images/logo.png" alt="Mefarnes"></a>

                </div>

                <div id="navbar" class="navbar-collapse collapse">

                    <ul class="nav navbar-nav">

                        <li><a href="<?= base_url() . 'how-it-works' ?>">How it Works</a></li>

                        <!--                        <li><a href="#">Success Stories</a></li>

                                                <li><a href="#">Press</a></li>-->

                        <li><a href="<?= base_url() . 'faq' ?>">Questions</a></li>

                        <li><a href="<?= base_url() . 'contact-us' ?>">Help</a></li>

                        <li class="dropdown user">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            	
                            	<?php if($this->session->userdata('userdata')->user_image !=''){?>
                            		<span class="d">
                            		<img src="<?= base_url() ?>uploads/campaign/gallery_images/thumb/<?= $this->session->userdata('userdata')->user_image ?>" alt="" style=" border: 1px solid;border-radius: 50%;height: 36px;width: 36px;"/>
                            		</span>
                            		<?php }else{?>
                            		<span class="char d" style="border: 1px solid;border-radius: 50%;height: 36px;width: 36px;margin-right:1px;">
                            		<?php if($this->session->userdata('userdata')->first_name !='') echo ucfirst($this->session->userdata('userdata')->first_name[0]); else echo ucfirst ($this->session->userdata('userdata')->org_name[0]) ?>
                            	</span>
                            		
                            	<?php }?>
    <?php if($this->session->userdata('userdata')->org_name !='') echo $this->session->userdata('userdata')->org_name; else echo $this->session->userdata('userdata')->first_name;?><i class="caret"></i></a>
                            	
                            		<?php //if($this->session->userdata('userdata')->first_name !='') echo Authenticate::getData('first_name') .' '. Authenticate::getData('last_name'); else echo $this->session->userdata('userdata')->org_name ?> 

                            <ul class="dropdown-menu">
								<?php if($this->session->userdata('profile_type')==2){?>
                                	<?php if($this->session->userdata('isProfile')>0){?>
                                    	<li><a href="<?= base_url() . 'organization-profile' ?>">Edit Profile</a></li>
                                        <li><a href="<?= base_url() . 'organization-profile' ?>/<?php echo $this->session->userdata('org_slug'); ?>">View Profile</a></li>
                                    <?php } else { ?>
                                    	<!-- <li><a href="<?= base_url() . 'organization-profile' ?>">Create Profile</a></li> -->
                                    <?php } ?>
                                <?php } else { ?>
                                    <!-- <li><a href="javascript:alert('Coming Soon....');">My Profile</a></li> -->
                                <?php } ?>

                                <li><a href="<?= base_url() . 'dashboard' ?>">My Campaigns</a></li>
                                
                              <!--  <li><a href="<?= base_url() . 'mydonation' ?>">My Donations</a></li> -->

                                <li><a href="<?= base_url() . 'settings' ?>">Settings</a></li>

                                <li><a href="<?= base_url() . '/home/withdraw' ?>">Withdraw</a></li>

                                <li><a href="<?= base_url() . 'logout' ?>">Log Out</a></li>

                            </ul>

                        </li> 

                        <li class="campaign"><a href="<?= base_url() . 'create-campaign' ?>"><span>Start a Campaign</span></a></li>

                    </ul>

                </div>

            </div>

        </div>

    </nav>

    <!-- /nav -->

</header>