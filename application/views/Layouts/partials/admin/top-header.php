   <!-- Main Header -->
  <header class="main-header"> 
    
    <!-- Logo --> 
    <a href="#" class="logo"> 
    <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini">Mefarnes-Manage</span> 
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg">Mefarnes-Manage</span> </a> 
    
    
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a> 
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
         
          <!-- User Account Menu -->
          <li class="dropdown user user-menu"> 
            <!-- Menu Toggle Button --> 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
            <!-- The user image in the navbar--> 
            <img src="<?= base_url() ?>assets/admin/dist/img/user.png" class="user-image" alt="User Image"/> 
            <!-- hidden-xs hides the username on small devices so only the image appears. --> 
            <span class="hidden-xs"><?//= Authenticate::getData('first_name').' '.Authenticate::getData('last_name') ?></span> </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu --> 
              
              <!-- Menu Body -->
              <li class="user-body">
                <div class="pull-left"> <a href="<?=  base_url('admin/change-password') ?>" class="btn btn-default btn-flat">Change Password</a> </div>
                <div class="pull-right"> <a href="<?=  base_url('admin/auth/logout') ?>" class="btn btn-default btn-flat">Sign out</a> </div>
              </li>
              <!-- Menu Footer-->
              
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li> <a href="#" data-toggle="control-sidebar"><i class="fa fa-outdent"></i></a> </li>
        </ul>
      </div>
    </nav>
  </header>