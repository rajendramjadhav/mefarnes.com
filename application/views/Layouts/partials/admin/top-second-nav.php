<!-- second top nav -->

<section>
    <nav class="navbar second-nav">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"> <i class="fa fa-bars"></i> </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav inner-nav">
                <li class="active"><a href="<?= base_url('admin/dashboard') ?>">Dashboard <span class="sr-only">(current)</span></a></li>

                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Campaigns <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= base_url('admin/campaigns/active') ?>">Active Campaigns list</a></li>
                        <li><a href="<?= base_url('admin/campaigns/expired') ?>">Expired Campaigns list</a></li>
                        <li><a href="<?= base_url('admin/campaigns/reported') ?>">Reported Campaigns list</a></li>
                        <li><a href="<?= base_url('admin/campaigns/campaign_home_page') ?>">Home Page Campaigns list</a></li>
                    </ul>
                </li>
                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= base_url('admin/users/active') ?>">Active/Inactive</a></li>
                        <li><a href="<?= base_url('admin/users/pending') ?>">Pending Activation</a></li>
                    </ul>
                </li>
                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Organizations<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Active</a></li>
                        <li><a href="#">Pending Activation</a></li>
                    </ul>
                </li>
                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= base_url('admin/categories') ?>">Campaign Categories</a></li>
                        <li><a href="<?= base_url('admin/pages') ?>">CMS Pages</a></li>
                        <li><a href="#">Withdral fees</a></li>
                        <li><a href="<?= base_url('admin/pages/contacted') ?>">Contact Through Site</a></li>
                        <li><a href="<?= base_url('admin/pages/pagelist') ?>">Meta Tags</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</section>
