<header>

    <!-- nav -->

    <nav class="navbar navbar-default navbar-fixed-top">

        <div class="container">

            <div class="row">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                        <span class="sr-only">Toggle navigation</span>

                        <i class="fa fa-bars"></i>

                    </button>

                    <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/images/logo.png" alt="Mefarnes"></a>

                </div>

                <div id="navbar" class="navbar-collapse collapse">

                    <ul class="nav navbar-nav">

                        <li><a href="<?= base_url() . 'how-it-works' ?>">How it Works</a></li>

                        <!--                        <li><a href="#">Success Stories</a></li>

                                                <li><a href="#">Press</a></li>-->

                        <li><a href="<?= base_url() . 'faq' ?>">Questions</a></li>

                        <li><a href="<?= base_url() . 'contact-us' ?>">Help</a></li>

                        <!--                        <li>-->

                        <?php if ($this->session->userdata('is_logged_in')) { ?>

                                <!--                               <a href="<?= base_url() . 'logout' ?>" title="Logout"><?php //echo 'Hello, '.$this->session->userdata('userdata') ->first_name.' '.$this->session->userdata('userdata') ->last_name;  ?></a>

                            -->

                            <!--                                <a href="javascript:;" type="button" data-toggle="dropdown">Hello, <?php if($this->session->userdata('userdata')->first_name!='')echo $this->session->userdata('userdata')->first_name . ' ' . $this->session->userdata('userdata')->last_name; else echo $this->session->userdata('userdata')->org_name ?>

                                                                <span class="caret"></span></a>

                                                            <ul class="dropdown-menu">

                                                                <li><a href="<?= base_url() . 'dashboard' ?>">My Account</a></li>

                                                                <li><a href="<?= base_url() . 'logout' ?>">Logout</a></li>

                                                            </ul>-->



                            <li class="dropdown user">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="char d" style=" border: 1px solid;border-radius: 50%;height: 36px;width: 36px;margin-right:1px;">

                                    <?php if($this->session->userdata('userdata')->first_name!='') echo ucfirst($this->session->userdata('userdata')->first_name[0]); else echo ucfirst($this->session->userdata('userdata')->org_name[0]) ?></span>

                                    <?php if($this->session->userdata('userdata')->first_name!='') echo $this->session->userdata('userdata')->first_name . ' ' . $this->session->userdata('userdata')->last_name; else echo  $this->session->userdata('userdata')->org_name?> <i class="caret"></i></a>

                                <ul class="dropdown-menu">
									<?php if($this->session->userdata('profile_type')==2){?>
                                    	<?php if($this->session->userdata('isProfile')==1){?>
                                            <li><a href="<?= base_url() . 'organization-profile' ?>">Edit Profile</a></li>
                                            <li><a href="<?= base_url() . 'organization-profile' ?>/<?php echo $this->session->userdata('org_slug'); ?>">View Profile</a></li>
                                        <?php } else { ?>
                                           <!-- <li><a href="<?= base_url() . 'organization-profile' ?>">Create Profile</a></li> -->
                                        <?php } ?>
									<?php } else { ?>
                                    	<li><a href="javascript:alert('Coming Soon....');">My Profile</a></li>
                                    <?php } ?>
                                    <li><a href="<?= base_url() . 'dashboard' ?>">My Campaigns</a></li>

                                    <li><a href="<?= base_url() . 'settings' ?>">Settings</a></li>

                                    <li><a href="<?= base_url() . '/home/withdraw' ?>">Withdraw</a></li>

                                    <li><a href="<?= base_url() . 'logout' ?>">Log Out</a></li>

                                </ul>

                            </li>  









                        <?php } else {

                            ?>



                            <li>  <a href="<?= base_url() . 'login' ?>">Sign In</a></li>

                        <?php } ?> 

                        <!--                        </li>-->

                        <li class="campaign">

                            <?php if ($this->session->userdata('is_logged_in')) { ?>

                                <a href="<?= base_url() . 'create-campaign' ?>"><span>Start a Campaign</span></a>

                            <?php } else { ?>

                                <a href="<?= base_url() . 'signup' ?>"><span>Start a Campaign</span></a>

                            <?php } ?>      

                        </li>

                    </ul>

                </div>

            </div>

        </div>

    </nav>

    <!-- /nav -->
    
    <script src="//load.sumome.com/" data-sumo-site-id="dc5fb1ca7cae671424284f983b79a7b210183902e9b504851d18aed00745d9f7" async="async"></script>

</header>