<!-- footer -->
<?php if (empty($type)) { ?>
    <div class="footer-top">
        <h2><!--Discover your campaign funding with your friends!-->One small step in this world, one GIANT step in the next!</h2>
        <!---<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br />sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>-->
        <p class="buttons">
            <a href="<?= base_url().'search'?>" class="big-button support"><span><i class="icon-mefarnes-logo-sm"></i><b>Support a Campaign</b></span></a>
            <a href="<?= base_url().'signup'?>" class="big-button"><span><i class="fa fa-bullhorn"></i> <b>Start your Campaign</b></span></a>
        </p>
    </div>
<?php } ?>
<footer>
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3>Quick Links</h3>
                            <div class="clearfix">
                                <ul class="links">
                                    <li><a href="<?= base_url() . 'how-it-works' ?>">How it Works</a></li>
                                    <li><a href="<?= base_url() . 'faq' ?>">Questions</a></li>
                                    <li><a href="<?= base_url() . 'contact-us' ?>">Help</a></li>
                                    <li><a href="<?= base_url() . 'login' ?>">Sign in</a></li>
                                    <li><a href="<?= base_url() . 'create-campaign' ?>">Start a campaign</a></li>
                                </ul>
<!--                                <ul class="links">
                                    <li><a href="#">Watch the Video</a></li>
                                    <li><a href="#">Mefares Reviews</a></li>
                                    <li><a href="#">Pricing &amp; Fees</a></li>
                                    <li><a href="#">Fundraising Ideas</a></li>
                                    <li><a href="#">We're Hiring!</a></li>
                                </ul>-->
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h3>Contact Info</h3>
                            <div class="contact">
                                <p><i class="fa fa-map-marker"></i>
                                    <span><!--Stratum House-->14 Aharon Brand<br>
                                        <!--Stafford Park 10<br>
                                        Telford TF3 3AB-->Jerusalem, Israel</span></p>
                                <p><i class="fa fa-phone"></i>
                                    <span><!--+44 (0) 1952 214000--->(617) 861-9235</span></p>
<!--                                <p><i class="fa fa-fax"></i>
                                    <span>+44 (0) 1952 214001</span></p>-->
                                <p><i class="fa fa-envelope"></i>
                                    <span><a href="mailto:admin@mefarnes.com"><!---enquiries@mefarnes.com--->admin@mefarnes.com</a></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <h3>Subscribe Our Newsletter</h3>
                    <p class="text-alert">Get Latest updates by us on your email.</p>
                    <div class="newsletter-form">
                        <span class="cover"><input type="email" name="subscription_email" id="subscription_email" placeholder="Enter your valid email here..." /></span>
                        <button type="submit" id="subscribe"><span>Subscribe Now!</span></button>
                    </div>
                    <div class="social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">&copy; 2016 Mefarnes, All Rights Reserved.</div>
                <div class="col-sm-6">
                    <div class="links">
                        <a href="#">Blog</a> |
                        <a href="#">Legal</a> |
                        <a href="<?= base_url() . 'terms' ?>">Terms</a> |
                        <a href="<?= base_url() . 'privacy' ?>">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /footer -->
<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

<script>
    $("#subscribe").click(function () {
        $(".text-alert").removeAttr('style');
        var subscription_email = $("#subscription_email").val();
        if (subscription_email !== '')
        {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "pages/subscription",
                dataType: 'json',
                data: {subscription_email: subscription_email, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        if (res.code == 200)
                        {
                            $(".text-alert").attr('style', 'color:green');
                            $(".text-alert").html('Subscribed successfully.');

                        } else if (res.code == 201) {
                            $(".text-alert").attr('style', 'color:red');
                            $(".text-alert").html('Invalid Email address format!');
                            $("#subscription_email").focus();
                        } else if (res.code == 202) {
                            $(".text-alert").attr('style', 'color:red');
                            $(".text-alert").html('You are already subscribed!');
                            $("#subscription_email").focus();
                        }
                        $("#csrf_token").val(res.token);
                    }
                }
            });
        } else {
            $(".text-alert").attr('style', 'color:red');
            $(".text-alert").html('Please enter Email address!');
            $("#subscription_email").focus();
        }
    });
</script>    