<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $title ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        <link href="<?= base_url() ?>assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />


        <!-- Theme style -->
        <link href="<?= base_url() ?>assets/admin/dist/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminPRO Skins. We have chosen the skin-blue for this starter
                  page. However, you can choose any other skin. Make sure you
                  apply the skin class to the body tag so the changes take effect.
        -->
        <link href="<?= base_url() ?>assets/admin/dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--sweet alert plugin-->
        <link href="<?= base_url() ?>assets/admin/plugins/sweetAlert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin/plugins/select2/css/select2.min.css') ?>"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin/plugins/bootstrap-colorselector/css/bootstrap-colorselector.css') ?>"/>
        <script>
            var POST_JQUERY_SCRIPTS = [];
        </script>
    </head>

    <body class="skin-blue sidebar-mini sidebar-collapse">
        <div class="wrapper"> 

            <!-- Main Header -->

            <?php include 'partials/admin/top-header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include 'partials/admin/left-side-nav.php'; ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper"> 
                <!-- Content Header (Page header) --> 

                <!-- second top nav -->
                <?php include 'partials/admin/top-second-nav.php'; ?>

                <!-- Main content -->
                <section class="content">
                    <?= $content ?>
                </section>
                <!-- Main content end --> 
            </div>

            <!-- Footer -->
            <?php include 'partials/admin/footer.php'; ?>

            <!-- right Sidebar -->
            <?php include 'partials/admin/right-side-nav.php'; ?>
        </div>

        <!-- jQuery 2.1.4 --> 
        <script src="<?= base_url() ?>assets/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script> 
        <!-- Bootstrap 3.3.2 JS --> 
        <script src="<?= base_url() ?>assets/admin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>

        <script src="<?= base_url() ?>assets/admin/plugins/sweetAlert2/sweetalert2.min.js" type="text/javascript"></script> 
        <script type="text/javascript" src="<?= base_url('assets/admin/plugins/select2/js/select2.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/admin/plugins/bootstrap-colorselector/js/bootstrap-colorselector.js') ?>"></script>
        <!-- AdminPRO App --> 
        <script src="<?= base_url() ?>assets/admin/dist/js/app.min.js" type="text/javascript"></script> 
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>

        // override jquery validate plugin defaults
            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $(function () {
<?php if (getMessage()) { ?>
                    var flashMsg = '<?= getMessage(); ?>';
                    var flashObj = $.parseJSON(flashMsg);
                    console.log(flashObj);
                    if (flashObj)
                    {
                        swal(flashObj)
    <?php if (getTask()) { ?>
                            .then(<?= gettask() ?>)
    <?php } ?>
                        ;
                    }

<?php } ?>

                // any script that needs to be run in document.ready();
                for (var i = 0; i < POST_JQUERY_SCRIPTS.length; i++)
                {
                    POST_JQUERY_SCRIPTS[i]();
                }

                // initialize form validation with this .validate-form class
                $(".validate-form").validate();

                $(".delete-item-data").on('click', function (e) {
                    e.preventDefault();
                    var _this = $(this);
                    askConfirm('', function () {
                        window.location.href = $(_this).closest('a').attr('href');
                    });
                });


            });

            function askConfirm(txt, yes_fun, no_fun)
            {
                return swal({
                    title: 'Are you sure?',
                    text: txt,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        yes_fun(isConfirm);
                    }
                    else
                    {
                        no_fun(isConfirm);
                    }
                })

            }

            function show_msg(flashObj) {
                swal(flashObj);
            }
            $(function () {
                $(".datepicker").datepicker(
                        {
                            dateFormat: 'dd/mm/yy',
                            minDate: -20, 
                            maxDate: "+1M +10D"
                });
                
                $(".datepicker-start").datepicker(
                        {
                            dateFormat: 'dd/mm/yy',
                            minDate: -20, 
                            maxDate: "0D"
                });
            });
        </script>
          <script>
  $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from-date" )
        .datepicker({
          defaultDate: "+0d",
          changeMonth: true,
          numberOfMonths: 1,
          maxDate:"+0d",
          minDate:"-60d"
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to-date" ).datepicker({
        defaultDate: "+0d",
        changeMonth: true,
        numberOfMonths: 1,
        maxDate:"60d"
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
  </script>
    </body>
</html>