<!-- content -->
<div id="content" class="page terms">
    <section class="header">
        <div class="container">
            <div class="row">
                <h2>Terms and conditions</h2>
            </div>
        </div>
    </section>

    <!-- form -->
    <div class="container">
        <div class="contents">
            <div class="row" style="padding-left: 5%">
            	<h3>Terms and conditions</h3>
				<p>PLEASE READ THESE TERMS AND CONDITIONS (“AGREEMENT”) BEFORE USING THE MEFARNES WEBSITE. (www.mefarnes.com “site” or “MEFARNES”). </p>
				<p> By using MEFARNES, (“services”) you agree to be bound by these Terms of Use ("Agreement"). This applies to both those individuals and corporations that list a campaign and those that donate to any campaign. 
				</p>
				
				<p> IF YOU DO NOT AGREE WITH THE TERMS AND CONDITIONS OF THIS AGREEMENT, DO NOT USE THIS WEBSITE, EITHER TO REGISTER A CAMPAIGN OR TO DONATE TO A CAMPAIGN.
				</p>
				
				<p> THE TERMS AND CONDITIONS OF THIS AGREEMENT ARE SUBJECT TO MODIFICATION AND CHANGE BY MEFARNES AT ANY TIME, EFFECTIVE UPON NOTICE TO YOU, WHICH NOTICE SHALL BE DEEMED TO HAVE BEEN PROVIDED UPON OUR POSTING OF THE CURRENT VERSION OF THIS AGREEMENT ON THE WEBSITE. YOUR CONTINUED USE OF THIS SITE OR THE SERVICES AFTER THE POSTING OF REVISIONS TO THIS AGREEMENT WILL CONSTITUTE YOUR ACCEPTANCE OF SUCH REVISIONS. PLEASE CONSULT THE END OF THIS AGREEMENT TO DETERMINE WHEN THE AGREEMENT WAS LAST REVISED.
				</p>
				
				<p> In these conditions the following words have the following meanings unless the context requires otherwise:
				</p>
				
				<p> "We, Us, Our" means MEFARNES. 
				</p>
				
				<p> "Agreement" means this agreement incorporating these terms and conditions.
				</p>
				
				<p>  “Users” means those that create a campaign (“campaign coordinator”) or those that contribute to a campaign (“donor”).
				</p>
				
				<p> “You” means campaign coordinator or donor.
				</p>
				
				<p> “Services” means the use of the site for creating a campaign and use of the site for donating to a campaign.  The Services permit a campaign coordinator to create and post to the site a campaign to raise funds for a designated cause (personal, charitable, start-up) (“campaign”) and to accept donations from those wishing to contribute to the cause (“donors”). 
				</p>
				
								
				<h3>Member Account</h3>
				<p>You must be at least 18 years old to create an account with MEFARNES.  You agree to provide and maintain true, accurate, current and complete information about yourself. You are responsible for maintaining the confidentiality of your password and account, if any, and are fully responsible for any and all activities that occur under your password or account. You agree to immediately notify MEFARNES of any unauthorized use of your password or account or any other breach of security.  MEFARNES will not be liable for any loss or damage arising from your failure to comply with this Section. </p>
				
				<h3>Account Content</h3>
				<p>Our Service allows you to post, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content") on our site.  You are solely responsible for all content that you post as a campaign on your account.  As a campaign coordinator you represent, warrant, and covenant that all information you provide in connection with a Campaign is true, accurate, complete, and not otherwise designed to mislead, defraud, or deceive any user and all donations contributed to your Campaign will be used solely as described in the Campaign.  Campaign coordinators will not post any content that is illegal or prohibited by law.  With respect to the content a campaign coordinator posts on the site you represent and warrant that you own all right, title and interest in and to, or otherwise have all necessary rights, permissions and consents to fully display such Content, including, without limitation, as it concerns all copyrights, trademark rights and rights of publicity or privacy related thereto. Campaign coordinators will not post any content that infringes any intellectual property or other proprietary rights of any party or that campaign coordinator does not have a right to post under any law or under any contractual or fiduciary relationship.  Campaign coordinators will not post content including, but not limited to, content that is fraudulent or misleading, unlawful, that contains pornography or other sexual content, that is offensive or graphic, that promotes hate, violence, harassment, discrimination, terrorism, or that violates the rights of any party relating to race, sex, ethnicity, national origin, religious affiliation, or serious disabilities or diseases or that discloses private information of any third party.   </p>
				
				<p> Campaign coordinators will not seek funding for any venture that includes illegal activity or gambling, debt collection, credit repair, binary options, stock trading, contests, promotions, political campaigning, or the like.  	
				</p>
				
				<p>MEFARNES does not review content, but MEFARNES will have the right to refuse or remove any content that is available via the Services. MEFARNES has the right to remove any content that violates these Terms and Conditions or is deemed by MEFARNES in its sole discretion, to be otherwise objectionable.  MEFARNES reserves the right to take appropriate action against anyone who violates any of the terms this agreement, including, without limitation, removing the offending content, terminating the account, stopping payments to any such campaign, or taking appropriate legal action. </p>
				
				<h3>Liability for Content of Campaign</h3>
				<p>MEFARNES is not responsible for any information provided by a campaign coordinator and hereby disclaims all liability in this regard to the fullest extent permitted by applicable law. MEFARNES makes no guarantee, explicit or implied, that any information provided through the Services by a campaign coordinator is accurate. MEFARNES does not and cannot verify the information that a Campaign coordinator posts, nor do we guarantee that the donations will be used in accordance with the purpose set forth in the campaign. MEFARNES assumes no responsibility to verify whether the donations are used in accordance with any applicable laws.  Every donor is responsible to make his or her own determination to contribute to any campaign.  </p>
				
				<h3>Third-Party Material</h3>
				<p>The Services may provide or third parties may provide, links or other access to other sites, services and materials, not owned by MEFARNES.  MEFARNES has no control over such sites, services and materials and MEFARNES is not responsible for such sites, services and materials. You acknowledge and agree that MEFARNES will not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any content, or services available on or through any such third party site, service or materials. Any dealings you have with third parties found while using the Services are between you and the third party, and you agree that MEFARNES is not liable for any loss or claim that you may have against any such third party.    </p>
				
				<h3>Success of campaign </h3>
				<p>MEFARNES does not guarantee that a campaign coordinator will receive donations or reach the goal set for the campaign.  MEFARNES disclaims any liability or responsibility for the success of any Campaign.   </p>
				
				<h3>Use of site </h3>
				<p>You will not attempt to circumvent any filtering or security measure installed to protect the site, its users and third parties.  You will not prevent other users from utilizing the site or collect information about users from the site, including but not limited to email addresses and you will not send unsolicited emails to other users or donors to the site.     </p>
				
				<h3>Use by Donor </h3>
				<p>In order to contribute to a Campaign a Donor will be required to provide MEFARNES information regarding its credit card or other payment instrument. As a Donor, you represent and warrant to MEFARNES that such credit or payment information is true and that you are authorized to use the payment instrument. Donors agree that a certain minimum Donation amount may apply, and that all Donation payments are final and will not be refunded unless MEFARNES, in its sole discretion, agrees to issue a refund.    </p>
				
				<h3>Fees </h3>
				<p> Although there are no fees to set up a Campaign, a percentage of each Donation will be charged as fees for our Services. MEFARNES retains a flat 5% percentage of each Donation contributed to a Campaign.   An additional payment processing fee is also deducted from each Donation and is payable directly to the credit card processing companies.  Certain aspects of our Services may also require you to register with (and agree to the terms of) third-party service providers (e.g., payment processors or charitable donation processors) in order to utilize such Services.  MEFARNES is not a party to any such relationships and disclaim any responsibility or liability for the performance by such third parties. MEFARNES may exchange information with such third-party services and where such information consists of Personal Data it will only be shared in accordance with our Privacy Policy. Please refer to the privacy policy for information on how MEFARNES collects, uses and discloses personal information.      </p>
				
				<p> Fees: Although there are no fees to set up a Campaign, a percentage of each Donation will be charged as fees for our Services. MEFARNES retains a flat 5% percentage of each Donation contributed to a Campaign.   An additional payment processing fee is also deducted from each Donation and is payable directly to the credit card processing companies.  Certain aspects of our Services may also require you to register with (and agree to the terms of) third-party service providers (e.g., payment processors or charitable donation processors) in order to utilize such Services.  MEFARNES is not a party to any such relationships and disclaim any responsibility or liability for the performance by such third parties. MEFARNES may exchange information with such third-party services and where such information consists of Personal Data it will only be shared in accordance with our Privacy Policy. Please refer to the privacy policy for information on how MEFARNES collects, uses and discloses personal information.You acknowledge that by contributing a donation to a campaign, the donor is agreeing to any and all applicable terms and conditions set forth by the processing company, in addition to these Terms of Service. Fees are deducted directly from each Donation, and will not be reflected in the amount which a Campaign coordinator can withdraw from the Campaign or which is directed to a Charity. MEFARNES reserves the right to change its fee pricing from time to time. Your continued use of the Services after the Fee change becomes effective constitutes your acceptance of the updated Fees. </p>


				<h3> Payment of Taxes</h3>
				<p>MEFARNES makes no representation about any taxes that may apply to the donations received by any campaign.  It is solely the responsibility of any campaign coordinator to pay taxes, if any, to the appropriate tax authority.    </p>
				
				<h3>Tax deduction </h3>
				<p> MEFARNES is not a tax deductible charitable organization.  However certain campaigns may be by charitable organizations in which case the organization may issue a charitable donation letter and you may be entitled to a tax deduction.  MEFARNES does not guarantee any tax deduction and makes no representation that your donation to any cause will be tax deductible.  MEFARNES does not verify the tax exempt status of any organization claiming to be a non- profit.   </p>
				
				<h3> Withdrawing Donations from a Campaign</h3>
				<p> A campaign coordinator may withdraw donations to a Campaign at any time up to the full amount of all donations credited to the Campaign, less Fees and subject to any holds that may be placed on the campaign account. A campaign coordinator may withdraw donations by electronic wire transfer to a bank account.  MEFARNES does not guarantee that withdrawals will be available to a campaign coordinator within any specific time frame, and MEFARNES expressly disclaims any and all responsibility for any delay or inability to access and use withdrawals at any specified time, and any consequences arising from such delay or inability. All campaign coordinators are responsible for ensuring that the information provided to MEFARNES in order to process a withdrawal, including bank account information, is accurate and up to date.   </p>
				
				<h3>Account Holds </h3>
				<p> From time to time, MEFARNES may place a hold on a Campaign account restricting withdrawals by a Campaign coordinator. MEFARNES may place a hold if we have reason to believe that information provided by a Campaign coordinator is false, misleading, or fraudulent, or that funds are being used in a prohibited manner, or a Campaign coordinator has violated these Terms and Conditions, or as otherwise required under applicable laws and regulations.  </p>
				
				<h3>Content Transmitted Through the Services </h3>
				<p> By posting any Content you hereby grant and will grant MEFARNES a nonexclusive, worldwide, royalty free, perpetual, irrevocable license to use, copy, display, publish, distribute, store, modify and otherwise use your Content in connection with the operation of the Services or the promotion, advertising or marketing thereof, in any form. Without limiting the foregoing, if any Content contains your name, image or likeness, you hereby release and hold harmless MEFARNES and its employees, from all claims for invasion of privacy, publicity or libel and any liability for claims made by you in connection with your Content, name, image or likeness.    </p>
				
				<p>You acknowledge and agree that MEFARNES may preserve content and may also disclose content if required to do so by law or in the good-faith belief that such preservation or disclosure is reasonably necessary to comply with legal process, applicable laws or government requests; enforce these Terms and Conditions; respond to claims that any content violates the rights of third parties; or protect the rights, property, or personal safety of MEFARNES its users or the public. </p>
				
				<h3>Copyright Policy </h3>
				<p>If you are a copyright owner, or authorized agent and believe that the copyrighted work has been copied in a way that constitutes copyright infringement, please submit your claim via email to admin@mefarnes.com, with the subject line: "Copyright Infringement" and include in your claim a detailed description of the alleged Infringement as detailed under the Digital Millennium Copyright Act (DMCA). (see 17 U.S.C 512(c)(3) for further detail.   </p>
				
				<h3>Intellectual Property </h3>
				<p> The Service and its original content are and will remain the exclusive property of Mefarnes.com and its licensors. The Service is protected by copyright, trademark, and other laws of both the United States and foreign countries. Our trademarks may not be used in connection with any product or service without the prior written consent of Mefarnes.com.  </p>
				
				<h3>Indemnity and Release </h3>
				<p>You agree to release, indemnify and hold MEFARNES and its affiliates and their officers, employees, directors and agents harmless from and against any and all claims, losses, damages, expenses, including reasonable attorneys` fees, costs, awards, fines, rights, actions of any kind and injury (including death) arising out of or relating to your use of the Services, any Donation or Campaign, any User Content, your connection to the Services, your violation of these Terms and Conditions or your violation of any rights of another, or the rights of any third party.     </p>
				
				<h3>Disclaimer of Warranties </h3>
				<p>  YOUR USE OF THE SERVICE IS AT YOUR SOLE RISK. THE SERVICE IS PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. MEFARNES AND ITS AFFILIATES EXPRESSLY DISCLAIM AND EXCLUDE, TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, ALL WARRANTIES, CONDITIONS AND REPRESENTATIONS OF ANY KIND, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT.		 </p>
				
				<p> 
				
				MEFARNES AND ITS AFFILIATES MAKE NO WARRANTY OR CONDITION THAT THE SERVICE WILL MEET YOUR REQUIREMENTS, THAT THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE, AND FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS AND THAT THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICE WILL MEET YOUR REQUIREMENTS. 
				
				</p>
				
				<h3>
Limitation of Liability </h3>
				<p> YOU EXPRESSLY UNDERSTAND AND AGREE THAT, TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, NEITHER MEFARNES NOR ITS AFFILIATES WILL BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES, DAMAGES FOR LOSS OF PROFITS, DAMAGES FOR LOSS OF GOODWILL, DAMAGES FOR LOSS OF USE, LOSS OF DATA, OR OTHER INTANGIBLE LOSSES (EVEN IF MEFARNES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, RESULTING FROM THE USE OR THE INABILITY TO USE THE SERVICE; UNAUTHORIZED ACCESS TO OR ALTERATION OF DATA; STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE SERVICE; OR ANY OTHER MATTER RELATING TO THE SERVICE. TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL MEFARNES’ TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES (INCLUDING CONTRACT, NEGLIGENCE, STATUTORY LIABILITY OR OTHERWISE) OR CAUSES OF ACTION EXCEED THE AMOUNT YOU HAVE PAID MEFARNES IN THE LAST SIX (6) MONTHS, OR, IF GREATER, ONE HUNDRED DOLLARS ($100). 

  </p>
  <p> Some jurisdictions do not allow the exclusion of certain warranties or the limitation or exclusion of liability for incidental or consequential damages. accordingly, some of the limitations set forth above may not apply to you. if you are dissatisfied with any portion of the service or with these terms of service, your sole and exclusive remedy is to discontinue use of the services. 
  </p> 
				
				<h3>Termination of Account </h3>
				<p> MEFARNES reserves the right to terminate temporarily or permanently, the use of the site, without prior notice to you in our sole discretion.  MEFARNES will not be liable for any discontinuance of the Services in accordance with these Terms and Conditions.   </p>
				
				<h3>Dispute Resolution </h3>
				<p> All users, both campaign coordinators and donors agree to resolve any disputes or claims in a recognized Bet Din, either in the United States or in Israel.  In the event that the Bet Din does not resolve the conflict and permits legal action to be instituted, you agree to submit to the personal and exclusive jurisdiction of the state and federal courts located within New Jersey.  These Terms of Service will be governed by the laws of the State of New Jersey without regard to its conflict of law provisions.   </p>
				
				<h3>Non Waiver </h3>
				<p>The failure of MEFARNES to exercise or enforce any right or provision of these Terms of Service will not constitute a waiver of such right or provision. If any provision of these Terms of Service is found by a court of competent jurisdiction to be invalid, the other provisions of these Terms of Service remain in full force and effect.    </p>
				
				<h3>Sections </h3>
				<p>The section titles in these Terms of Service are for convenience only and have no legal or contractual effect.    </p>
				
				<h3>Notices </h3>
				<p>Notices to you may be made via either email or regular mail.   All contact with MEFARNES should be directed to admin@mefarnes.com    </p>
				
				<h3>Entire Agreement </h3>
				<p>  These Terms of Service constitute the entire agreement between you and MEFARNES and govern your use of the Services, superseding any prior agreements between you and MEFARNES with respect to the Services. You also may be subject to additional terms and conditions that may apply when you use third party services or third party content.  </p>
				
				<p>Last revised: August 25, 2016</p>
            	<!----
                <h3>Terms of Use</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non risus sagittis, lobortis ligula in, malesuada orci. Donec ac justo sed est accumsan lobortis ut tincidunt urna. Duis quis erat id leo mollis posuere eget at sem. Morbi pulvinar ut nulla posuere sagittis. Phasellus quis lorem sit amet elit suscipit dictum. Morbi ut magna interdum, suscipit lorem eget, volutpat diam. Etiam consequat vulputate magna, a dapibus justo bibendum eget. Nulla lacinia et est in maximus.</p>

                <h3>Definitions</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non risus sagittis, lobortis ligula in, malesuada orci. Donec ac justo sed est accumsan lobortis ut tincidunt urna. Duis quis erat id leo mollis posuere eget at sem. Morbi pulvinar ut nulla posuere sagittis.</p>

                <h3>Mefarnes is a Venue</h3>
                <p>Etiam ac ex purus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce laoreet lobortis dignissim. Praesent vestibulum, purus vulputate facilisis malesuada, felis ante fermentum lorem, vitae rhoncus dolor nisi quis nibh. Aliquam pretium, mauris quis tempus faucibus, est mi sollicitudin orci, vitae semper purus metus cursus sapien. Duis laoreet scelerisque eros, ut venenatis arcu blandit eget. Vivamus ac dignissim justo.</p>
                <p>Aenean accumsan elementum iaculis. Nullam vehicula consectetur euismod. Maecenas varius est vitae mauris sollicitudin euismod. Etiam rutrum placerat molestie. Nunc commodo porttitor faucibus. Mauris varius felis a velit feugiat feugiat. Donec placerat enim vitae commodo sollicitudin. Sed et pulvinar diam, vitae vestibulum ante. In dictum augue urna, in posuere elit lobortis vel. Nullam ullamcorper turpis eget est egestas maximus. Vivamus at pharetra ligula.</p>

                <h3>Eligibility to Use the Services</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet dignissim eros. Duis quis congue lacus, quis auctor tortor. Donec non vehicula lorem.</p>
                <ul class="list-arrow-bullet">
                    <li>Vivamus sagittis quam sit amet suscipit aliquet.</li>
                    <li>Curabitur ut odio ac tellus semper suscipit nec a ligula.</li>
                    <li>Vivamus tristique sapien non purus mollis, sed sagittis lectus aliquet.</li>
                    <li>Phasellus vel justo egestas, pellentesque lectus et, molestie orci.</li>
                    <li>Curabitur elementum nisi sed odio eleifend, in euismod orci auctor.</li>
                    <li>Nulla placerat augue id felis aliquam semper.</li>
                </ul>

                <h3>Prohibited Campaigns</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet dignissim eros. Duis quis congue lacus, quis auctor tortor. Donec non vehicula lorem.</p>

                <h3>Prohibited Perks</h3>
                <ul class="list-arrow-bullet">
                    <li>Vivamus sagittis quam sit amet suscipit aliquet.</li>
                    <li>Curabitur ut odio ac tellus semper suscipit nec a ligula.</li>
                    <li>Vivamus tristique sapien non purus mollis, sed sagittis lectus aliquet.</li>
                    <li>Phasellus vel justo egestas, pellentesque lectus et, molestie orci.</li>
                </ul>

                <h3>Community Guidelines</h3>
                <ul class="list-arrow-bullet">
                    <li>Vivamus sagittis quam sit amet suscipit aliquet.</li>
                    <li>Curabitur ut odio ac tellus semper suscipit nec a ligula.</li>
                    <li>Vivamus tristique sapien non purus mollis, sed sagittis lectus aliquet.</li>
                    <li>Phasellus vel justo egestas, pellentesque lectus et, molestie orci.</li>
                </ul>
                ------------>
            </div>
        </div>
    </div>
    <!-- /form -->

    <div class="scroll-up"><i class="icon-long-arrow-up"></i></div>	

</div>
<!-- /content -->

<!-- content -->
<!-----
<div id="content" class="page terms">
    <section class="header">
        <div class="container">
            <div class="row">
                <h2>Terms of Use</h2>
            </div>
        </div>
    </section>
----->
    <!-- form -->
<!---
    <div class="container">
        <div class="contents">
            <div class="row">
                <h3>Terms of Use</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non risus sagittis, lobortis ligula in, malesuada orci. Donec ac justo sed est accumsan lobortis ut tincidunt urna. Duis quis erat id leo mollis posuere eget at sem. Morbi pulvinar ut nulla posuere sagittis. Phasellus quis lorem sit amet elit suscipit dictum. Morbi ut magna interdum, suscipit lorem eget, volutpat diam. Etiam consequat vulputate magna, a dapibus justo bibendum eget. Nulla lacinia et est in maximus.</p>

                <h3>Definitions</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non risus sagittis, lobortis ligula in, malesuada orci. Donec ac justo sed est accumsan lobortis ut tincidunt urna. Duis quis erat id leo mollis posuere eget at sem. Morbi pulvinar ut nulla posuere sagittis.</p>

                <h3>Mefarnes is a Venue</h3>
                <p>Etiam ac ex purus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce laoreet lobortis dignissim. Praesent vestibulum, purus vulputate facilisis malesuada, felis ante fermentum lorem, vitae rhoncus dolor nisi quis nibh. Aliquam pretium, mauris quis tempus faucibus, est mi sollicitudin orci, vitae semper purus metus cursus sapien. Duis laoreet scelerisque eros, ut venenatis arcu blandit eget. Vivamus ac dignissim justo.</p>
                <p>Aenean accumsan elementum iaculis. Nullam vehicula consectetur euismod. Maecenas varius est vitae mauris sollicitudin euismod. Etiam rutrum placerat molestie. Nunc commodo porttitor faucibus. Mauris varius felis a velit feugiat feugiat. Donec placerat enim vitae commodo sollicitudin. Sed et pulvinar diam, vitae vestibulum ante. In dictum augue urna, in posuere elit lobortis vel. Nullam ullamcorper turpis eget est egestas maximus. Vivamus at pharetra ligula.</p>

                <h3>Eligibility to Use the Services</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet dignissim eros. Duis quis congue lacus, quis auctor tortor. Donec non vehicula lorem.</p>
                <ul class="list-arrow-bullet">
                    <li>Vivamus sagittis quam sit amet suscipit aliquet.</li>
                    <li>Curabitur ut odio ac tellus semper suscipit nec a ligula.</li>
                    <li>Vivamus tristique sapien non purus mollis, sed sagittis lectus aliquet.</li>
                    <li>Phasellus vel justo egestas, pellentesque lectus et, molestie orci.</li>
                    <li>Curabitur elementum nisi sed odio eleifend, in euismod orci auctor.</li>
                    <li>Nulla placerat augue id felis aliquam semper.</li>
                </ul>

                <h3>Prohibited Campaigns</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet dignissim eros. Duis quis congue lacus, quis auctor tortor. Donec non vehicula lorem.</p>

                <h3>Prohibited Perks</h3>
                <ul class="list-arrow-bullet">
                    <li>Vivamus sagittis quam sit amet suscipit aliquet.</li>
                    <li>Curabitur ut odio ac tellus semper suscipit nec a ligula.</li>
                    <li>Vivamus tristique sapien non purus mollis, sed sagittis lectus aliquet.</li>
                    <li>Phasellus vel justo egestas, pellentesque lectus et, molestie orci.</li>
                </ul>

                <h3>Community Guidelines</h3>
                <ul class="list-arrow-bullet">
                    <li>Vivamus sagittis quam sit amet suscipit aliquet.</li>
                    <li>Curabitur ut odio ac tellus semper suscipit nec a ligula.</li>
                    <li>Vivamus tristique sapien non purus mollis, sed sagittis lectus aliquet.</li>
                    <li>Phasellus vel justo egestas, pellentesque lectus et, molestie orci.</li>
                </ul>
            </div>
        </div>
    </div>
---->
    <!-- /form -->
<!---

    <div class="scroll-up"><i class="icon-long-arrow-up"></i></div>	

</div>
---->
<!-- /content -->