<!-- content -->
<div id="content" class="faq-main">
	<!-- faq-header -->
	<section class="faq-header">
		<div class="container">
			<div class="page-header text-center">
				<h1>How we can help you!</h1>
				<p>Want to ask us a question instead? <a href="contact-us">[ Contact Us ]</a></p>
				<div class="faq-search">
					<div class="row">
					  <div class="col-sm-8 col-sm-offset-2">
					    <div class="input-group">
					    	<input type="text" class="form-control" placeholder="Ask your question here">
					    	<span class="input-group-btn">
					        	<button class="btn btn-default" type="button">Go!</button>
					      	</span>
					    </div><!-- /input-group -->
					  </div><!-- /.col-sm-6 -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /faq-header -->

	<!-- faq-content -->
	<section class="faq-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="question-group">
						<h3>JEWISH CONCEPTS</h3>
						<div class="panel-group" id="getting-started" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingOne">
						      <h4 class="panel-title">
						        <a role="button" data-toggle="collapse" data-parent="#getting-started" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						         What is the Jewish view on giving?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						      <div class="panel-body">
Unlike philanthropy or charity, which is completely voluntary, tzedakah is a religious obligation, which must be performed regardless of financial standing, and must even be performed by poor people. 
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwo">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#getting-started" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
What is tzedaka?						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						      <div class="panel-body">
Tzedakah is not charity.  The literal meaning of the Hebrew word Tzedakah (צדקה)- from the root word (צדק, Tzedek)- is justice, fairness or righteousness. Tzedakah differs from charity because tzedakah is an obligation and charity is a spontaneous act of goodwill and a measure of generosity. Tzedakah is the religious obligation to do what is right and just. 
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingThree">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#getting-started" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
How important a mitzvah is giving?						        </a>
						      </h4>
						    </div>
						    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						      <div class="panel-body">
The mitzvah of Tzedakah is equal to all other mitzvohs: "שקולה צדקה כנגד כל המצוות" (ב"ב ט, א). 
</br>
By feeling other individual suffering and helping ease their lot in life, we repair this world and bring ultimate redemption.  Those who do not give Tzedakah are considered as though they worship idols because idolaters do not believe in the possibility of repairing the world- tikun olam. As it says: 
</br>
 "גדולה צדקה שמקרבת את הגאולה, שנאמר (ישעיה נו): כֹּה אָמַר ה' שִׁמְרוּ מִשְׁפָּט וַעֲשׂוּ צְדָקָה כִּי קְרוֹבָה יְשׁוּעָתִי לָבוֹא וְצִדְקָתִי לְהִגָּלוֹת" (ב"ב י, א)., שנאמר (ישעיה נו): כֹּה אָמַר ה' שִׁמְרוּ מִשְׁפָּט וַעֲשׂוּ צְדָקָה כִּי קְרוֹבָה יְשׁוּעָתִי לָבוֹא וְצִדְקָתִי לְהִגָּלוֹת" (ב"ב י, א).

						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingFour">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#getting-started" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
Does the Torah explicitly obligate us to give? 
						        </a>
						      </h4>
						    </div>
						    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
						      <div class="panel-body">
According to the Rambam giving Tzedakah is a commandment and not giving is a sin as follows:
</br>
א  מצות עשה ליתן צדקה לעניי ישראל כפי מה שראוי לעני, אם הייתה יד הנותן משגת--שנאמר "פתוח תפתח את ידך, לו" (דברים טו,ח), ונאמר "והחזקת בו, גר ותושב וחי עימך" (ויקרא כה,לה), ונאמר "וחי אחיך, עימך" (ויקרא כה,לו).  [ב] וכל הרואה עני מבקש, והעלים עיניו ממנו, ולא נתן לו צדקה--עובר בלא תעשה, שנאמר "לא תאמץ את לבבך, ולא תקפוץ את ידך, מאחיך, האביון" (דברים טו,ז). (הלכות מתנות עניים פרק ז)						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingFive">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#getting-started" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
How much is the one in need supposed to receive?
					        </a>
						      </h4>
						    </div>
						    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
						      <div class="panel-body">
						    </br>
According to the Rambam you give Tzedakah according to need as follows:
</br>
ב  [ג] לפי מה שחסר העני, אתה מצווה ליתן לו--אם אין לו כסות, מכסין אותו; אין לו כלי בית, קונין לו כלי בית; אין לו אישה, משיאין לו אישה; ואם הייתה אישה, משיאין אותה לאיש:  אפילו היה דרכו של זה העני לרכוב על הסוס ועבד רץ לפניו, והעני וירד מנכסיו--קונין לו סוס לרכוב עליו ועבד לרוץ לפניו, שנאמר "די מחסורו, אשר יחסר לו" (דברים טו,ח); ומצווה אתה להשלים חסרונו, ואין אתה מצווה לעשרו. (הלכות מתנות עניים פרק ז)	
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingSix">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#getting-started" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
What causes do we have an obligation to fund?						        </a>
						      </h4>
						    </div>
						    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
						      <div class="panel-body">
The Rambam gives as an example: For an orphan who is about to get married, you rent a house and provide him with a bed and household items and then marry him off. In the words of the Rambam
</br>
ג  [ד] יתום שבא להשיאו אישה--שוכרין לו בית, ומציעים לו מיטה וכל כלי תשמישו; ואחר כך משיאין לו אישה.

						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingSeven">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#getting-started" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
How much should you give?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
						      <div class="panel-body">
If a poor person comes for help and you do not have enough to provide for his needs, you can give up to 20% of your net worth and the average person gives 10% but those who give less are deemed stingy. At the very least one is obligated to give the half shekel once a year, even one who is poverty stricken must give to another.

</br>

ד  [ה] בא עני ושאל די מחסורו, ואין יד הנותן משגת--נותן לו כפי השגת ידו.  וכמה:  עד חמיש נכסיו, מצוה מן המובחר; ואחד מעשרה בנכסיו, בינוני; פחות מכאן, עין רעה.  ולעולם אל ימנע אדם עצמו משלישית השקל בשנה; וכל הנותן פחות מזה, לא קיים מצוה.  ואפילו עני המתפרנס מן הצדקה, חייב ליתן צדקה לאחר. (הלכות מתנות עניים פרק ז)						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingEight">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#getting-started" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
Must we verify the need?						        </a>
						      </h4>
						    </div>
						    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
						      <div class="panel-body">
According to the Rambam, if a poor person, who you not know, says that he is starving, you feed him and do not examine him that maybe he is deceiving you. If he is without clothes and asks you to clothe him, you do verify his need. If you know him, you dress him immediately according to his status in life and do not verify his need. In the words of the Rambam:

</br> ה  [ו] עני שאין מכירין אותו, ואמר רעב אני, האכילוני--אין בודקין אחריו שמא רמאי הוא, אלא מפרנסין אותו מיד.  היה ערום, ואמר כסוני--בודקין אחריו שמא רמאי הוא; ואם היו מכירין אותו--מכסין אותו לפי כבודו מיד, ואין בודקין אחריו. (הלכות מתנות עניים פרק ז)


						      </div>
						    </div>
						  </div>
						</div>
						<a class="view-all" href="">[ View all ]</a>
					</div>
					<!-- /question-group -->

					<div class="question-group">
						<h3>Running a successful campaign</h3>
						<div class="panel-group" id="successful-campaign" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingNine">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#successful-campaign" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
What if the person in need does not want to accept charity?						        </a>
						      </h4>
						    </div>
						    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
						      <div class="panel-body">
A person in need who does not want to accept tzedakah, you misrepresent to him that the donation is a present or a loan.

</br>ח  [ט] עני שאינו רוצה ליקח צדקה, מערימין עליו ונותנין לו לשם מתנה או לשם הלוואה; ועשיר המרעיב את עצמו, ועינו צרה בממונו שלא יאכל ממנו ולא ישתה--אין משגיחין בו. (הלכות מתנות עניים פרק ז)

						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#successful-campaign" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
What are the different levels of tzedakah?



						        </a>
						      </h4>
						    </div>
						    <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
						      <div class="panel-body">
</br>
 The Rambam (Maimonides) lists his Eight Levels of Giving, as written in the Mishneh Torah, Hilkhot matanot aniyim ("Laws about Giving to Poor People"), Chapter 10:7–14:
</br>

1.	Giving an interest-free loan to a person in need; forming a partnership with a person in need; giving a grant to a person in need; finding a job for a person in need; so long as that loan, grant, partnership, or job results in the person no longer living by relying upon others.
2.	Giving tzedakah anonymously to an unknown recipient via a person (or public fund) which is trustworthy, wise, and can perform acts of tzedakah with your money in a most impeccable fashion.
3.	Giving tzedakah anonymously to a known recipient.
4.	Giving tzedakah publicly to an unknown recipient.
5.	Giving tzedakah before being asked.
6.	Giving adequately after being asked.
7.	Giving willingly, but inadequately.
8.	Giving "in sadness" (giving out of pity): It is thought that Maimonides was referring to giving because of the sad feelings one might have in seeing people in need (as opposed to giving because it is a religious obligation). Other translations say "Giving unwillingly."						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingEleven">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#successful-campaign" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
Is giving money to a start-up even charity?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
						      <div class="panel-body">
Crowdfunding a start-up is the highest form of tzedakah as your money supports a friend to be self-sufficient.						      </div>
						    </div>
						  </div>
						  
						
						  
						  
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwelve">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#successful-campaign" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
How is Mefarnes the highest level of tzedakah?						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
						      <div class="panel-body">
The Rambam says that, while the second highest form of tzedakah is to give donations anonymously to unknown recipients, the highest form is to give a gift, loan, or partnership that will result in the recipient supporting himself instead of living upon others. 						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingThirteen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#successful-campaign" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
So what’s in it for Number 1? What do I get by donating?						        </a>
						      </h4>
						    </div>
						    <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
						      <div class="panel-body">
Your life! Tzedakah saves you from death in the words of Shlomo Hamelech:

</br>
"לא יועילו אוצרות רשע, וצדקה תציל ממות" (משלי י ב):
"לא יועיל הון ביום עברה, וצדקה תציל ממות" (משלי יא ד):
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingFourteen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#successful-campaign" href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
And what is the big picture behind Mefarnes?						        </a>
						      </h4>
						    </div>
						    <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
						      <div class="panel-body">
חסד עולם יבנה
"כל המקיים נפש אחת מישראל מעלה עליו הכתוב כאילו קיים עולם מלא" (משנה סנהדרין ד, ה)
"וְכִי יָמוּךְ אָחִיךָ וּמָטָה יָדוֹ עִמָּךְ וְהֶחֱזַקְתָּ בּוֹ". (ויקרא כה, לה): 
Support by your friend so that he can be self-sufficient.
And if you are in need, give the donor the opportunity to save his life! 
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingFifteen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#successful-campaign" href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
In a nutshell, what is the point?						        </a>
						      </h4>
						    </div>
						    <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
						      <div class="panel-body">

<ul>
<li>Why was poverty created? For there to be interdependence between people.</li>
<li>Jews don&rsquo;t call it charity &ndash; that is an IRS term.</li>
<li>Jews call it tzedakah &ndash; from the word tzedek &ndash; justice. Some of the pleas for financial assistance may be &ldquo;charities&rdquo; and IRS tax deductible. Others are not.</li>
<li>A full ten percent of what you get in wages or gross receipts from business does not belong to you. It is &ldquo;just&rdquo; for you to return it to the just owners, the poor, infirm, and those in need as defined by the crystal clear lines in Jewish law.</li>
<li>Tzedakah unlike the non-Jewish concept of charity, is not an altruistic option but an obligation.</li>
<li>Mefarnes is the highest of eight levels that the Rambam delineates in the Jewish laws of &ldquo;charity&rdquo; providing the wherewithal to be self-sufficient.</li>
<li>While helping an individual become self- sufficient is the ideal, that does not abdicate the obligation to give the lower levels of charity &ndash; a starving orphan needs food and the need is immediate and urgent &ndash; the child is collapsing at your door &ndash; you do not have the luxury to become anonymous or set him up in business &ndash; he needs food or he dies.</li>
<li>Yeshivas, mikvehs, and communal Jewish service organizations may charge but frequently there is a severe shortfall from what people can afford to pay. If the deficit is not supplemented by the public support, these organizations may be forced to close their doors.</li>
<li>Fund raisers can keep as much as 50% of what they collect for themselves, which by Jewish law may be legitimate. Here 92% of your donation goes straight to the person, yeshiva, etc. in need rather than to the fundraiser. We retain only 5% and the cost of credit card processing (2.9% plus .30 is deducted). We also save you all costs of advertising, mailings and door to door soliciting.&nbsp;</li>
<li>It is best not to give all the tzedakah to one poor person but to spread it so that others in need benefit.</li>
<li>Here is your opportunity to bequeath that 10 percent of your earnings or business gross receipts to those who in your judgment would benefit the most, support the causes you most strongly believe in and actualize start-ups.</li>
</ul>
						    </div>
						  </div>
					
						<a class="view-all" href="">[ View all ]</a>
					</div>
					
				<!-- /question-group --> 
				
				</div>
				<!-- /col-sm-6 -->
				 
				</div> 
			</div>
				<div class="col-sm-6">
<!-----------
					<div class="question-group">
						<h3>Running a successful campaign2</h3>
						<div class="panel-group" id="receiving-donations" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingSeventeen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
What points should an individual campaign stress? 						        </a>
						      </h4>
						    </div>
						    
						   
						    <div id="collapseSeventeen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeventeen">
						      <div class="panel-body">
<ol>
<li>It should always be a story.</li>
<li>It should be personal.</li>
<li>It should include emotions. Share your emotional upset- pain- over the problem.</li>
<li>It should be logical. Be clear on what you need the money for.</li>
<li>Be clear that you do not have the money and can&rsquo;t get it from close family and friends.</li>
<li>Be specific on how the money is going to be spent.</li>
</ol>						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingEighteen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseEighteen" aria-expanded="false" aria-controls="collapseEighteen">
What points should an organization campaign stress? 						        </a>
						      </h4>
						    </div>
						    <div id="collapseEighteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEighteen">
						      <div class="panel-body">
<ol>
<li>It should always be a story.</li>
<li>Share your emotional upset over the problem.</li>
<li>Be logical-what you need the money for.</li>
<li>Be clear that you do not have the money.</li>
<li>Be specific on how the money is going to be spent.</li>
<li>Be clear on how it will impact the community.</li>
</ol>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingNineteen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseNineteen" aria-expanded="false" aria-controls="collapseNineteen">
What points should start-up campaigns stress?						        </a>
						      </h4>
						    </div>
						    <div id="collapseNineteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNineteen">
						      <div class="panel-body">
<ol>
<li>It should tell people what the issue is.</li>
<li>It should include a clear and concise statement of what you&rsquo;re doing to solve the issue.</li>
<li>It should explain the impact- how what you&rsquo;re doing will make a difference.</li>
<li>It should set forth how much you need to raise.</li>
<li>It should state how you are going to spend the money.</li>
</ol>

						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwenty">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseTwenty" aria-expanded="false" aria-controls="collapseTwenty">
Can I donate from any country?						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwenty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwenty">
						      <div class="panel-body">
Yes, pay pal accepts donations in various currencies. 						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyOne">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseTwentyOne" aria-expanded="false" aria-controls="collapseTwentyOne">
Can I donate by check?						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyOne">
						      <div class="panel-body">
Yes you can mail a check to our address posted on the site and we will add it to the campaign funds. Checks are to be made out to… 						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyTwo">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseTwentyTwo" aria-expanded="false" aria-controls="collapseTwentyTwo">
Are donations tax deductible?						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyTwo">
						      <div class="panel-body">
All donations to charity are tax deductible. … 						      </div>
						    </div>
						  </div>
						  
						
						
						<a class="view-all" href="">[ View all ]</a>
					</div>
----->
<div class="question-group">
						<h3>Receiving Donations</h3> 
						<div class="panel-group" id="receiving-donations" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingSeventeen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
						          Is my country supported?
						        </a>
						      </h4>
						    </div>
						    
						   
						    <div id="collapseSeventeen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeventeen">
						      <div class="panel-body">
						        At this stage, we use a highly reliable and secure service -  WePay the same service used by GoFundMe. WePay receives the donations and puts it directly into bank accounts.   Due to government regulations, this service requires a US EIN number and a US bank account.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingEighteen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseEighteen" aria-expanded="false" aria-controls="collapseEighteen">
						          Can donors in other countries donate to my campaign?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseEighteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEighteen">
						      <div class="panel-body">
						       Yes, anyone from any country with an international credit card can donate.  Donations are accepted in various currencies.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingNineteen">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseNineteen" aria-expanded="false" aria-controls="collapseNineteen">
						          Will my donors get charged any fees?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseNineteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNineteen">
						      <div class="panel-body">
						        Donors are not charged any fees. The amount you donate is the amount you are charged by your credit card. 
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwenty">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseTwenty" aria-expanded="false" aria-controls="collapseTwenty">
						          Can people donate from their phones?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwenty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwenty">
						      <div class="panel-body">
						        Definitely yes.  Mefarnes is a mobile phone friendly site.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyOne">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseTwentyOne" aria-expanded="false" aria-controls="collapseTwentyOne">
						          We raised money offline, can it be listed on our campaign?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyOne">
						      <div class="panel-body">
						        Tell us your story and put up the campaign for the rest of the money you need. If your campaign is already listed, we will add the amount of offline donations to the campaign goals.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyTwo">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseTwentyTwo" aria-expanded="false" aria-controls="collapseTwentyTwo">
						          Can donors give privately?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyTwo">
						      <div class="panel-body">
						        Periodically lump sums are deposited into the campaign recipient’s account. The individuals who donated are not revealed to the recipient. Donors can be listed anonymously on the site.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyThree">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseTwentyThree" aria-expanded="false" aria-controls="collapseTwentyThree">
						          How can I withdraw the money?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyThree">
						      <div class="panel-body">
						        When you set up a campaign with Mefarnes, you also open an account with WePay giving them your bank information and you instruct them how often to deposit the money into your account.  You can also login and withdraw the money before the scheduled date.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyFour">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#receiving-donations" href="#collapseTwentyFour" aria-expanded="false" aria-controls="collapseTwentyFour">
						          Can people give on a monthly basis?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyFour">
						      <div class="panel-body">
						        The campaigns are generally short term. Keep checking in. Your favorite organizations may be putting up additional calls depending on their immediate need.
						      </div>
						    </div>
						  </div>
						</div>
						<a class="view-all" href="">[ View all ]</a>
					</div>
					<!-- /question-group -->
					
					<div class="question-group">
						<h3>Other common questions</h3>
						<div class="panel-group" id="common-questions" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyFive">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#common-questions" href="#collapseTwentyFive" aria-expanded="false" aria-controls="collapseTwentyFive">
						          When will I appear in Mefarnes's search results?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyFive">
						      <div class="panel-body">
						        After your campaign is uploaded and all the information is complete your campaign will appear in the Mefarnes search results.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentySix">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#common-questions" href="#collapseTwentySix" aria-expanded="false" aria-controls="collapseTwentySix">
						          Where do I choose which category I want to appear in?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentySix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentySix">
						      <div class="panel-body">
						        When you put up a campaign, whether you are an organization or individual you can select one of the categories and it will be found by people looking to donate to specific types of causes.  If you want, we can help you decide which category to list your campaign in. 
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentySeven">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#common-questions" href="#collapseTwentySeven" aria-expanded="false" aria-controls="collapseTwentySeven">
						          How do I know it's safe to donate to someone?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentySeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentySeven">
						      <div class="panel-body">
						        It is relatively easy to verify any organization’s work and accomplishments.  With individuals faced with a sudden need it is often a friend or Rav who puts up the profile and you can ask the one putting up the campaign for more details. Mefarnes assumes no liability for either organizations or individuals who violate halacha and the public trust.  When someone asks for help, unless you have reason to suspect a fraud, you must trust that the need is real. 
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyEight">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#common-questions" href="#collapseTwentyEight" aria-expanded="false" aria-controls="collapseTwentyEight">
						          Does Mefarnes have a mobile app?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyEight">
						      <div class="panel-body">
						        The Mefarnes site is mobile friendly, but we plan to make a mobile app in the future.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwentyNive">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#common-questions" href="#collapseTwentyNive" aria-expanded="false" aria-controls="collapseTwentyNive">
						          Why does Mefarnes have to charge 5%?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwentyNive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyNive">
						      <div class="panel-body">
						        The credit card processing company charges about 2% and to cover our operational costs, we charge 5% so that the charity gets much more money than fund collectors who charge anywhere from 18% to 50% plus their expenses.
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingThirty">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#common-questions" href="#collapseThirty" aria-expanded="false" aria-controls="collapseThirty">
						          Do I have to be a non-profit to use this?
						        </a>
						      </h4>
						    </div>
						    <div id="collapseThirty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirty">
						      <div class="panel-body">
						        No. You do not have to be either a tax-exempt non-profit or a non-profit without tax exemption. Anyone, whether an organization or an individual, who needs money should have the opportunity to raise funds, whether as a charity or the highest form of tzedakah, to be set up in business so that they can be self-sufficient.  Our name Mefarnes covers the whole spectrum of tzedakah.
						      </div>
						    </div>
						  </div>
						</div>
						
						
						<a class="view-all" href="">[ View all ]</a>
					</div>
					<!-- /question-group -->
				</div>





			</div>
		</div>
	</section>
	
	
	<!-- /faq-header -->
</div>
<!-- /content -->
