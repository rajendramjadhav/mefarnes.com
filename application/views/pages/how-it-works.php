<!-- content -->
<div id="content" class="page how-it-works-page">
	<section class="header">
		<div class="container">
			<div class="row">
				<h2>How it works!</h2>
				<p>Quick, easy and secure connection between donors and those individuals and organizations in need. </p>
			</div>
		</div>
	</section>
	
	<div class="container">
		<!-- <section class="video">
			<h2>WATCH THE VIDEO</h2>
			<div class="video-box"><img src="<?= base_url().'assets/'?>images/data/how-it-works-video.jpg" alt="" /></div>
		</section> -->
		
		<section class="step-box one">
			<div class="content">
				<div class="step">STEP<b>1</b></div>
				<h2>Create Your Fundraising Campaign</h2>
				<h5>There’s no easier way to share your story and attract support.</h5>
			</div>
			<figure><img src="<?= base_url().'assets/'?>images/how-it-works-step-1.png" alt="" /></figure>
		</section>
		
		<section class="step-box two">
			<div class="content">
				<div class="step">STEP<b>2</b></div>
				<h2>Share with Family &amp; Friends</h2>
				<h5>Our built-in connections to Facebook, Twitter &amp; <br class="hidden-xs" />Email make sharing a breeze.</h5>
			</div>
			<figure><img src="<?= base_url().'assets/'?>images/how-it-works-step-2.png" alt="" /></figure>
		</section>
		
		<section class="step-box three">
			<div class="content">
				<div class="step">STEP<b>3</b></div>
				<h2>Easily Accept Donations</h2>
				<h5>Receive your money by requesting a check or bank transfer.</h5>
			</div>
			<figure><img src="<?= base_url().'assets/'?>images/how-it-works-step-3.png" alt="" /></figure>
		</section>
		
		<section class="step-box four">
			<div class="content">
				<div class="step">STEP<b>4</b></div>
				<h2>Enjoy the Results</h2>
				<h5>Make changes, post updates and <br class="hidden-xs" />send thank-you notes from your dashboard.</h5>
			</div>
			<figure><img src="<?= base_url().'assets/'?>images/how-it-works-step-4.png" alt="" /></figure>
		</section>
	</div>
</div>
<!-- /content -->