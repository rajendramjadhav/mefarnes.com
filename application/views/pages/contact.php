<!-- content -->
<div id="content" class="page contact-us">
    <section class="header">
        <div class="container">
            <div class="row">
                <h2>Contact Us</h2>
                <p>Let’s get in touch!</p>
            </div>
        </div>
    </section>

    <!-- form -->
    <div class="container">
        <br/>
        <?php if ($error != '') { ?>
            <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php } ?>
        <?php if ($this->session->flashdata('message') != '') { ?>
            <div class="alert alert-success">
            <?php echo $this->session->flashdata('message'); ?>

        </div>
    <?php } ?>
    <?php echo form_open(current_url()); ?> 
    <div class="form">
        <div class="row">
            <div class="col-sm-6">
                <label for="txtfname">First Name <span>[ *Please Enter First Name ]</span></label>
                <input type="text" id="fname" placeholder="First Name" name="fname" required="" pattern="[a-zA-Z]+" title="Please enter alphabetic character only"  x-moz-errormessage="Please enter alphabetic character only" value="<?php echo set_value('fname'); ?>" />
            </div>
            <div class="col-sm-6">
                <label for="txtlname">Last Name <span>[ *Please Enter Last Name ]</span></label>
                <input type="text" id="lname" placeholder="Last Name" name="lname" required="" pattern="[a-zA-Z]+" title="Please enter alphabetic character only"  x-moz-errormessage="Please enter alphabetic character only" value="<?php echo set_value('lfname'); ?>" />
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <label for="txtemail">Your Email Address <span>[ *Please Enter Email Address ]</span></label>
                <input type="email" id="email" name="email" required="" placeholder="Email address" value="<?php echo set_value('email'); ?> "/>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <label for="txtphone">Phone Number <span>[ *Please Enter Phone Number ]</span></label>
                <input type="tel" id="phone" name="phone"  value="<?php echo set_value('phone'); ?> " required="" pattern="\d{3}[\-]\d{3}[\-]\d{4}" placeholder="123-456-7890" x-moz-errormessage="Requested format is like 123-456-7890" title="Requested format is like 123-456-7890"  />
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <span class="select-box">
                    <select class="fancy-selectbox" name="category" id="category" required="">
                       <option value="">Select</option>
                       <?php foreach($categories as $category):?>
                       <option value="<?= $category -> id?>"><?= $category -> category?></option>
                       <?php endforeach;?>
                    </select>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <label for="txtmessage">Message</label>
                <textarea rows="3" cols="10" id="message" name="message" required="" placeholder="Your message here..."> <?php echo set_value('message'); ?></textarea>
            </div>
        </div>

        <p class="buttons"><button type="submit" name="submit" id="submit" class="button blue"><span>Send Message</span></button></p>
    </div>

    <?php echo form_close(); ?> 
</div>
<!-- /form -->

<div class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-one">
                <span class="icon"><i class="icon-mobile-green"></i></span>
                <span class="content">(617) 861-9235</span>
            </div>
            <div class="col-two">
                <span class="icon"><i class="icon-envelope-green"></i></span>
                <span class="content"><a href="mailto:admin@mefarnes.com">admin@mefarnes.com</a></span>
            </div>
            <div class="col-three">
                <span class="icon"><i class="icon-map-marker-green"></i></span>
                <span class="content">14 Aharon Brand, Jerusalem<br>
</span>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /content -->