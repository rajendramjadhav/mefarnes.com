<!-- content -->
<div id="content" class="page terms">
    <section class="header">
        <div class="container">
            <div class="row">
                <h2>Privacy Policy</h2>
            </div>
        </div>
    </section>

    <!-- form -->
    <div class="container">
        <div class="contents">
            <div class="row" style="padding-left: 5%">
            	<h3>Privacy Policy</h3>
                  <p><b>Last revised: September 1, 2016</b></p>   
				<p>This Privacy Policy explains what personal information and non- personal data will be collected when you use our Website www.mefarnes.com </p>
				<p> The website allows individuals or organizations to post a fundraising campaign and to accept donations for the campaign (the “service”).  This Privacy Policy informs you of MEFARNES’ policy with respect to the collection, use, sharing, storage and disclosure of personal information and other information that is collected from users of the site, whether campaign coordinators or donors. 
				</p>
				
				<p> By using the Service, you agree to the collection and use of information in accordance with this privacy policy. 
				</p>
                                 <h3>Personal Information we collect</h3>
				
				<p>In order to use our services, we need personal information from you.  In order to identify you or contact you we will request information such as your name, email address, phone number or address ("Personal Information").  We will also request your login and password information in order to create an account with us.
				</p>
				
				<p> When you create a campaign we may ask you to provide your business name, business address, tax ID, and bank information.  As a donor we may request your name, email address, credit card or bank information.  In order to verify your identity, we may further require your driver’s license or other personally identifiable information for verification. 
				</p>
				
				<p>We may also collect Personal Information from you as a result of your email communications with us which we may retain along with our responses.
				</p>
				
				<p> You can sign up for or sign in to your account using Facebook Connect. When you sign in using Facebook Connect, we will receive your public profile, friend list and email address. We use this information to authenticate your identity, display your number of Facebook friends on your account page, show you campaigns your friends have funded, and link your Facebook Profile information to your account. Facebook Connect gives you the option to post information about your activities on this Website to your profile page to share with others within your network. If you desire you can disconnect Facebook Connect.
				</p>
				
				<p> Any personal information you post as content is available to the public without any expectation of privacy or confidentiality. Copies may remain viewable in stored pages even if you remove the user content. Your name and location are publicly displayed in your profile page. The profile you create on our site will be publicly visible. 
				</p>
                                 <h3>How We Use Your Personal Information</h3>
				
				<p>We collect this personal information for the purpose of providing the Service, identifying and communicating with you, and improving our services. For example, we use the personal information to register use and to identify you when you sign-in to your account, to administer your account and funds, to communicate with you and respond to your emails and to conduct research and analyze data for our services. 
				</p>
				
				<p> MEFARNES does not knowingly collect Personal Data from children under the age of 13. If you are under the age of 13, please do not use the services or submit any personal information through the Services. If you have reason to believe that a child under the age of 13 has provided personal information to MEFARNES through the Services, please contact us and we will take steps to delete that information from our databases. 
				</p>
				
								
				<h3>Non Personal Log Data</h3>
				<p>We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data is not your personal information but may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics. </p>
                                 <p>When you use our mobile site, we may receive information about your location and your mobile device, including a unique identifier for your device.</p>
				
				<h3>Cookies</h3>
				<p>When you visit our site, our web server sends a cookie to your computer or mobile device. Cookies are files with small pieces of information which are issued to your computer or mobile device and which store and sometimes track information about your use of the site. For example, cookies tell us when you are logged in, protect your account from being used by anyone other than you, and store your username. We use cookies to collect information in order to improve our services for you.</p>
				
				<p>Most web and mobile device browsers automatically accept cookies but, if you prefer, you can change your browser to prevent that or to notify you each time a cookie is set. You can also learn more about cookies by visiting  <a href="http://www.allaboutcookies.org" target="_blank">www.allaboutcookies.org</a>, which includes additional useful information on cookies and how to block cookies using different types of browsers or mobile devices. Blocking or deleting cookies may prevent you from taking full advantage of the Services.   	
				</p>
				<h3>Performance and Analytics</h3>
				<p>In addition, we may use third party services such as Google Analytics that collect, monitor and analyze this type of information in order to increase our Service's functionality. These third party service providers have their own privacy policies addressing how they use such information.</p>
                                 
<p>Mefarnes.com may also use remarketing services to advertise on third party web sites to you after you visited our Service. We, and our third party vendors, use cookies to inform, optimize and serve ads based on your past visits to our Service.</p>
                                
<p>Google AdWords remarketing service is provided by Google Inc.</p>
                                
<p>You can opt-out of Google Analytics for Display Advertising and customize the Google Display Network ads by visiting the Google Ads Settings page: <a href="http://www.google.com/settings/ads" target="_blank">http://www.google.com/settings/ads</a></p>
                                
<p>Google also recommends installing the Google Analytics Opt-out Browser Add-on - <a href="https://tools.google.com/dlpage/gaoptout" target="_blank">https://tools.google.com/dlpage/gaoptout</a> - for your web browser. Google Analytics Opt-out Browser Add-on provides visitors with the ability to prevent their data from being collected and used by Google Analytics.</p>
				
				<h3>Service Providers</h3>
				<p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>

                                <p>These third parties have access to your personal information only to perform specific tasks on our behalf and are obligated not to disclose or use your information for any other purpose and to protect your personal information in the same manner as we do. </p> 
				
				<h3>Social Networking Integrations</h3>
				<p>One of the features of the Services is that it allows you to enable or log into the Services via various social networking services like Facebook or Twitter. To use this feature, you log into or grant us permission via the relevant social networking service. when you add a social networking services account to the services or log into the services using your social networking services account, we will collect relevant information necessary to enable the services to access that social networking service and your data contained within that social networking service. As part of such integration, the social networking service will provide us with access to certain information that you have provided to the social networking service, and we will use, store, and disclose such information in accordance with this Privacy Policy. The manner in which social networking services use, store, and disclose your information is governed by the privacy policies of such third parties, and MEFARNES shall have no liability or responsibility for the privacy practices or other actions of any social networking services that may be enabled within the services.    </p>
				
				<h3>Mobile devices </h3>
				<p>We may offer you the ability to connect with our Website, or use our applications, services, and tools using a mobile device, either through a mobile application or via a mobile optimized website. The provisions of this Privacy Policy apply to all such mobile access and use of mobile devices. This Privacy Policy will be referenced by all such mobile applications or mobile optimized websites.  </p>
				
				<h3>Compliance with Laws</h3>
				<p>We will disclose your Personal Information where required to do so by law if we believe that such action is necessary to comply with the law or legal obligation, to protect the rights and property of MEFARNES, to protect the security or integrity of our Service and the personal safety of users of the Services or the public.</p>
				
				<h3>Security</h3>
				<p>We will use our best efforts to protect your personal information from unauthorized access, destruction, use, modification, or disclosure. However, no method of transmission over the internet, or method of electronic storage is 100% secure and we are unable to guarantee the absolute security of the personal information we have collected from you. </p>
				
				<h3>International Transfer</h3>
				<p> Your information, including Personal Information, may be transferred to and maintained on computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction. </p>
				
				<p> If you are located outside United States and choose to provide information to us, please note that we transfer the information, including personal information, to the United States and process it there. </p>

                                <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>


				<h3>Links to Other Sites</h3>
				<p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site.  You should review the Privacy Policy of every site you visit. We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services. </p>
				
				<h3>Changes to this Privacy Policy</h3>
				<p>We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. If you disagree with our changes to the Privacy Policy, you may de-activate your account or discontinue the use of our Services. Your continued use of the Service after we post any changes to the Privacy. </p>
                             
                                <p>Policy on this page will constitute your acknowledgment of the changes and your consent to abide and be bound by the revised Privacy Policy.</p>

                                <p>If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.</p>

                                <p>This Privacy Policy is effective as of September 1, 2016 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.</p>
				
				<h3>Contact Us</h3>
				<p>If you have any questions about this Privacy Policy, please <a href="https://www.mefarnes.com/contact-us">contact us.</a></p>
		
				
            	
            </div>
        </div>
    </div>
    <!-- /form -->

    <div class="scroll-up"><i class="icon-long-arrow-up"></i></div>	

</div>
<!-- /content -->

<!-- /content -->