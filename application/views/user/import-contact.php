<!-- content -->
<div id="content" class="page dashboard campaign-creator">
    <div class="contents">
        <div class="container">
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-4 col-md-3">
                    <div class="left-panel">
                        <h2>My Status</h2>
                        <div class="box">
                            <h3><span>People in<br />My Networks</span><span class="badge">108</span></h3>
                            <ul>
                                <li><label>Facebook Friends</label> <span>[ 30 ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Twitter Friends</label> <span>[ 70 ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Email Contacts</label> <span>[ 08 ]</span> <a href="javascript:;" data-toggle="modal" data-target="#email-options"><i class="fa fa-plus-circle"></i></a></li>
                            </ul>
                        </div>
                        <div class="box">
                            <h3><span>Visits to<br />My campaigns</span><span class="badge">358</span></h3>
                        </div>
                        <div class="box">
                            <h3><span>People Sharing<br />My Campaigns</span><span class="badge">53</span></h3>
                            <ul>
                                <li><label>Facebook Shares</label> <span>[ 25 ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Twitter Followers</label> <span>[ 25 ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Email Shares</label> <span>[ 03 ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /left panel -->
                <!-- /right panel -->
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<!-- /content -->
<!-- Modal -->
<div id="email-options" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">               
                <h4 class="modal-title">Import Email Contacts</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <img src="<?= base_url().'assets/images/gears.gif'?>" alt="Loading.." />

                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#email-options').modal('show');
    });
</script>