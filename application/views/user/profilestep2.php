<script src="<?php echo base_url(); ?>assets/js/jquery.cropit.js"></script>

<style>
	/* header */
	header { min-height:100px; padding:15px 0; background-color:#fff; }
	
	/* nav */
	nav {}
	.navbar-default { min-height:100px; background-color:#fff; border:0; border-bottom:1px solid #cbcccf; }
	#navbar { float:right; margin:25px 0 0 0; }
	.navbar-default .navbar-toggle { background-color:#99cc00; border-radius:0; position:relative; width:40px; height:42px; line-height:30px; overflow:hidden; margin:30px 15px 0 0; }
	.navbar-default .navbar-toggle:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#67b300; transition:height linear 0.2s; }
	.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus { background-color:#99cc00; }
	.navbar-default .navbar-toggle:hover:after { height:100%; z-index:1; }
		.navbar-toggle i { color:#fff; font-size:1.5em; position:relative; z-index:2; }
	.navbar-default .navbar-nav > li > a { color:#454545; }
	.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus { color:#67b300; }
	.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover { color:#67b300; background-color:transparent; }
	.navbar-default .navbar-nav > li.user > a { padding-top:7px; }
		.navbar-default a .char { width:36px; height:36px; line-height:36px; font-size:1.28em; margin:0 15px 0 0; }
		.navbar-default .navbar-nav > li.user > a i { margin:0 0 0 15px; }
	.navbar-default .navbar-nav > li.campaign { margin:5px 0 0 0; }
		.navbar-default .navbar-nav > li.campaign > a { background-color:#fc6c06; border-radius:3px; color:#fff; text-transform:uppercase; padding:10px 12px; font-weight:700; font-size:0.93em; position:relative; overflow:hidden; }
			.navbar-default .navbar-nav > li.campaign > a span { position:relative; z-index:2; }
		.navbar-default .navbar-nav > li.campaign > a:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#c25407; transition:height linear 0.2s; border-radius:0 0 3px 3px; z-index:1; }
		.navbar-default .navbar-nav > li.campaign > a:hover:after { height:100%; }
		
		.dropdown-menu { border:1px solid #dedede; box-shadow:0 0 16px rgba(0, 0, 0, 0.2); padding:15px 25px; min-width:180px; left:auto; right:0; }
		.dropdown-menu:before {}
			.dropdown-menu > li > a { padding:7px 5px; }
			.dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover { background-color:transparent; color:#99cc00; }
			.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover { background-color:transparent; color:#99cc00; }
	/* /nav */
	/* /header */
	/* profile page */
	.profile { padding:30px 0 30px 0; }
		.profile-banner {}
			.profile-banner > figure {}
			.profile-banner .content { margin:20px 0 0 0; }
				.profile-banner .content figure { width:55px; height:55px; border:2px solid #ddd; border-radius:50%; overflow:hidden; float:left; }
					.profile-banner .content figure img { width:100%; height:100%; }
				.profile-banner .content .data { margin:0 0 0 90px; }
				.profile-banner .content .data:before, .profile-banner .content .data:after { content:""; display:table; }
				.profile-banner .content .data:after { clear:both; }
					.profile-banner .content .data .left { float:left; width:50%; }
					.profile-banner .content .data .right { float:right; width:50%; }
						.profile-banner .content .data h3 { font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.28em; }
							.profile-banner .content .data h3 a { color:#0064d2; font-size:0.78em; position:relative; top:-2px; left:5px; }
						.profile-banner .content .data .location { float:left; }
						.profile-banner .content .data .time-stamp { float:left; margin:0 0 0 25px; }
						.profile-banner .content .data .location i, .profile-banner .content .data .time-stamp i { color:#0064d2; margin:0 8px 0 0; }
						.profile-banner .content .data .right .likes { font-weight:300; font-size:1.71em; color:#0064d2; float:right; margin:15px 0 0 0; }
						.profile-banner .content .data .right .shares { font-weight:300; font-size:1.71em; color:#0064d2; float:right; margin:15px 10px 0 50px; }
							.profile-banner .content .data .right .shares small { font-size:0.66em; }
						.profile-banner .content .data .right .shares i, .profile-banner .content .data .right .likes i { margin:0 10px 0 0; }		
		.profile .tab-area { margin:30px 0 0 0; }
			.profile .tab-area .tabs { border-bottom:1px solid #dedede; }
			.tab-area .tabs:before, .tab-area .tabs:after { content:""; display:table; }
			.tab-area .tabs:after { clear:both; }
				.tab-area .tabs { margin:0; padding:0; list-style:none; }
					.tab-area .tabs li { float:left; position:relative; top:1px; }
						.profile .tab-area .tabs li a { color:#999; font-size:1.14em; background:transparent url(../images/tab-separator.png) right bottom no-repeat; text-transform:uppercase; display:inline-block; padding:0 4px 0 0; }
							.profile .tab-area .tabs li a span { border-bottom:1px solid transparent; padding:20px 35px 20px 35px; display:inline-block; }
							
							.profile .tab-area .tabs li a .badge { color:#fff; background-color:#99cc00; min-width:28px; margin:-2px 0 0 5px; }
						.profile .tab-area .tabs li a:hover, .profile .tab-area .tabs li a:focus, .profile .tab-area .tabs li.active a { text-decoration:none; color:#0064d2; }
							.profile .tab-area .tabs li a:hover span, .profile .tab-area .tabs li a:focus span, .profile .tab-area .tabs li.active a span { border-color:#0064d2; }
					.tab-btn-xs { border-bottom:1px solid #dedede; color:#999; font-size:1.14em; text-transform:uppercase; display:block; text-align:left; margin:0 0 2px 0; display:none; }
					.tab-btn-xs:hover, .tab-btn-xs:focus, .tab-btn-xs.activer { border-color:#0064d2; }
						.tab-btn-xs span { border-bottom:1px solid transparent; padding:10px 10px 10px 10px; display:block; background-color:#fafafa;  }
						.tab-btn-xs:hover, .tab-btn-xs:focus, .tab-btn-xs.active { text-decoration:none; color:#0064d2; }
						.tab-btn-xs .badge { color:#fff; background-color:#99cc00; min-width:28px; margin:-2px 0 0 5px; float:right; }
					
			.tab-area .tab-data {}
				 .tab-data .tab-contents { display:none; padding:40px 0; }
				 .profile .tab-area .tab-data #tab1 { display:block; }
				 	.tab-area .tab-data #tab1 h4 { font-weight:600; font-size:1.14em; margin:0 0 5px 0; text-align:center; }
					.tab-area .tab-data #tab1 h4 + p { text-align:center; }
					.tab-area .tab-data #tab1 .row { margin-top:20px; margin-bottom:20px; }
					.tab-area .tab-data #tab1 .footer .row { margin-top:0; margin-bottom:0; }
					.tab-area .tab-data #tab1 p { text-align:justify; margin-bottom:25px; }				
					.tab-area .tab-data #tab1 p.more { margin-bottom:10px; }
				 .profile #tab2 h4 { font-size:1.28em; font-weight:600; }
					.profile #tab2 .pic-slider { margin:0 -7px; transition:all linear 0.2s; }
						.profile #tab2 .pic-slider .carousel-cell { opacity:1; width:50%; padding:0 7px; }
						.profile #tab2 .pic-slider .flickity-prev-next-button { background-color: rgba(0, 0, 0, .4); box-shadow: none; opacity: 1; transition: all linear 0.2s; width: 80px; height: 100%; border-radius: 0; }
						.profile #tab2 .pic-slider .flickity-prev-next-button:hover:not(:disabled) { background-color: rgba(0, 0, 0, .6); }
						.profile #tab2 .pic-slider .flickity-prev-next-button.previous { left: 7px; }
						.profile #tab2 .pic-slider .flickity-prev-next-button.next { right: 7px; }
				 .tab-area .tab-data #tab3 article { border-top:1px solid #dedede; padding:30px 0 25px 0; }
				 .tab-area .tab-data #tab3 article:first-child { border:0; padding-top:0; }
				 .tab-area .tab-data #tab3 article:last-child { padding-bottom:0; }
					 .tab-area .tab-data #tab3 article h2 { font-weight:300; font-size:1.71em; border-left:5px solid #127dbe; padding:8px 0 8px 25px; font-weight:normal; margin:0 0 15px 0; }
					 .tab-area .tab-data #tab3 h4 { font-weight:600; font-size:1.14em; margin:0 0 5px 0; }
					 .tab-area .tab-data #tab3 p { text-align:justify; }
					 .tab-area .tab-data #tab3 .narrator { margin:20px 0 0 0; }
					 .tab-area .tab-data #tab3 .narrator:before, .tab-area .tab-data #tab3 .narrator:after { display:table; content:""; }
					 .tab-area .tab-data #tab3 .narrator:after { clear:both; }
					 	 .tab-area .tab-data #tab3 .narrator figure { width:60px; height:60px; border:2px solid #ddd; border-radius:50%; float:left; overflow:hidden; }
						 	 .tab-area .tab-data #tab3 .narrator figure img { width:100%; height:100%; }
						  .tab-area .tab-data #tab3 .narrator h3 { float:left; font-weight:300; font-size:1.71em; padding:12px 15px 12px 15px; }
						  	 .tab-area .tab-data #tab3 .narrator h3 a { color:#454545; }
							 .tab-area .tab-data #tab3 .narrator h3 a:hover { color:#99cc00; text-decoration:none; }
							 .tab-area .tab-data #tab3 .narrator .likes { float:right; font-size:1.28em; margin:15px 10px 0 0; }
							 .tab-area .tab-data #tab3 .narrator .likes a:hover { text-decoration:none; }
					.tab-area .tab-data #tab4 { padding-top:15px; }
		.alert { padding:20px; margin-bottom:20px; border:0; border-radius:0; font-weight:300; text-align:center; }
		.alert-info { background-color:#e5eef4; }
					#tab4 .alert-info a { font-weight:700; }
					#tab4 .media { border-bottom:1px solid #dedede; padding:0 0 30px 0; margin:30px 0 0 0; }
					#tab4 .media:last-child { border:0; padding:0; }
					#tab4 .media-left {}
					#tab4 .media-left .user-pic { border-left:5px solid #127dbe; padding:6px 5px 6px 10px; }
						#tab4 .media-left figure { width:38px; height:38px; border:2px solid #ddd; border-radius:50%; overflow:hidden; }
							#tab4 .media-left figure img { width:100%; height:100%; }
					#tab4 .media-heading { font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.28em; padding:12px 0 15px 0; }
						#tab4 .media-heading small { font-weight:300; font-size:0.78em; margin:0 0 0 10px; }
						#tab4 .media-heading .likes { float:right; }
						#tab4 .media-heading .likes:hover { text-decoration:none; }
					#tab4 p { text-align:justify; }
						#tab4 .media .media { background-color:#f5f5f5; margin-top:20px; border:0; }
						#tab4 .media .media + .media { margin-top:4px; }
							#tab4 .media .media .user-pic { border:0; }
							#tab4 .media .media .media-body { padding-right:20px; }
								#tab4 .media .media .media-heading small { float:right; }
								#tab4 .media-heading .small { color:#0064d2; font-weight:300; font-size:0.66em; margin:0 0 0 10px; }
					#tab4 .media .media-body .more-replies { margin:10px 0 0 0; }				
				.tab-area .tab-data .tab-contents .footer { border-top:1px solid #dedede; padding:20px 0 0 0; margin:20px 0 0 0; }
					.profile .tab-area .tab-data .tab-contents .footer .paging { margin:0; padding:0; }
					.button { display:inline-block; min-width:250px; border:2px solid #999; padding:10px 30px; color:#999; margin:0 2px; font-weight:700; font-size:1.28em; position:relative; text-align:center; }
					button.button { background-color:transparent; transition:all linear 0.2s; }
						.button span { position:relative; z-index:2; }
					.button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:0; background-color:#999; transition:height linear 0.2s; z-index:1; }
					.button:hover:after { height:100%; }					
					.button.green { color:#99cc00; border-color:#99cc00; }
					.button.green:after { background-color:#99cc00; }					
					.button.blue { color:#0064d2; border-color:#0064d2; }
					.button.blue:after { background-color:#0064d2; }					
					.button:hover { color:#fff; text-decoration:none; }
					.button-sm { display:inline-block; min-width:100px; border:1px solid #999; border-radius:5px; padding:7px 30px; color:#999; margin:0 2px; font-weight:700; font-size:1.14em; position:relative; text-align:center; }
						.button-sm span { position:relative; z-index:2; }
					.button-sm:after { content:""; position:absolute; left:0; right:0; bottom:0; height:0; background-color:#999; transition:height linear 0.2s; z-index:1; }
					.button-sm:hover:after { height:100%; }					
					.button-sm.green { color:#99cc00; border-color:#99cc00; }
					.button-sm.green:after { background-color:#99cc00; }					
					.button-sm.blue { color:#0064d2; border-color:#0064d2; }
					.button-sm.blue:after { background-color:#0064d2; }					
					.button-sm:hover { color:#fff; text-decoration:none; }
					.tab-area .tab-data .tab-contents .footer .button { margin-top:30px; min-width:260px; }		
		.profile-right {}
			.profile-right h3 { font-size:1.28em; color:#99cc00; margin:0; }
			.profile-right h2 { font-size:2.14em; font-family:'Montserrat', sans-serif; font-weight:400; text-transform:uppercase; }
			.profile-right .metadata { margin:40px 0 0 0; font-weight:300; color:#999; }
				.profile-right .metadata .highlight { font-size:2.86em; color:#0064d2; display:block; margin:0 0 15px 0; }
					.profile-right .metadata .highlight small { font-size:0.6em; }
				.profile-right .metadata .c100 { font-size:60px; display:inline-block; float:right; top:-50px; }
					.profile-right .metadata .c100 > span { font-size:24px; color:#999; top:12px; }
			.profile-right .buttons { margin:30px 0 0 0; }
				
				.profile-right .buttons .donate-button { display:inline-block; width:100%; font-weight:700; font-size:1.71em; text-align:center; padding:15px 20px; background-color:#99cc00; color:#fff; border-radius:5px; position:relative; overflow:hidden; }
				.profile-right .buttons .donate-button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#67b400; transition:height linear 0.2s; z-index:1; }
				.profile-right .buttons .donate-button:hover { text-decoration:none; }
				.profile-right .buttons .donate-button:hover:after { height:100%; }
					.profile-right .buttons .donate-button span { position:relative; z-index:2; text-shadow:0 2px 1px rgba(103, 180, 0, 1); }
				
				.profile-right .buttons .fb-share-button { display:inline-block; width:49%; float:left; font-weight:700; font-size:1.28em; text-align:center; padding:15px 10px; background-color:#4460b0; color:#fff; border-radius:5px; position:relative; overflow:hidden; margin:5px 0 0 0; }
				.profile-right .buttons .fb-share-button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#223d89; transition:height linear 0.2s; z-index:1; }
				.profile-right .buttons .fb-share-button:hover { text-decoration:none; }
				.profile-right .buttons .fb-share-button:hover:after { height:100%; }
					.profile-right .buttons .fb-share-button span { position:relative; z-index:2; text-shadow:0 2px 1px rgba(34, 61, 137, 1); }
				
				.profile-right .buttons .follow-tweets-button { display:inline-block; width:49%; float:right; font-weight:700; font-size:1.28em; text-align:center; padding:15px 10px; background-color:#3fbffb; color:#fff; border-radius:5px; position:relative; overflow:hidden; margin:5px 0 0 0; }
				.profile-right .buttons .follow-tweets-button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#248ebf; transition:height linear 0.2s; z-index:1; }
				.profile-right .buttons .follow-tweets-button:hover { text-decoration:none; }
				.profile-right .buttons .follow-tweets-button:hover:after { height:100%; }
					.profile-right .buttons .follow-tweets-button span { position:relative; z-index:2; text-shadow:0 2px 1px rgba(36, 142, 191, 1); }		
		.recent-donations { margin:70px 0 0 0; }
			.recent-donations h2 { font-size:1.28em; border-bottom:1px solid #dedede; padding:0 0 25px 0; margin:0 0 20px 0; }
				.recent-donations h2 i { float:right; color:#127dbe; font-size:2em; position:relative; top:-8px; }
			.recent-donations .donation-box { border-bottom:1px solid #dedede; padding:0 0 20px 0; margin:0 0 25px 0; }
			.donation-box:before, .donation-box:after { content:""; display:table; }
			.donation-box:after { clear:both; }
			.char { display:inline-block; width:50px; height:50px; line-height:46px; text-align:center; color:#fff; border-radius:50%; font-weight:300; font-size:24px; background-color:#ddd; vertical-align:middle; }
				.char.a { background-color:#4cc25d; } .char.b { background-color:#333; } .char.c { background-color:#3c9fe5; } .char.d { background-color:#99cc00; }
				.char.e { background-color:#333; } .char.f { background-color:#333; } .char.g { background-color:#e53c9f; } .char.h { background-color:#333; }
				.char.i { background-color:#333; } .char.j { background-color:#333; } .char.k { background-color:#2fdf86; } .char.l { background-color:#333; }
				.char.m { background-color:#e81f5c; } .char.n { background-color:#333; } .char.o { background-color:#e5d33c; } .char.p { background-color:#e7a248; }
				.char.q { background-color:#333; } .char.r { background-color:#333; } .char.s { background-color:#333; } .char.t { background-color:#487de7; }
				.char.u { background-color:#333; } .char.v { background-color:#333; } .char.w { background-color:#333; } .char.x { background-color:#333; }
				.char.y { background-color:#333; } .char.z { background-color:#333; }
			.donation-box .char { float:left; }
			.donation-box .content { margin:0 0 0 70px; position:relative; padding:0 30px 0 0; }
				.donation-box .content h3 { font-size:1.28em; margin:0 0 5px 0; }
				.donation-box .content .time-stamp { font-size:0.92em; color:#999; }
					.donation-box .content .love { margin:0 0 0 30px; color:#999; }
						.donation-box .content .love i { margin-right:5px; }
					.donation-box .content .love:hover, .donation-box .content .love.active { text-decoration:none; color:#99cc00; }
				.donation-box .content p { font-size:0.92em; margin:15px 0 0 0; }
				.donation-box .content .price-tag { font-weight:300; font-size:2.57em; color:#0064d2; position:absolute; top:-10px; right:0; }
					.donation-box .content .price-tag small { font-size:0.44em; color:#999; }
					.donation-box .content .bar { display:block; height:20px; background-color:#ddd; margin:0 0 8px 0; border-radius:2px; }
			.profile .paging { margin:20px 0 0 0; padding:0 20px; }
				.profile .paging span { float:left; width:70%; display:inline-block; color:#999; }
				.profile .paging a { float:right; }
	/* /profile page */
	
	/* public profile (organization owner) */
	.public-profile {}
		.dashboard .left-panel .box .share { margin:10px -5px 0 -5px; text-align:center; }
			.dashboard .left-panel .box .share-button { display:inline-block; padding:10px 40px; border:2px solid #333; border-radius:2px; min-width:105px; text-align:center; font-size:1.71em; margin:0 1px 10px; }
			.dashboard .left-panel .box .share-button.facebook { color:#375a9b; border-color:#375a9b; }
			.dashboard .left-panel .box .share-button.twitter { color:#40b9ff; border-color:#40b9ff; }
			.dashboard .left-panel .box .share-button.facebook:hover { color:#fff; background-color:#375a9b; }
			.dashboard .left-panel .box .share-button.twitter:hover { color:#fff; background-color:#40b9ff; }
			.dashboard.organization.public-profile .tab-area .tabs li a { width:auto; padding:0 10px 20px 10px; }
			.dashboard.public-profile.organization .tab-area .tab-data .campaigns { padding:0; }
			.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box { display:block; }
				.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box figure { width:100%; float:none; display:block; position:relative; }
					.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box figure img { position:relative; z-index:1; }
					.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box figure figcaption { position:absolute; z-index:2; left:0; right:0; bottom:0; background-color:rgba(153, 204, 0, 0.5); padding:10px; text-align:center; color:#fff; font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.14em; }
					.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; display:block; }
					.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents:before, .dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents:after { content:""; display:table; }
					.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents:after { clear:both; }
						.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-one { display:block; float:left; width:50%; text-align:center; font-size:0.92em; color:#686868; padding:5px 0 15px 0; margin:0; }
						.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-two { display:block; float:left; width:50%; text-align:center; font-size:0.92em; color:#686868; padding:5px 0 15px 0; margin:0; }
							.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { font-weight:700; font-size:1.42em; color:#4460b0; display:block; }
						.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .buttons { display:block; width:100%; border-top:1px solid #dedede; padding:12px 20px 10px; }
							.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .buttons .facebook { font-size:1.42em; color:#375a9b; }
							.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .buttons .twitter { font-size:1.42em; color:#40b9ff; margin:0 0 0 20px; }
							.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .buttons .donate { font-size:1.14em; color:#99cc00; float:right; font-weight:700; }
			.dashboard.public-profile.organization .tab-area .tab-data .campaigns-ends { padding:0; display:none; }
	/* /public profile (organization owner) */
	/* public profile (organization) */
	.profile-org { background-color:#fff; padding:0 0 1px 0; }
		
		.profile-org .banner { background:transparent url(../images/data/org-donate-header-02.jpg) center center no-repeat; background-size:cover; display:table; width:100%; }
			.profile-org .banner .contents { height:400px; background-color:rgba(0, 0, 0, 0.6); color:#fff; font-size:1.14em; font-weight:300; text-align:center; display:table-cell; width:100%; vertical-align:middle; }
				.profile-org .banner .contents h2 { font-size:3.125em; margin:0; text-transform:none; }
				.profile-org .banner .contents p + p { font-size:1.125em; }
				.profile-org .banner .buttons { margin:30px 0 0 0; }
					.profile-org .donate-button { display:inline-block; font-weight:700; font-size:1.5em; text-align:center; padding:12px 20px; background-color:#99cc00; color:#fff; border-radius:5px; position:relative; overflow:hidden; min-width:250px; }
					.profile-org .donate-button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#67b400; transition:height linear 0.2s; z-index:1; }
					.profile-org .donate-button:hover { text-decoration:none; }
					.profile-org .donate-button:hover:after { height:100%; }
						.profile-org .donate-button span { position:relative; z-index:2; text-shadow:0 2px 1px rgba(103, 180, 0, 1); }
		
		.profile-org .tab-panel { background-color:#f5f5f5; }
			.profile-org .tab-panel .tabs { float:left; margin:0; padding:0; list-style:none; }
				.profile-org .tab-panel .tabs li { float:left; margin:30px 20px 0 20px; }
				.profile-org .tab-panel .tabs li:first-child { margin-left:0; }
				.profile-org .tab-panel .tabs li:last-child { margin-right:0; }
					.profile-org .tab-panel .tabs li a { font-size:1.14em; color:#454545; padding:0 10px 16px 10px; border-bottom:2px solid transparent; position:relative; }
					.profile-org .tab-panel .tabs li a:hover, .profile-org .tab-panel .tab-header .tabs li.active a { text-decoration:none; color:#0064d2; border-color:#0064d2; }
					.profile-org .tab-panel .tabs li a:after { content:""; border-top:5px solid #0064d2; border-left:5px solid transparent; border-right:5px solid transparent; position:absolute; bottom:-7px; left:50%; margin-left:-4px; opacity:0; transition:all linear 0.2s; }
					.profile-org .tab-panel .tabs li.active a:after, .profile-org .tab-panel .tabs li a:hover:after { opacity:1; }
			.profile-org .tab-panel .buttons { float:right; margin:12px 0; }
				.profile-org .tab-panel .buttons .fb-share-button { font-size:1.71em; color:#4c6aac; border:1px solid #4c6aac; width:45px; height:45px; line-height:45px; text-align:center; border-radius:5px; display:inline-block; vertical-align:middle; }
				.profile-org .tab-panel .buttons .fb-share-button:hover { background-color:#4c6aac; color:#fff; }
				.profile-org .tab-panel .buttons .tw-share-button { font-size:1.71em; color:#39e2d8; border:1px solid #39e2d8; width:45px; height:45px; line-height:45px; text-align:center; border-radius:5px; display:inline-block; vertical-align:middle; }
				.profile-org .tab-panel .buttons .tw-share-button:hover { background-color:#39e2d8; color:#fff; }
				.profile-org .tab-panel .buttons .donate-button { vertical-align:middle; padding:8px 20px; min-width:20px; }
		
		.profile-org .overview { padding:40px 0 50px 0; }
			.profile-org .overview h3 { font-size:1.71em; color:#0064d2; }
			.profile-org .overview q { font-weight:600; font-style:italic; font-size:1.28em; margin:0 0 20px 0; }
			.profile-org .overview p { margin:0 0 20px 0; font-size:1.14em; color:#666; }
			.profile-org .overview .box { background-color:#f5f5f5; padding:5px; display:inline-block; }
	
		.profile-org .campaign-scroller { background-color:#fafafa; padding:40px 0; }
			.profile-org .campaign-scroller .campaigns-slider { margin:0 -15px; position:relative; left:0; }
				.profile-org .campaign-scroller .campaigns-slider .slide-container { width:100%; overflow:hidden; }
					.profile-org .campaign-scroller .campaigns-slider .slides li { padding:0 15px; float:left; width:25%; }
					.slides:before, .slides:after { content:""; display:table; }
					.slides:after { clear:both; }
					.profile-org .campaign-scroller .campaign-box { background-color:#fff; }
						.profile-org .campaign-scroller .campaign-box figure { width:100%; float:none; display:block; position:relative; }
							.profile-org .campaign-scroller .campaign-box figure img { position:relative; z-index:1; }
							.profile-org .campaign-scroller .campaign-box figure figcaption { position:absolute; z-index:2; left:0; right:0; bottom:0; background-color:rgba(153, 204, 0, 0.5); padding:10px; text-align:center; color:#fff; font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.14em; }
							.profile-org .campaign-scroller .campaign-box .contents { width:100%; display:block; }
							.profile-org .campaign-scroller .campaign-box .contents:before, .dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents:after { content:""; display:table; }
							.profile-org .campaign-scroller .campaign-box .contents:after { clear:both; }
								.profile-org .campaign-scroller .campaign-box .contents .col-one { display:block; float:left; width:50%; text-align:center; font-size:0.92em; color:#686868; padding:5px 0 15px 0; margin:0; }
								.profile-org .campaign-scroller .campaign-box .contents .col-two { display:block; float:left; width:50%; text-align:center; font-size:0.92em; color:#686868; padding:5px 0 15px 0; margin:0; }
									.profile-org .campaign-scroller .campaign-box .contents [class^="col-"] span { font-weight:700; font-size:1.42em; color:#4460b0; display:block; }
								.profile-org .campaign-scroller .campaign-box .buttons { display:block; width:100%; border-top:1px solid #dedede; padding:12px 20px 10px; }
									.profile-org .campaign-scroller .campaign-box .buttons .facebook { font-size:1.42em; color:#375a9b; }
									.profile-org .campaign-scroller .campaign-box .buttons .twitter { font-size:1.42em; color:#40b9ff; margin:0 0 0 20px; }
									.profile-org .campaign-scroller .campaign-box .buttons .donate { font-size:1.14em; color:#99cc00; float:right; font-weight:700; }
					.profile-org .campaign-scroller .buttons-container { padding:5px 0 0 0; text-align:right; }
						.profile-org a.campaigns-slider.prev, .profile-org a.campaigns-slider.next { position:static; margin:0; width:25px; height:25px; line-height:22px; text-align:center; border-radius:2px; background-color:#9c0; color:#fff; display:inline-block; font-size:1.28em; }
						.profile-org a.campaigns-slider.prev.disabled, .profile-org a.campaigns-slider.next.disabled { opacity:0.5; }
		
		.profile-org .our-network { padding:50px 0 60px 0; }
			.profile-org .our-network h3 { text-align:center; margin:0 0 50px 0; }
				.profile-org .our-network .box { text-align:center; padding:0 50px; }
					.profile-org .our-network .box .icon { width:100px; height:100px; line-height:100px; text-align:center; border-radius:50%; margin:0 auto; background-color:#dedede; font-size:3.57em; color:#fff; }
					.profile-org .our-network .box.facebook .icon { background-color:#375b9b; }
					.profile-org .our-network .box.twitter .icon { background-color:#40b9ff; }
					.profile-org .our-network .box.mail .icon { background-color:#fc6c06; }
					.profile-org .our-network .box p { font-size:1.28em; margin:10px 0 0 0; }
					.profile-org .our-network .box h4 { font-weight:700; font-size:2.14em; }
					.profile-org .our-network .box.facebook h4 { color:#375b9b; }
					.profile-org .our-network .box.twitter h4 { color:#40b9ff; }
					.profile-org .our-network .box.mail h4 { color:#fc6c06; }
						.profile-org .our-network .box p a { font-weight:700; font-size:0.88em; }
		
		.profile-org .gallery-slider { margin:50px 0; }
			.carousel-cell { width:50%; opacity:0.5; transition:opacity linear 0.2s; padding:0 3px; }
			.carousel-cell.is-selected { opacity:1; }
				.carousel-cell figure { position:relative; z-index:1; }
					.carousel-cell figure img { width:100%; }
				.carousel-cell .content { padding:25px 30px; background-color:rgba(0, 0, 0, 0.6); position:absolute; z-index:2; right:3px; bottom:0; left:3px; font-weight:600; font-style:italic; color:#fff; font-size:1.28em; opacity:0; transition:all linear 0.2s; }
				.carousel-cell.is-selected .content { opacity:1; }
				.carousel-cell .content:before, .carousel-cell .content:after { display:table; content:""; }
				.carousel-cell .content:after { clear:both; }
					.carousel-cell .content span { float:left; }
					.carousel-cell .content a { float:right; color:#fff; }
					.flickity-prev-next-button { background-color:#9c0; box-shadow:0 0 13px rgba(0, 0, 0, 0.4); opacity:0.8; transition:opacity linear 0.2s; }
					.flickity-prev-next-button:hover { background-color:#9c0; opacity:1; }
					.flickity-prev-next-button:focus { box-shadow:0 0 13px rgba(0, 0, 0, 0.4); }
						.flickity-prev-next-button svg { width:30%; height:30%; top:35%; left:35%; }
						.flickity-prev-next-button .arrow { fill:#fff; }
		
		.profile-org .contact-us { padding:50px 0; }
			.profile-org .contact-us h3 { text-align:center; margin:0 0 50px 0; }
			.profile-org .contact-us .box { padding:0 50px 0; }
				.profile-org .contact-us .box .icon { width:80px; height:80px; line-height:80px; text-align:center; float:left; box-shadow:0 0 25px rgba(0, 0, 0, 0.25); color:#0064d2; font-size:2.86em; }
				.profile-org .contact-us .box .content { margin:0 0 0 105px; font-size:1.28em; }
					.profile-org .contact-us .box .content a { color:#454545; }
					.profile-org .contact-us .box .content a:hover { color:#9c0; text-decoration:none; }
	/* /public profile (organization) */
	/* create organization profile */
	.organization-profile { background-color:#f5f5f5; padding:0 0 80px 0; }
		.organization-profile .contents { padding:60px 0 0 0; }
		.organization-profile .container { width:770px; max-width:100%; }
			.organization-profile h2 { font-size:2.57em; text-align:center; text-transform:none; }
			.organization-profile .dashboard-header-sm h2 { text-align:center; font-weight:300; font-size:2.14em; color:#fff; text-transform:uppercase; }
			.organization-profile h3 { font-weight:300; font-size:1.28em; text-align:center; }
			
			.organization-profile .tab-area { margin:60px 0 0 0; }
			.organization-profile.edit .tab-area { margin:30px 0 0 0; }
			.organization-profile .tab-area .tabs { border-bottom:1px solid #dedede; margin:0 40px; }
				.organization-profile .tab-area .tabs li { position:relative; top:1px; display:inline-block; width:50%; text-align:center; padding:0 10px 20px 0; font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.14em; color:#ababab; text-transform:uppercase; border-bottom:1px solid transparent; }
				.organization-profile .tab-area .tabs li.active { color:#454545; border-bottom-color:#99cc00; }
			.organization-profile .tab-area .tab-data { background-color:#fff; margin:30px 0 0 0; padding:40px; }
				.organization-profile .tab-data .step-2 { display:none; }
				
				.organization-profile .tab-data .form {}
				.organization-profile .tab-data .form .form-row { margin:0 0 20px 0; }
				.organization-profile .tab-data .form h4 { font-weight:700; font-size:1.28em; margin:0 0 10px 0; }
					.organization-profile .tab-data .form h4 small { font-weight:400; font-size:0.88em; color:#999; }
				.organization-profile .tab-data .form label { color:#999; font-weight:normal; font-size:1.14em; margin:0 0 10px 0; }
				.organization-profile .tab-data .form input[type="text"], .create-campaign .tab-data .form input[type="email"], .create-campaign .tab-data .form input[type="tel"] { font-weight:300; font-size:1.14em; background-color:#f5f5f5; border-radius:0; border:0; padding:15px 20px; height:auto; width:100%; }
				.organization-profile .tab-data .form input:focus { box-shadow:none; }
				.organization-profile .tab-data ::-webkit-input-placeholder { color:#686868; font-weight:300; font-size:1em; opacity:1; }
				.organization-profile .tab-data :-ms-input-placeholder { color:#686868; font-weight:300; font-size:1em; opacity:1; }
				.organization-profile .tab-data ::-moz-placeholder { color:#686868; font-weight:300; font-size:1em; opacity:1; }
				.organization-profile .tab-data .form .input-group-addon { border:0; background-color:#f5f5f5; font-weight:400; color:#999; }
				.organization-profile .tab-data .form .input-group.left .input-group-addon { padding-right:6px; color:#454545; font-size:1em; }
				.organization-profile .tab-data .form .input-group.left .input-group-addon + .form-control { padding-left:4px; }
				.organization-profile .tab-data .form .input-box { background-color:#f5f5f5; background-color:#f5f5f5; padding:15px 15px; text-align:center; margin:0 0 20px 0; }
				.organization-profile .tab-data .form .input-box:before, .create-campaign .tab-data .form .input-box:after { content:""; display:table; }
				.organization-profile .tab-data .form .input-box:after { clear:both; }
				.organization-profile .tab-data .form .buttons { margin:40px 0 0 0; text-align:center; }
				.organization-profile .tab-data .step-1 .form .sbSelector { background-color:#f5f5f5; text-indent:20px; font-weight:300; font-size:1.14em; }
					.organization-profile .tab-data .step-1 .form .sbOptions { padding:25px 25px; }
				.organization-profile .tab-data .form .full-pic-box {}
					.organization-profile .tab-data .form .full-pic-box figure { position:relative; }
				.organization-profile .tab-data .form .pictures { margin:0 -5px; }
					.organization-profile .tab-data .form .pictures:before, .organization-profile .tab-data .form .pictures:after { content:""; display:table; }
					.organization-profile .tab-data .form .pictures:after { clear:both; }
						.organization-profile .tab-data .form .pictures .pic-box { float:left; padding:0 5px 20px 5px; min-height:170px; }
							.organization-profile .tab-data .form .pictures .pic-box figure { width:200px; position:relative; }
							.organization-profile .tab-data .form .pictures .pic-box video { width:200px; position:relative; }
								.organization-profile .tab-data .form .pictures .pic-box figure img { width:100%; }
								.organization-profile .tab-data .form .pictures .pic-box figure a, .organization-profile .tab-data .form .full-pic-box figure a { width:25px; height:25px; line-height:23px; text-align:center; background-color:#99cc00; position:absolute; top:0; right:0; color:#fff; z-index:2; font-size:1em; }
								.organization-profile .tab-data .form .pictures .pic-box figure a:hover, .organization-profile .tab-data .form .full-pic-box figure a:hover { background-color:#333; }
								.organization-profile .tab-data .form .pictures .pic-box .select { margin:8px 0 0 0; font-size:0.8125em; }
									.organization-profile .tab-data .form .pictures .pic-box .select .fancy-checkbox.sm + label:before { top:-1px; }
									.organization-profile .tab-data .form .pictures .pic-box .select label { color:#454545; }
									.organization-profile .tab-data .form .more i { margin-right:10px; }
	/* /create organization profile */
	/* custom selectbox */
	.fancy-selectbox {}
	.sbHolder { background-color:#fff; height:48px; position:relative; }
		.sbHolder a, .sbHolder a:hover, .sbHolder a:focus, .sbHolder a:active { color:#686868; text-decoration:none; }
		.sbSelector { display:block; width:100%; height:48px; line-height:48px; text-indent:10px; overflow:hidden; position:absolute; left:0; top:0; z-index:1; }
		.sbToggle { background:transparent url(../images/selectbox-arrow.png) center center no-repeat; display:block; width:30px; height:48px; outline:none; position:absolute; right:0; top:0; z-index:2; }
		.select-box.double .sbToggle { background:transparent url(../images/selectbox-double-arrow.png) center center no-repeat; }
		.sbHolder:hover .sbToggle { background-color:transparent; }
		.sbOptions { background-color:#fff; list-style:none; position:absolute; right:0px; top:30px; margin:0; padding:5px 5px; width:100%; z-index:101; overflow-y:auto; box-shadow:0 0 16px rgba(0, 0, 0, .2); }
			.sbOptions li {}
			.sbOptions li:last-child { border:0; }
			.sbOptions a {display: block; outline: none; padding:6px 10px; color:#686868; }
			.sbOptions a:hover, .sbOptions a:focus { color:#99cc00; }
	/* /custom selectbox */
	/* custom checkbox and radio button */
	.fancy-checkbox { display:none; }
	.fancy-checkbox + label { margin:0 0 0 40px !important; position:relative; }
	.fancy-checkbox.sm + label { margin:0 0 0 30px !important; }
	.fancy-checkbox + label:before { content:""; display:inline-block; width:30px; height:30px; border:1px solid #dedede; position:absolute; top:-3px; left:-40px; }
	.fancy-checkbox.sm + label:before { width:20px; height:20px; top:2px; left:-30px; }
	.fancy-checkbox:checked + label:before { background:transparent url(../images/checkbox-checked.png) center center no-repeat; }
	.fancy-checkbox.sm:checked + label:before { background-image:url(../images/checkbox-checked-sm.png); }
	.fancy-radio { display:none; }
	.fancy-radio + label { margin:0 0 0 35px !important; position:relative; }
	.fancy-radio.sm + label { margin:0 0 0 25px !important; }
	.fancy-radio + label:before { content:""; display:inline-block; width:25px; height:25px; border:1px solid #dedede; border-radius:50%; position:absolute; top:-2px; left:-35px; }
	.fancy-radio.sm + label:before { width:19px; height:19px; top:2px; left:-25px; }
	.fancy-radio:checked + label:before { background:transparent url(../images/radio-checked.png) center center no-repeat; }
	/* /custom checkbox and radio button */
	/* footer */
.footer-top { background-color:#0064d2; color:#fff; padding:40px 0 50px 0; text-align:center; }
	.footer-top h2 { font-weight:300; font-size:3em; margin:0 0 30px 0; text-transform:none; }
	.footer-top h4 { font-weight:300; font-size:1.43em; }
	.footer-top .buttons { margin:40px 0 0 0; }
		.big-button { border-radius:28px; position:relative; overflow:hidden; display:inline-block; margin:0 10px; }
			.big-button span { display:inline-block; padding:0 10px 0 30px; min-height:55px; background-color:#99cc00; color:#fff; font-size:1.28em; border-top:1px solid #defc82; border-bottom:2px solid #67b400; border-radius:28px; text-shadow:1px 1px 0px #67b400; }
				.big-button span * { position:relative; z-index:2; }
				.big-button span i { font-weight:normal; display:inline-block; padding:18px 20px 16px 0; position:relative; float:left; }
				.big-button span i:after { content:""; position:absolute; right:0; top:2px; bottom:0; width:1px; background:linear-gradient(rgba(148, 209, 0, .4) 5%, rgba(148, 209, 0, 1), rgba(148, 209, 0, .4) 95%); }
				.big-button span b { font-weight:700; display:inline-block; padding:18px 30px 18px 30px; position:relative; }
				.big-button span b:before { content:""; position:absolute; left:0; top:2px; bottom:0; width:1px; background:linear-gradient(rgba(103, 180, 0, .4) 5%, rgba(103, 180, 0, 1), rgba(103, 180, 0, .4) 95%); }
		.big-button:hover { text-decoration:none; }
			.big-button span:after  { content:""; position:absolute; left:0; right:0; bottom:0; height:0px; background-color:#67b400; transition:height linear 0.2s; z-index:1; }
			.big-button span:hover:after { height:100%; }
		
		.big-button.facebook span { background-color:#3258a6; border-top-color:#7699f3; border-bottom-color:#173689; text-shadow:2px 2px 0px #173689; }
		.big-button.facebook span:after { background-color:#173689; }
		.big-button.facebook span i:after { background:linear-gradient(rgba(118, 153, 243, .4) 5%, rgba(118, 153, 243, 1), rgba(118, 153, 243, .4) 95%); }
		.big-button.facebook span b:before { background:linear-gradient(rgba(23, 54, 137, .4) 5%, rgba(23, 54, 137, 1), rgba(23, 54, 137, .4) 95%); }
		
		.big-button.support span { background-color:#fc6c06; border-top-color:#f9b27f; border-bottom-color:#ce5602; text-shadow:2px 2px 0px #ce5602; }
		.big-button.support span:after { background-color:#ce5602; }
		.big-button.support span i:after { background:linear-gradient(rgba(252, 94, 5, .4) 5%, rgba(250, 59, 3, 1), rgba(252, 94, 5, .4) 95%); }
		.big-button.support span b:before { background:linear-gradient(rgba(245, 81, 5, .4) 5%, rgba(237, 54, 3, 1), rgba(245, 81, 5, .4) 95%); }
			.big-button.support .icon-mefarnes-logo-sm { margin:0 0 0 -10px; border-radius:10px 0 0 10px; }
footer { background-color:#24292e; color:#838283; }
	footer a { color:#838283; }
	footer .top { padding:50px 0 40px 0; font-size:0.93em; }
		footer .top h3 { color:#99cc00; font-size:1.38em; text-transform:uppercase; margin:0 0 25px 0; }
		footer .top .links { margin:0; padding:0; list-style:none; float:left; width:50%; }
			footer .top .links li { margin:0 0 5px 0; }
			footer .top .links li:before { content:"\f101"; font-family:FontAwesome; color:#99cc00; margin:0 5px 0 0; }
			footer .top .links li a {}
			footer .top .links li:hover a { text-decoration:none; }
		footer .top .contact {}
			footer .top .contact p { line-height:150%; }
			footer .top .contact p:before, footer .top .contact p:after { content:""; display:table; }
			footer .top .contact p:after { clear:both; }
				footer .top .contact .fa { color:#99cc00; min-width:20px; margin:4px 0 0 0; float:left; }
				footer .top .contact span { float:left; }
		footer .top .newsletter-form { margin:10px 0 0 0; position:relative; }
			footer .top .newsletter-form .cover { display:block; margin:0 160px 0 0; }
				footer .top .newsletter-form input[type="email"] { border:1px solid #838283; border-radius:3px; background-color:transparent; padding:10px 10px; width:100%; color:#838283; }
			footer .top .newsletter-form button[type="submit"] { background-color:#99cc00; min-width:150px; text-align:center; border:0; border-top:1px solid #defc82; border-radius:3px; color:#fff; padding:10px 20px; position:absolute; overflow:hidden; top:0; right:0; }
				footer .top .newsletter-form button[type="submit"] span { position:relative; z-index:2; }
			footer .top .newsletter-form button[type="submit"]:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#67b300; transition:height linear 0.2s; z-index:1; }
			footer .top .newsletter-form button[type="submit"]:hover:after { height:100%; }			
			footer .top .newsletter-form ::-webkit-input-placeholder { color:#838283; font-weight:400; font-size:0.93em; opacity:1; }
			footer .top .newsletter-form :-ms-input-placeholder { color:#838283; font-weight:400; font-size:0.93em; opacity:1; }
			footer .top .newsletter-form ::-moz-placeholder { color:#838283; font-weight:400; font-size:0.93em; opacity:1; }		
		footer .top .social { margin:35px 0 0 0; }
			footer .top .social a { display:inline-block; width:40px; height:40px; line-height:36px; text-align:center; border-radius:50%; border:1px solid #838283; font-size:1.38em; margin:0 15px 0 0; }
			footer .top .social a:hover { border-color:#99cc00;}		
	footer .bottom { padding:40px 0; border-top:1px solid #494d51; background:transparent url(../images/logo-footer.png) center center no-repeat; }
		footer .bottom .links { text-align:right; }
			footer .bottom .links a { margin:0 15px 0 15px; }
			footer .bottom .links a:last-child { margin-right:0; }
/* /footer */
@media screen and (max-width: 480px) {
	.create-campaign .tab-data .form .days-container { float:none; width:100%; text-align:center; margin:0 0 15px 0; }
	.create-campaign .tab-data .form .slider-bar { float:none; width:100%; }
	.create-campaign .tab-data .form .slider-bar b { width:20%; }
	.create-campaign .tab-data .form .slider-bar #day-slider { width:60%; }
	.dashboard .tab-area .tabs li { width:50%; }
	.dashboard .tab-area .tabs li a { width:100%; }
	.dashboard .tab-area .tabs li a span { padding:0; }
	.withdraw .form .form-row .available input[type="text"] { width:60%; }
	.withdraw .form .form-row .available button { width:39%; }
	.withdraw .radios span { width:60%; }
	.withdraw .radios span + span { width:40%; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata { font-size:0.92em; margin-left:10px; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight { font-size:1.5em; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight small { font-size:0.6em; }
	.edit-campaign .form .days-container { float:none; width:100%; text-align:center; margin:0 0 15px 0; }
	.edit-campaign .form .slider-bar { float:none; width:100%; }
	.edit-campaign .form .slider-bar b { width:20%; }
	.edit-campaign .form .slider-bar #day-slider { width:60%; }
	.donate.organization .form .radios span { width:100%; margin:0 0 10px 0; }
}

@media screen and (max-width: 360px) {
	.main-slider .one .contents { top:20px; }
	.main-slider .one .contents h2 { font-size:1.5em; }
	.main-slider .one .contents h4 { font-size:1em; }
	.main-slider .one .contents .buttons { display:none; }
	.main-slider .flex-control-nav { bottom:8px; }
	.profile-banner .content .data .location { float:none; }
	.profile-banner .content .data .time-stamp { float:none; display:block; margin:0; }
	.profile-banner .content .data .right .shares { margin-left:30px; font-size:1.40em; }
	.profile-banner .content .data .right .likes { font-size:1.40em; }
	.tab-area .tabs li a { font-size:1em; text-align:center; }
	.tab-area .tabs li a span { padding:20px 20px 10px 20px; min-height:70px; }
	.tab-area .tabs li a br { display:inline; }
	.donation-box .content { margin:10px 0 0 0; float:left;  }
	.profile-right .buttons .fb-share-button { padding:15px 5px; }
	.profile-right .buttons .follow-tweets-button { padding:15px 5px; }
	.campaign-category ul.nav > li { width: 100%; float: none; }
	.withdraw .form .form-row .available input[type="text"] { width:100%; }
	.withdraw .form .form-row .available button { width:100%; margin:5px 0 0 0; }
	.popup.payment-methods .content-box { margin:70px 0 0; }
	.withdraw .radios span { width:100%; }
	.withdraw .radios span + span { margin-top:10px; }
	.organization-profile .tab-area .tabs { margin:0; }
}

@media screen and (max-width: 320px) {
	.tab-area .tabs li a span { padding:20px 15px 10px 15px; min-height:72px; }
	.profile-right .buttons .fb-share-button { font-size:1.1em; }
	.profile-right .buttons .follow-tweets-button { font-size:1.1em; }
	.create-campaign .tab-area .tabs li { padding:0 5px 10px 5px; font-size:1em; }
	.edit-campaign .form input.date-time-input[type="text"] { width:220px; }
	.edit-campaign .form .ui-datepicker-trigger { margin-top:0; }
	.big-button span { font-size:1.14em; }
}

@media screen and (min-width:361px) and (max-width: 480px) {
	.main-slider .one .contents { top:30px; }
	.main-slider .one .contents h2 { font-size:2.5em; }
	.main-slider .one .contents h4 { font-size:1.5em; }
	.main-slider .one .contents .buttons { display:none; }
	.main-slider .flex-control-nav { bottom:10px; }
	.tab-area .tabs li a span { padding:20px 20px 10px 20px; }
}

@media screen and (min-width:481px) and (max-width: 640px) {
	.main-slider .one .contents { top:40px; }
	.main-slider .one .contents h2 { font-size:3em; }
	.main-slider .one .contents h4 { font-size:2em; }
	.main-slider .one .contents .buttons { display:none; }
	.main-slider .flex-control-nav { bottom:20px; }
}

/* extra small device (xs)*/
@media screen and (max-width: 767px) {
	body { padding:0 5px; }
	.container { padding:0 10px; }
	.row { margin:0 -10px; }
	[class^="col-"] { padding:0 10px; }
	.navbar-brand { max-width:70%; }
	#navbar { float:none; text-align:center; background-color:#67b300; }
	.navbar-default .navbar-nav > li > a { color:#fff; }
	.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus { background-color:#99cc00; color:#fff; }
	.navbar-nav { margin:0 -15px; }
	.navbar-default .navbar-nav > li.campaign > a { border-radius:0; }
	.navbar-default .navbar-nav > li.campaign > a:after { border-radius:0; }
	.navbar-default .navbar-nav .dropdown.open { background-color:#99cc00; }
	.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover { color:#fff; }
	.navbar-default .navbar-nav .dropdown .char { box-shadow:0 0 3px rgba(0, 0, 0, 0.2); }
	.navbar-default .navbar-nav .dropdown.open .dropdown-menu { padding:5px 0; }
	.navbar-default .navbar-nav .dropdown.open .dropdown-menu > li > a { color:#fff; text-align:center; padding:10px 20px; }
	.navbar-default .navbar-nav .dropdown.open .dropdown-menu > li > a:hover, .navbar-default .navbar-nav .dropdown.open .dropdown-menu > li > a:focus { background-color:#67b300; color:#fff; }	
	.main-slider { margin:0 -5px; }
	.featured-campaigns .box { margin:0 0 20px 0; }
	.featured-campaigns .box .contents {}
	.featured-campaigns .campaign-scroller li { float:none; width:100%; margin:0 0 20px 0; }	
	.footer-top { margin:0 -5px; padding-left:5px; padding-right:5px; }
	.footer-top h2 { font-size:2em; }
	.footer-top .buttons a { margin-bottom:5px; }
	.big-button span i { padding:18px 10px 16px 0; }
	.big-button span b { padding:18px 20px 18px 20px; }	
	footer { margin:0 -5px; }
	footer .top .links { margin:0 0 20px 0; }
	footer .top .contact { margin:0 0 20px 0; }
	footer .top .newsletter-form { text-align:center; }
	footer .top .newsletter-form .cover { margin:0 0 5px 0; }
	footer .top .newsletter-form button[type="submit"] { position:relative; margin:0 auto; width:100%; }
	footer .bottom { padding-bottom:80px; background-position:center bottom; }
	footer .bottom .links { text-align:left; margin:10px 0 0 0; }
	footer .bottom .links a { margin:0 5px; }
	footer .bottom .links a:first-child { margin-left:0; }
	.fund-campaigns .campaign-search .input-box, .fund-campaigns .campaign-search .select-box { float:none; width:100%; margin:0 0 -1px 0; }
	.fund-campaigns .campaign-search button[type="submit"] { float:none; margin:5px auto; display:block; width:100%; }
	.profile-banner .content .data { margin:0 0 0 70px; }
	.profile-banner .content .data .left { width:100%; }
	.profile-banner .content .data .right { width:100%; }
	.profile-right { margin:20px 0 0 0; }
	.profile-right .metadata .highlight { font-size:2.4em; }
	.profile .tab-area .tab-data .tab-contents .footer .pull-right { margin-top:10px; }
	.profile .tab-area .tabs li { width:50%; }
	.tab-area .tab-data .tab-contents .footer .button { margin-top:5px; }
	.tab-area .tab-data #tab2 article img { margin-bottom:20px; }
	.profile .tab-area .tabs { display:none; }
	.tab-data .tab-contents { padding: 20px 0 40px 0; }
	.tab-btn-xs { display:block; }
	.profile #tab2 .pic-slider .carousel-cell { width:100%; }
	.profile #tab2 .pic-slider .flickity-prev-next-button { width:40px; }
	.donate-banner-right > h3 { margin-top:10px; }
	.donate-banner-right .profile-data { width:100%; }
	.donate-banner-right .metadata { margin:20px 0 0 0; width:100%; }
	.donate-banner-right .percent-circle .c100 { margin:0 5px -60px 0; top:-60px; }
	.donate .tab-area .tabs li { width:100%; margin-bottom:40px; }
	.donate .tab-area .tabs li:last-child { margin-bottom:0; }
	.donate .tab-area .tab-data .tab-contents .buttons { text-align:center; }
	.donate .tab-area .tab-data .tab-contents .text-right { text-align:center; }
	.donate .tab-area .tab-data .tab-contents .form { margin:20px 0 0 0; }
	.donate .tab-area .tab-data #tab3 .ccv { right:0px; }
	.signup-box { display:block; }
	.signup-box .col { padding:20px 5px 60px; display:block; width:100%; }
	.signup-box .col:last-child { padding:60px 5px 20px; border-left:0; border-top:1px solid #dedede; }
	.signup-form-box { width:98%; }
	.signup-form-box { display:block; height:auto; }
	.signup-form-box .col-left { display:block; width:100%; border-radius:5px 5px 0 0; }
	.signup-form-box .col-right { display:block; width:100%; padding:20px 10px; }
	.signup .section_organization_2 .radios span { width:50%; margin:0 0 20px 0; }
	.create-campaign .tab-data .form .radios span { width:50%; }
	.container .jumbotron { padding:10px 10px; }
	.create-campaign .tab-area .tabs li { width:33.333333%; padding-bottom:10px; }
	.create-campaign .tab-area .tab-data { margin:30px 10px 0; padding:20px 20px; }
	.create-campaign .tab-data .step-1 .form .one-third .sbOptions li { width:100%; float:none; }
	.create-campaign .tab-data .form .buttons .button { margin-bottom:10px; }
	.create-campaign .tab-data .right-panel { padding:0; }
	.create-campaign .tab-data .right-panel h2 { padding:0; }
	.create-campaign .tab-data .right-panel h3 { padding:0; }
	.campaign-preview-box { margin:20px auto; max-width:250px; position:relative; }
	.campaign-preview-box .data { padding:30px 20px 0 20px; }
	.dashboard .dashboard-header { text-align:center; padding-top:20px; }
	.dashboard .dashboard-header .right { text-align:center; float:none; }
	.dashboard .dashboard-header .right span:first-child { margin:0; }
	.dashboard .dashboard-header .right span a:last-child { margin-right:0; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box { width:250px; margin:0 auto 10px auto; padding-bottom:10px; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box figure { display:block; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents { display:block; padding:0; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .share { display:block; border-left:0; border-top:1px solid #dedede; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box figure { width:100%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share { width:100%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share .actions { border:0; display:block; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; width:100%; text-align:left; border-top:1px solid #dedede; padding:10px 10px; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents h3 { border-bottom:1px solid #dedede; padding:0 10px 10px 10px; margin:0 -10px 5px -10px; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box figure { width:100%; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .actions { width:100%; display:block; border-left:0; border-top:1px solid #dedede; padding:10px 0 0 0; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; width:100%; text-align:left; border-top:1px solid #dedede; padding:10px 10px; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
	.dashboard.campaign-creator .tab-area .tabs li { width:100%; padding-top:20px; }
	.dashboard.campaign-creator .tab-area .tabs li:first-child { padding-top:0px; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box figure { width:100%; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .actions { width:100%; display:block; border-left:0; border-top:1px solid #dedede; padding:10px 0 0 0; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; width:100%; text-align:left; border-top:1px solid #dedede; padding:10px 10px; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
	.dashboard.organization .tab-area .tabs li a { width:100%; }
	.dashboard.organization .tab-area .tabs li { width:100%; padding-top:20px; }
	.dashboard.organization .tab-area .tabs li:first-child { padding-top:0px; }
	.dashboard .tab-area .tab-data .campaigns .create-campaign-box h3 i { display:block; margin:0 0 20px 0; text-align:center; }
	.dashboard .tab-area .tab-data { overflow:hidden; }
	.dashboard .tab-area .tab-data .updates .updates-group .date { margin-bottom:20px; }
	.dashboard .tab-area .tab-data .updates .updates-box { display:block; }
	.dashboard .tab-area .tab-data .updates .updates-box .col-one { display:block; width:100%; text-align:center; }
	.dashboard .tab-area .tab-data .updates .updates-box .col-two { display:block; width:100%; text-align:center; }
	.dashboard .tab-area .tab-data .updates .updates-box .col-three { display:block; width:100%; text-align:center; }
	.dashboard .tab-area .tab-data .updates .updates-box .col-four { display:block; position:absolute; top:10px; right:10px; }
	.dashboard .tab-area .tab-data .updates .paging { text-align:center; }
	.dashboard .dashboard-header figure { position:relative; top:0; margin:0 auto; }
	.dashboard .dashboard-header figure + .content { margin:10px 0 0 0; }
	.dashboard .left-panel { margin-bottom:30px; }
	.dashboard.account-settings .inner-tabs { padding-bottom:0; }
	.dashboard.account-settings .inner-tabs li { margin:0 0 30px 0; width:100%; }
	.account-settings .friend-box figure { margin:0 auto 20px auto; float:none; }
	.account-settings .friend-box .contents { float:none; max-width:100%; text-align:center; margin:0 0 20px 0; }
	.account-settings .friend-box .dropdown { float:none; margin:0 auto; }
	.account-settings .discover-campaign-box { padding:30px 10px; }
	.account-settings .discover-campaign-box .icon { width:100%; text-align:center; }
	.account-settings .discover-campaign-box .contents { width:100%; text-align:center; }
	.account-settings .discover-campaign-box .buttons { width:100%; text-align:center; margin:5px 0 0 0; }
	.account-settings .tab-data .tab-contents.account .right-panel { margin-top:30px; }
	.dashboard.account-settings .tab-data { padding:20px 10px; }
	.buttons { text-align:center; }
	.button { min-width:220px; padding:10px 15px; margin-bottom:5px; }
	.row-eq-height { display: block; }
	div.search-campaigns .campaign-search .input-box { margin: 0 0 5px 0; width: 100%; }
	div.search-campaigns .campaign-search .select-box { margin: 0 0 5px 0; width: 100%; }
	div.search-campaigns .campaign-search .list-inline { margin-left: 0; }
	div.search-campaigns .campaign-search .list-inline > li { display: block; }
	div.search-campaigns .campaign-search { margin-top: 30px; }
	.btn-gridview { width: 100%; }
	.campaign-category .nav > li { width: 50%; float: left; }
	.campaign-footer p, .campaign-footer nav { width:100%; text-align:center; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box { max-width:270px; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box > img { display:block; float:none; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box	.data { margin:0; text-align:center; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box .category-meta { position:static; width:100%; border:0; border-top:1px solid #ccc; margin:10px 0 0 0; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box .percent-circle { position:static; margin:-55px auto 0 auto; }
	.search-campaigns .campaign-boxes .box .data .user-info { margin:10px 0 0 0; }
	.search-campaigns .campaign-boxes.listview .box .category-meta > span { float:left; width:33.333333%; }
	.search-organization .campaign-search .input-box { margin: 0 0 5px 0; width: 100%; }
	.search-organization .campaign-search .select-box { margin: 0 0 5px 0; width: 100%; }
	.search-organization .campaign-search .list-inline { margin-left: 0; }
	.search-organization .campaign-search .list-inline > li { display: block; }
	.btn-gridview { width: 100%; }
	.search-organization .campaign-search { margin-top: 30px; }
	.search-organization .fund-campaigns .campaign-boxes.listview .box { max-width:270px; }
	.search-organization .fund-campaigns .campaign-boxes.listview .box > img { display:block; float:none; }
	.search-organization .fund-campaigns .campaign-boxes.listview .box	.data { margin:0; text-align:center; }
	.search-organization .fund-campaigns .campaign-boxes.listview .box .category-meta { position:static; width:100%; border:0; border-top:1px solid #ccc; margin:20px 0 0 0; }
	.search-organization .campaign-boxes.listview .box .category-meta > span { float:left; width:50%; padding:10px 0 10px 0; }
	.popup { width:98%; left:1%; margin:0 !important; }
	.popup h2 { padding:15px 30px 15px 15px; }
	.popup.edit-picture .left-panel { width:100%; float:none; padding:0; }
	.popup.edit-picture .right-panel { width:100%; float:none; margin-top:30px; border:0; padding:0; }
	.how-it-works-page .step-box.one:after, .how-it-works-page .step-box.two:after, .how-it-works-page .step-box.three:after  { -webkit-transform:rotate(45deg); -moz-transform:rotate(45deg); -ms-transform:rotate(45deg); -o-transform:rotate(45deg); transform:rotate(45deg); background-size:76%; height:190px; }
	.how-it-works-page .step-box .content { margin-bottom:20px; }
	.how-it-works-page .step-box figure { margin-bottom:20px; }
	.create-campaign-preview .main-box { padding:40px 0; }
	.create-campaign-preview .profile-banner .content .data .right .likes { float:left; }
	.profile.create-campaign-preview .tab-area .tabs li a span { padding:20px 10px 10px 10px; min-height:50px; }
	.create-campaign-preview .donation-box .content { margin:5px 0 0 60px; padding:0 10px 0 0; float:none; }
	.withdraw .form .form-row [class^="col-"] + [class^="col-"] { margin-top:10px; }
	.withdraw .form-row .shift-next { margin:0; }
	.transaction-history .form .form-row label { display:block; margin:20px 0 0 0; }
	.transaction-history .form .input-box { width:100%; margin:10px 0 0 0; }
	.transaction-history .form .form-row label + .input-box { margin:5px 0 0 0; }
	.transaction-history .form button { margin:10px 0 0 0; width:100%; }
	.transaction-history .form .links { margin:10px 0 0 0; width:100%; text-align:center; }
	.ui-datepicker-trigger { width:40px !important; margin:5px 0 0 0 !important; }
	.grid-header { display:none; }
	.grid-row b { display:block; }
	.datagrid [class^="col-"] { display:block; width:100% !important; text-align:left !important; }
	.edit-campaign .left-panel .buttons { margin-bottom:-100px; }
	.edit-campaign .right-panel { padding:0; }
	.edit-campaign .form .one-third .sbOptions li { width:100%; float:none; }
	.contact-us .contacts [class^="col-"] { display:block; text-align:center; width:100%; margin:0 0 40px 0 }
	.contact-us .contacts [class^="col-"] span { display:block; float:none; margin:0 !important; }
	.scroll-up { right:5px; bottom:5px; }
	.profile-org .tab-panel .tab-header .tabs { display:none; }
	.profile-org .tab-panel .tab-header .buttons { float:none; text-align:center; }
	.profile-org .tab-header-xs { display:block; }
	.profile-org .campaign-scroller .campaigns-slider { width:270px; margin:0 auto; }
	.profile-org .campaign-scroller .campaigns-slider .slides li { width:100%; }
	.profile-org .our-network .box { margin-bottom:30px; }
	.profile-org .contact-us .box { padding:0; margin-bottom:30px; }
	.carousel-cell { width:100%; }
	.carousel-cell .content { padding:10px 15px; font-size:1em; }
}
/* /extra small device (xs)*/

/* small device (sm) */
@media screen and (min-width: 768px) and (max-width: 991px) {
	.navbar-header { max-width:20%; margin:10px 0 0 0; }
	.nav > li > a { padding:10px 5px; }
	.navbar-default .navbar-nav > li.campaign { margin:0; }
	#navbar { margin:30px 0 0 0; }	
	.main-slider .one .contents { top:30px; }
	.main-slider .one .contents h2 { font-size:2.6em; }
	.main-slider .one .contents h4 { font-size:1.8em; }
	.banner-button .icon { left:-40px; }
	.banner-button.donate .icon { right:-40px; }
	.main-slider .flex-control-nav { bottom:25px; }
	.fund-campaigns .campaign-boxes .box .data { min-height:240px; }
	.fund-campaigns .campaign-boxes .box .category-meta { padding:0 10px; }
	.fund-campaigns .campaign-search .input-box { width:50%; }
	.fund-campaigns .campaign-search .select-box { width:30.8%; margin-top:-1px; }
	.fund-campaigns .campaign-search button[type="submit"] { width:48px; height:48px; margin:1px 2px 0 0; }	
	footer .top .contact { margin:0 0 20px 0; }
	footer .bottom .links a { margin:0 10px; }
	.donate-banner-right .profile-data { width:100%; }
	.donate-banner-right .metadata { margin-left:80px; }
	.donate-banner-right .metadata + .metadata + .metadata { margin-left:0; }
	.donate .tab-area .tab-data #tab3 .ccv { right:0px; }
	.signup-box .col { padding:100px 80px 100px 50px; }
	.signup-box .col:last-child { padding:100px 50px 100px 80px; }
	.signup-form-box { width:96%; }
	.create-campaign .tab-data .form .days-container { width:25%; }
	.create-campaign .tab-data .form .slider-bar { width:75%; }
	.create-campaign .tab-data .form .slider-bar b { width:25%; }
	.create-campaign .tab-data .form .slider-bar #day-slider { width:50%; }
	.create-campaign .tab-data .form .buttons .button { margin-bottom:10px; }
	.create-campaign .tab-data .right-panel { padding:0; }
	.create-campaign .tab-data .right-panel h2 { padding:0; }
	.create-campaign .tab-data .right-panel h3 { padding:0; }
	.campaign-preview-box { margin:20px 0; }
	.create-campaign .tab-area .tab-data { padding:20px 20px; }
	.dashboard .dashboard-header { padding-top:30px; }
	.dashboard .dashboard-header figure { width:130px; height:130px; top:-24px; }
	.dashboard .dashboard-header figure + .content { margin-left:150px; }
	.dashboard .dashboard-header h2 { margin-top:0px; }
	.dashboard .dashboard-header .right { margin:0; }
	.dashboard .dashboard-header .right span:first-child { margin:0; }
	.dashboard .dashboard-header .right span a:last-child { margin-right:0; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box { position:relative; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box:before, .dashboard .tab-area .tab-data .campaigns .campaign-box:after { content:""; display:table; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box:after { clear:both; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box figure { display:block; float:left; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box figure { width:35%; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents { display:block; float:left; padding:0; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents { width:65%; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .share { display:block; padding:10px 0 10px 0; border:0; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share { width:35%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share .actions { border:0; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .share:before, .dashboard .tab-area .tab-data .campaigns .campaign-box .share:after { content:""; display:table; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .share:after { clear:both; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .share p { float:left; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .share p:first-child { margin:0 10px 0 0; padding:10px 0 0 10px; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; float:left; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; margin:0 20px 10px 20px; border-bottom:1px solid #dedede; padding:10px 0; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:30%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:30%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-four { width:20%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-five { width:20%; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one h3 { margin-bottom:5px; }
	.dashboard .tab-area .tab-data .campaigns .create-campaign-box h3 { font-size:1.6em; }
	.dashboard.campaign-creator .tab-area .tabs li { width:33.333333%; }
	.dashboard.campaign-creator .tab-area .tabs li a { width:auto; font-size:1em; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box { display:block; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box figure { width:50%; display:block; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; display:block; padding:0 0 15px 0; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .actions { width:50%; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; display:block; margin:0 10px 10px 10px; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:50%; text-align:left; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:50%; text-align:left; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
	.dashboard.organization .tab-area .tabs li { width:30%; }
	.dashboard.organization .tab-area .tabs li:first-child { width:40%; }
	.dashboard.organization .tab-area .tabs li a { width:auto; font-size:1em; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box { display:block; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box figure { width:50%; display:block; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; display:block; padding:0 0 15px 0; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .actions { width:50%; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; display:block; margin:0 10px 10px 10px; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:50%; text-align:left; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:50%; text-align:left; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
	.dashboard.public-profile.organization .tab-area .tabs li { width:50%; }
	.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { margin:0; }
	.dashboard .left-panel .box { padding:15px 10px; }
	.dashboard.account-settings .tab-area .tabs li a { width:auto; padding:0 20px 20px 20px; }
	.account-settings .discover-campaign-box .contents { width:90%; padding-left:10px; }
	.account-settings .discover-campaign-box .buttons { width:100%; float: left; padding:12px 0 0 70px; text-align:left; }
	.dashboard.account-settings .tab-data { padding:20px 20px; }
	div.search-campaigns .campaign-search .input-box { margin:0 0 5px 0; width: 100%; }
	div.search-campaigns .campaign-search .select-box { margin:0 1% 5px 0; width: 49.5%; }
	div.search-campaigns .campaign-search .select-box.double { margin-right:0; }
	div.search-campaigns .campaign-search .list-inline { margin-left: 0; float:left; width:100%; }
	div.search-campaigns .campaign-search .list-inline > li { width:49.5%; margin:0 1% 0 0; display:block; float:left; }
	div.search-campaigns .campaign-search .list-inline > li:last-child { margin-right:0; }
	div.search-campaigns .campaign-search button[type="submit"] { width:100%; }
	div.search-campaigns .campaign-search .btn-gridview { width: 100%; }
	div.search-campaigns .campaign-boxes .box .data { min-height: 340px; }	
	.search-campaigns .fund-campaigns .campaign-category { margin-bottom:30px; }
	.search-campaigns .fund-campaigns .campaign-category.listview .nav > li { width:33.333333%; float:left; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box > img { width:240px; height:100%; position:absolute; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box .data { margin:0 120px 0 290px; padding:10px; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box .percent-circle { left:-100px; }	
	.search-organization .campaign-search .input-box { margin:0 0 5px 0; width: 100%; }
	.search-organization .campaign-search .select-box { margin:0 1% 5px 0; width: 49.5%; }
	.search-organization .campaign-search .select-box.double { margin-right:0; }
	.search-organization .campaign-search .list-inline { margin-left: 0; float:left; width:100%; }
	.search-organization .campaign-search .list-inline > li { width:49.5%; margin:0 1% 0 0; display:block; float:left; }
	.search-organization .campaign-search .list-inline > li:last-child { margin-right:0; }
	.search-organization .campaign-search button[type="submit"] { width:100%; }
	.search-organization .campaign-search .btn-gridview { width: 100%; }
	.search-organization .fund-campaigns .campaign-category { margin-bottom:30px; }
	.search-organization .fund-campaigns .campaign-category.listview .nav > li { width:33.333333%; float:left; }
	.search-organization .fund-campaigns .campaign-boxes.listview .box	.data { padding:10px; }
	.search-organization .fund-campaigns .campaign-boxes.listview .box > img { position:absolute; }
	.how-it-works-page .step-box.one:after, .how-it-works-page .step-box.two:after, .how-it-works-page .step-box.three:after  { background-size:45%; }
	.how-it-works-page .step-box .content { width:60%; }
	.how-it-works-page .step-box figure { width:40%; }
	.how-it-works-page .step-box .step { margin-top:0; }
	.how-it-works-page .step-box h2 { font-size:1.5em; }
	.create-campaign-preview .main-box { padding:20px 0; }
	.profile-banner .content .data { margin:0 0 0 70px; }
	.profile-banner .content .data .time-stamp { margin:10px 0 0 0px; }
	.profile-banner .content .data .right .shares { margin-top:0; }
	.profile .tab-area .tabs li a span { padding:15px 10px 15px 10px; font-size:0.92em; }
	.char { width:40px; height:40px; line-height:38px; }
	.donation-box .content { padding:0; margin:0 0 0 50px; }
	.donation-box .content .price-tag { font-size:2.0em; }
	.transaction-history .form .input-box { margin:0 10px 10px 0; width:220px; }
	.transaction-history .form .input-box.selectbox { margin-left:50px; }
	.transaction-history .form .form-row label { min-width:40px; }
	.transaction-history .form button { vertical-align:top; }
	.transaction-history .form .links { margin:10px 0 0 0; width:100%; text-align:center; }
	.popup.blue { width:98%; margin-left:1%; left:0; }
	.dashboard-header-sm h2 { margin-left:120px; }
	.edit-campaign .right-panel { padding:0; }
	.edit-campaign .form .one-third .sbOptions li { width:50%; }
	.button { margin-bottom:10px; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata { font-size:0.92em; margin-left:10px; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight { font-size:1.5em; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight small { font-size:0.6em; }
	.popup.edit-picture .left-panel { width:55%; }
	.popup.edit-picture .right-panel { width:45%; }
	.contact-us .contacts [class^="col-"] { text-align:center; vertical-align:top; }
	.contact-us .contacts [class^="col-"] span { display:block; float:none; margin:0 !important; min-height:70px; }
	.contact-us .contacts [class^="col-"] span + span { min-height:0; }
	.scroll-up { right:10px; bottom:10px; }
	.donate.organization .form .radios span { width:50%; }
	.profile-org .campaign-scroller .campaigns-slider .slides li { width:33.333333%; }
	.profile-org .our-network .box { padding:0 10px; }
	.profile-org .contact-us .box { padding:0; margin-bottom:20px; min-height:80px; }
	.carousel-cell { width:70%; }
	.carousel-cell .content { padding:10px 15px; font-size:1em; }
	.profile #tab2 .pic-slider .flickity-prev-next-button { width:50px; }
}
/* /small device (sm) */

/* medium device (md) */
@media screen and (min-width: 992px) and (max-width: 1199px) {
	.main-slider .one .contents { top:50px; }
	.main-slider .flex-control-nav { bottom:35px; }
	.fund-campaigns .campaign-boxes .box .data { min-height:240px; }
	.fund-campaigns .campaign-boxes .box .category-meta { padding:0 20px; }
	.fund-campaigns .campaign-search button[type="submit"] { width:48px; }
	.donate-banner-right .profile-data { width:60%; }
	.donate-banner-right .metadata { margin-left:50px; }
	.donate-banner-right .percent-circle .c100 { margin-right:0; }
	.signup-form-box { width:85%; }
	.create-campaign .tab-data .right-panel { padding:0; }
	.create-campaign .tab-data .right-panel h2 { padding:0; max-width:250px; margin:0 auto; }
	.create-campaign .tab-data .right-panel h3 { padding:0; max-width:250px; margin:0 auto; }
	.campaign-preview-box { margin:20px auto; max-width:250px }
	.create-campaign .tab-area .tab-data { padding:20px 20px; }
	.dashboard .dashboard-header .right { margin:-10px 0 0 0;}
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box figure { width:30%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents { width:54%; padding:0; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share { width:16%; padding:50px 0 0 0; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share .actions { border:0; display:block; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; float:left; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; margin:0 20px 10px 20px; border-bottom:1px solid #dedede; padding:10px 0; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:30%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:30%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-four { width:20%; }
	.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-five { width:20%; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one h3 { margin-bottom:5px; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box figure { width:40%; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents { width:60%; padding:0; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .actions { width:100%; padding:5px 5px; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; display:block; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:50%; text-align:left; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-two p { padding-left:5px; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:50%; text-align:left; }
	.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box figure { width:40%; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents { width:60%; padding:0; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .actions { width:100%; padding:5px 5px; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; display:block; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:50%; text-align:left; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-two p { padding-left:5px; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:50%; text-align:left; }
	.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
	.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a:first-child { margin-top:0; }
	.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { margin:0; }
	.dashboard .left-panel .box { padding:15px 10px; }
	div.search-campaigns .campaign-boxes .box .data { min-height: 350px; padding:0 10px; }
	.search-campaigns .campaign-boxes .box .data .user-info { padding:0 20px 0 10px; }
	div.search-campaigns .campaign-search .input-box { width:33%; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box > img { width:240px; height:100%; position:absolute; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box .data { margin:0 120px 0 290px; padding:10px; }
	.search-campaigns .fund-campaigns .campaign-boxes.listview .box .percent-circle { left:-100px; }
	.search-organization .campaign-search .input-box { width:33%; }
	.search-organization .fund-campaigns .campaign-boxes.listview .box	.data { padding:10px; }
	.search-organization .fund-campaigns .campaign-boxes.listview .box > img { position:absolute; }
	.how-it-works-page .step-box.one:after, .how-it-works-page .step-box.two:after, .how-it-works-page .step-box.three:after  { background-size:45%; }
	.how-it-works-page .step-box .content { width:60%; }
	.how-it-works-page .step-box figure { width:40%; }
	.how-it-works-page .step-box h2 { font-size:1.8em; }
	.profile-banner .content .data { margin:0 0 0 70px; }
	.profile-banner .content .data .location { float:none; display:block; }
	.profile-banner .content .data .time-stamp { float:none; display:block; margin:5px 0 0 0px; }
	.donation-box .content { padding:0 0 10px 0; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata { font-size:0.92em; margin-left:10px; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight { font-size:1.5em; }
	.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight small { font-size:0.6em; }
	.contact-us .contacts [class^="col-"] { text-align:center; vertical-align:top; }
	.contact-us .contacts [class^="col-"] span { display:block; float:none; margin:0 !important; min-height:70px; }
	.contact-us .contacts [class^="col-"] span + span { min-height:0; }
	.scroll-up { right:20px; bottom:20px; }
	.profile-org .contact-us .box { padding:0; margin-bottom:20px; }
	.profile .tab-area .tabs li a span { padding:15px 20px 15px 20px; }
	.profile #tab2 .pic-slider .flickity-prev-next-button { width:60px; }
}
/* /medium device (md) */

/* large device (lg) */
@media screen and (min-width: 1200px) {
	.campaign-facts { padding:0 100px; }
}
/* /large device (lg) */
  .deafultcoverimg{
		background-color: #f8f8f8;
		background-size: cover;
		border: 1px solid #ccc;
		border-radius: 3px;
		margin-top: 7px;
		width: 700px;
		height: 250px;
	}	
  .cropit-preview {
	background-color: #f8f8f8;
	background-size: cover;
	border: 1px solid #ccc;
	border-radius: 3px;
	margin-top: 7px;
	width: 700px;
	height: 250px;
  }

  .cropit-preview-image-container {
	cursor: move;
  }

  .image-size-label {
	margin-top: 10px;
  }

  input, .export {
	display: block;
  }

  button {
	margin-top: 10px;
  }
</style>
<div id="content" class="page organization-profile">
	<div class="container">
		<div class="row">
			<div class="contents">
				<h2>Create your profile</h2>
				<h3>Make a great first impression with your organizational profile.</h3>
				<div class="tab-area">
					<ul class="tabs">
						<li class="one">Basic Info</li>
						<li class="active two">Images/Video</li>
					</ul>
					
					<div class="tab-data">
				
						
						<!-- images/video tab -->
                        	
                                <div class="form">
                                    <div class="form-row" id="coverimg">
                                        <h4>Cover Background Image</h4>
                                        <div class="full-pic-box">
                                          <figure>
                                          		<?php if(isset($profiledetail->image) || $profiledetail->image!='' || strlen($profiledetail->image) !=0){?>
                                          			<img src="<?php echo base_url(); ?>uploads/organization_profile/cover_images/<?php echo $profiledetail->image; ?>" alt="" />
                                                <?php } else { ?>
                                          			<div class="deafultcoverimg"></div>
                                                <?php } ?>
                                          </figure>
                                            <p class="mt10"><a href="javascript:;" id="upload_link" data-target="#myModal12" data-toggle="modal" class="more"><i class="fa fa-camera"></i>Add Another Image</a></p>
                                            <!--<input type="file" id="orgcoverpics" style="display:none"  name="orgcoverpics" onchange="savecoverimage();" >-->
                                        </div>
                                    </div>
                                    <form name="frm_gellary" id="frm_gellary" method="post" action="">
                                        <div class="form-row">
                                            <h4>Gallery Images</h4>
                                            <div class="pictures">
                                            <?php 
												if(count($gellary_image)>0){
													foreach($gellary_image as $dis_gellary_image){
											?>
                                            
                                                <div class="pic-box" id="ID_<?php echo $dis_gellary_image->id; ?>">
                                                  <figure class="galleryimages">
                                                        <img src="<?php echo base_url(); ?>uploads/organization_profile/gellary_image/<?php echo $dis_gellary_image->image_name; ?>" width="270" height="119" alt="" />
                                                        <a href="javascript:fun_delete_gellaey_image(<?php echo $dis_gellary_image->id; ?>);"><i class="fa fa-trash-o"></i></a>
                                                  </figure>
                                                    <!--<div class="select">
                                                        <input type="radio" name="isDeafult" value="1" <?php// if($dis_gellary_image->isDeafult==1){?> checked="checked" <?php// } ?> /> <label for="profile_pic_1">Set as profile picture</label>
                                                        
                                                    </div>-->
                                                </div>
                                            	
                                            <?php } }else { ?>
                                                    <div class="pic-box">
                                                        <figure class="galleryimages">
                                                            <img src="<?php echo base_url(); ?>assets/images/data/campaign-17.jpg" alt="" />
                                                            <a href="#"><i class="fa fa-pencil"></i></a>
                                                        </figure>
                                                        <!--<div class="select">
                                                        	<input type="checkbox" id="profile_pic_1" class="fancy-checkbox sm" checked="checked" /> <label for="profile_pic_1">Set as profile picture</label>
                                                        </div>-->
                                                    </div>
                                            <?php } ?>
                                            </div>
                                      </form>      
                                            <a href="javascript:void(0);" data-target="#myModal" data-toggle="modal" class="upload-box">Choose an image from your computer</a>
                                            <input type="file" id="orggallarypics" multiple style="display:none"  name="orggallarypics" >
                                        </div>
                                        <form name="frm_videos" id="frm_videos" method="post" action="<?php echo base_url(); ?>user/savevideos">
                                        <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                        <input type="hidden" name="counter" id="counter" value="1" />
                                            <div class="form-row">
                                                <h4>Video <small>(optional)</small></h4>
                                                <div class="pictures">
                                                <?php foreach($allVideosLink as $disVideoLink){?>
                                                    <div class="pic-box">         
                                                        <div class="video">
                                                            <iframe width="200" height="115"
                                                                    src="https://www.youtube.com/embed/<?php echo $disVideoLink->image_name; ?>">
                                                            </iframe>
                                                        </div>
                                                        <div class="select">
                                                            <input type="checkbox" id="profile_vid_1" class="fancy-checkbox sm" checked="checked" /> <label for="profile_vid_1">Set as profile video</label>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                </div>
                                                <input type="text" placeholder="Youtube or Vimeo URL" name="videourl1" value="">
                                                <br /><br />
                                                <div id="fooBar"></div>
                                                <p class="mt10"><a href="javascript:add();" id="addPet" class="more"><i class="fa fa-video-camera"></i>Add More Videos</a></p>
                                            </div>
                                            <div class="form-row">
                                                <div class="buttons">
                                                    <a href="javascript:frm_submit();" class="button blue"><span>Save &amp; Preview</span></a>
                                                    <a href="javascript:void(0);" class="button"><span>Discard Changes</span></a>
                                                </div>
                                            </div>
                                    	</form>
                                </div>
                            
						<!-- /images/video tab -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Gellary Image</h4>
            </div>
            <div class="modal-body">
               <form name="frm_gellary_image" id="frm_gellary_image" method="post" action="<?php echo base_url(); ?>user/savegellaeyimage" enctype="multipart/form-data"> 
               <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                   <section class="step-1">
                        <div class="form">
                            <div class="form-row">
                                <h4>Name</h4>
                                <input type="text" name="title" id="title"  class="form-control" value="" placeholder="Image Caption" />
                            </div>
                            <br />
                            <div class="form-row">
                                <div class="input-group">
                                    <p class="mt10"><a href="javascript:;" class="more" id="upload_link1"><i class="fa fa-camera"></i> Choose an image from your computer</a></p>
                                    <input type="file" style="display:none;" name="image_name" id="image_name">
                                </div>
                            </div>
                            <div class="pic-box">
                                <figure class="galleryimages">
                                    <img id="blah1" src="" alt="" />
                                </figure>
                            </div>
                            <br />
                            <div class="form-row">
                                <div class="buttons">
                                    <input type="submit" name="save" class="button blue" value="Save" />
                                    <button type="button" class="button blue" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="myModal12" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:735px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <div class="image-editor">
                    <input type="file" class="cropit-image-input" required>
                    <div class="cropit-preview"></div>
                    <div class="image-size-label">
                        Resize image
                    </div>
                    <input type="range" class="cropit-image-zoom-input">
                    <!--<button class="rotate-ccw">Rotate counterclockwise</button>
                    <button class="rotate-cw">Rotate clockwise</button>-->
                    <button class="button blue export">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  $(function() {
	$('.image-editor').cropit({
	  imageState: {
		src: '//lorempixel.com/500/400/',
	  },
	});

	$('.rotate-cw').click(function() {
	  $('.image-editor').cropit('rotateCW');
	});
	$('.rotate-ccw').click(function() {
	  $('.image-editor').cropit('rotateCCW');
	});

	$('.export').click(function() {
								
	  var imageData = $('.image-editor').cropit('export');
	  //window.open(imageData);
	  //alert(imageData);
	  $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>user/file_cover_image",          
		data: {'imgdata':imageData,'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
			success: function(total){
				$('#myModal12').modal('hide');
				window.location.reload();
			}
		});
	 });
  });
</script>
<script type="text/javascript">
	
	var tmp = 2;
	function add() 
	{
		window.document.getElementById('counter').value=tmp;
		//Create an input type dynamically.
		var element = document.createElement("input");
		
		//var mybr = document.createElement('br');
		//Assign different attributes to the element.
		element.setAttribute("type", "text");
		element.setAttribute("value", "");
		element.setAttribute("name", "videourl"+tmp);
		element.setAttribute("placeholder", "Youtube or Vimeo URL");
		//element.setAttribute("style", "width:200px");
		
		var brElement = document.createElement("br");
		var brElement1 = document.createElement("br");
	
		
		// 'foobar' is the div id, where new fields are to be added
		var foo = document.getElementById("fooBar");
		
		//Append the element in page (in span).
		foo.appendChild(element);
		foo.appendChild(brElement );
		foo.appendChild(brElement1 );
		
		tmp++;
		//document.write('<br/>');
	}

</script>
<script type="text/javascript">
	$(function(){
		$("#upload_link").on('click', function(e){
			e.preventDefault();
			$("#orgcoverpics:hidden").trigger('click');
		});
	});
</script>
<script type="text/javascript">
	$(function(){
		$("#upload_link1").on('click', function(e){
			e.preventDefault();
			$("#image_name:hidden").trigger('click');
		});
	});
</script>
<script type="text/javascript">
	function frm_submit()
	{
		var frm_obj = window.document.frm_videos;
		frm_obj.submit();
	}
	function savecoverimage()
	{
		var frm_obj = window.document.frm_cover_image;
		frm_obj.submit();
		/*$.ajax({
           type: "POST",
           url: '<?php echo base_url(); ?>user/file_cover_image',
           data: $("#frm_cover_image").serialize(),
           success: function(data)
           {
               alert(data); 
           }
         });*/
	}
	function fun_delete_cover_image()
	{
		$.ajax({
           type: "POST",
           url: '<?php echo base_url(); ?>user/delete_cover_image',
           data: {'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
           success: function(data)
           {
               window.location.reload();
           }
         });	
	}
	function fun_delete_gellaey_image(id)
	{
		if(confirm("Are you sure want to delete this image? "))
		{
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>user/delete_gellery_image",
				data: {'id':id,'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
				success: function(total){
					//$("#ID_"+id).animate({ opacity: "hide" }, "slow");
					window.location.reload();
				}
			});
		}
	}	
</script>

<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
	
			reader.onload = function (e) {
				$('#blah1').attr('src', e.target.result).height(160).width(270);;
			}
	
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$("#image_name").change(function(){
		readURL(this);
	});
</script>

