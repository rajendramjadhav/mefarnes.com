<!-- content -->
<?php //var_dump($users->user_image);exit;?>
<div id="content" class="page dashboard campaign-creator">
    <div class="dashboard-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
<!--                    <figure><img src="<?php echo base_url(); ?>assets/images/data/user-05.jpg" alt="" /><a href="#"><i class="fa fa-pencil"></i></a></figure>-->
                    <div class="content">
                        <h2>Hi, <?= $this->session->userdata('userdata') ->first_name.' '.$this->session->userdata('userdata') ->last_name.''.$this->session->userdata('userdata') ->org_name?></h2>
<!--                        <p>Your profile 60% complete. <span>[ <a href="<?= base_url().'settings'?>">Edit</a> ]</span></p>-->
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="right">
                        <span>Backed Campaigns <b><?= count($campaigns)?></b></span>
<!--                        <span>Share your profile <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a></span>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contents">
        <div class="container">
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-4 col-md-3">
                    <div class="left-panel">
                        <h2>My Status</h2>
                        <div class="box">
                            <h3><span>People in<br />My Networks</span><span class="badge">108</span></h3>
                            <ul>
                                <li><label>Facebook Friends</label> <span>[ 30 ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Twitter Followers</label> <span>[ <?php if(!empty($twitteralldata->user_id)){ echo $twitteralldata->followers_count;}else{ echo 0;}?> ]</span> <a href="<?php echo base_url(); ?>count-sharetwitter"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Email Contacts</label> <span>[ <?= count($email_contacts)?> ]</span> <a href="javascript:;" data-toggle="modal" data-target="#email-options"><i class="fa fa-plus-circle"></i></a></li>
                            </ul>
                        </div>
                        <div class="box">
                            <h3><span>Visits to<br />My campaigns</span><span class="badge"><!--358--><?php echo $Totalcampaignvisit->count;?></span></h3>
                        </div>
                        <div class="box">
                        	<?php
                        	$totalshare = ($Fbsharecount->count+$Twittersharecount->count+$emailsharecount->count);
							?>
                            <h3><span>People Sharing<br />My Campaigns</span><span class="badge"><!--53--><?php echo $totalshare;?> 	</span></h3>
                            <ul>
                                <li><label>Facebook Shares</label> <span>[ <?php echo $Fbsharecount->count; ?> ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Twitter Shares</label> <span>[ <?php echo $Twittersharecount->count; ?> ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Email Shares</label> <span>[ <?php echo $emailsharecount->count;?> ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /left panel -->

                <!-- /right panel -->
                <div class="col-sm-8 col-md-9">
                    <div class="tab-area">
                        <ul class="tabs">
                            <li class="active"><a href="javascript:void(0);" data-toggle="campaigns">My Campaigns <span>[ <?= count($campaigns) ?> ]</span></a></li>
                            <!---<li><a href="javascript:void(0);" data-toggle="comments">Comments <span>[ 0 ]</span></a></li>-->
                            <li><a href="javascript:void(0);" data-toggle="updates">Updates <span>[ <?php echo (!empty($Totalupdate->count))?$Totalupdate->count:0;?> ]</span></a></li>
                        </ul>

                        <div class="tab-data">
                            <section class="tab-contents campaigns">
                                <?php
                                foreach ($campaigns as $campaign):
//     var_dump(file_exists($_SERVER['DOCUMENT_ROOT'].'/mefarnes/uploads/campaign/cover_images/thumb/'.$campaign->image));
                                    $image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/gallery_images/thumb/' . $campaign->image)) ? base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image : base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image;
//                                    echo $image_file;
                                    ?>
                                    <div class="campaign-box" id="campaign-list-<?= $campaign->id ?>">
<?php if($campaign->image!=""){?>
                                        <figure><a href="<?= base_url() . 'campaign/' . $campaign->slug ?>"><img src="<?php echo $image_file; ?>" alt="" width="270"  /></a></figure>
										<?php }else{?>
										<figure> <img src="<?php echo base_url()?>uploads/campaign/gallery_images/thumb/noimage.png" height="" width="270"></figure>	
										<?php }?>
                                        <div class="contents">
                                            <div class="col-one">
                                                <h3><a href="<?= base_url() . 'campaign/' . $campaign->slug ?>"><?php echo htmlentities($campaign->title); ?></a></h3>
                                                <p id="less<?= $campaign->id ?>"><?php
                                                    if (strlen($campaign->description) > 150) {
                                                        echo substr(($campaign->description), 0, 150) . '<a href="javascript:;" onclick="ShowHide(' . $campaign->id . ');" title="More">+++</a>';
                                                    } else
                                                        echo ($campaign->description);
                                                    ?></p>
                                                <p id="more<?= $campaign->id ?>" style="display: none;"><?php echo htmlentities($campaign->description) . '<a href="javascript:;" onclick="ShowHide(' . $campaign->id . ');" title="Less">---</a>'; ?></p>
                                            </div>
                                            <div class="col-two">
                                                <p>Raised<span>$<?= $campaign -> donation_amount?></span></p>
                                            </div>
                                            <div class="col-three">
                                                <p>Donors<span><?php echo $campaign->donor; ?></span></p>
                                            </div>
                                        </div>
                                        <div class="actions">
                                            <a href="<?php echo base_url(); ?>edit-campaign/<?php echo $campaign->id; ?>" class="edit" data-toggle="tooltip" data-placement="top" title="Edit Campaign"><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url(); ?>create-update/<?php echo $campaign->id; ?>" class="edit" data-toggle="tooltip" data-placement="top" title="Create Update"><i class="fa fa-pencil-square-o"></i></a>
                                            <a href="javascript:;" id="edit<?= $campaign->id ?>" onclick="if (confirm('Are sure to turn off donation ?'))
                                                            turnOffDonation(<?= $campaign->id ?>);" class="<?php
                                               if ($campaign->donation_status == 1)
                                                   echo 'turn-off';
                                               else
                                                   echo 'turn-on';
                                               ?>" data-toggle="tooltip" data-placement="top" title="<?php
                                               if ($campaign->donation_status == 1)
                                                   echo 'Turn off Donations';
                                               else
                                                   echo 'Turn on Donations';
                                               ?>"><i class="fa fa-power-off"></i></a>
                                            <a href="javascript:;" onclick="if (confirm('Are sure to delete ?'))
                                                            callDelete(<?= $campaign->id ?>);
                                                        ;" class="delete" data-toggle="tooltip" data-placement="top" title="Delete Campaign"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </div>

                                <?php endforeach; ?>

                                <div class="create-campaign-box">
                                    <a href="<?php echo base_url(); ?>create-campaign" class="button blue"><span>Create a New Campaign</span></a>
                                </div>
                            </section>

                            <section class="tab-contents comments">
                                Comments
                            </section>

                            <section class="tab-contents updates">
                                <!--Updates--->
<?php
                                        $i = 0;
                                        foreach ($Totalupdatesbycampaign as $updatesbycampaign):
                                            $i++;
                                           
//     var_dump(file_exists($_SERVER['DOCUMENT_ROOT'].'/mefarnes/uploads/campaign/cover_images/thumb/'.$campaign->image));
                                    $image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/gallery_images/thumb/' . $users->user_image)) ? base_url() . 'uploads/campaign/gallery_images/thumb/' . $users->user_image : base_url() . 'uploads/campaign/gallery_images/thumb/' . $users->user_image
//                                    echo $image_file;
                                    ?>
                                    <div class="campaign-box">
                                    	<?php if($users->user_image!=""){?>
                                        <figure><img src="<?php echo $image_file; ?>" alt="" width="100"  /></figure>
										<?php }else{?>
										<figure><img src="<?php echo base_url()?>uploads/campaign/gallery_images/thumb/noimage.png" height="" width="100"></figure>	
										<?php }?>
										<h3>
											<?php if(!empty($users->org_name)){?>
											<a href="#"><?php echo $users->org_name;?></a>
											<?php }?>
											</h3>
                                        <div class="contents">
                                            <div class="col-one">
                                            	<h2><?= time_elapsed_string(strtotime($updatesbycampaign->create_date)) ?></h2>
                                                <!--<h3><?php echo $users->org_name;?></h3>--->
                                                <p ><?= htmlentities($updatesbycampaign->title); ?></p>
                                                <p ><?= nl2br(substr(($updatesbycampaign->description), 0, 450)) ?></p>
                                                
                                            </div>
                                            <?php
                                foreach ($campaigns as $campaign){?>
                                	<?php if($updatesbycampaign->campaign_id == $campaign->id){?>
                                		<div class="col-two">
                                               
                                		   <a href="<?= base_url() . 'campaign/' . $campaign->slug ?>"><?php echo $campaign->title;?></a>
                                		</div>
                                	 	<?php }?>
									
									 <?php }?>
                                            
                                        </div>
                                        
                                    </div>

                                <?php endforeach; ?>

                                
                            </section>

                            </section>
                        </div>
                    </div>
                </div>
                <!-- /right panel -->
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<!-- /content -->
<!-- Modal -->
<div id="email-options" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Import Email Contacts</h4>
            </div>
            <div class="modal-body">
                <p><a style="font-size:25px;font-weight:bold;" href="<?= $google_redirect_url ?>"><img src="<?= base_url() ?>assets/images/Gmail-icon.png" /></a>
                 
                </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    function ShowHide(id)
    {
        $('#more' + id).slideToggle('slow');
        $('#less' + id).slideToggle('slow');
    }

    function callDelete(id)
    {
        var campaign_id = id;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "user/deleteCampaign",
            dataType: 'json',
            data: {campaign_id: campaign_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                if (res)
                {
                    //console.log(res);
                    if (res.code == 200)
                    {
                        $("#campaign-list-" + id).slideUp('slow');

                    }
                    $("#csrf_token").val(res.token);
                }
            }
        });

    }


    function turnOffDonation(id)
    {
       
        var campaign_id = id;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "user/turnoffCampaign",
            dataType: 'json',
            data: {campaign_id: campaign_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                if (res)
                {
                    //console.log(res);
                    if (res.code == 200)
                    {
                        if ($("#edit" + id).hasClass('turn-on'))
                        {
                            $("#edit" + id).removeClass('turn-on');
                            $("#edit" + id).addClass('turn-off');
                            $("#edit" + id).attr('title', 'Turn on donations');
                            $("#edit" + id).attr('data-original-title', 'Turn on donations');
                        }
                        else {
                            $("#edit" + id).addClass('turn-on');
                            $("#edit" + id).removeClass('turn-off');
                            $("#edit" + id).attr('title', 'Turn off donations');
                            $("#edit" + id).attr('data-original-title', 'Turn off donations');
                        }

                    }
                    $("#csrf_token").val(res.token);
                }
            }
        });

    }
</script>
<script src="//js.live.net/v5.0/wl.debug.js"></script>
<script type="text/javascript">
    WL.init({
        client_id: "31b5d2d3-08cf-4705-b7f8-551294c2a62b",
        redirect_uri: "https://www.mefarnes.com/dashboard",
        scope: ["wl.basic", "wl.contacts_emails"],
        response_type: "token"
    });

    jQuery(document).ready(function () {

        //live.com api
        jQuery('#import').click(function (e) {
            e.preventDefault();
            WL.login({
                scope: ["wl.basic", "wl.contacts_emails"]
            }).then(function (response)
            {
                WL.api({
                    path: "me/contacts",
                    method: "GET"
                }).then(
                        function (response) {
                            //your response data with contacts 
                            console.log(response.data);
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>" + "user/microsoft",
                                dataType: 'json',
                                data: {campaign_id: response.data, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                                success: function (res) {
                                    if (res)
                                    {
                                        
                                    }
                                }
                            });
                        },
                        function (responseFailed) {
                            //console.log(responseFailed);
                        }
                );

            },
                    function (responseFailed)
                    {
                        //console.log("Error signing in: " + responseFailed.error_description);
                    });
        });

    });
</script>