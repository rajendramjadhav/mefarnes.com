<!-- content -->
<div id="content" class="page dashboard public-profile organization">
    <div class="dashboard-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
<!--                    <figure><img src="images/data/user-06.jpg" alt="" /></figure>-->
                    <div class="content">
                        <h2>Hi, <?= $this->session->userdata('userdata')->first_name . ' ' . $this->session->userdata('userdata')->last_name . '' . $this->session->userdata('userdata')->org_name ?></h2>
<!--                        <p>one move to save our future!</p>-->
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="right">
                        <span>Campaigns <b><b><?= count($campaigns) ?></b></span>
                        <span>Follow Us <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contents">
        <div class="container">
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-4 col-md-3">
                    <div class="left-panel">
                        <h2>Status</h2>
                        <div class="box">
                            <h3><span>People in<br />Our Networks</span><span class="badge">868</span></h3>
                            <ul>
                                <li><label>Facebook Friends</label> <span>[ 530 ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Twitter Friends</label> <span>[ 270 ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                                <li><label>Email Shares</label> <span>[ <?php echo $emailsharecount->count;?> ]</span> <a href="#"><i class="fa fa-plus-circle"></i></a></li>
                            </ul>
                        </div>
                        <div class="box">
                            <h3><span>Visits to<br />Our campaigns</span><span class="badge">1,858</span></h3>
                        </div>
                        <div class="box">
                            <h3 class="text-center">Share Our profile</h3>
                            <p class="share">
                                <a href="#" class="share-button facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="share-button twitter"><i class="fa fa-twitter"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /left panel -->

                <!-- /right panel -->
                <div class="col-sm-8 col-md-9">
                    <div class="tab-area">
                        <ul class="tabs">
                            <li class="active"><a href="javascript:void(0);" data-toggle="running-campaigns">Running Campaigns <span>[ 2 ]</span></a></li>
                            <li><a href="javascript:void(0);" data-toggle="campaigns-ends">Campaigns Ends <span>[ 1 ]</span></a></li>
                        </ul>

                        <div class="tab-data">
                            <section class="tab-contents campaigns running-campaigns">
                                <div class="row">
                                    <?php
                                    foreach ($campaigns as $campaign):
                                        $image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/cover_images/thumb/' . $campaign->image)) ? base_url() . 'uploads/campaign/cover_images/thumb/' . $campaign->image : base_url() . 'uploads/campaign/cover_images/' . $campaign->image;
                                        ?>

                                        <div class="col-sm-6 col-md-4">
                                            <div class="campaign-box">
                                                <figure>
                                                    <a href="<?= base_url() . 'campaign/' . $campaign->slug ?>"><img src="<?php echo $image_file; ?>" alt="" width="270"  /></a>
                                                    <figcaption><h3><?php echo htmlentities($campaign->title); ?></h3></figcaption>
                                                </figure>

                                                <div class="contents">
                                                    <div class="col-one">Raised<span>$<?= $campaign->donation_amount ?></span></div>
                                                    <div class="col-two">Donors<span><?php echo $campaign->donor; ?></span></div>
                                                </div>

                                                <div class="buttons">
                                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                                    <a href="<?= base_url() . 'campaign/' . $campaign->slug ?>" class="donate">Donate Now!</a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?> 
                                </div>								
                            </section>

                            <section class="tab-contents campaigns campaigns-ends">
                                Campaigns Ends
                            </section>
                        </div>
                    </div>
                </div>
                <!-- /right panel -->
            </div>
        </div>
    </div>
</div>
<!-- /content -->