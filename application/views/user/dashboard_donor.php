

<body>
<?php //var_dump($_SESSION); ?>

<!-- content -->
<div id="content" class="page dashboard donor">
	<div class="dashboard-header">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="content">
						<h2>Hi, <?= $this->session->userdata('userdata') ->first_name.' '.$this->session->userdata('userdata') ->last_name.''.$this->session->userdata('userdata') ->org_name?></h2>
						<!-- <p>We welcome you again!</p> -->
					</div>
				</div>
				<div class="col-sm-5">
					<div class="right">
						<!-- <span>Backed Campaigns <b>2</b></span>
						<span>Share your profile <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a></span> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="contents">
		<div class="container">
			<div class="row">
				<div class="tab-area">
					<ul class="tabs">
						<li class="active"><a href="javascript:void(0);" data-toggle="campaigns">Donations <span>[ <?php echo $total_donation; ?> ]</span></a></li>
						<li><a href="javascript:void(0);" data-toggle="updates">Updates <span>[ 0 ]</span></a></li>
					</ul>
					
					<div class="tab-data">
						<section class="tab-contents campaigns">
							<!-- <div class="campaign-box">
								<figure><img src="images/data/campaign-01.jpg" alt="" /></figure>
								<div class="contents">
									<div class="col-one">
										<h3>Campaign Title here</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do tempor incididunt ...</p>
									</div>
									<div class="col-two">
										<p>Your Contribution<span>$150</span></p>
									</div>
									<div class="col-three">
										<p>Total Fund<span>$23,265</span></p>
									</div>
									<div class="col-four">
										<p>Donors<span>635</span></p>
									</div>
									<div class="col-five">
										<p>Days Left<span>21</span></p>
									</div>
								</div>
								<div class="share">
									<p>Share Campaign</p>
									<p class="actions">
										<a href="#" class="facebook" data-toggle="tooltip" data-placement="top" title="Share on Facebook"><i class="fa fa-facebook"></i></a>
										<a href="#" class="twitter" data-toggle="tooltip" data-placement="top" title="Share on Twitter"><i class="fa fa-twitter"></i></a>
										<a href="#" class="email" data-toggle="tooltip" data-placement="top" title="Share on Email"><i class="fa fa-envelope"></i></a>
									</p>
								</div>
							</div> -->

<?php
//print_r($donarcampaigns);
 foreach ($donarcampaigns as $key => $value) {?>							

							<div class="campaign-box">
								<figure><img src="<?php echo base_url(); ?>uploads/campaign/gallery_images/thumb/<?= $value->image;?>" alt="" /></figure>
								<div class="contents">
									<div class="col-one">
										<h3><?php echo $value->title; ?></h3>
										<p><?php echo $value->description; ?></p>
									</div>
									<div class="col-two">
										<p>Your Contribution<span>$<?php echo $value->amount; ?></span></p>
									</div>
									<div class="col-three">
										<p>Total Fund<span><?php $TotalFund = $this->CPM->get_total_donation($value->id);print_r($TotalFund->total_donation); ?></span></p>
									</div>
									<div class="col-four">
										<p>Donors<span><?php $total_dona = $this->CPM->getTotaldonor($value->id);print_r($total_dona['0']->t_donor); ?></span></p>
									</div>
									<div class="col-four">
										<p>Donation Date<span><?php echo date_format(date_create($value->d_date), 'd/m/Y'); ?></span></p>
									</div>
									<div class="col-five">

										<p>Days Left<span><?php
											if ($value->end_date_time <= date('Y-m-d')) {
												echo "0";
											} else {
												$datetime1 = new DateTime(date('Y-m-d'));
												$datetime2 = new DateTime($value->end_date_time);
												$interval = $datetime1->diff($datetime2);
												echo $interval->format('%a');
											}
											
										?>
										</span></p>
									</div>
								</div>
								<div class="share">
									<p>Share Campaign</p>
									<p class="actions">
										<a href="#" class="facebook" data-toggle="tooltip" data-placement="top" title="Share on Facebook"><i class="fa fa-facebook"></i></a>
										<a href="#" class="twitter" data-toggle="tooltip" data-placement="top" title="Share on Twitter"><i class="fa fa-twitter"></i></a>
										<a href="#" class="email" data-toggle="tooltip" data-placement="top" title="Share on Email"><i class="fa fa-envelope"></i></a>
									</p>
								</div>
							</div>
<?php } ?>

							<div class="create-campaign-box">
								<div class="row">
									<div class="col-sm-7 col-md-8">
										<h3><i class="fa fa-bullhorn"></i>Want to create your own Campaign? Let’s go for it... it‘s free.</h3>
									</div>
									<div class="col-sm-5 col-md-4">
										<a href="#" class="button blue"><span>Create Campaign</span></a>
									</div>
								</div>
							</div>
						</section>
						
						<section class="tab-contents updates">
							<!-- <div class="updates-group">
								<div class="row">
									<div class="col-sm-3"><span class="date">01 July</span></div>
									<div class="col-sm-9">
										<div class="updates-box">
											<div class="col-one"><span class="char d">D</span></div>
											<div class="col-two">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod dolore magna aliqua. Ut enim ad minim veniam, ex ea commodo consequat.</div>
											<div class="col-three">3 Days ago</div>
											<div class="col-four"><a href="#"><i class="icon-close-x-lg"></i></a></div>
										</div>
										
										<div class="updates-box">
											<div class="col-one"><span class="char m">M</span></div>
											<div class="col-two">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod dolore magna aliqua. Ut enim ad minim veniam, ex ea commodo consequat.</div>
											<div class="col-three">3 Days ago</div>
											<div class="col-four"><a href="#"><i class="icon-close-x-lg"></i></a></div>
										</div>
										
										<div class="updates-box">
											<div class="col-one"><span class="char t">T</span></div>
											<div class="col-two">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod dolore magna aliqua. Ut enim ad minim veniam, ex ea commodo consequat.</div>
											<div class="col-three">3 Days ago</div>
											<div class="col-four"><a href="#"><i class="icon-close-x-lg"></i></a></div>
										</div>
									</div>
								</div>
							</div> -->
							
						
							
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /content -->

<!-- footer -->
<footer>
	<div class="top">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-7">
					<div class="row">
						<div class="col-sm-6">
							<h3>Quick Links</h3>
							<div class="clearfix">
								<ul class="links">
									<li><a href="#">How it Works</a></li>
									<li><a href="#">Success Stories</a></li>
									<li><a href="#">Common Questions</a></li>
									<li><a href="#">Ask a Question</a></li>
									<li><a href="#">Sign Up Now</a></li>
								</ul>
								<ul class="links">
									<li><a href="#">Watch the Video</a></li>
									<li><a href="#">Mefares Reviews</a></li>
									<li><a href="#">Pricing &amp; Fees</a></li>
									<li><a href="#">Fundraising Ideas</a></li>
									<li><a href="#">We're Hiring!</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-6">
							<h3>Contact Info</h3>
							<div class="contact">
								<p><i class="fa fa-map-marker"></i>
								   <span>Stratum House<br>
								   Stafford Park 10<br>
								   Telford TF3 3AB</span></p>
								<p><i class="fa fa-phone"></i>
								   <span>+44 (0) 1952 214000</span></p>
								<p><i class="fa fa-fax"></i>
								   <span>+44 (0) 1952 214001</span></p>
								<p><i class="fa fa-envelope"></i>
								   <span><a href="mailto:enquiries@mefarnes.com">enquiries@mefarnes.com</a></span></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-5">
					<h3>Subscribe Our Newsletter</h3>
					<p>Get Latest updates by us on your email.</p>
					<div class="newsletter-form">
						<form method="post">
							<span class="cover"><input type="email" placeholder="Enter your valid email here..." /></span>
							<button type="submit"><span>Subscribe Now!</span></button>
						</form>
					</div>
					<div class="social">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-linkedin"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-youtube"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="bottom">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">&copy; 2016 Mefarnes, All Rights Reserved.</div>
				<div class="col-sm-6">
					<div class="links">
						<a href="#">Blog</a> |
						<a href="#">Legal</a> |
						<a href="#">Terms</a> |
						<a href="#">Privacy Policy</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- /footer -->
</body>
</html>
