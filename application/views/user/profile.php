
<?php /*?><script type="text/javascript">
	function fun_basic()
	{
		var frm_obj = window.document.frm_basicinfo;
		
		var orgname = frm_obj.org_name.value;
		var orgtag = frm_obj.org_tag.value;
		
		var oEditor = FCKeditorAPI.GetInstance('about_me') ;
		var aboutme =  oEditor.GetXHTML( true ) ; 
		
		
		jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "user/profilebasic",
            data: {'org_name':orgname, 'org_tag':orgtag, 'about_me':aboutme,'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                
            }
        });
		
	}
</script><?php */?>
<div id="content" class="page organization-profile">
	<div class="container">
		<div class="row">
			<div class="contents">
				<h2>Create your profile</h2>
				<h3>Make a great first impression with your organizational profile.</h3>
				<div class="tab-area">
					<ul class="tabs">
						<li class="active one">Basic Info</li>
						<li class="two">Images/Video</li>
					</ul>
					
					<div class="tab-data">
						<!-- basic info tab -->
						<form name="frm_basicinfo" id="frm_basicinfo" method="post" action="<?php echo base_url(); ?>user/profilebasic">     
                        <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />                   
                            <section class="step-1">
                                <div class="form">
                                    <div class="form-row">
                                        <h4>Name</h4>
                                        <label>Set a identity to your organization.</label>
                                        <input type="text" name="org_name" id="org_name" value="<?php echo @$profiledetail->org_name; ?>" placeholder="Your Organization Name" required />
                                    </div>
                                    
                                    <div class="form-row">
                                        <h4>Status</h4>
                                        <label>Let people inspire from your motto line.</label>
                                        <div class="input-group">
                                            <input type="text" name="org_tag" id="org_tag" value="<?php echo @$profiledetail->org_tag; ?>" class="form-control" placeholder="Our Motto" required>
                                            <span class="input-group-addon">30</span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <h4>Overview</h4>
                                        <label>Why this organization is for?</label>
                                        <div class="input-box">
                                        	<?php
												/*include ("fckeditor/fckeditor.php");
												$FCKeditor = new FCKeditor('about_me');
												$FCKeditor->BasePath = base_url().'/fckeditor/';
												$FCKeditor->Value = @$about_me;
												$FCKeditor->Width = "100%";
												$FCKeditor->Height = "350px";															
												$FCKeditor->Create();	*/																											 
											 ?> 
                                        	<!--<img src="<?php echo base_url(); ?>assets/images/data/wysiwyg-editor.jpg" alt="" >-->
                                            <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                            <script>
                                                tinymce.init({
                                                    selector: 'textarea',
                                                    toolbar: 'fontsizeselect',
                                                    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
                                                    font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
                                                    forced_root_block: 'p',
                                                    content_css: "<?= base_url() ?>assets/css/content.css",
                                                    menu: {
                                                        file: false,
                                                        edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
                                                        insert: {title: 'Insert', items: 'link media | template hr'},
                                                        view: false,
                                                        format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
                                                        table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
                                                        tools: {title: 'Tools', items: 'spellchecker code'}
                                                    }
                                                });

                                            </script>
                                            <textarea name="about_me" cols="108" rows="5" id="about_me" placeholder="Why this campign is for?"><?php echo @$profiledetail->about_me; ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="buttons">
                                            <!--<a href="javascript:fun_basic();" class="button blue continue-section-1"><span>Save &amp; Continue</span></a>-->
                                            <input type="submit" name="save" class="button blue" value="Save &amp; Continue" />
                                            <a href="javascript:void(0);" class="button"><span>Discard Changes</span></a>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </form>
						<!-- /basic info tab -->
						
						<!-- images/video tab -->
						<section class="step-2">
							<div class="form">
								<div class="form-row">
									<h4>Cover Background Image</h4>
									<div class="full-pic-box">
										<figure>
											<img src="<?php echo base_url(); ?>assets/images/data/org-donate-header-02.jpg" alt="" />
											<a href="#"><i class="fa fa-pencil"></i></a>
									  </figure>
										<p class="mt10"><a href="#" class="more"><i class="fa fa-camera"></i>Add Another Image</a></p>
									</div>
								</div>
								
								<div class="form-row">
									<h4>Gallery Images</h4>
									<div class="pictures">
										<div class="pic-box">
											<figure>
												<img src="<?php echo base_url(); ?>assets/images/data/campaign-17.jpg" alt="" />
												<a href="#"><i class="fa fa-pencil"></i></a>
										  </figure>
											<div class="select">
												<input type="checkbox" id="profile_pic_1" class="fancy-checkbox sm" checked="checked" /> <label for="profile_pic_1">Set as profile picture</label>
											</div>
										</div>
										
										<div class="pic-box">
											<figure>
												<img src="<?php echo base_url(); ?>assets/images/data/campaign-01.jpg" alt="" />
												<a href="#"><i class="fa fa-pencil"></i></a>
										  </figure>
											<div class="select">
												<input type="checkbox" id="profile_pic_1" class="fancy-checkbox sm" /> <label for="profile_pic_1">Set as profile picture</label>
											</div>
										</div>
										
										<div class="pic-box">
											<figure>
												<img src="<?php echo base_url(); ?>assets/images/data/campaign-16.jpg" alt="" />
												<a href="#"><i class="fa fa-pencil"></i></a>
										  </figure>
											<div class="select">
												<input type="checkbox" id="profile_pic_2" class="fancy-checkbox sm" /> <label for="profile_pic_2">Set as profile picture</label>
											</div>
										</div>
									</div>
									
									<a href="#" class="upload-box">Choose an image from your computer</a>
								</div>
								
								<div class="form-row">
									<h4>Video <small>(optional)</small></h4>
									<div class="pictures">
										<div class="pic-box">
											<div class="video">
												<img src="<?php echo base_url(); ?>assets/images/data/video-2.jpg" alt="" />
											</div>
											<div class="select">
												<input type="checkbox" id="profile_vid_1" class="fancy-checkbox sm" checked="checked" /> <label for="profile_vid_1">Set as profile video</label>
											</div>
										</div>
									</div>
									
									<input type="text" placeholder="Youtube or Vimeo URL" >
									<p class="mt10"><a href="#" class="more"><i class="fa fa-video-camera"></i>Add More Videos</a></p>
								</div>
								
								<div class="form-row">
									<div class="buttons">
										<a href="javascript:void(0);" class="button blue"><span>Save &amp; Preview</span></a>
										<a href="javascript:void(0);" class="button"><span>Discard Changes</span></a>
									</div>
								</div>
							</div>
						</section>
						<!-- /images/video tab -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>