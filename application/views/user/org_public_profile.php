<!--script src="js/jquery.lint.js" type="text/javascript" charset="utf-8"></script-->
<link rel="stylesheet" href="<?= base_url() ?>assets/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="<?= base_url() ?>assets/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<style type="text/css">
	.pp_social{ display:none;}
</style>
<?php //var_dump($profiledetail);exit;?>
<div id="content" class="page profile-org">
	<div class="banner" style="background:transparent url(<?php echo base_url(); ?>uploads/organization_profile/cover_images/<?php echo $profiledetail->image; ?>) center center no-repeat; background-size:cover; display:table; width:100%;">
		<div class="contents">
			<h2><?php echo $profiledetail->org_name; ?></h2>
			<p><?php echo $profiledetail->org_tag; ?></p>
			<p><?php echo $profiledetail->city; ?></p>
			<div class="buttons"><a href="#" class="donate-button"><span>Donate Now!</span></a></div>
		</div>
	</div>
	
	<!-- tab panel -->
	<div class="tab-panel">
		<div class="container">
			<div class="row">
				<ul class="tabs">
					<li><a href="javascript:void(0);" data-link="#overview" class="section-link">Overview</a></li>
					<li><a href="javascript:void(0);" data-link="#campaigns" class="section-link">Campaigns</a></li>
					<li><a href="javascript:void(0);" data-link="#network" class="section-link">Our Network</a></li>
					<li><a href="javascript:void(0);" data-link="#gallery" class="section-link">Gallery</a></li>
					<li><a href="javascript:void(0);" data-link="#contact" class="section-link">Contact</a></li>
				</ul>
				<div class="buttons">
					<a href="https://www.facebook.com/dialog/share?app_id=<?php echo FB_APP_ID ?>&display=popup&href=<?php echo current_url(); ?>&redirect_uri=<?php echo current_url(); ?>" class="fb-share-button"><i class="fa fa-facebook"></i></a>
					<!--<input type="hidden" name="share_twitter" id="share_twitter" value="<?php echo $profiledetail->org_name;?>">
                     <a href="javascript:;" class="tw-share-button" id="shareB" onclick="shareTwitterlinkorganisation()"><i class="fa fa-twitter"></i></a>-->
					<a href="http://twitter.com/intent/tweet?original_referer=<?php echo current_url(); ?>&status=<?php echo base_url();?><?php echo "organization-profile";?>/<?php echo $profiledetail->org_slug;?>" class="tw-share-button" id="shareTw"><i class="fa fa-twitter"></i></a>
					<a href="#" class="donate-button"><span>Donate Now!</span></a>
				</div>
			</div>
		</div>
	</div>
	<!-- /tab panel -->
		              
	<div id="overview" class="overview">
		<div class="container">
			<div class="row">
				<h3 class="underline left">Overview</h3>
				<div class="row">
					<div class="col-sm-8">
						<q><?php echo $profiledetail->org_tag; ?></q>
						<p><?php echo $profiledetail->about_me; ?></p>
					</div>
					<div class="col-sm-4 text-right">
						<div class="box">
                        	<!--<img src="<?php echo base_url(); ?>assets/images/data/video-1.jpg" alt="" />-->
                            <iframe width="320" height="190"
                                    src="https://www.youtube.com/embed/<?php echo $videos_link->image_name; ?>">
                            </iframe> 
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /overview -->
	
	<!-- campaign scroller -->
	<div id="campaigns" class="campaign-scroller">
		<div class="container">
			<div class="row">
				<div class="col-xs-9">
					<h3 class="underline left">Campaigns</h3>
				</div>
				<div class="col-xs-3">
					<div class="buttons-container">
						<a href="javascript:void(0);" onClick="Slidebox(this, '.campaigns-slider', 'prev')" class="campaigns-slider prev disabled"><i class="fa fa-angle-left"></i></a>
						<a href="javascript:void(0);" onClick="Slidebox(this, '.campaigns-slider', 'next')" class="campaigns-slider next"><i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="campaigns-slider">
						<div class="slide-container">
							<ul class="slides">
                            	<?php foreach($user_campagin as $displayCampagin){?>
                                    <li>
                                        <div class="campaign-box">
                                            <figure>
                                                <img src="<?php echo base_url(); ?>uploads/campaign/gallery_images/thumb/<?php echo $displayCampagin->image; ?>" height="155" width="270" alt="<?php echo $displayCampagin->image; ?>" title="<?php echo $displayCampagin->image; ?>" />
                                                <figcaption><?php echo $displayCampagin->title; ?></figcaption>
                                          </figure>
                                            
                                            <div class="contents">
                                                <div class="col-one">Raised<span>$10,000</span></div>
                                                <div class="col-two">Donors<span>58</span></div>
                                            </div>
                                            
                                            <div class="buttons">
                                                <a href="https://www.facebook.com/dialog/share?app_id=579509698900395&display=popup&href=<?php echo base_url(); ?>campaign/<?php echo $displayCampagin->slug; ?>&redirect_uri=<?php echo current_url(); ?>" class="facebook"><i class="fa fa-facebook"></i></a>
                                                <a href="http://twitter.com/intent/tweet?original_referer=<?php echo current_url(); ?>&status=test+<?php echo base_url(); ?>campaign/<?php echo $displayCampagin->slug; ?>" class="twitter"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="donate">Donate Now!</a>
                                            </div>
                                        </div>
                                    </li>
								<?php } ?>
								<!--<li>
									<div class="campaign-box">
										<figure>
											<img src="<?php echo base_url(); ?>assets/images/data/campaign-14.jpg" alt="" />
											<figcaption>Campaign Title here</figcaption>
									  </figure>
										
										<div class="contents">
											<div class="col-one">Raised<span>$45,265</span></div>
											<div class="col-two">Donors<span>358</span></div>
										</div>
										
										<div class="buttons">
											<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
											<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
											<a href="#" class="donate">Donate Now!</a>
										</div>
									</div>
								</li>
								
								<li>
									<div class="campaign-box">
										<figure>
											<img src="<?php echo base_url(); ?>assets/images/data/campaign-15.jpg" alt="" />
											<figcaption>Campaign Title here</figcaption>
									  </figure>
										
										<div class="contents">
											<div class="col-one">Raised<span>$75,000</span></div>
											<div class="col-two">Donors<span>1448</span></div>
										</div>
										
										<div class="buttons">
											<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
											<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
											<a href="#" class="donate">Donate Now!</a>
										</div>
									</div>
								</li>
								
								<li>
									<div class="campaign-box">
										<figure>
											<img src="<?php echo base_url(); ?>assets/images/data/campaign-13.jpg" alt="" />
											<figcaption>Campaign Title here</figcaption>
									  </figure>
										
										<div class="contents">
											<div class="col-one">Raised<span>$50,000</span></div>
											<div class="col-two">Donors<span>965</span></div>
										</div>
										
										<div class="buttons">
											<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
											<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
											<a href="#" class="donate">Donate Now!</a>
										</div>
									</div>
								</li>
								
								<li>
									<div class="campaign-box">
										<figure>
											<img src="<?php echo base_url(); ?>assets/images/data/campaign-01.jpg" alt="" />
											<figcaption>Campaign Title here</figcaption>
									  </figure>
										
										<div class="contents">
											<div class="col-one">Raised<span>$10,000</span></div>
											<div class="col-two">Donors<span>58</span></div>
										</div>
										
										<div class="buttons">
											<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
											<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
											<a href="#" class="donate">Donate Now!</a>
										</div>
									</div>
								</li>
								
								<li>
									<div class="campaign-box">
										<figure>
											<img src="<?php echo base_url(); ?>assets/images/data/campaign-14.jpg" alt="" />
											<figcaption>Campaign Title here</figcaption>
									  </figure>
										
										<div class="contents">
											<div class="col-one">Raised<span>$45,265</span></div>
											<div class="col-two">Donors<span>358</span></div>
										</div>
										
										<div class="buttons">
											<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
											<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
											<a href="#" class="donate">Donate Now!</a>
										</div>
									</div>
								</li>
								
								<li>
									<div class="campaign-box">
										<figure>
											<img src="<?php echo base_url(); ?>assets/images/data/campaign-15.jpg" alt="" />
											<figcaption>Campaign Title here</figcaption>
									  </figure>
										
										<div class="contents">
											<div class="col-one">Raised<span>$75,000</span></div>
											<div class="col-two">Donors<span>1448</span></div>
										</div>
										
										<div class="buttons">
											<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
											<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
											<a href="#" class="donate">Donate Now!</a>
										</div>
									</div>
								</li>
								
								<li>
									<div class="campaign-box">
										<figure>
											<img src="<?php echo base_url(); ?>assets/images/data/campaign-13.jpg" alt="" />
											<figcaption>Campaign Title here</figcaption>
									  </figure>
										
										<div class="contents">
											<div class="col-one">Raised<span>$50,000</span></div>
											<div class="col-two">Donors<span>965</span></div>
										</div>
										
										<div class="buttons">
											<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
											<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
											<a href="#" class="donate">Donate Now!</a>
										</div>
									</div>
								</li>-->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /campaign scroller -->
	
	<!-- our network -->
	<div id="network" class="our-network">
		<div class="container">
			<div class="row">
				<h3 class="underline">Our Network</h3>
				<div class="row">
					<div class="col-sm-4">
						<div class="box facebook">
							<div class="icon"><i class="fa fa-facebook"></i></div>
							<p>Facebook Friends</p>
							<h4>530</h4>
							<hr />
							<p><a href="#">Share Our Profile</a></p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="box twitter">
							<div class="icon"><i class="fa fa-twitter"></i></div>
							<p>Twitter Friends</p>
							<h4>657</h4>
							<hr />
							<p><a href="#">Share Our Profile</a></p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="box mail">
							<div class="icon"><i class="fa fa-envelope-o"></i></div>
							<p>Email Friends</p>
							<h4>457</h4>
							<hr />
							<p><a href="#">Share Our Profile</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /our network -->
	
	<!-- gallery slider -->
    <?php if(count($gellary_image)>0){?>
	<div id="gallery" class="gallery-slider">
    	<?php foreach($gellary_image as $dis_images){?>
        	
            <div class="carousel-cell">
                <figure>
                	<img src="<?php echo base_url(); ?>uploads/organization_profile/gellary_image/<?php echo $dis_images->image_name; ?>" width="289" height="320" alt="" />
                </figure>
                <div class="content">
                	<span>
						<?php echo $dis_images->title; ?>
                    </span> 
                    <ul class="gallery clearfix">	
                    	<li style="list-style:none;">
                        	<a href="<?php echo base_url(); ?>uploads/organization_profile/gellary_image/<?php echo $dis_images->image_name; ?>" rel="prettyPhoto">
                                <i class="fa fa-expand"></i>
                            </a>
                        </li>
                    </ul>
               </div>
            </div>
        <?php } ?>
		<!--<div class="carousel-cell">
			<figure><img src="<?php echo base_url(); ?>assets/images/data/org-gallery-scroller-2.jpg" alt="" /></figure>
			<div class="content"><span>Nature Nomads clearing</span> <a href="#"><i class="fa fa-expand"></i></a></div>
		</div>
		<div class="carousel-cell">
			<figure><img src="<?php echo base_url(); ?>assets/images/data/org-gallery-scroller-3.jpg" alt="" /></figure>
			<div class="content"><span>Nature Nomads clearing</span> <a href="#"><i class="fa fa-expand"></i></a></div>
		</div>
		<div class="carousel-cell">
			<figure><img src="<?php echo base_url(); ?>assets/images/data/org-gallery-scroller-4.jpg" alt="" /></figure>
			<div class="content"><span>Nature Nomads clearing</span> <a href="#"><i class="fa fa-expand"></i></a></div>
		</div>
		<div class="carousel-cell">
			<figure><img src="<?php echo base_url(); ?>assets/images/data/org-gallery-scroller-5.jpg" alt="" /></figure>
			<div class="content"><span>Nature Nomads clearing</span> <a href="#"><i class="fa fa-expand"></i></a></div>
		</div>
		<div class="carousel-cell">
			<figure><img src="<?php echo base_url(); ?>assets/images/data/org-gallery-scroller-6.jpg" alt="" /></figure>
			<div class="content"><span>Nature Nomads clearing</span> <a href="#"><i class="fa fa-expand"></i></a></div>
		</div>-->
	</div>
    <?php } ?>
	<!-- /gallery slider -->
	
	<!-- contact us -->
	<!--<div id="contact" class="contact-us">
		<h3 class="underline">Contact Us</h3>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4">
					<div class="box">
						<div class="icon"><i class="fa fa-map-marker"></i></div>
						<div class="content">
							<strong>Address</strong><br />4756 Hiney Road,<br />Helca, ND 57446
						</div>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-4">
					<div class="box">
						<div class="icon"><i class="fa fa-phone"></i></div>
						<div class="content pt15">
							<strong>Contact Number</strong><br />01-234-56789
						</div>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-4">
					<div class="box">
						<div class="icon"><i class="fa fa-envelope"></i></div>
						<div class="content pt15">
							<strong>Send Email</strong><br /><a href="mailto:info@naturecare.com">info@naturecare.com</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->
	<!-- /contact us -->
	
	<div class="scroll-up"><i class="icon-long-arrow-up"></i></div>	
</div>
<script type=text/javascript charset=utf-8>  
 $(document).ready(function(){  
$("a[rel^='prettyPhoto']").prettyPhoto({theme:'pp_default'});  
 });  
</script>
<style>
.easyzoom-flyout {min-height:450px; min-width:450px;}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
	$("area[rel^='prettyPhoto']").prettyPhoto();
	
	$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: true});
	$(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});

	$("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
		custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
		changepicturecallback: function(){ initialize(); }
	});

	$("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
		custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
		changepicturecallback: function(){ _bsap.exec(); }
	});
});


</script>
<script>
        function shareTwitterlink() {
            //alert('sgggsdgdf');
            var twitterid = $('#share_twitter').val();
            //var check = '<?php echo base_url(); ?>'+"PublicCampaign/shareCount";
            //alert(check);
            //alert(twitterid);
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "PublicCampaign/shareCount",
                data: {
                    twitterid: twitterid,

                },
                success: function (res) {
                    console.log(res);
                    //alert(res);
                    var twUrl = $("#shareTw").attr('href');
                    //alert(twUrl);
                    window.location.assign(twUrl);

                }
            });

        }
    </script>