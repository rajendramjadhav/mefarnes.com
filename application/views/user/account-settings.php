<!-- content -->
<?php //var_dump($data);exit; ?>
<style>
	.cropit-preview {
		background-color: #f8f8f8;
		background-size: cover;
		border: 1px solid #ccc;
		border-radius: 3px;
		margin-top: 7px;
		width: 250px;
		height: 250px;
	  }
	
	  .cropit-preview-image-container {
		cursor: move;
	  }
	
	  .image-size-label {
		margin-top: 10px;
	  }
	  
	  input, .export {
		display: block;
	  }
	
	  button {
		margin-top: 10px;
	  }
	  .cropit-preview-image-container img{
	  	max-width: none !important;
	  }
</style>

<div id="content" class="page dashboard account-settings">
	<div class="dashboard-header">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="content">
						<h2>Settings</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="contents">
		<div class="container">
			<div class="tab-area">
				<ul class="tabs">
					<li class="active"><a href="javascript:void(0);" data-toggle="account">Account</a></li>
					<li><a href="javascript:void(0);" data-toggle="edit-profile">Edit Profile</a></li>
					<li><a href="javascript:void(0);" data-toggle="notifications">Notifications</a></li>
					<!-- <li><a href="javascript:void(0);" data-toggle="find-friends">Find Friends</a></li> -->
				</ul>
				
				<div class="tab-data">
					<section class="tab-contents account">
						<?php echo $this->session->flashdata('success'); ?>
						<div class="row">
							<div class="col-sm-8">
								<form  method="POST" action="<?php echo base_url('user/resetPassword'); ?>">
									<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<div class="form">
									<div class="form-row">
										<label>Email</label>
										<input type="email" value="<?php echo (!empty($data->email)) ? $data->email: '';?>" />
									</div>
									
									<div class="form-row">
										<label>Password</label>
										<a href="javascript:void(0);" onClick="$('.hidden-password').toggle(200)" class="button green"><span>Change Password</span></a>
										
										<div class="hidden-password">
											<div class="form-row">
												<input type="password"name="password" placeholder="New Password" />
												<span class="info">Minimum 6 characters</span>
											</div>
											<div class="form-row">
												<input type="password" name="confPass" placeholder="Confirm Password" />
												<span class="info">Make sure they match!</span>
											</div>
										</div>
									</div>
									
									<div class="form-row">
										<label>Current Password</label>
										<input type="password" name="currtpass" placeholder="Enter Current Password" />
										<span class="info">Enter your current password to save these changes.</span>
									</div>
									
									<div class="form-row">
										<button type="submit" class="button blue" value="">Save Changes</button>
									</div>									
								</div>
								</form>
							</div>
							<div class="col-sm-4">
								<div class="right-panel">
									<ul>
										<!-- <li>
											<div class="icon facebook"><i class="fa fa-facebook"></i></div>
											<h3>Facebook</h3>
											<p><a href="#">Disconnect this account from Facebook</a></p>
										</li>
										<li>
											<div class="icon security"><i class="fa fa-shield"></i></div>
											<h3>Security</h3>
											<p><a href="#">Set up two-factor authentication</a></p>
											<p><a href="javascript:void(0);" class="toggle-btn off" data-toggle-text="on">off</a></p>
										</li>
										<li>
											<div class="icon sessions"><i class="fa fa-key"></i></div>
											<h3>Manage Sessions</h3>
											<p><a href="#">Log me out on all other devices</a></p>
										</li>-->
										<li> 
											<div class="icon delete"><i class="fa fa-trash-o"></i></div>
											<h3>Delete Account</h3>
											<p><a href="#" data-target="#myData" data-toggle="modal">Delete my Mefarnes account</a></p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</section>
					
					<section class="tab-contents edit-profile">
						<div class="row">
							<div class="col-sm-6">
								<div class="form">
									<div class="form-row">
										<label>Name</label>
                                                                                    
										<input type="text" value="<?php echo (!empty($data->first_name) && $data->last_name) ? $data->first_name . ' ' . $data->last_name: '';?>" />
										<span class="info">Your name is displayed on your profile.</span>
									</div>
									
									<div class="form-row">
										<label>Picture</label>
										<div class="pic-box" id="image-box-<?= $data->id ?>">
                                                    <figure>
                                                        <img id="previewFinal"  src="<?= base_url() ?>uploads/campaign/gallery_images/thumb/<?= $data->user_image ?>" alt="" />
                                                        <!--
                                                        <a href="javascript:;" onclick="if (confirm('Are you sure ?'))
                                                                    deleteGalImage(<?= $gallery_image->id ?>);"><i class="fa fa-remove"></i></a>
                                                                    ---->
                                                    </figure>
                                                    
                                                </div>
										
										<a href="javascript:;" data-target="#myModal" data-toggle="modal" class="upload-box">Choose an image from your computer</a>
										<span class="info">JPEG, PNG, GIF, or BMP • 50MB file limit</span>
									</div>
									
									<!-- <div class="form-row">
										<label>Name</label>
										<textarea rows="2" cols="10" placeholder="Write something about you...."></textarea>
										<span class="info">We suggest a short bio. If it's 300 characters or less it'll look great on your profile.</span>
									</div> -->
									
									<div class="form-row">
										<a href="javascript:void(0);" class="button blue" onclick="save_next()"><span>Save Changes</span></a>
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form">
									<div class="form-row">
										<label>Location</label>
										<select class="fancy-selectbox">
											<option><?php echo (!empty($data->city)) ? $data->city: '' ; ?></option>
											<option><?php echo (!empty($data->country)) ? $data->country: '' ; ?></option>
											<option><?php echo (!empty($data->registar_city)) ? $data->cityregistar_city: '' ; ?></option>
										</select>
									</div>
									
									<!-- <div class="form-row">
										<label>Website</label>
										<input type="text" placeholder="Your website URL" />
										<a href="#" class="add-more"><i class="fa fa-plus-circle"></i></a>
									</div> -->
								</div>
							</div>
						</div>
					</section>
					
					<section class="tab-contents notifications">
						<div class="form">
							<label>Newsletters</label>
							<div class="form-row">
								<input type="checkbox" id="chkbox1" checked="checked" class="fancy-checkbox" /><label for="chkbox1">Accept email subscription</label>
							</div>
							<!-- <div class="form-row">
								<input type="checkbox" id="chkbox2" checked="checked" class="fancy-checkbox" /><label for="chkbox2">SLorem ipsum dolor sit amet, consectetur adipiscing elit.</label>
							</div> -->
							<!-- <div class="form-row">
								<input type="checkbox" id="chkbox3" class="fancy-checkbox" /><label for="chkbox3">Sed et ex sed dui varius ornare sit amet eu velit.</label>
							</div> -->
							<!-- <div class="form-row">
								<input type="checkbox" id="chkbox4" class="fancy-checkbox" /><label for="chkbox4">In eu quam interdum, aliquet leo at, commodo quam.</label>
							</div> -->
							<!-- <div class="form-row">
								<input type="checkbox" id="chkbox5" checked="checked" class="fancy-checkbox" /><label for="chkbox5">Sed condimentum odio a porttitor luctus.</label>
							</div> -->
							
							<div class="form-row">
								<a href="#" class="button blue"><span>Subscribe to all</span></a>
							</div>
						</div>
					</section>
					
					<section class="tab-contents find-friends">
						<ul class="inner-tabs">
							<li class="active"><a href="javascript:void(0);" data-toggle="following-friends">Following <span>[ 4 ]</span></a></li>
							<li><a href="javascript:void(0);" data-toggle="follower-friends">Followers <span>[ 4 ]</span></a></li>
							<li><a href="javascript:void(0);" data-toggle="blocked-friends">Blocked</a></li>
						</ul>
						
						<section class="inner-tab-contents following-friends">
							<div class="row">
								<div class="col-sm-12 col-md-6">
									<div class="friend-box">
										<figure><img src="<?= base_url().'assets/'?>images/data/user-07.jpg" alt="" /></figure>
										<div class="contents">
											<h3><a href="#">Mike Goldstien</a></h3>
											<p><a href="#">225 friends</a></p>
										</div>
										<div class="dropdown">
											<select class="fancy-selectbox">
												<option>Friends</option>
												<option>Family</option>
												<option>Colleagues</option>
											</select>
										</div>
									</div>
									
									<div class="friend-box">
										<figure><img src="<?= base_url().'assets/'?>images/data/user-09.jpg" alt="" /></figure>
										<div class="contents">
											<h3><a href="#">Prabhas Bahubali</a></h3>
											<p><a href="#">455 friends</a></p>
										</div>
										<div class="dropdown">
											<select class="fancy-selectbox">
												<option>Friends</option>
												<option>Family</option>
												<option>Colleagues</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<div class="friend-box">
										<figure><img src="<?= base_url().'assets/'?>images/data/user-08.jpg" alt="" /></figure>
										<div class="contents">
											<h3><a href="#">Tiger Sraff</a></h3>
											<p><a href="#">2023 friends</a></p>
										</div>
										<div class="dropdown">
											<select class="fancy-selectbox">
												<option>Friends</option>
												<option>Family</option>
												<option>Colleagues</option>
											</select>
										</div>
									</div>
									
									<div class="friend-box">
										<figure><img src="<?= base_url().'assets/'?>images/data/user-10.jpg" alt="" /></figure>
										<div class="contents">
											<h3><a href="#">Olivia Brown</a></h3>
											<p><a href="#">225 friends</a></p>
										</div>
										<div class="dropdown">
											<select class="fancy-selectbox">
												<option>Friends</option>
												<option>Family</option>
												<option>Colleagues</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</section>
						
						<section class="inner-tab-contents follower-friends">
							Followers
						</section>
						
						<section class="inner-tab-contents blocked-friends">
							Blocked
						</section>
						
						<div class="hr"></div>
						
						<div class="discover-campaign-box">
					<div class="icon"><i class="fa fa-comments"></i></div>
					<div class="contents">
						<h3>Discover Campaigns with your friends!</h3>
						<h4>Follow your friends and we'll notify you whenever they launch or back a new Campaign.</h4>
					</div>
					<div class="buttons">
						<a href="#" class="button blue"><span>Learn More</span></a>
					</div>
				</div>
					</section>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div id="myData" class="modal fade"  role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <h4 class="modal-title">Are you sure to deactivate your account?</h4>
            </div>
            <div class="modal-body">
                <!-- <div class="image-editor">
                    <input type="file" class="cropit-image-input" required>
                    <div class="cropit-preview"></div>
                    <div class="image-size-label">
                        Resize image
                    </div>
                    <input type="range" class="cropit-image-zoom-input"> -->
                    <!--<button class="rotate-ccw">Rotate counterclockwise</button>
                    <button class="rotate-cw">Rotate clockwise</button>-->
                    <a href="<?php echo base_url(); ?>home/deactivate" id="sure" class="button blue">Sure</a>
                    <!-- <a id="no" class="button blue export">No</a> -->
                </div>
            </div>
        </div>
    </div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <div class="image-editor">
                    <input type="file" class="cropit-image-input" required>
                    <div class="cropit-preview"></div>
                    <div class="image-size-label">
                        Resize image
                    </div>
                    <input type="range" class="cropit-image-zoom-input">
                    <!--<button class="rotate-ccw">Rotate counterclockwise</button>
                    <button class="rotate-cw">Rotate clockwise</button>-->
                    <button class="button blue export">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.cropit.js"></script>
<script>
  $(function() {
$('.image-editor').cropit({});
	$('.rotate-cw').click(function() {
	  $('.image-editor').cropit('rotateCW');
	});
	$('.rotate-ccw').click(function() {
	  $('.image-editor').cropit('rotateCCW');
	});

	$('.export').click(function() {
								
	  var imageData = $('.image-editor').cropit('export');
	  
	  //window.open(imageData);
	  //alert(imageData);
	  $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>User/file_upload_parser",          
		data: {'imgdata':imageData,'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
			success: function(total){
				$('#myModal').modal('hide');
				window.location.reload();
			}
		});
	 });
  });
</script>
 

