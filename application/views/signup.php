<!-- content -->
<div id="content" class="page signup">
    <div class="container">
        <div class="row">
        	<form name="frm_signup" id="frm_signup" method="post" action="">
            <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
            <!-- main signup panel -->
            <section class="sectionMain">
                <h2>Let's get started!</h2>
                <h3><!--First, tell us what you're looking for.--->First tell us about yourself</h3>
                <div class="signup-box">
                    <div class="or">OR</div>
                    <div class="col">
                        <h2><!--I’m Individual-->I am an individual</h2>
                        <div class="buttons"><a href="javascript:fun_redirect('individualuser');" class="blue"><i class="fa fa-user"></i></a></div>
                        <p><!--Create campaign, find friends and organization and raise fund.--->Create a campaign, spread the word, reach your goal!</p>
                    </div>
                    <div class="col">
                        <h2>We are an organization</h2>
                        <div class="buttons"><a href="javascript:fun_redirect('organizationuser');" class="orange"><i class="fa fa-users"></i></a></div>
                        <p><!--Create campaign, find followers and backers and raise fund.-->Create a campaign, spread the word, reach your goal!</p>
                    </div>
                </div>
                <p class="bbb"><i class="icon-bbb"></i></p>
            </section>
            <!-- /main signup panel -->
			</form>
            
        </div>
    </div>
</div>
<!-- /content -->

<style type="text/css">
    body {
        font-family: sans-serif;
        font-size: 14px;
    }
</style>

<script type="text/javascript">
	function fun_redirect(usertype)
	{
		var frm_obj = window.document.frm_signup;
		
		if(usertype == 'individualuser')
		{
			frm_obj.action = "<?php echo base_url(); ?>individualuser";
			frm_obj.submit();
		}
		if(usertype == 'organizationuser')
		{
			frm_obj.action = "<?php echo base_url(); ?>organizationuser";
			frm_obj.submit();
		}
		
	}
</script>