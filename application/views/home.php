<div id="content">
    <!-- main slider -->
    <div class="main-slider">
        <ul class="slides">
            <li class="one">
                <figure><img src="<?= base_url() . 'assets/' ?>images/main-slider-1.png" alt="" /></figure>
                <div class="contents">
                    <h2>CROWDFUNDING FOR THE JEWISH COMMUNITY</h2>
                    <h4>Support Great Jewish Causes, Community Wide Projects And Innovative Startups</h4>
                    <div class="buttons">
                        <a href="<?= base_url('signup'); ?>" class="banner-button">
                            <span class="icon"><i class="icon-bullhorn"></i></span>
                            <span class="content">
                                <strong>Post a campaign</strong>
                                <small>Tell us your story<br class="hidden-xs" />Raise funds for it!</small>
                            </span>
                        </a>
                        <a href="<?= base_url('search'); ?>" class="banner-button donate">
                            <span class="icon"><i class="icon-bag-heart"></i></span>
                            <span class="content">
                                <strong>Donate </strong>
                                <small>Support a campaign close<br class="hidden-xs" /> to your heart!</small>
                            </span>
                        </a>
                    </div>
                </div>
            </li>
            <li class="one">
                <figure><img src="<?= base_url() . 'assets/' ?>images/main-slider-2.png" alt="" /></figure>
                <div class="contents">
                    <h2>CROWDFUNDING FOR THE JEWISH COMMUNITY</h2>
                    <h4>Support Great Jewish Causes, Community Wide Projects And Innovative Startups</h4>
                    <div class="buttons">
                        <a href="<?= base_url('signup'); ?>" class="banner-button">
                            <span class="icon"><i class="icon-bullhorn"></i></span>
                            <span class="content">
                                <strong>Post a campaign</strong>
                                <small>Tell us your story<br class="hidden-xs" />Raise funds for it!</small>
                            </span>
                        </a>
                        <a href="<?= base_url('search'); ?>" class="banner-button donate">
                            <span class="icon"><i class="icon-bag-heart"></i></span>
                            <span class="content">
                                <strong>Donate </strong>
                                <small>Support a campaign close<br class="hidden-xs" />to your heart!</small>
                            </span>
                        </a>
                    </div>
                </div>
            </li>
            <li class="one">
                <figure><img src="<?= base_url() . 'assets/' ?>images/main-slider-3.png" alt="" /></figure>
                <div class="contents">
                    <h2>CROWDFUNDING FOR THE JEWISH COMMUNITY</h2>
                    <h4>Support Great Jewish Causes, Community Wide Projects And Innovative Startups</h4>
                    <div class="buttons">
                        <a href="<?= base_url('signup'); ?>" class="banner-button">
                            <span class="icon"><i class="icon-bullhorn"></i></span>
                            <span class="content">
                                <strong>Post a campaign</strong>
                                <small>Tell us your story<br class="hidden-xs" />Raise funds for it!</small>
                            </span>
                        </a>
                        <a href="<?= base_url('search'); ?>" class="banner-button donate">
                            <span class="icon"><i class="icon-bag-heart"></i></span>
                            <span class="content">
                                <strong>Donate </strong>
                                <small>Support a campaign close<br class="hidden-xs" />to your heart!</small>
                            </span>
                        </a>
                    </div>
                </div>
            </li>
        </ul>
        <div class="slider-cover"><img src="<?= base_url() . 'assets/' ?>images/banner-bottom-cover.png" alt="" /></div>
    </div>
    <!-- /main slider -->

    <!-- how it works -->
    <section class="how-it-works">
        <div class="container">
            <h2 class="underline">How it works</h2>
            <h4>Any individual or organization can post a campaign drive to raise money for their cause. 
                <br class="hidden-xs" />Help ease the burden for a person in need or benefit a community at large.</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="box">
                        <figure><i class="icon-create-campaign"></i></figure>
                        <h5>Create your campaign</h5>
                        <p>Describe your cause in a few sentences, include pictures and your campaign goal.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <figure><i class="icon-share-friends"></i></figure>
                        <h5>Share with friends</h5>
                        <p>More people that know about your campaign, the easier to reach your goal.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <figure><i class="icon-accept-donations"></i></figure>
                        <h5>Accept Donations</h5>
                        <p>It’s easy, simply withdraw the funds directly into your bank account.</p>
                    </div>
                </div>
            </div>
            <div class="steps">
                <div class="clearfix">
                    <span>1</span> <span>2</span> <span>3</span>
                </div>
            </div>
        </div>
    </section>
    <!-- /how it works -->


    <!-- featured campaigns -->
    <div class="featured-campaigns">
        <div class="container">
            <h2 class="underline text-center">Featured Campaigns</h2>
            <div class="row">
                <!--                <div class="col-sm-6">
                                    <div class="box">
                                        <figure><a href="#"><img src="<?= base_url() . 'assets/' ?>images/data/featured-campaign-01.jpg" alt="" /></a></figure>
                                        <div class="contents">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <h4><a href="#">Community</a></h4>
                                                    <h3><a href="#">Save the Life!</a></h3>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p>
                                                        By <a href="#">John Doe</a>, 8 hours ago
                                                        <span><img src="<?= base_url() . 'assets/' ?>images/data/author-01.jpg" alt="" /></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                
                                    <div class="box">
                
                                        <figure><a href="#"><img src="<?= base_url() . 'assets/' ?>images/data/featured-campaign-02.jpg" alt="" /></a></figure>
                                        <div class="contents">
                                            <div class="row">
                
                                                <div class="col-xs-6">
                                                    <h4><a href="#">Education</a></h4>
                                                    <h3><a href="#">Help me study abroad!</a></h3>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p>
                                                        By <a href="#">Mary Lee</a>, 8 hours ago
                                                        <span><img src="<?= base_url() . 'assets/' ?>images/data/author-02.jpg" alt="" /></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->


                <?php foreach ($featured_campaigns as $campaign):
                      $image_file = base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image;
                      $image_nofile = base_url() . 'uploads/campaign/gallery_images/thumb/' . "no-image.png";
              ?>
                    <div  class="col-sm-6">

                        <div class="box">

                            <figure><a href="<?= base_url() . 'campaign/' . $campaign->slug ?>"> <?php if ($campaign->image != "") { ?>
                                            <img src="<?php echo $image_file; ?>" alt="" />
                                        <?php } else { ?>
                                            <img class="responsive-img" width="560" height="400" src="<?php echo base_url();?>uploads/campaign/gallery_images/thumb/no-image.png" alt="" />
                                        <?php } ?></a></figure>
                            <div class="contents">
                                <div class="row">

                                    <div class="col-xs-6">
                                        <h4><a href="<?= base_url() . 'campaign/' . $campaign->slug ?>"><?= $campaign->name ?></a></h4>
                                    </div>
                                    <div class="col-xs-6">
                                        <p>
                                            By <a href="#"><?= $campaign->user_name ?></a>, <?= time_elapsed_string(strtotime($campaign->create_date)) ?>
                                            <!--<span><img src="<?= base_url() . 'assets/' ?>images/data/author-02.jpg" alt="" /></span>-->
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3><a href="<?= base_url() . 'campaign/' . $campaign->slug ?>"><?php echo substr($campaign->title,0,40);if(strlen($campaign->title)>40)echo '...'; ?></a></h3>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php endforeach;
                ?>
            </div>
        </div>

        <!-- campaign scroller -->
        <div class="campaign-scroller">
            <h2 class="underline text-center mt40">Featured Organizations</h2> 

            <ul>
                <li>
                    <figure><a href="https://www.mefarnes.com/campaign/united-hatzalah"><img src="<?= base_url(); ?>/uploads/campaign/gallery_images/thumb/79_58452da5087d8.png" alt="" /></a></figure>
                    <div class="contents">
                        <h4><a href="https://www.mefarnes.com/campaign/united-hatzalah">Health</a></h4>
                        <h3><a href="https://www.mefarnes.com/campaign/united-hatzalah">United Hatzalah</a></h3>
                        <p>When emergencies occur, rapid medical treatment increases...</p>
                        <a href="https://www.mefarnes.com/campaign/united-hatzalah" class="more"><i class="icon-more-arrow"></i></a>
                    </div>
                </li>
                <li>
                    <figure><a href="https://www.mefarnes.com/campaign/support-thousands-learning-torah-from-early-morning-until-late-night"><img src="<?= base_url(); ?>/uploads/campaign/gallery_images/thumb/81_584533bdd3c70.png" alt="" /></a></figure>
                    <div class="contents">
                        <h4><a href="https://www.mefarnes.com/campaign/support-thousands-learning-torah-from-early-morning-until-late-night">Yeshivas</a></h4>
                        <h3><a href="https://www.mefarnes.com/campaign/support-thousands-learning-torah-from-early-morning-until-late-night">Support thousands learning Torah from early morning until late night.</a></h3>
                        <p>The Mir Yeshiva, known as the Mirrer Yeshiva or The Mir,..</p>
                        <a href="https://www.mefarnes.com/campaign/support-thousands-learning-torah-from-early-morning-until-late-night" class="more"><i class="icon-more-arrow"></i></a>
                    </div>
                </li>
                <li>
                    <figure><a href="https://www.mefarnes.com/campaign/zaka-search-and-rescue"><img src="<?= base_url(); ?>/uploads/campaign/gallery_images/thumb/147_586a24801cfeb.png" alt="" /></a></figure>
                    <div class="contents">
                        <h4><a href="https://www.mefarnes.com/campaign/zaka">Funerals</a></h4>
                        <h3><a href="https://www.mefarnes.com/campaign/zaka">ZAKA search And Rescue</a></h3>
                        <p>Support the ZAKA Volunteers in Israel and around the world</p>
                        <a href="https://www.mefarnes.com/campaign/zaka" class="more"><i class="icon-more-arrow"></i></a>
                    </div>
                </li>
            </ul>
        </div>
        <!-- campaign scroller -->
    </div>
    <!-- /featured campaigns -->

    <!-- fund campaigns -->
    <div class="fund-campaigns">
        <div class="container">
            <h2 class="underline text-center">Fund campaigns you love.</h2>

            <!-- tags -->
            <div class="campaign-tags">
                <ul>
                    <?php foreach ($categories as $category): ?>
                        <li><a href="<?= base_url().'search?category='.$category->id ?>"><?= $category->name ?></a></li>
                    <?php endforeach;
                    ?>
                </ul>
            </div>
            <!-- /tags -->

            <!-- search -->
            <div class="campaign-search">
                <?php echo form_open('search', array('method' => 'get')); ?> 
                <div class="input-box">
                    <input type="search" placeholder="Search by campaign name" id="keyword" name="keyword" />
                </div>
                <div class="input-box">
                    <input type="search" placeholder="Search by Organization">
                </div>
                <div class="input-box">
                    <input type="search" placeholder="Search by Location" id="location" name="location" />

                </div>
                <div class="select-box double">
                    <select class="fancy-selectbox" name="category" id="category">
                        <option value="">Type</option>
                        <?php foreach ($categories as $category): ?>
                            <option value="<?= $category->id ?>"><?= $category->name ?></option>
                        <?php endforeach;
                        ?>
                    </select>
                </div>
                <div class="select-box double">
                    <select name="sortby" class="fancy-selectbox">
                        <option value="fund-desc" <?php echo (isset($sortby) && $sortby == 'fund-desc') ? "selected" : "" ?>>Hightest Funded</option>
                        <option value="fund-asc" <?php echo (isset($sortby) && $sortby == 'fund-asc') ? "selected" : "" ?>>Lowest Funded</option>
                        <option value="date-desc" <?php echo (isset($sortby) && $sortby == 'date-desc') ? "selected" : "" ?>>Ending soon</option>
                        <option value="date-asc" <?php echo (isset($sortby) && $sortby == 'date-asc') ? "selected" : "" ?>>New Started</option>
                    </select>
                </div>
                <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- /search -->

            <!-- campaigns -->
           <div class="search-campaigns">
            <div class="campaign-boxes">
                <div class="row">

     
                  
                  
                  
                    <?php

                   foreach ($home_campaigns as $campaign):
                                //$image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/gallery_images/thumb/' . $campaign->image)) ? base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image : base_url() . 'uploads/campaign/gallery_images/' . $campaign->image;
                                $image_file = base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image;
                                $image_nofile = base_url() . 'uploads/campaign/gallery_images/thumb/' . "no-image.png";
                                $now = time(); // or your date as well
                                $your_date = strtotime(date('Y-m-d', strtotime($campaign->end_date_time)));
                                $datediff = $your_date - $now;
                                $days_left = floor($datediff / (60 * 60 * 24));

                                if ($campaign->fund_needed > 0) {
                                    $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                                } else
                                    $percentage = 0;

                                //echo $campaign->collected_fund*100/$campaign->fund_needed;
                                ?> 
                                <div class="col-sm-4 col-md-3">
                                    <a href="<?php echo base_url() . 'campaign/' . $campaign->slug; ?>" class="box">
                                        <?php if ($campaign->image != "") { ?>
                                            <img width="100%" height="160" src="<?php echo $image_file; ?>" alt="" />
                                        <?php } else { ?>
                                            <img width="100%" height="160" src="<?php echo base_url();?>uploads/campaign/gallery_images/thumb/no-image.png" alt="" />
                                        <?php } ?>
                                        <span class="data">
                                            <span class="percent-circle">
                                                <span class="c100 p<?= $percentage ?>">
                                                    <span><?= $percentage ?><small>%</small> <b>FUNDED</b></span>
                                                    <b class="slice"><span class="bar"></span><span class="fill"></span></b>
                                                </span>
                                            </span>
                                            <span class="category-name"><?= $campaign->category_name ?></span>
                                            <span class="category-title"><?php echo substr((strip_tags($campaign->title)),0,20)." "; ?></span>
                                            <span class="category-desc"><?php echo substr((strip_tags($campaign->description)),0,90)." ...";?></span>
                                            <span class="user-info clearfix">
                                                <img src="<?php echo base_url(); ?>uploads/campaign/gallery_images/thumb/<?= $campaign->user_image ?>" alt="" />
                                                <span class="content">By <b><?php echo substr((strip_tags($campaign->first_name . " " . $campaign->last_name." ".$campaign->org_name)),0,15)." "; if(strlen(($campaign->first_name . " " . $campaign->last_name." ".$campaign->org_name))>15)echo '...'; ?></b><br /><?php echo substr($campaign->location,0,18);if(strlen($campaign->location)>18)echo '...'; ?></span>
                                            </span>
                                            <span class="category-meta">
                                                <span>Raised<b>$<?= format_num($campaign->collected_fund, 0) ?></b></span>
                                                <span>Backers<b><?= $campaign->donor ?></b></span>
                                                <span>Days Left<b><?php echo $days_left; ?></b></span>
                                            </span>
                                        </span>
                                    </a>
                                </div>		

                            <?php endforeach; ?>


                    

                </div>
            </div>
            </div>
            <!-- /campaigns -->			

        </div>
    </div>
    <!-- /fund campaigns -->
</div>
<style type="text/css">
    body {
        font-family: sans-serif;
        font-size: 14px;
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js??v=3.13&libraries=places&key=AIzaSyBgS4A2KbRY159HA-HDIRxaoanaYQscQy8" type="text/javascript"></script>

<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>


<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585f9ce134708e03"></script> 