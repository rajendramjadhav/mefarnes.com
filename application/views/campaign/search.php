<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.counterup.min.js"></script>
<style>
    .selected-category{  
        color: #A6CE3F !important;
        display: block;
        padding: 7px 15px;
        position: relative;
    }  
</style>   
<?php //var_dump($all_campainin);exit;?> 
<!-- content -->
<div id="content" class="page search-campaigns">
    <!-- search header -->
    <section class="search-header">
        <div class="jumbotron">
            <div class="container text-center">
                <div class="page-header text-center">
                    <?php //foreach ($all_campainin as $campaign):?>
                    <h1> <?php foreach ($categories as $eachcategory): ?>
                            <?php if (isset($category) && $category == $eachcategory->id) echo $eachcategory->name ?>
                        <?php endforeach; ?>

                    </h1>

                    <?php //endforeach; ?>
                </div>
                <div class="row campaign-facts">
                    <div class="col-xs-6 col-sm-3">
                        <p><span class="counter"><?php echo $tot_rec; ?></span> Campaigns</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <p><span class="counter">0</span> Organizations</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <p><span class="counter"><?php
                                $total_donor = 0.00;
                                foreach ($all_campainin as $campaign):
                                    $total_donor += $campaign->donor;
                                endforeach;
                                echo number_format($total_donor,0);
                                ?></span> Backers</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <p><small class="dollar-sign">$</small><span class="counter" id="raised">
                                <?php
                                $total_rised_fund = 0.00;
                                foreach ($all_campainin as $campaign):
                                    $total_rised_fund += $campaign->collected_fund;
                                endforeach;
                                echo number_format($total_rised_fund,0);
                                ?>
                            </span>Raised</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /search header -->
    <?php //var_dump($categories);exit;?>
    <!-- fund campaigns -->
    <div class="fund-campaigns">
        <div class="container">
            <?php echo form_open(current_url(), 'id=search-form'); ?>
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-4 col-md-3">
                    <nav class="campaign-category">
                        <h3>All Categories</h3>
                        <ul class="nav">
                            <?php foreach ($categories as $eachcategory): ?>
                                <!--                                <li><a href="<?php //echo base_url() . "search/category/" . $category->id      ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php //echo $eachcategory->name      ?></a></li>-->
                                <li><a href="javascript:;" class="<?php if (isset($category) && $category == $eachcategory->id) echo 'selected-category' ?>" onclick="setcat(<?= $eachcategory->id ?>);"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $eachcategory->name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <input type="hidden" name="category" id="category" value="<?php echo isset($category) ? $category : "" ?>">
                    </nav>
                </div>
                <!-- /left panel -->
                <!-- right panel -->
                <div class="col-sm-8 col-md-9">
                    <!-- search -->
                    <div class="campaign-search">
                        <p class="search-result">We found <span><?php echo $tot_rec; ?></span> campaigns for <span>Community</span></p>

                        <div class="clearfix">
                            <div class="input-box">
                                <input type="search" name="keyword" placeholder="Search by campaign name" id="keyword" value="<?php echo isset($keyword) ? $keyword : "" ?>" />

                            </div>
                            <!--                            <div class="select-box">
                                                            <select name="location" class="fancy-selectbox">
                                                                <option value="">Location</option>
                            <?php foreach ($locations as $loc): ?>
                                                                                        <option value="<?= $loc->id ?>" <?php if (isset($location) && $loc->id == $location) echo 'selected'; ?>><?= $loc->location ?></option>
                            <?php endforeach;
                            ?>
                                                            </select>
                                                        </div>-->
                            <div class="input-box">
                                <input type="search" name="location" placeholder="Search by campaign location" id="location" value="<?php echo isset($location) ? $location : "" ?>" />

                            </div>

                            <div class="select-box double">
                                <select name="sortby" class="fancy-selectbox">
                                    <option value="fund-desc" <?php echo (isset($sortby) && $sortby == 'fund-desc') ? "selected" : "" ?>>Hightest Funded</option>
                                    <option value="fund-asc" <?php echo (isset($sortby) && $sortby == 'fund-asc') ? "selected" : "" ?>>Lowest Funded</option>
                                    <option value="date-desc" <?php echo (isset($sortby) && $sortby == 'date-desc') ? "selected" : "" ?>>Ending soon</option>
                                    <option value="date-asc" <?php echo (isset($sortby) && $sortby == 'date-asc') ? "selected" : "" ?>>New Started</option>
                                </select>
                            </div>
                            <ul class="list-inline list-unstyled">
                                <li>
                                    <button type="submit" name="src_btn" id="src_btn" value="submit"><i class="fa fa-search"></i></button>
                                </li>
                                <li>
                                    <a class="btn btn-default btn-gridview pull-right" href="javascript:void(0)" onclick="changeView(this)" role="button"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /search -->
                    <!-- campaigns -->
                    <?php //var_dump($all_campainin);?>
                    <?php //echo base_url();exit;?>
                    <div class="campaign-boxes" id="gridview">
                        <div class="row">
                            <?php
                            foreach ($all_campainin as $campaign):
                                //$image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/gallery_images/thumb/' . $campaign->image)) ? base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image : base_url() . 'uploads/campaign/gallery_images/' . $campaign->image;
                                $image_file = base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image;
                                $image_nofile = base_url() . 'uploads/campaign/gallery_images/thumb/' . "no-image.png";
                                $now = time(); // or your date as well
                                $your_date = strtotime(date('Y-m-d', strtotime($campaign->end_date_time)));
                                $datediff = $your_date - $now;
                                $days_left = floor($datediff / (60 * 60 * 24));

                                if ($campaign->fund_needed > 0) {
                                    $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                                } else
                                    $percentage = 0;

                                //echo $campaign->collected_fund*100/$campaign->fund_needed;
                                ?>
                                <div class="col-sm-6 col-md-4">
                                    <a href="<?php echo base_url() . 'campaign/' . $campaign->slug; ?>" class="box">
                                        <?php if ($campaign->image != "") { ?>
                                            <img width="100%" height="160" src="<?php echo $image_file; ?>" alt="" />
                                        <?php } else { ?>
                                            <img width="100%" height="160" src="<?php echo base_url();?>uploads/campaign/gallery_images/thumb/no-image.png" alt="" />
                                        <?php } ?>
                                        <span class="data">
                                            <span class="percent-circle">
                                                <span class="c100 p<?= $percentage ?>">
                                                    <span><?= $percentage ?><small>%</small> <b>FUNDED</b></span>
                                                    <b class="slice"><span class="bar"></span><span class="fill"></span></b>
                                                </span>
                                            </span>
                                            <span class="category-name"><?= $campaign->category_name ?></span>
                                            <span class="category-title"><?php echo substr((strip_tags($campaign->title)),0,20)." "; ?></span>
                                            <span class="category-desc"><?php echo substr((strip_tags($campaign->description)),0,90)." ...";?></span>
                                            <span class="user-info clearfix">
                                                <img src="<?php echo base_url(); ?>uploads/campaign/gallery_images/thumb/<?= $campaign->user_image ?>" alt="" /> 
                                                <span class="content">By <b><?php echo substr((strip_tags($campaign->first_name . " " . $campaign->last_name." ".$campaign->org_name)),0,15)." "; if(strlen(($campaign->first_name . " " . $campaign->last_name." ".$campaign->org_name))>15)echo '...'; ?></b><br /><?php echo substr($campaign->location,0,18);if(strlen($campaign->location)>18)echo '...'; ?></span>
                                            </span>
                                            <span class="category-meta">
                                                <span>Raised<b>$<?= format_num($campaign->collected_fund, 0) ?></b></span>
                                                <span>Backers<b><?= $campaign->donor ?></b></span>
                                                <span>Days Left<b><?php echo $days_left; ?></b></span>
                                            </span>
                                        </span>
                                    </a>
                                </div>		

                            <?php endforeach; ?>

                        </div>
                        <div class="campaign-footer clearfix">
                            <p class="pull-left">Showing <span></span> out of <span><?php echo $tot_rec; ?></span> campaigns</p>
                            <!--                            <nav class="pull-right">
                                                            <ul class="pagination">
                                                                <li><a href="#" aria-labels="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                                                            </ul>
                                                        </nav>-->
                        </div>
                    </div>
                    <!-- /campaigns -->
                    <div class="campaign-boxes listview hide" id="listview">
                        <div class="clearfix">
                            <?php
                            foreach ($all_campainin as $campaign):
                                //$image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/gallery_images/thumb/' . $campaign->image)) ? base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image : base_url() . 'uploads/campaign/gallery_images/' . $campaign->image;
                                $image_file = base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image;
                                $image_nofile = base_url() . 'uploads/campaign/gallery_images/thumb/' . "no-image.png";
                                $now = time(); // or your date as well
                                $your_date = strtotime(date('Y-m-d', strtotime($campaign->end_date_time)));
                                $datediff = $your_date - $now;
                                $days_left = floor($datediff / (60 * 60 * 24));

                                if ($campaign->fund_needed > 0) {
                                    $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                                } else
                                    $percentage = 0;
                                ?>
                                <?php //echo $image_nofile;exit;?>
                                <a href="<?php echo base_url() . 'campaign/' . $campaign->slug; ?>" class="box">
                                    <?php if ($campaign->image != "") { ?>
                                        <img style="width:30%; height: auto; overflow:hidden;" src="<?= $image_file; ?>" alt="" />
                                    <?php } else { ?>
                                        <img style="width:30%; height: auto; overflow:hidden;" src="<?= $image_nofile; ?>" alt="" />
                                    <?php } ?>
                                    <span class="data">
                                        <span class="percent-circle">
                                            <span class="c100 p<?= $percentage ?>">
                                                <span><?= $percentage ?><small>%</small> <b>FUNDED</b></span>
                                                <b class="slice"><span class="bar"></span><span class="fill"></span></b>
                                            </span>
                                        </span>
                                        <span class="category-name"><?= $campaign->category_name ?></span>
                                        <span class="category-title"><?php echo $campaign->title; ?></span>
                                        <span class="category-desc"><?php echo substr((strip_tags($campaign->description)),0,110)." ...";?></span>
                                        <span class="user-info clearfix">
                                            <img src="<?php echo base_url(); ?>uploads/campaign/gallery_images/thumb/<?= $campaign->user_image ?>" alt="" />
                                            <span class="content">By <b><?php echo $campaign->first_name . " " . $campaign->last_name ?></b><br /><?php echo $campaign->location ?></span>
                                        </span>
                                    </span>
                                    <span class="category-meta">
                                        <span>Raised<b>$<?= format_num($campaign->collected_fund, 0) ?></b></span>
                                        <span>Backers<b><?= $campaign->donor ?></b></span>
                                        <span>Days Left<b><?php echo $days_left ?></b></span>
                                    </span>
                                </a>
                            <?php endforeach; ?>
                        </div>

                        <div class="campaign-footer clearfix">
                            <p class="pull-left">Showing <span>10</span> out of <span><?php $tot_rec ?></span> campaigns</p>
                            <!--                            <nav class="pull-right">
                                                            <ul class="pagination">
                                                                <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                                                            </ul>
                                                        </nav>-->
                        </div>
                    </div>
                </div>
                <!-- /right panel -->
            </div>
            </form>

        </div>
    </div>
    <!-- /fund campaigns -->
</div>
<!-- /content -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>
<script>
                                        function changeView(e)
                                        {
                                            $("#gridview").toggleClass('hide');
                                            $("#listview").toggleClass('hide');
                                            if ($(e).children('i').hasClass('fa-th-list')) {
                                                $(e).children('i').addClass('fa-th').removeClass('fa-th-list');
                                            } else {
                                                $(e).children('i').addClass('fa-th-list').removeClass('fa-th');
                                            }
                                        }
                                        function ShowHide(id)
                                        {
                                            $('#more' + id).slideToggle('slow');
                                            $('#less' + id).slideToggle('slow');
                                        }


                                        function setcat(catId)
                                        {
                                            //alert(catId);
                                            $("#category").val(catId);
                                            $("#src_btn").trigger("click");
                                        }
</script>