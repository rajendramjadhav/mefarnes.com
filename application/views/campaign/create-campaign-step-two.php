<script src="<?php echo base_url(); ?>assets/js/jquery.cropit.js"></script>
<style type="text/css">
	/* header */
	header { min-height:100px; padding:15px 0; background-color:#fff; }
	
	/* nav */
	nav {}
	.navbar-default { min-height:100px; background-color:#fff; border:0; border-bottom:1px solid #cbcccf; }
	#navbar { float:right; margin:25px 0 0 0; }
	.navbar-default .navbar-toggle { background-color:#99cc00; border-radius:0; position:relative; width:40px; height:42px; line-height:30px; overflow:hidden; margin:30px 15px 0 0; }
	.navbar-default .navbar-toggle:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#67b300; transition:height linear 0.2s; }
	.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus { background-color:#99cc00; }
	.navbar-default .navbar-toggle:hover:after { height:100%; z-index:1; }
		.navbar-toggle i { color:#fff; font-size:1.5em; position:relative; z-index:2; }
	.navbar-default .navbar-nav > li > a { color:#454545; }
	.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus { color:#67b300; }
	.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover { color:#67b300; background-color:transparent; }
	.navbar-default .navbar-nav > li.user > a { padding-top:7px; }
		.navbar-default a .char { width:36px; height:36px; line-height:36px; font-size:1.28em; margin:0 15px 0 0; }
		.navbar-default .navbar-nav > li.user > a i { margin:0 0 0 15px; }
	.navbar-default .navbar-nav > li.campaign { margin:5px 0 0 0; }
		.navbar-default .navbar-nav > li.campaign > a { background-color:#fc6c06; border-radius:3px; color:#fff; text-transform:uppercase; padding:10px 12px; font-weight:700; font-size:0.93em; position:relative; overflow:hidden; }
			.navbar-default .navbar-nav > li.campaign > a span { position:relative; z-index:2; }
		.navbar-default .navbar-nav > li.campaign > a:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#c25407; transition:height linear 0.2s; border-radius:0 0 3px 3px; z-index:1; }
		.navbar-default .navbar-nav > li.campaign > a:hover:after { height:100%; }
		
		.dropdown-menu { border:1px solid #dedede; box-shadow:0 0 16px rgba(0, 0, 0, 0.2); padding:15px 25px; min-width:180px; left:auto; right:0; }
		.dropdown-menu:before {}
			.dropdown-menu > li > a { padding:7px 5px; }
			.dropdown-menu > li > a:focus, .dropdown-menu > li > a:hover { background-color:transparent; color:#99cc00; }
			.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover { background-color:transparent; color:#99cc00; }
	/* /nav */
	/* /header */
	/* create campaign page */
	.create-campaign { padding:60px 0 40px 0; background-color:#f5f5f5; }
		.create-campaign h2 { font-size:2.57em; text-align:center; text-transform:none; }
		.create-campaign h3 { font-weight:300; font-size:1.28em; text-align:center; }		
		.create-campaign .tab-area { margin:60px 0 0 0; }
			.create-campaign .tab-area .tabs { border-bottom:1px solid #dedede; }
				.create-campaign .tab-area .tabs li { position:relative; top:1px; display:inline-block; width:200px; text-align:center; padding:0 10px 20px 0; font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.14em; color:#ababab; text-transform:uppercase; border-bottom:1px solid transparent; }
				.create-campaign .tab-area .tabs li.active { color:#454545; border-bottom-color:#99cc00; }
			.create-campaign .tab-area .tab-data { background-color:#fff; margin:30px 0 0 0; padding:40px; }
				.create-campaign .tab-data .step-2 { display:none; }
				.create-campaign .tab-data .step-3 { display:none; }				
				.create-campaign .tab-data .form {}
					.create-campaign .tab-data .form .form-row { margin:0 0 20px 0; }
					.create-campaign .tab-data .form h4 { font-weight:700; font-size:1.28em; margin:0 0 5px 0; }
						.create-campaign .tab-data .form h4 small { font-weight:400; font-size:0.88em; color:#999; }
					.create-campaign .tab-data .form label { color:#999; font-weight:normal; font-size:1.14em; margin:0 0 10px 0; }
					.upload-box { border:2px dashed #99cc00; text-align:center; padding:40px 40px; margin:10px 0 15px 0; display:block; }
					.upload-box:hover { text-decoration:none; background-color:#f5f5f5; }
					.create-campaign .tab-data .step-1 .form .sbSelector { background-color:#f5f5f5; text-indent:20px; font-weight:300; font-size:1.14em; }
					.create-campaign .tab-data .step-1 .form .sbOptions { padding:25px 25px; }
						.create-campaign .tab-data .step-1 .form .one-third .sbOptions li { width:33.333333%; float:left; }
					.create-campaign .tab-data .form input[type="text"], .create-campaign .tab-data .form input[type="email"], .create-campaign .tab-data .form input[type="tel"] { font-weight:300; font-size:1.14em; background-color:#f5f5f5; border-radius:0; border:0; padding:15px 20px; height:auto; width:100%; }
					.create-campaign .tab-data .form input:focus { box-shadow:none; }
					.create-campaign .tab-data ::-webkit-input-placeholder { color:#686868; font-weight:300; font-size:1em; opacity:1; }
					.create-campaign .tab-data :-ms-input-placeholder { color:#686868; font-weight:300; font-size:1em; opacity:1; }
					.create-campaign .tab-data ::-moz-placeholder { color:#686868; font-weight:300; font-size:1em; opacity:1; }
					.create-campaign .tab-data .step-1 .form .input-group-addon { border:0; background-color:#f5f5f5; font-weight:400; color:#999; }
					.create-campaign .tab-data .step-1 .form .input-group.left .input-group-addon { padding-right:6px; color:#454545; font-size:1em; }
					.create-campaign .tab-data .step-1 .form .input-group.left .input-group-addon + .form-control { padding-left:4px; }
					.form-control { box-shadow:none; }
					.create-campaign .tab-data .form .input-box { background-color:#f5f5f5; background-color:#f5f5f5; padding:15px 15px; text-align:center; margin:0 0 20px 0; }
					.create-campaign .tab-data .form .input-box:before, .create-campaign .tab-data .form .input-box:after { content:""; display:table; }
					.create-campaign .tab-data .form .input-box:after { clear:both; }
					.create-campaign .tab-data .form .days-container { float:left; font-size:1.14em; width:20%; text-align:left; }
					.create-campaign .tab-data .form .slider-bar { float:right; width:80%; }
					.create-campaign .tab-data .form .slider-bar b { float:left; width:15%; font-weight:300; font-size:0.86em; color:#999; font-weight:normal; }
					.create-campaign .tab-data .form .slider-bar #day-slider { float:left; width:70%; margin:7px 0 0 0; }
					.create-campaign .tab-data .form .buttons { margin:60px 0 0 0; }
					.create-campaign .tab-data .form .radios { margin:20px 0 30px 0; }
					.create-campaign .tab-data .form .radios span { float:left; width:25%; }
					.create-campaign .ui-widget-content { height:7px; background-color:#999; border-radius:5px; }
					.create-campaign .ui-slider-horizontal .ui-slider-handle { width:26px; height:26px; border:1px solid #dedede; box-shadow:0 0 5px rgba(0, 0, 0, 0.4); border-radius:50%; background:#fff none; top:-10px; margin-left:-9px; cursor:e-resize; }
					.create-campaign .ui-slider .ui-slider-range { height:7px; background-color:#99cc00; border-radius:5px; }
					.create-campaign .ui-slider.norange .ui-slider-range + .ui-slider-handle { display:none; }
					.create-campaign .tab-data .form .row [class^="col-"] { margin-bottom:10px; }					
				.create-campaign .tab-data .right-panel { padding:0 20px; }
					.create-campaign .tab-data .right-panel h2 { font-size:1.14em; text-align:left; padding:0 20px; }
					.create-campaign .tab-data .right-panel h3 { font-size:1em; text-align:left; padding:0 20px; }
					.campaign-preview-box { min-height:480px; box-shadow:0 0 32px rgba(0, 0, 0, 0.2); display:block; margin:20px 20px 40px 20px; position:relative; }
						.campaign-preview-box .data { padding:60px 20px 0 20px; display:block; text-align:center; }
							.campaign-preview-box .data .category-name { display:block; color:#99cc00; margin:0 0 5px 0; }
							.campaign-preview-box .data .category-title { display:block; font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.14em; margin:0 0 10px 0; }
							.campaign-preview-box .data .category-desc { display:block; color:#686868; margin:0 0 20px 0; }
							.campaign-preview-box .data .user-info { display:block; font-weight:300; font-size:0.92em; }
								.campaign-preview-box .data .user-info img { width:38px; height:38px; border-radius:50%; float:left; }
								.campaign-preview-box .data .user-info .content { margin:0 0 0 58px; display:block; text-align:left; }
							.campaign-preview-box .data .category-meta { position:absolute; bottom:0; left:0; right:0; border-top:1px solid #ebebeb; padding:10px 0 20px 0; }
								.campaign-preview-box .data .category-meta > span { float:left; width:33.333333%; font-size:0.92em; color:#686868; }
									.campaign-preview-box .data .category-meta > span b { display:block; font-weight:700; font-size:1.38em; }
						.create-campaign .tab-data .form .pictures { margin:0 -5px; }
						.create-campaign .tab-data .form .pictures:before, .create-campaign .tab-data .form .pictures:after { content:""; display:table; }
						.create-campaign .tab-data .form .pictures:after { clear:both; }
							.create-campaign .tab-data .form .pictures .pic-box { float:left; padding:0 5px 20px 5px; min-height:165px; }
								.create-campaign .tab-data .form .pictures .pic-box figure { width:200px; position:relative; }
								.create-campaign .tab-data .form .pictures .pic-box video { width:200px; position:relative; }
									.create-campaign .tab-data .form .pictures .pic-box figure img { width:100%; }
									.create-campaign .tab-data .form .pictures .pic-box figure a { width:25px; height:25px; line-height:23px; text-align:center; background-color:#99cc00; position:absolute; top:0; right:0; color:#fff; z-index:2; font-size:1em; }
									.create-campaign .tab-data .form .pictures .pic-box figure a:hover { background-color:#333; }
								.create-campaign .tab-data .form .pictures .pic-box .select { margin:5px 0 0 0; font-size:0.8125em; }
									.create-campaign .tab-data .form .pictures .pic-box .select .fancy-checkbox.sm + label:before { top:-1px; }
									.create-campaign .tab-data .form .pictures .pic-box .select label { color:#454545; }
									.create-campaign .tab-data .form .more-videos i { margin-right:10px; }						
					.create-campaign .tab-data .right-panel > p { margin:0 0 20px 0; }
						.create-campaign .tab-data .right-panel .support-btn { display:block; padding:20px 0 20px 0; background-color:#66a2e4; color:#fff; font-weight:300; font-size:1.07em; display:table; }
						.create-campaign .tab-data .right-panel .support-btn.blue { background-color:#3383db; }
						.create-campaign .tab-data .right-panel .support-btn.dark-blue { background-color:#1973d6; }
						.create-campaign .tab-data .right-panel .support-btn:hover { text-decoration:none; }
							.create-campaign .tab-data .right-panel .support-btn .left { display:table-cell; width:25%; text-align:center; vertical-align:middle; font-size:2.4em; }
							.create-campaign .tab-data .right-panel .support-btn .right { display:table-cell; width:75%; padding:0 10px 0 0; }
								.create-campaign .tab-data .right-panel .support-btn .header { font-size:1.6em; margin:0 0 10px 0; display:block; }
								.create-campaign .tab-data .right-panel .support-btn .content { display:block; }
	/* /create campaign page */
	
	/* create capaign preview */
	.create-campaign-preview { padding:60px 0 40px 0; background-color:#f5f5f5; }
		.create-campaign-preview .header h2 { font-size:2.57em; text-align:center; text-transform:none; }
		.create-campaign-preview .header h3 { font-weight:300; font-size:1.28em; text-align:center; }	
		.create-campaign-preview .main-box { background-color:#fff; padding:50px 40px; margin:40px 0 0 0; }
	/* /create campaign preview */
	/* dashboard (campaign creator) */
	.campaign-creator {}	
		.dashboard .left-panel { background-color:#fff; }
			.dashboard .left-panel h2 { color:#fff; background-color:#99cc00; font-family:'Montserrat', sans-serif; font-weight:700; font-size:1.28em; padding:15px 20px; }
			.dashboard .left-panel .box { padding:15px 20px; border-bottom:1px solid #ddd; font-weight:300; }
				.dashboard .left-panel .box h3 { font-weight:700; font-size:1.07em; text-transform:uppercase; line-height:1.2; }
				.dashboard .left-panel .box h3:before, .dashboard .left-panel .box h3:after { content:""; display:table; }
				.dashboard .left-panel .box h3:after { clear:both; }
					.dashboard .left-panel .box h3 span { float:left; }
					.dashboard .left-panel .box h3 .badge { float:right; background-color:transparent; color:#454545; font-weight:300; font-size:1.6em; font-weight:normal; padding:5px 0 0 0; }
					.dashboard .left-panel .box ul, .dashboard .left-panel .box li { margin:0; padding:0; list-style:none; }
					.dashboard .left-panel .box li:before, .dashboard .left-panel .box li:after { content:""; display:table; }
					.dashboard .left-panel .box li:after { clear:both; }
					.dashboard .left-panel .box ul { margin:20px 0 0; }
						.dashboard .left-panel .box li { margin:0 0 5px 0; }
						.dashboard .left-panel .box li:last-child { margin:0; }
								.dashboard .left-panel .box li label { font-weight:normal; float:left; min-width:125px; }
								.dashboard .left-panel .box li span { float:left; }
								.dashboard .left-panel .box li a { float:right; color:#4460b0; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns { display:block; }
			.dashboard .tab-area .tab-data .campaigns .campaign-box { margin:0 0 10px 0; background-color:#fff; display:table; }
			.dashboard .tab-area .tab-data .campaigns .campaign-box figure { display:table-cell; }
			.dashboard .tab-area .tab-data .campaigns .campaign-box .contents { display:table-cell; vertical-align:top; padding:10px 0 0; }
			.dashboard .tab-area .tab-data .campaigns .campaign-box .actions { display:table-cell; border-left:1px solid #dedede; text-align:center; vertical-align:top; }
			.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box figure {  width:18%; }
			.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents { width:60%; }
			.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .actions { width:22%; }
			.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .actions { padding:24px 0 0 0; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one { display:table-cell; padding:0 20px; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-two { display:table-cell; padding:0 15px; text-align:center; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-three { display:table-cell; padding:0 15px; text-align:center; }
				.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:60%; }
				.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:20%; }
				.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:20%; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a { display:inline-block; width:35px; height:35px; line-height:32px; text-align:center; border:1px solid #333; border-radius:50%; font-size:1.28em; color:#99cc00; border-color:#99cc00; margin:5px 3px 0 3px; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a.edit { color:#99cc00; border-color:#99cc00; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a.edit:hover { color:#fff; background-color:#99cc00; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a.update { color:#0064d2; border-color:#0064d2; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a.update:hover { color:#fff; background-color:#0064d2; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a.turn-off { color:#ffcc00; border-color:#ffcc00; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a.turn-off:hover { color:#fff; background-color:#ffcc00; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a.delete { color:#ff2020; border-color:#ff2020; }
				.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a.delete:hover { color:#fff; background-color:#ff2020; }
			.dashboard.campaign-creator .tab-area .tab-data .campaigns .create-campaign-box { margin:30px 0 0 0; text-align:center; }				
			.tooltip-inner { background-color:#7b7b7b; }
			.tooltip.top .tooltip-arrow { border-top-color:#7b7b7b; }
			.tooltip.bottom .tooltip-arrow { border-top-color:#7b7b7b; }
			.tooltip.right .tooltip-arrow { border-top-color:#7b7b7b; }
			.tooltip.left .tooltip-arrow { border-top-color:#7b7b7b; }
	/* /dashboard (campaign creator) */
	/* icons */
	[class^="icon-"] { display:inline-block; display:inline-block; background:transparent url(../images/sprites.png) 0 0 no-repeat; }
	.icon-bullhorn { width:54px; height:42px; }
	.icon-bag-heart { width:31px; height:42px; background-position:-70px 0px; }
	.icon-create-campaign { width:111px; height:95px; background-position:0px -70px; }
	.icon-share-friends { width:75px; height:95px; background-position:-130px -70px; }
	.icon-accept-donations { width:71px; height:102px; background-position:-220px -70px; }
	.icon-more-arrow { width:23px; height:21px; background-position:-120px 0px; }
	.icon-check { width:23px; height:17px; background-position:-160px 0px; }
	.icon-ccv { width:62px; height:44px; background-position:-200px 0px; }
	.icon-bbb { width:156px; height:75px; background-position:0 -180px; }
	.icon-close-x-lg { width:25px; height:25px; background-position:-160px -30px; }
	.icon-close-x-lg-white { width:23px; height:23px; background-position:-130px -30px; }
	.icon-login-user { width:46px; height:62px; background-position:-280px 0px; }
	.icon-arrow-green-sm { width:15px; height:10px; background-position:-300px -70px; }
	.icon-plus-green { width:16px; height:16px; background-position:-300px -90px; }
	.icon-paypal-sm { width:59px; height:16px; background-position:-170px -180px; }
	.icon-payoneer-sm { width:49px; height:19px; background-position:-170px -200px; }
	.icon-mobile-green { width:34px; height:60px; background-position:0 -270px; }
	.icon-envelope-green { width:67px; height:47px; background-position:-50px -270px; }
	.icon-map-marker-green { width:49px; height:60px; background-position:-130px -270px; }
	.icon-long-arrow-up { width:16px; height:27px; background-position:-300px -120px; }
	.icon-mefarnes-logo-sm { width:36px; height:44px; background-position:0px -366px; }
	/* /icons */
	
	/* extras */
	.text-warning { color:#fc6c06; }
	.mt10 { margin-top:10px !important; }
	.mt30 { margin-top:30px !important; }
	.mt40 { margin-top:40px !important; }
	.mb10 { margin-bottom:10px !important; }
	.pt15 { padding-top:15px; }
	/* /extras */
	
	/* footer */
	.footer-top { background-color:#0064d2; color:#fff; padding:40px 0 50px 0; text-align:center; }
		.footer-top h2 { font-weight:300; font-size:3em; margin:0 0 30px 0; text-transform:none; }
		.footer-top h4 { font-weight:300; font-size:1.43em; }
		.footer-top .buttons { margin:40px 0 0 0; }
			.big-button { border-radius:28px; position:relative; overflow:hidden; display:inline-block; margin:0 10px; }
				.big-button span { display:inline-block; padding:0 10px 0 30px; min-height:55px; background-color:#99cc00; color:#fff; font-size:1.28em; border-top:1px solid #defc82; border-bottom:2px solid #67b400; border-radius:28px; text-shadow:1px 1px 0px #67b400; }
					.big-button span * { position:relative; z-index:2; }
					.big-button span i { font-weight:normal; display:inline-block; padding:18px 20px 16px 0; position:relative; float:left; }
					.big-button span i:after { content:""; position:absolute; right:0; top:2px; bottom:0; width:1px; background:linear-gradient(rgba(148, 209, 0, .4) 5%, rgba(148, 209, 0, 1), rgba(148, 209, 0, .4) 95%); }
					.big-button span b { font-weight:700; display:inline-block; padding:18px 30px 18px 30px; position:relative; }
					.big-button span b:before { content:""; position:absolute; left:0; top:2px; bottom:0; width:1px; background:linear-gradient(rgba(103, 180, 0, .4) 5%, rgba(103, 180, 0, 1), rgba(103, 180, 0, .4) 95%); }
			.big-button:hover { text-decoration:none; }
				.big-button span:after  { content:""; position:absolute; left:0; right:0; bottom:0; height:0px; background-color:#67b400; transition:height linear 0.2s; z-index:1; }
				.big-button span:hover:after { height:100%; }
			
			.big-button.facebook span { background-color:#3258a6; border-top-color:#7699f3; border-bottom-color:#173689; text-shadow:2px 2px 0px #173689; }
			.big-button.facebook span:after { background-color:#173689; }
			.big-button.facebook span i:after { background:linear-gradient(rgba(118, 153, 243, .4) 5%, rgba(118, 153, 243, 1), rgba(118, 153, 243, .4) 95%); }
			.big-button.facebook span b:before { background:linear-gradient(rgba(23, 54, 137, .4) 5%, rgba(23, 54, 137, 1), rgba(23, 54, 137, .4) 95%); }
			
			.big-button.support span { background-color:#fc6c06; border-top-color:#f9b27f; border-bottom-color:#ce5602; text-shadow:2px 2px 0px #ce5602; }
			.big-button.support span:after { background-color:#ce5602; }
			.big-button.support span i:after { background:linear-gradient(rgba(252, 94, 5, .4) 5%, rgba(250, 59, 3, 1), rgba(252, 94, 5, .4) 95%); }
			.big-button.support span b:before { background:linear-gradient(rgba(245, 81, 5, .4) 5%, rgba(237, 54, 3, 1), rgba(245, 81, 5, .4) 95%); }
				.big-button.support .icon-mefarnes-logo-sm { margin:0 0 0 -10px; border-radius:10px 0 0 10px; }
	footer { background-color:#24292e; color:#838283; }
		footer a { color:#838283; }
		footer .top { padding:50px 0 40px 0; font-size:0.93em; }
			footer .top h3 { color:#99cc00; font-size:1.38em; text-transform:uppercase; margin:0 0 25px 0; }
			footer .top .links { margin:0; padding:0; list-style:none; float:left; width:50%; }
				footer .top .links li { margin:0 0 5px 0; }
				footer .top .links li:before { content:"\f101"; font-family:FontAwesome; color:#99cc00; margin:0 5px 0 0; }
				footer .top .links li a {}
				footer .top .links li:hover a { text-decoration:none; }
			footer .top .contact {}
				footer .top .contact p { line-height:150%; }
				footer .top .contact p:before, footer .top .contact p:after { content:""; display:table; }
				footer .top .contact p:after { clear:both; }
					footer .top .contact .fa { color:#99cc00; min-width:20px; margin:4px 0 0 0; float:left; }
					footer .top .contact span { float:left; }
			footer .top .newsletter-form { margin:10px 0 0 0; position:relative; }
				footer .top .newsletter-form .cover { display:block; margin:0 160px 0 0; }
					footer .top .newsletter-form input[type="email"] { border:1px solid #838283; border-radius:3px; background-color:transparent; padding:10px 10px; width:100%; color:#838283; }
				footer .top .newsletter-form button[type="submit"] { background-color:#99cc00; min-width:150px; text-align:center; border:0; border-top:1px solid #defc82; border-radius:3px; color:#fff; padding:10px 20px; position:absolute; overflow:hidden; top:0; right:0; }
					footer .top .newsletter-form button[type="submit"] span { position:relative; z-index:2; }
				footer .top .newsletter-form button[type="submit"]:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#67b300; transition:height linear 0.2s; z-index:1; }
				footer .top .newsletter-form button[type="submit"]:hover:after { height:100%; }			
				footer .top .newsletter-form ::-webkit-input-placeholder { color:#838283; font-weight:400; font-size:0.93em; opacity:1; }
				footer .top .newsletter-form :-ms-input-placeholder { color:#838283; font-weight:400; font-size:0.93em; opacity:1; }
				footer .top .newsletter-form ::-moz-placeholder { color:#838283; font-weight:400; font-size:0.93em; opacity:1; }		
			footer .top .social { margin:35px 0 0 0; }
				footer .top .social a { display:inline-block; width:40px; height:40px; line-height:36px; text-align:center; border-radius:50%; border:1px solid #838283; font-size:1.38em; margin:0 15px 0 0; }
				footer .top .social a:hover { border-color:#99cc00;}		
		footer .bottom { padding:40px 0; border-top:1px solid #494d51; background:transparent url(../images/logo-footer.png) center center no-repeat; }
			footer .bottom .links { text-align:right; }
				footer .bottom .links a { margin:0 15px 0 15px; }
				footer .bottom .links a:last-child { margin-right:0; }
	/* /footer */
	
	@media screen and (max-width: 480px) {
		.create-campaign .tab-data .form .days-container { float:none; width:100%; text-align:center; margin:0 0 15px 0; }
		.create-campaign .tab-data .form .slider-bar { float:none; width:100%; }
		.create-campaign .tab-data .form .slider-bar b { width:20%; }
		.create-campaign .tab-data .form .slider-bar #day-slider { width:60%; }
		.dashboard .tab-area .tabs li { width:50%; }
		.dashboard .tab-area .tabs li a { width:100%; }
		.dashboard .tab-area .tabs li a span { padding:0; }
		.withdraw .form .form-row .available input[type="text"] { width:60%; }
		.withdraw .form .form-row .available button { width:39%; }
		.withdraw .radios span { width:60%; }
		.withdraw .radios span + span { width:40%; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata { font-size:0.92em; margin-left:10px; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight { font-size:1.5em; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight small { font-size:0.6em; }
		.edit-campaign .form .days-container { float:none; width:100%; text-align:center; margin:0 0 15px 0; }
		.edit-campaign .form .slider-bar { float:none; width:100%; }
		.edit-campaign .form .slider-bar b { width:20%; }
		.edit-campaign .form .slider-bar #day-slider { width:60%; }
		.donate.organization .form .radios span { width:100%; margin:0 0 10px 0; }
	}
	
	@media screen and (max-width: 360px) {
		.main-slider .one .contents { top:20px; }
		.main-slider .one .contents h2 { font-size:1.5em; }
		.main-slider .one .contents h4 { font-size:1em; }
		.main-slider .one .contents .buttons { display:none; }
		.main-slider .flex-control-nav { bottom:8px; }
		.profile-banner .content .data .location { float:none; }
		.profile-banner .content .data .time-stamp { float:none; display:block; margin:0; }
		.profile-banner .content .data .right .shares { margin-left:30px; font-size:1.40em; }
		.profile-banner .content .data .right .likes { font-size:1.40em; }
		.tab-area .tabs li a { font-size:1em; text-align:center; }
		.tab-area .tabs li a span { padding:20px 20px 10px 20px; min-height:70px; }
		.tab-area .tabs li a br { display:inline; }
		.donation-box .content { margin:10px 0 0 0; float:left;  }
		.profile-right .buttons .fb-share-button { padding:15px 5px; }
		.profile-right .buttons .follow-tweets-button { padding:15px 5px; }
		.campaign-category ul.nav > li { width: 100%; float: none; }
		.withdraw .form .form-row .available input[type="text"] { width:100%; }
		.withdraw .form .form-row .available button { width:100%; margin:5px 0 0 0; }
		.popup.payment-methods .content-box { margin:70px 0 0; }
		.withdraw .radios span { width:100%; }
		.withdraw .radios span + span { margin-top:10px; }
		.organization-profile .tab-area .tabs { margin:0; }
	}
	
	@media screen and (max-width: 320px) {
		.tab-area .tabs li a span { padding:20px 15px 10px 15px; min-height:72px; }
		.profile-right .buttons .fb-share-button { font-size:1.1em; }
		.profile-right .buttons .follow-tweets-button { font-size:1.1em; }
		.create-campaign .tab-area .tabs li { padding:0 5px 10px 5px; font-size:1em; }
		.edit-campaign .form input.date-time-input[type="text"] { width:220px; }
		.edit-campaign .form .ui-datepicker-trigger { margin-top:0; }
		.big-button span { font-size:1.14em; }
	}
	
	@media screen and (min-width:361px) and (max-width: 480px) {
		.main-slider .one .contents { top:30px; }
		.main-slider .one .contents h2 { font-size:2.5em; }
		.main-slider .one .contents h4 { font-size:1.5em; }
		.main-slider .one .contents .buttons { display:none; }
		.main-slider .flex-control-nav { bottom:10px; }
		.tab-area .tabs li a span { padding:20px 20px 10px 20px; }
	}
	
	@media screen and (min-width:481px) and (max-width: 640px) {
		.main-slider .one .contents { top:40px; }
		.main-slider .one .contents h2 { font-size:3em; }
		.main-slider .one .contents h4 { font-size:2em; }
		.main-slider .one .contents .buttons { display:none; }
		.main-slider .flex-control-nav { bottom:20px; }
	}
	
	/* extra small device (xs)*/
	@media screen and (max-width: 767px) {
		body { padding:0 5px; }
		.container { padding:0 10px; }
		.row { margin:0 -10px; }
		[class^="col-"] { padding:0 10px; }
		.navbar-brand { max-width:70%; }
		#navbar { float:none; text-align:center; background-color:#67b300; }
		.navbar-default .navbar-nav > li > a { color:#fff; }
		.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus { background-color:#99cc00; color:#fff; }
		.navbar-nav { margin:0 -15px; }
		.navbar-default .navbar-nav > li.campaign > a { border-radius:0; }
		.navbar-default .navbar-nav > li.campaign > a:after { border-radius:0; }
		.navbar-default .navbar-nav .dropdown.open { background-color:#99cc00; }
		.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover { color:#fff; }
		.navbar-default .navbar-nav .dropdown .char { box-shadow:0 0 3px rgba(0, 0, 0, 0.2); }
		.navbar-default .navbar-nav .dropdown.open .dropdown-menu { padding:5px 0; }
		.navbar-default .navbar-nav .dropdown.open .dropdown-menu > li > a { color:#fff; text-align:center; padding:10px 20px; }
		.navbar-default .navbar-nav .dropdown.open .dropdown-menu > li > a:hover, .navbar-default .navbar-nav .dropdown.open .dropdown-menu > li > a:focus { background-color:#67b300; color:#fff; }	
		.main-slider { margin:0 -5px; }
		.featured-campaigns .box { margin:0 0 20px 0; }
		.featured-campaigns .box .contents {}
		.featured-campaigns .campaign-scroller li { float:none; width:100%; margin:0 0 20px 0; }	
		.footer-top { margin:0 -5px; padding-left:5px; padding-right:5px; }
		.footer-top h2 { font-size:2em; }
		.footer-top .buttons a { margin-bottom:5px; }
		.big-button span i { padding:18px 10px 16px 0; }
		.big-button span b { padding:18px 20px 18px 20px; }	
		footer { margin:0 -5px; }
		footer .top .links { margin:0 0 20px 0; }
		footer .top .contact { margin:0 0 20px 0; }
		footer .top .newsletter-form { text-align:center; }
		footer .top .newsletter-form .cover { margin:0 0 5px 0; }
		footer .top .newsletter-form button[type="submit"] { position:relative; margin:0 auto; width:100%; }
		footer .bottom { padding-bottom:80px; background-position:center bottom; }
		footer .bottom .links { text-align:left; margin:10px 0 0 0; }
		footer .bottom .links a { margin:0 5px; }
		footer .bottom .links a:first-child { margin-left:0; }
		.fund-campaigns .campaign-search .input-box, .fund-campaigns .campaign-search .select-box { float:none; width:100%; margin:0 0 -1px 0; }
		.fund-campaigns .campaign-search button[type="submit"] { float:none; margin:5px auto; display:block; width:100%; }
		.profile-banner .content .data { margin:0 0 0 70px; }
		.profile-banner .content .data .left { width:100%; }
		.profile-banner .content .data .right { width:100%; }
		.profile-right { margin:20px 0 0 0; }
		.profile-right .metadata .highlight { font-size:2.4em; }
		.profile .tab-area .tab-data .tab-contents .footer .pull-right { margin-top:10px; }
		.profile .tab-area .tabs li { width:50%; }
		.tab-area .tab-data .tab-contents .footer .button { margin-top:5px; }
		.tab-area .tab-data #tab2 article img { margin-bottom:20px; }
		.profile .tab-area .tabs { display:none; }
		.tab-data .tab-contents { padding: 20px 0 40px 0; }
		.tab-btn-xs { display:block; }
		.profile #tab2 .pic-slider .carousel-cell { width:100%; }
		.profile #tab2 .pic-slider .flickity-prev-next-button { width:40px; }
		.donate-banner-right > h3 { margin-top:10px; }
		.donate-banner-right .profile-data { width:100%; }
		.donate-banner-right .metadata { margin:20px 0 0 0; width:100%; }
		.donate-banner-right .percent-circle .c100 { margin:0 5px -60px 0; top:-60px; }
		.donate .tab-area .tabs li { width:100%; margin-bottom:40px; }
		.donate .tab-area .tabs li:last-child { margin-bottom:0; }
		.donate .tab-area .tab-data .tab-contents .buttons { text-align:center; }
		.donate .tab-area .tab-data .tab-contents .text-right { text-align:center; }
		.donate .tab-area .tab-data .tab-contents .form { margin:20px 0 0 0; }
		.donate .tab-area .tab-data #tab3 .ccv { right:0px; }
		.signup-box { display:block; }
		.signup-box .col { padding:20px 5px 60px; display:block; width:100%; }
		.signup-box .col:last-child { padding:60px 5px 20px; border-left:0; border-top:1px solid #dedede; }
		.signup-form-box { width:98%; }
		.signup-form-box { display:block; height:auto; }
		.signup-form-box .col-left { display:block; width:100%; border-radius:5px 5px 0 0; }
		.signup-form-box .col-right { display:block; width:100%; padding:20px 10px; }
		.signup .section_organization_2 .radios span { width:50%; margin:0 0 20px 0; }
		.create-campaign .tab-data .form .radios span { width:50%; }
		.container .jumbotron { padding:10px 10px; }
		.create-campaign .tab-area .tabs li { width:33.333333%; padding-bottom:10px; }
		.create-campaign .tab-area .tab-data { margin:30px 10px 0; padding:20px 20px; }
		.create-campaign .tab-data .step-1 .form .one-third .sbOptions li { width:100%; float:none; }
		.create-campaign .tab-data .form .buttons .button { margin-bottom:10px; }
		.create-campaign .tab-data .right-panel { padding:0; }
		.create-campaign .tab-data .right-panel h2 { padding:0; }
		.create-campaign .tab-data .right-panel h3 { padding:0; }
		.campaign-preview-box { margin:20px auto; max-width:250px; position:relative; }
		.campaign-preview-box .data { padding:30px 20px 0 20px; }
		.dashboard .dashboard-header { text-align:center; padding-top:20px; }
		.dashboard .dashboard-header .right { text-align:center; float:none; }
		.dashboard .dashboard-header .right span:first-child { margin:0; }
		.dashboard .dashboard-header .right span a:last-child { margin-right:0; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box { width:250px; margin:0 auto 10px auto; padding-bottom:10px; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box figure { display:block; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents { display:block; padding:0; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .share { display:block; border-left:0; border-top:1px solid #dedede; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box figure { width:100%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share { width:100%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share .actions { border:0; display:block; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; width:100%; text-align:left; border-top:1px solid #dedede; padding:10px 10px; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents h3 { border-bottom:1px solid #dedede; padding:0 10px 10px 10px; margin:0 -10px 5px -10px; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box figure { width:100%; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .actions { width:100%; display:block; border-left:0; border-top:1px solid #dedede; padding:10px 0 0 0; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; width:100%; text-align:left; border-top:1px solid #dedede; padding:10px 10px; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
		.dashboard.campaign-creator .tab-area .tabs li { width:100%; padding-top:20px; }
		.dashboard.campaign-creator .tab-area .tabs li:first-child { padding-top:0px; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box figure { width:100%; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .actions { width:100%; display:block; border-left:0; border-top:1px solid #dedede; padding:10px 0 0 0; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; width:100%; text-align:left; border-top:1px solid #dedede; padding:10px 10px; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
		.dashboard.organization .tab-area .tabs li a { width:100%; }
		.dashboard.organization .tab-area .tabs li { width:100%; padding-top:20px; }
		.dashboard.organization .tab-area .tabs li:first-child { padding-top:0px; }
		.dashboard .tab-area .tab-data .campaigns .create-campaign-box h3 i { display:block; margin:0 0 20px 0; text-align:center; }
		.dashboard .tab-area .tab-data { overflow:hidden; }
		.dashboard .tab-area .tab-data .updates .updates-group .date { margin-bottom:20px; }
		.dashboard .tab-area .tab-data .updates .updates-box { display:block; }
		.dashboard .tab-area .tab-data .updates .updates-box .col-one { display:block; width:100%; text-align:center; }
		.dashboard .tab-area .tab-data .updates .updates-box .col-two { display:block; width:100%; text-align:center; }
		.dashboard .tab-area .tab-data .updates .updates-box .col-three { display:block; width:100%; text-align:center; }
		.dashboard .tab-area .tab-data .updates .updates-box .col-four { display:block; position:absolute; top:10px; right:10px; }
		.dashboard .tab-area .tab-data .updates .paging { text-align:center; }
		.dashboard .dashboard-header figure { position:relative; top:0; margin:0 auto; }
		.dashboard .dashboard-header figure + .content { margin:10px 0 0 0; }
		.dashboard .left-panel { margin-bottom:30px; }
		.dashboard.account-settings .inner-tabs { padding-bottom:0; }
		.dashboard.account-settings .inner-tabs li { margin:0 0 30px 0; width:100%; }
		.account-settings .friend-box figure { margin:0 auto 20px auto; float:none; }
		.account-settings .friend-box .contents { float:none; max-width:100%; text-align:center; margin:0 0 20px 0; }
		.account-settings .friend-box .dropdown { float:none; margin:0 auto; }
		.account-settings .discover-campaign-box { padding:30px 10px; }
		.account-settings .discover-campaign-box .icon { width:100%; text-align:center; }
		.account-settings .discover-campaign-box .contents { width:100%; text-align:center; }
		.account-settings .discover-campaign-box .buttons { width:100%; text-align:center; margin:5px 0 0 0; }
		.account-settings .tab-data .tab-contents.account .right-panel { margin-top:30px; }
		.dashboard.account-settings .tab-data { padding:20px 10px; }
		.buttons { text-align:center; }
		.button { min-width:220px; padding:10px 15px; margin-bottom:5px; }
		.row-eq-height { display: block; }
		div.search-campaigns .campaign-search .input-box { margin: 0 0 5px 0; width: 100%; }
		div.search-campaigns .campaign-search .select-box { margin: 0 0 5px 0; width: 100%; }
		div.search-campaigns .campaign-search .list-inline { margin-left: 0; }
		div.search-campaigns .campaign-search .list-inline > li { display: block; }
		div.search-campaigns .campaign-search { margin-top: 30px; }
		.btn-gridview { width: 100%; }
		.campaign-category .nav > li { width: 50%; float: left; }
		.campaign-footer p, .campaign-footer nav { width:100%; text-align:center; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box { max-width:270px; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box > img { display:block; float:none; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box	.data { margin:0; text-align:center; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box .category-meta { position:static; width:100%; border:0; border-top:1px solid #ccc; margin:10px 0 0 0; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box .percent-circle { position:static; margin:-55px auto 0 auto; }
		.search-campaigns .campaign-boxes .box .data .user-info { margin:10px 0 0 0; }
		.search-campaigns .campaign-boxes.listview .box .category-meta > span { float:left; width:33.333333%; }
		.search-organization .campaign-search .input-box { margin: 0 0 5px 0; width: 100%; }
		.search-organization .campaign-search .select-box { margin: 0 0 5px 0; width: 100%; }
		.search-organization .campaign-search .list-inline { margin-left: 0; }
		.search-organization .campaign-search .list-inline > li { display: block; }
		.btn-gridview { width: 100%; }
		.search-organization .campaign-search { margin-top: 30px; }
		.search-organization .fund-campaigns .campaign-boxes.listview .box { max-width:270px; }
		.search-organization .fund-campaigns .campaign-boxes.listview .box > img { display:block; float:none; }
		.search-organization .fund-campaigns .campaign-boxes.listview .box	.data { margin:0; text-align:center; }
		.search-organization .fund-campaigns .campaign-boxes.listview .box .category-meta { position:static; width:100%; border:0; border-top:1px solid #ccc; margin:20px 0 0 0; }
		.search-organization .campaign-boxes.listview .box .category-meta > span { float:left; width:50%; padding:10px 0 10px 0; }
		.popup { width:98%; left:1%; margin:0 !important; }
		.popup h2 { padding:15px 30px 15px 15px; }
		.popup.edit-picture .left-panel { width:100%; float:none; padding:0; }
		.popup.edit-picture .right-panel { width:100%; float:none; margin-top:30px; border:0; padding:0; }
		.how-it-works-page .step-box.one:after, .how-it-works-page .step-box.two:after, .how-it-works-page .step-box.three:after  { -webkit-transform:rotate(45deg); -moz-transform:rotate(45deg); -ms-transform:rotate(45deg); -o-transform:rotate(45deg); transform:rotate(45deg); background-size:76%; height:190px; }
		.how-it-works-page .step-box .content { margin-bottom:20px; }
		.how-it-works-page .step-box figure { margin-bottom:20px; }
		.create-campaign-preview .main-box { padding:40px 0; }
		.create-campaign-preview .profile-banner .content .data .right .likes { float:left; }
		.profile.create-campaign-preview .tab-area .tabs li a span { padding:20px 10px 10px 10px; min-height:50px; }
		.create-campaign-preview .donation-box .content { margin:5px 0 0 60px; padding:0 10px 0 0; float:none; }
		.withdraw .form .form-row [class^="col-"] + [class^="col-"] { margin-top:10px; }
		.withdraw .form-row .shift-next { margin:0; }
		.transaction-history .form .form-row label { display:block; margin:20px 0 0 0; }
		.transaction-history .form .input-box { width:100%; margin:10px 0 0 0; }
		.transaction-history .form .form-row label + .input-box { margin:5px 0 0 0; }
		.transaction-history .form button { margin:10px 0 0 0; width:100%; }
		.transaction-history .form .links { margin:10px 0 0 0; width:100%; text-align:center; }
		.ui-datepicker-trigger { width:40px !important; margin:5px 0 0 0 !important; }
		.grid-header { display:none; }
		.grid-row b { display:block; }
		.datagrid [class^="col-"] { display:block; width:100% !important; text-align:left !important; }
		.edit-campaign .left-panel .buttons { margin-bottom:-100px; }
		.edit-campaign .right-panel { padding:0; }
		.edit-campaign .form .one-third .sbOptions li { width:100%; float:none; }
		.contact-us .contacts [class^="col-"] { display:block; text-align:center; width:100%; margin:0 0 40px 0 }
		.contact-us .contacts [class^="col-"] span { display:block; float:none; margin:0 !important; }
		.scroll-up { right:5px; bottom:5px; }
		.profile-org .tab-panel .tab-header .tabs { display:none; }
		.profile-org .tab-panel .tab-header .buttons { float:none; text-align:center; }
		.profile-org .tab-header-xs { display:block; }
		.profile-org .campaign-scroller .campaigns-slider { width:270px; margin:0 auto; }
		.profile-org .campaign-scroller .campaigns-slider .slides li { width:100%; }
		.profile-org .our-network .box { margin-bottom:30px; }
		.profile-org .contact-us .box { padding:0; margin-bottom:30px; }
		.carousel-cell { width:100%; }
		.carousel-cell .content { padding:10px 15px; font-size:1em; }
		
	}
	/* /extra small device (xs)*/
	
	/* small device (sm) */
	@media screen and (min-width: 768px) and (max-width: 991px) {
		.navbar-header { max-width:20%; margin:10px 0 0 0; }
		.nav > li > a { padding:10px 5px; }
		.navbar-default .navbar-nav > li.campaign { margin:0; }
		#navbar { margin:30px 0 0 0; }	
		.main-slider .one .contents { top:30px; }
		.main-slider .one .contents h2 { font-size:2.6em; }
		.main-slider .one .contents h4 { font-size:1.8em; }
		.banner-button .icon { left:-40px; }
		.banner-button.donate .icon { right:-40px; }
		.main-slider .flex-control-nav { bottom:25px; }
		.fund-campaigns .campaign-boxes .box .data { min-height:240px; }
		.fund-campaigns .campaign-boxes .box .category-meta { padding:0 10px; }
		.fund-campaigns .campaign-search .input-box { width:50%; }
		.fund-campaigns .campaign-search .select-box { width:30.8%; margin-top:-1px; }
		.fund-campaigns .campaign-search button[type="submit"] { width:48px; height:48px; margin:1px 2px 0 0; }	
		footer .top .contact { margin:0 0 20px 0; }
		footer .bottom .links a { margin:0 10px; }
		.donate-banner-right .profile-data { width:100%; }
		.donate-banner-right .metadata { margin-left:80px; }
		.donate-banner-right .metadata + .metadata + .metadata { margin-left:0; }
		.donate .tab-area .tab-data #tab3 .ccv { right:0px; }
		.signup-box .col { padding:100px 80px 100px 50px; }
		.signup-box .col:last-child { padding:100px 50px 100px 80px; }
		.signup-form-box { width:96%; }
		.create-campaign .tab-data .form .days-container { width:25%; }
		.create-campaign .tab-data .form .slider-bar { width:75%; }
		.create-campaign .tab-data .form .slider-bar b { width:25%; }
		.create-campaign .tab-data .form .slider-bar #day-slider { width:50%; }
		.create-campaign .tab-data .form .buttons .button { margin-bottom:10px; }
		.create-campaign .tab-data .right-panel { padding:0; }
		.create-campaign .tab-data .right-panel h2 { padding:0; }
		.create-campaign .tab-data .right-panel h3 { padding:0; }
		.campaign-preview-box { margin:20px 0; }
		.create-campaign .tab-area .tab-data { padding:20px 20px; }
		.dashboard .dashboard-header { padding-top:30px; }
		.dashboard .dashboard-header figure { width:130px; height:130px; top:-24px; }
		.dashboard .dashboard-header figure + .content { margin-left:150px; }
		.dashboard .dashboard-header h2 { margin-top:0px; }
		.dashboard .dashboard-header .right { margin:0; }
		.dashboard .dashboard-header .right span:first-child { margin:0; }
		.dashboard .dashboard-header .right span a:last-child { margin-right:0; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box { position:relative; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box:before, .dashboard .tab-area .tab-data .campaigns .campaign-box:after { content:""; display:table; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box:after { clear:both; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box figure { display:block; float:left; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box figure { width:35%; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents { display:block; float:left; padding:0; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents { width:65%; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .share { display:block; padding:10px 0 10px 0; border:0; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share { width:35%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share .actions { border:0; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .share:before, .dashboard .tab-area .tab-data .campaigns .campaign-box .share:after { content:""; display:table; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .share:after { clear:both; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .share p { float:left; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .share p:first-child { margin:0 10px 0 0; padding:10px 0 0 10px; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; float:left; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; margin:0 20px 10px 20px; border-bottom:1px solid #dedede; padding:10px 0; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:30%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:30%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-four { width:20%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-five { width:20%; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one h3 { margin-bottom:5px; }
		.dashboard .tab-area .tab-data .campaigns .create-campaign-box h3 { font-size:1.6em; }
		.dashboard.campaign-creator .tab-area .tabs li { width:33.333333%; }
		.dashboard.campaign-creator .tab-area .tabs li a { width:auto; font-size:1em; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box { display:block; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box figure { width:50%; display:block; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; display:block; padding:0 0 15px 0; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .actions { width:50%; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; display:block; margin:0 10px 10px 10px; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:50%; text-align:left; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:50%; text-align:left; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
		.dashboard.organization .tab-area .tabs li { width:30%; }
		.dashboard.organization .tab-area .tabs li:first-child { width:40%; }
		.dashboard.organization .tab-area .tabs li a { width:auto; font-size:1em; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box { display:block; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box figure { width:50%; display:block; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents { width:100%; display:block; padding:0 0 15px 0; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .actions { width:50%; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; display:block; margin:0 10px 10px 10px; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:50%; text-align:left; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:50%; text-align:left; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
		.dashboard.public-profile.organization .tab-area .tabs li { width:50%; }
		.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { margin:0; }
		.dashboard .left-panel .box { padding:15px 10px; }
		.dashboard.account-settings .tab-area .tabs li a { width:auto; padding:0 20px 20px 20px; }
		.account-settings .discover-campaign-box .contents { width:90%; padding-left:10px; }
		.account-settings .discover-campaign-box .buttons { width:100%; float: left; padding:12px 0 0 70px; text-align:left; }
		.dashboard.account-settings .tab-data { padding:20px 20px; }
		div.search-campaigns .campaign-search .input-box { margin:0 0 5px 0; width: 100%; }
		div.search-campaigns .campaign-search .select-box { margin:0 1% 5px 0; width: 49.5%; }
		div.search-campaigns .campaign-search .select-box.double { margin-right:0; }
		div.search-campaigns .campaign-search .list-inline { margin-left: 0; float:left; width:100%; }
		div.search-campaigns .campaign-search .list-inline > li { width:49.5%; margin:0 1% 0 0; display:block; float:left; }
		div.search-campaigns .campaign-search .list-inline > li:last-child { margin-right:0; }
		div.search-campaigns .campaign-search button[type="submit"] { width:100%; }
		div.search-campaigns .campaign-search .btn-gridview { width: 100%; }
		div.search-campaigns .campaign-boxes .box .data { min-height: 340px; }	
		.search-campaigns .fund-campaigns .campaign-category { margin-bottom:30px; }
		.search-campaigns .fund-campaigns .campaign-category.listview .nav > li { width:33.333333%; float:left; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box > img { width:240px; height:100%; position:absolute; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box .data { margin:0 120px 0 290px; padding:10px; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box .percent-circle { left:-100px; }	
		.search-organization .campaign-search .input-box { margin:0 0 5px 0; width: 100%; }
		.search-organization .campaign-search .select-box { margin:0 1% 5px 0; width: 49.5%; }
		.search-organization .campaign-search .select-box.double { margin-right:0; }
		.search-organization .campaign-search .list-inline { margin-left: 0; float:left; width:100%; }
		.search-organization .campaign-search .list-inline > li { width:49.5%; margin:0 1% 0 0; display:block; float:left; }
		.search-organization .campaign-search .list-inline > li:last-child { margin-right:0; }
		.search-organization .campaign-search button[type="submit"] { width:100%; }
		.search-organization .campaign-search .btn-gridview { width: 100%; }
		.search-organization .fund-campaigns .campaign-category { margin-bottom:30px; }
		.search-organization .fund-campaigns .campaign-category.listview .nav > li { width:33.333333%; float:left; }
		.search-organization .fund-campaigns .campaign-boxes.listview .box	.data { padding:10px; }
		.search-organization .fund-campaigns .campaign-boxes.listview .box > img { position:absolute; }
		.how-it-works-page .step-box.one:after, .how-it-works-page .step-box.two:after, .how-it-works-page .step-box.three:after  { background-size:45%; }
		.how-it-works-page .step-box .content { width:60%; }
		.how-it-works-page .step-box figure { width:40%; }
		.how-it-works-page .step-box .step { margin-top:0; }
		.how-it-works-page .step-box h2 { font-size:1.5em; }
		.create-campaign-preview .main-box { padding:20px 0; }
		.profile-banner .content .data { margin:0 0 0 70px; }
		.profile-banner .content .data .time-stamp { margin:10px 0 0 0px; }
		.profile-banner .content .data .right .shares { margin-top:0; }
		.profile .tab-area .tabs li a span { padding:15px 10px 15px 10px; font-size:0.92em; }
		.char { width:40px; height:40px; line-height:38px; }
		.donation-box .content { padding:0; margin:0 0 0 50px; }
		.donation-box .content .price-tag { font-size:2.0em; }
		.transaction-history .form .input-box { margin:0 10px 10px 0; width:220px; }
		.transaction-history .form .input-box.selectbox { margin-left:50px; }
		.transaction-history .form .form-row label { min-width:40px; }
		.transaction-history .form button { vertical-align:top; }
		.transaction-history .form .links { margin:10px 0 0 0; width:100%; text-align:center; }
		.popup.blue { width:98%; margin-left:1%; left:0; }
		.dashboard-header-sm h2 { margin-left:120px; }
		.edit-campaign .right-panel { padding:0; }
		.edit-campaign .form .one-third .sbOptions li { width:50%; }
		.button { margin-bottom:10px; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata { font-size:0.92em; margin-left:10px; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight { font-size:1.5em; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight small { font-size:0.6em; }
		.popup.edit-picture .left-panel { width:55%; }
		.popup.edit-picture .right-panel { width:45%; }
		.contact-us .contacts [class^="col-"] { text-align:center; vertical-align:top; }
		.contact-us .contacts [class^="col-"] span { display:block; float:none; margin:0 !important; min-height:70px; }
		.contact-us .contacts [class^="col-"] span + span { min-height:0; }
		.scroll-up { right:10px; bottom:10px; }
		.donate.organization .form .radios span { width:50%; }
		.profile-org .campaign-scroller .campaigns-slider .slides li { width:33.333333%; }
		.profile-org .our-network .box { padding:0 10px; }
		.profile-org .contact-us .box { padding:0; margin-bottom:20px; min-height:80px; }
		.carousel-cell { width:70%; }
		.carousel-cell .content { padding:10px 15px; font-size:1em; }
		.profile #tab2 .pic-slider .flickity-prev-next-button { width:50px; }
	}
	/* /small device (sm) */
	.profile { padding:30px 0 30px 0; }
		.profile-banner {}
			.profile-banner > figure {}
			.profile-banner .content { margin:20px 0 0 0; }
				.profile-banner .content figure { width:55px; height:55px; border:2px solid #ddd; border-radius:50%; overflow:hidden; float:left; }
					.profile-banner .content figure img { width:100%; height:100%; }
				.profile-banner .content .data { margin:0 0 0 90px; }
				.profile-banner .content .data:before, .profile-banner .content .data:after { content:""; display:table; }
				.profile-banner .content .data:after { clear:both; }
					.profile-banner .content .data .left { float:left; width:50%; }
					.profile-banner .content .data .right { float:right; width:50%; }
						.profile-banner .content .data h3 { font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.28em; }
							.profile-banner .content .data h3 a { color:#0064d2; font-size:0.78em; position:relative; top:-2px; left:5px; }
						.profile-banner .content .data .location { float:left; }
						.profile-banner .content .data .time-stamp { float:left; margin:0 0 0 25px; }
						.profile-banner .content .data .location i, .profile-banner .content .data .time-stamp i { color:#0064d2; margin:0 8px 0 0; }
						.profile-banner .content .data .right .likes { font-weight:300; font-size:1.71em; color:#0064d2; float:right; margin:15px 0 0 0; }
						.profile-banner .content .data .right .shares { font-weight:300; font-size:1.71em; color:#0064d2; float:right; margin:15px 10px 0 50px; }
							.profile-banner .content .data .right .shares small { font-size:0.66em; }
						.profile-banner .content .data .right .shares i, .profile-banner .content .data .right .likes i { margin:0 10px 0 0; }		
		.profile .tab-area { margin:30px 0 0 0; }
			.profile .tab-area .tabs { border-bottom:1px solid #dedede; }
			.tab-area .tabs:before, .tab-area .tabs:after { content:""; display:table; }
			.tab-area .tabs:after { clear:both; }
				.tab-area .tabs { margin:0; padding:0; list-style:none; }
					.tab-area .tabs li { float:left; position:relative; top:1px; }
						.profile .tab-area .tabs li a { color:#999; font-size:1.14em; background:transparent url(../images/tab-separator.png) right bottom no-repeat; text-transform:uppercase; display:inline-block; padding:0 4px 0 0; }
							.profile .tab-area .tabs li a span { border-bottom:1px solid transparent; padding:20px 35px 20px 35px; display:inline-block; }
							
							.profile .tab-area .tabs li a .badge { color:#fff; background-color:#99cc00; min-width:28px; margin:-2px 0 0 5px; }
						.profile .tab-area .tabs li a:hover, .profile .tab-area .tabs li a:focus, .profile .tab-area .tabs li.active a { text-decoration:none; color:#0064d2; }
							.profile .tab-area .tabs li a:hover span, .profile .tab-area .tabs li a:focus span, .profile .tab-area .tabs li.active a span { border-color:#0064d2; }
					.tab-btn-xs { border-bottom:1px solid #dedede; color:#999; font-size:1.14em; text-transform:uppercase; display:block; text-align:left; margin:0 0 2px 0; display:none; }
					.tab-btn-xs:hover, .tab-btn-xs:focus, .tab-btn-xs.activer { border-color:#0064d2; }
						.tab-btn-xs span { border-bottom:1px solid transparent; padding:10px 10px 10px 10px; display:block; background-color:#fafafa;  }
						.tab-btn-xs:hover, .tab-btn-xs:focus, .tab-btn-xs.active { text-decoration:none; color:#0064d2; }
						.tab-btn-xs .badge { color:#fff; background-color:#99cc00; min-width:28px; margin:-2px 0 0 5px; float:right; }
					
			.tab-area .tab-data {}
				 .tab-data .tab-contents { display:none; padding:40px 0; }
				 .profile .tab-area .tab-data #tab1 { display:block; }
				 	.tab-area .tab-data #tab1 h4 { font-weight:600; font-size:1.14em; margin:0 0 5px 0; text-align:center; }
					.tab-area .tab-data #tab1 h4 + p { text-align:center; }
					.tab-area .tab-data #tab1 .row { margin-top:20px; margin-bottom:20px; }
					.tab-area .tab-data #tab1 .footer .row { margin-top:0; margin-bottom:0; }
					.tab-area .tab-data #tab1 p { text-align:justify; margin-bottom:25px; }				
					.tab-area .tab-data #tab1 p.more { margin-bottom:10px; }
				 .profile #tab2 h4 { font-size:1.28em; font-weight:600; }
					.profile #tab2 .pic-slider { margin:0 -7px; transition:all linear 0.2s; }
						.profile #tab2 .pic-slider .carousel-cell { opacity:1; width:50%; padding:0 7px; }
						.profile #tab2 .pic-slider .flickity-prev-next-button { background-color: rgba(0, 0, 0, .4); box-shadow: none; opacity: 1; transition: all linear 0.2s; width: 80px; height: 100%; border-radius: 0; }
						.profile #tab2 .pic-slider .flickity-prev-next-button:hover:not(:disabled) { background-color: rgba(0, 0, 0, .6); }
						.profile #tab2 .pic-slider .flickity-prev-next-button.previous { left: 7px; }
						.profile #tab2 .pic-slider .flickity-prev-next-button.next { right: 7px; }
				 .tab-area .tab-data #tab3 article { border-top:1px solid #dedede; padding:30px 0 25px 0; }
				 .tab-area .tab-data #tab3 article:first-child { border:0; padding-top:0; }
				 .tab-area .tab-data #tab3 article:last-child { padding-bottom:0; }
					 .tab-area .tab-data #tab3 article h2 { font-weight:300; font-size:1.71em; border-left:5px solid #127dbe; padding:8px 0 8px 25px; font-weight:normal; margin:0 0 15px 0; }
					 .tab-area .tab-data #tab3 h4 { font-weight:600; font-size:1.14em; margin:0 0 5px 0; }
					 .tab-area .tab-data #tab3 p { text-align:justify; }
					 .tab-area .tab-data #tab3 .narrator { margin:20px 0 0 0; }
					 .tab-area .tab-data #tab3 .narrator:before, .tab-area .tab-data #tab3 .narrator:after { display:table; content:""; }
					 .tab-area .tab-data #tab3 .narrator:after { clear:both; }
					 	 .tab-area .tab-data #tab3 .narrator figure { width:60px; height:60px; border:2px solid #ddd; border-radius:50%; float:left; overflow:hidden; }
						 	 .tab-area .tab-data #tab3 .narrator figure img { width:100%; height:100%; }
						  .tab-area .tab-data #tab3 .narrator h3 { float:left; font-weight:300; font-size:1.71em; padding:12px 15px 12px 15px; }
						  	 .tab-area .tab-data #tab3 .narrator h3 a { color:#454545; }
							 .tab-area .tab-data #tab3 .narrator h3 a:hover { color:#99cc00; text-decoration:none; }
							 .tab-area .tab-data #tab3 .narrator .likes { float:right; font-size:1.28em; margin:15px 10px 0 0; }
							 .tab-area .tab-data #tab3 .narrator .likes a:hover { text-decoration:none; }
					.tab-area .tab-data #tab4 { padding-top:15px; }
		.alert { padding:20px; margin-bottom:20px; border:0; border-radius:0; font-weight:300; text-align:center; }
		.alert-info { background-color:#e5eef4; }
					#tab4 .alert-info a { font-weight:700; }
					#tab4 .media { border-bottom:1px solid #dedede; padding:0 0 30px 0; margin:30px 0 0 0; }
					#tab4 .media:last-child { border:0; padding:0; }
					#tab4 .media-left {}
					#tab4 .media-left .user-pic { border-left:5px solid #127dbe; padding:6px 5px 6px 10px; }
						#tab4 .media-left figure { width:38px; height:38px; border:2px solid #ddd; border-radius:50%; overflow:hidden; }
							#tab4 .media-left figure img { width:100%; height:100%; }
					#tab4 .media-heading { font-family:'Montserrat', sans-serif; font-weight:400; font-size:1.28em; padding:12px 0 15px 0; }
						#tab4 .media-heading small { font-weight:300; font-size:0.78em; margin:0 0 0 10px; }
						#tab4 .media-heading .likes { float:right; }
						#tab4 .media-heading .likes:hover { text-decoration:none; }
					#tab4 p { text-align:justify; }
						#tab4 .media .media { background-color:#f5f5f5; margin-top:20px; border:0; }
						#tab4 .media .media + .media { margin-top:4px; }
							#tab4 .media .media .user-pic { border:0; }
							#tab4 .media .media .media-body { padding-right:20px; }
								#tab4 .media .media .media-heading small { float:right; }
								#tab4 .media-heading .small { color:#0064d2; font-weight:300; font-size:0.66em; margin:0 0 0 10px; }
					#tab4 .media .media-body .more-replies { margin:10px 0 0 0; }				
				.tab-area .tab-data .tab-contents .footer { border-top:1px solid #dedede; padding:20px 0 0 0; margin:20px 0 0 0; }
					.profile .tab-area .tab-data .tab-contents .footer .paging { margin:0; padding:0; }
					.button { display:inline-block; min-width:250px; border:2px solid #999; padding:10px 30px; color:#999; margin:0 2px; font-weight:700; font-size:1.28em; position:relative; text-align:center; }
					button.button { background-color:transparent; transition:all linear 0.2s; }
						.button span { position:relative; z-index:2; }
					.button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:0; background-color:#999; transition:height linear 0.2s; z-index:1; }
					.button:hover:after { height:100%; }					
					.button.green { color:#99cc00; border-color:#99cc00; }
					.button.green:after { background-color:#99cc00; }					
					.button.blue { color:#0064d2; border-color:#0064d2; }
					.button.blue:after { background-color:#0064d2; }					
					.button:hover { color:#fff; text-decoration:none; }
					.button-sm { display:inline-block; min-width:100px; border:1px solid #999; border-radius:5px; padding:7px 30px; color:#999; margin:0 2px; font-weight:700; font-size:1.14em; position:relative; text-align:center; }
						.button-sm span { position:relative; z-index:2; }
					.button-sm:after { content:""; position:absolute; left:0; right:0; bottom:0; height:0; background-color:#999; transition:height linear 0.2s; z-index:1; }
					.button-sm:hover:after { height:100%; }					
					.button-sm.green { color:#99cc00; border-color:#99cc00; }
					.button-sm.green:after { background-color:#99cc00; }					
					.button-sm.blue { color:#0064d2; border-color:#0064d2; }
					.button-sm.blue:after { background-color:#0064d2; }					
					.button-sm:hover { color:#fff; text-decoration:none; }
					.tab-area .tab-data .tab-contents .footer .button { margin-top:30px; min-width:260px; }		
		.profile-right {}
			.profile-right h3 { font-size:1.28em; color:#99cc00; margin:0; }
			.profile-right h2 { font-size:2.14em; font-family:'Montserrat', sans-serif; font-weight:400; text-transform:uppercase; }
			.profile-right .metadata { margin:40px 0 0 0; font-weight:300; color:#999; }
				.profile-right .metadata .highlight { font-size:2.86em; color:#0064d2; display:block; margin:0 0 15px 0; }
					.profile-right .metadata .highlight small { font-size:0.6em; }
				.profile-right .metadata .c100 { font-size:60px; display:inline-block; float:right; top:-50px; }
					.profile-right .metadata .c100 > span { font-size:24px; color:#999; top:12px; }
			.profile-right .buttons { margin:30px 0 0 0; }
				
				.profile-right .buttons .donate-button { display:inline-block; width:100%; font-weight:700; font-size:1.71em; text-align:center; padding:15px 20px; background-color:#99cc00; color:#fff; border-radius:5px; position:relative; overflow:hidden; }
				.profile-right .buttons .donate-button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#67b400; transition:height linear 0.2s; z-index:1; }
				.profile-right .buttons .donate-button:hover { text-decoration:none; }
				.profile-right .buttons .donate-button:hover:after { height:100%; }
					.profile-right .buttons .donate-button span { position:relative; z-index:2; text-shadow:0 2px 1px rgba(103, 180, 0, 1); }
				
				.profile-right .buttons .fb-share-button { display:inline-block; width:49%; float:left; font-weight:700; font-size:1.28em; text-align:center; padding:15px 10px; background-color:#4460b0; color:#fff; border-radius:5px; position:relative; overflow:hidden; margin:5px 0 0 0; }
				.profile-right .buttons .fb-share-button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#223d89; transition:height linear 0.2s; z-index:1; }
				.profile-right .buttons .fb-share-button:hover { text-decoration:none; }
				.profile-right .buttons .fb-share-button:hover:after { height:100%; }
					.profile-right .buttons .fb-share-button span { position:relative; z-index:2; text-shadow:0 2px 1px rgba(34, 61, 137, 1); }
				
				.profile-right .buttons .follow-tweets-button { display:inline-block; width:49%; float:right; font-weight:700; font-size:1.28em; text-align:center; padding:15px 10px; background-color:#3fbffb; color:#fff; border-radius:5px; position:relative; overflow:hidden; margin:5px 0 0 0; }
				.profile-right .buttons .follow-tweets-button:after { content:""; position:absolute; left:0; right:0; bottom:0; height:2px; background-color:#248ebf; transition:height linear 0.2s; z-index:1; }
				.profile-right .buttons .follow-tweets-button:hover { text-decoration:none; }
				.profile-right .buttons .follow-tweets-button:hover:after { height:100%; }
					.profile-right .buttons .follow-tweets-button span { position:relative; z-index:2; text-shadow:0 2px 1px rgba(36, 142, 191, 1); }		
		.recent-donations { margin:70px 0 0 0; }
			.recent-donations h2 { font-size:1.28em; border-bottom:1px solid #dedede; padding:0 0 25px 0; margin:0 0 20px 0; }
				.recent-donations h2 i { float:right; color:#127dbe; font-size:2em; position:relative; top:-8px; }
			.recent-donations .donation-box { border-bottom:1px solid #dedede; padding:0 0 20px 0; margin:0 0 25px 0; }
			.donation-box:before, .donation-box:after { content:""; display:table; }
			.donation-box:after { clear:both; }
			.char { display:inline-block; width:50px; height:50px; line-height:46px; text-align:center; color:#fff; border-radius:50%; font-weight:300; font-size:24px; background-color:#ddd; vertical-align:middle; }
				.char.a { background-color:#4cc25d; } .char.b { background-color:#333; } .char.c { background-color:#3c9fe5; } .char.d { background-color:#99cc00; }
				.char.e { background-color:#333; } .char.f { background-color:#333; } .char.g { background-color:#e53c9f; } .char.h { background-color:#333; }
				.char.i { background-color:#333; } .char.j { background-color:#333; } .char.k { background-color:#2fdf86; } .char.l { background-color:#333; }
				.char.m { background-color:#e81f5c; } .char.n { background-color:#333; } .char.o { background-color:#e5d33c; } .char.p { background-color:#e7a248; }
				.char.q { background-color:#333; } .char.r { background-color:#333; } .char.s { background-color:#333; } .char.t { background-color:#487de7; }
				.char.u { background-color:#333; } .char.v { background-color:#333; } .char.w { background-color:#333; } .char.x { background-color:#333; }
				.char.y { background-color:#333; } .char.z { background-color:#333; }
			.donation-box .char { float:left; }
			.donation-box .content { margin:0 0 0 70px; position:relative; padding:0 30px 0 0; }
				.donation-box .content h3 { font-size:1.28em; margin:0 0 5px 0; }
				.donation-box .content .time-stamp { font-size:0.92em; color:#999; }
					.donation-box .content .love { margin:0 0 0 30px; color:#999; }
						.donation-box .content .love i { margin-right:5px; }
					.donation-box .content .love:hover, .donation-box .content .love.active { text-decoration:none; color:#99cc00; }
				.donation-box .content p { font-size:0.92em; margin:15px 0 0 0; }
				.donation-box .content .price-tag { font-weight:300; font-size:2.57em; color:#0064d2; position:absolute; top:-10px; right:0; }
					.donation-box .content .price-tag small { font-size:0.44em; color:#999; }
					.donation-box .content .bar { display:block; height:20px; background-color:#ddd; margin:0 0 8px 0; border-radius:2px; }
			.profile .paging { margin:20px 0 0 0; padding:0 20px; }
				.profile .paging span { float:left; width:70%; display:inline-block; color:#999; }
				.profile .paging a { float:right; }
	/* /profile page */
	/* medium device (md) */
	@media screen and (min-width: 992px) and (max-width: 1199px) {
		.main-slider .one .contents { top:50px; }
		.main-slider .flex-control-nav { bottom:35px; }
		.fund-campaigns .campaign-boxes .box .data { min-height:240px; }
		.fund-campaigns .campaign-boxes .box .category-meta { padding:0 20px; }
		.fund-campaigns .campaign-search button[type="submit"] { width:48px; }
		.donate-banner-right .profile-data { width:60%; }
		.donate-banner-right .metadata { margin-left:50px; }
		.donate-banner-right .percent-circle .c100 { margin-right:0; }
		.signup-form-box { width:85%; }
		.create-campaign .tab-data .right-panel { padding:0; }
		.create-campaign .tab-data .right-panel h2 { padding:0; max-width:250px; margin:0 auto; }
		.create-campaign .tab-data .right-panel h3 { padding:0; max-width:250px; margin:0 auto; }
		.campaign-preview-box { margin:20px auto; max-width:250px }
		.create-campaign .tab-area .tab-data { padding:20px 20px; }
		.dashboard .dashboard-header .right { margin:-10px 0 0 0;}
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box figure { width:30%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents { width:54%; padding:0; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share { width:16%; padding:50px 0 0 0; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .share .actions { border:0; display:block; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] { display:block; float:left; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; margin:0 20px 10px 20px; border-bottom:1px solid #dedede; padding:10px 0; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:30%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:30%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-four { width:20%; }
		.dashboard.donor .tab-area .tab-data .campaigns .campaign-box .contents .col-five { width:20%; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .contents .col-one h3 { margin-bottom:5px; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box figure { width:40%; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents { width:60%; padding:0; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .actions { width:100%; padding:5px 5px; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; display:block; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:50%; text-align:left; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-two p { padding-left:5px; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:50%; text-align:left; }
		.dashboard.campaign-creator .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box figure { width:40%; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents { width:60%; padding:0; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .actions { width:100%; padding:5px 5px; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-one { width:auto; display:block; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-two { width:50%; text-align:left; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-two p { padding-left:5px; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents .col-three { width:50%; text-align:left; }
		.dashboard.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { display:inline-block; margin:0 0 0 10px; }
		.dashboard .tab-area .tab-data .campaigns .campaign-box .actions a:first-child { margin-top:0; }
		.dashboard.public-profile.organization .tab-area .tab-data .campaigns .campaign-box .contents [class^="col-"] span { margin:0; }
		.dashboard .left-panel .box { padding:15px 10px; }
		div.search-campaigns .campaign-boxes .box .data { min-height: 350px; padding:0 10px; }
		.search-campaigns .campaign-boxes .box .data .user-info { padding:0 20px 0 10px; }
		div.search-campaigns .campaign-search .input-box { width:33%; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box > img { width:240px; height:100%; position:absolute; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box .data { margin:0 120px 0 290px; padding:10px; }
		.search-campaigns .fund-campaigns .campaign-boxes.listview .box .percent-circle { left:-100px; }
		.search-organization .campaign-search .input-box { width:33%; }
		.search-organization .fund-campaigns .campaign-boxes.listview .box	.data { padding:10px; }
		.search-organization .fund-campaigns .campaign-boxes.listview .box > img { position:absolute; }
		.how-it-works-page .step-box.one:after, .how-it-works-page .step-box.two:after, .how-it-works-page .step-box.three:after  { background-size:45%; }
		.how-it-works-page .step-box .content { width:60%; }
		.how-it-works-page .step-box figure { width:40%; }
		.how-it-works-page .step-box h2 { font-size:1.8em; }
		.profile-banner .content .data { margin:0 0 0 70px; }
		.profile-banner .content .data .location { float:none; display:block; }
		.profile-banner .content .data .time-stamp { float:none; display:block; margin:5px 0 0 0px; }
		.donation-box .content { padding:0 0 10px 0; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata { font-size:0.92em; margin-left:10px; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight { font-size:1.5em; }
		.edit-campaign .left-panel .campaign-box .author-box .metadata .highlight small { font-size:0.6em; }
		.contact-us .contacts [class^="col-"] { text-align:center; vertical-align:top; }
		.contact-us .contacts [class^="col-"] span { display:block; float:none; margin:0 !important; min-height:70px; }
		.contact-us .contacts [class^="col-"] span + span { min-height:0; }
		.scroll-up { right:20px; bottom:20px; }
		.profile-org .contact-us .box { padding:0; margin-bottom:20px; }
		.profile .tab-area .tabs li a span { padding:15px 20px 15px 20px; }
		.profile #tab2 .pic-slider .flickity-prev-next-button { width:60px; }
	}
	/* /medium device (md) */
	
	/* large device (lg) */
	@media screen and (min-width: 1200px) {
		.campaign-facts { padding:0 100px; }
	}
	/* /large device (lg) */
	.cropit-preview {
		background-color: #f8f8f8;
		background-size: cover;
		border: 1px solid #ccc;
		border-radius: 3px;
		margin-top: 7px;
		width: 652px;
		height: 452px;
                
	  }
	
	  .cropit-preview-image-container {
		cursor: move;
	  }
	
	  .image-size-label {
		margin-top: 10px;
	  }
	
	  input, .export {
		display: block;
	  }
	
	  button {
		margin-top: 10px;
	  }
</style>
<!-- content -->
<div id="content" class="page create-campaign">
    <div class="container">
        <div class="row">
            <h2>Create your campaign</h2>
            <h3>Make a great first impression with your campaign’s title and image,<br class="hidden-xs" />and set your funding goal, campaign duration, and  category.</h3>
            <div class="tab-area">
                <ul class="tabs">
                    <li class="one">Basic Info</li>
                    <li class="two active">Images/Video</li>
                    <li class="three">Preview</li>
                </ul>

                <div class="tab-data">
                    <div class="row">
                        <!-- left panel -->
                        <div class="col-sm-7 col-md-8">
                            <section class="step-1" style="display: none;">

                            </section>

                            <section class="step-2"  style="display: block;">
                                <div class="form">
                                    <div class="form-row">
                                        <h4>Picture</h4>
                                        <label>Personalize your campaign by adding a picture.</label>
                                    </div>
                                    <?php //var_dump($gallery_images);exit;?>

                                    <div class="form-row">
                                        <div class="pictures">
                                            <?php foreach ($gallery_images as $gallery_image) { ?>
                                                <div class="pic-box" id="image-box-<?= $gallery_image->id ?>">
                                                    <figure>
                                                        <img id="previewFinal"  src="<?= base_url() ?>uploads/campaign/gallery_images/thumb/<?= $gallery_image->image_name ?>" alt="" />
                                                        <a href="javascript:;" onclick="if (confirm('Are you sure ?'))
                                                                    deleteGalImage(<?= $gallery_image->id ?>);"><i class="fa fa-remove"></i></a>
                                                    </figure>
                                                    <div class="select">
                                                        <input type="radio" id="profile_pic_<?= $gallery_image->id; ?>" class="fancy-checkbox sm" name="profile_picture" value="<?= $gallery_image->id ?>" <?php if($gallery_image->status == 1){?> checked="checked" <?php } ?> /> 
                                                        <label for="profile_pic_<?= $gallery_image->id ?>">Set as profile picture</label>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>

                                        <a href="javascript:;" data-target="#myModal" data-toggle="modal" class="upload-box">Choose an image from your computer</a>
                                    </div>
                                    <div class="form-row">
                                        <div class="buttons">
                                            <a href="javascript:void(0);" class="button blue" onclick="save_next()"><span>Save &amp; Finish</span></a>
                                            <a href="javascript:fun_backbutton();" class="button"><span>Discard Changes</span></a>
                                        </div>
                                    </div>
                                </div>				
                            </section>
                        </div>
                        <!-- /left panel -->

                        <!-- right panel -->
                        <!--<div class="col-sm-5 col-md-4">
                            <div class="right-panel">
                                <h2>Preview</h2>
                                <h3>Your profile will look like this.</h3>
                                <div class="campaign-preview-box">
                                    <!--<img src="<?= base_url() ?>assets/images/data/campaign-01.jpg" alt="" />-->
<!---
                                    <img src="" alt="" />
                                    <span class="data">
                                        <span class="category-name">Community</span>
                                        <span class="category-title">My Campaign Title</span>
                                        <span class="category-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ...</span>
                                        <span class="user-info clearfix">
                                            <img src="<?= base_url() ?>assets/images/data/user-04.jpg" alt="" />
                                            <span class="content">By <a href="#">Mary Lee</a><br />Los Angeles, CA</span>
                                        </span>
                                        <span class="category-meta">
                                            <span>Raised<b>$0</b></span>
                                            <span>Backers<b>0</b></span>
                                            <span>Days Left<b>30</b></span>
                                        </span>
                                    </span>
                                </div>
                                <p>
                                    <a href="<?php echo base_url(); ?>contact-us" class="support-btn">
                                        <span class="left"><i class="fa fa-lightbulb-o"></i></span>
                                        <span class="right">
                                            <span class="header">Need Help!</span>
                                            <span class="content">Let’s our expert help you to create your best campaign ever!</span>
                                        </span>
                                    </a>
                                </p>
                            </div>
                        </div>
--->
                        <!-- /right panel -->
                    </div>					
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:735px;"> 
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <div class="image-editor">
                    <input type="file" class="cropit-image-input" required>
                    <div class="cropit-preview"></div>
                    <div class="image-size-label">
                        Resize image
                    </div>
                    <input type="range" class="cropit-image-zoom-input">
                    <!--<button class="rotate-ccw">Rotate counterclockwise</button>
                    <button class="rotate-cw">Rotate clockwise</button>-->
                    <button class="button blue export">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
  $(function() {
	$('.image-editor').cropit({
//	  imageState: {
//		src: '//lorempixel.com/500/400/',
//	  },
	});

	$('.rotate-cw').click(function() {
	  $('.image-editor').cropit('rotateCW');
	});
	$('.rotate-ccw').click(function() {
	  $('.image-editor').cropit('rotateCCW');
	});

	$('.export').click(function() {
								
	  var imageData = $('.image-editor').cropit('export');
	  //window.open(imageData);
	  //alert(imageData);
	  if(!imageData){
	  	alert('Image is not valid. Please choose another image');
	  	return false;
	  };
	  $.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>CampaignController/file_upload_parser",          
		data: {'imgdata':imageData,'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
			success: function(total){
				$('#myModal').modal('hide');
				window.location.reload();
			}
		});
	 });
  });
</script>
<script type="text/javascript">
	function fun_backbutton()
	{
		window.location.href='<?php echo base_url(); ?>create-campaign';	
	}
</script>
<!-- /content -->


<script>
	function deleteGalImage(id)
    {
        $.ajax({
            url: "<?= base_url() . 'CampaignController/delete_image' ?>",
            'type': 'POST',
            dataType: 'json',
            'data': {'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val(), 'id': id},
            success: function (result) {

                if (result.code == '200')
                {
                    $("#image-box-" + id).remove();
                    location.reload();
                }
                else
                {
                    alert("An error occured.Please try again");
                }
                $("#csrf_token").val(result.token);
            }});
    }
	function save_next()
    {
        var profile_picture = $('input[name="profile_picture"]:checked').val();
         if(!profile_picture){
               alert("Please select image");return false;
         }
      
		
        $.ajax({
            url: "<?= base_url() . 'CampaignController/set_profile' ?>",
            'type': 'POST',
            'data': {'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val(), 'profile_picture': profile_picture},
             success: function (result) {
                 //alert(result);
				if(result!="")
             	{
             	window.location.href = "<?= base_url() . 'campaign-success/' ?>"+result;
             	}else{
                alert('Please Upload Image First');
             	window.location.href = "<?= base_url() . 'edit-campaign/step-two/'?>";
             	}
                   
                    //window.location.href = "<?= base_url() . 'preview/' . $campaign->slug ?>";
            }
		});

    }
   /* $("#save").click(function () {
        var imgSrc = $("#preview_crop").attr('src');

        $.ajax({
            url: "<?= base_url() . 'CampaignController/save_image' ?>",
            'type': 'POST',
            dataType: 'json',
            'data': {'<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val(), 'imgSrc': imgSrc},
            success: function (result) {
                //
                if (result.message == 'success')
                {
                    //
                    location.reload();
                }
                else
                    $("#csrf_token").val(result.token);
            }});
    });

    

   */
</script>    