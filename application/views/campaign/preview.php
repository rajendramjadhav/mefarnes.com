<!-- content -->
<div id="content" class="page profile create-campaign-preview">
    <div class="container">
        <div class="row">
            <div class="header">
                <h2>Your Campaign Preview</h2>
                <h3>Please check all information of your campaign and see how it’ll look on Mefarnes.<br class="hidden-xs" /> Please confirm preview to upload campaign.</h3>
            </div>

            <div class="main-box">
                <div class="clearfix">
                    <!-- left panel -->
                    <div class="col-sm-8">
                        <div class="profile-banner">
                            <?php $image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/gallery_images/thumb/' . $campaign->image)) ? base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image : base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image; ?>
                            <figure><img class="responsive-img" src="<?= $image_file ?>" alt="" width="770" height="450" /></figure>
                            <div class="content">
                                <figure>
                                	<span class="char d"><?php echo ($campaign->user_name !='') ? substr(ucfirst($campaign->user_name),0,1) : "";?></span>
                                	<!---
                                	<img src="<?= base_url(); ?>assets/images/data/user-01.jpg" alt="" />
                                	---->
                                </figure>
                                <div class="data">
                                    <div class="left">
                                        <h3><?= $campaign->user_name ?> <a href="#"><i class="fa fa-envelope-o"></i></a></h3>
                                        <div class="clearfix">
                                            <span class="location"><i class="fa fa-map-marker"></i><?= substr($campaign->location,0,10)." ..." ?></span>
                                            <span class="time-stamp"><i class="fa fa-history"></i>Created  <?= date('M d, Y', strtotime($campaign->create_date)) ?></span>
                                        </div>
                                    </div>
                                    <div class="right">
                                    	<div class="shares"><i class="fa fa-line-chart"></i><?php echo $shareCount; ?> <small>Shares</small></div>
                                    	<!---
                                        <div class="shares"><i class="fa fa-line-chart"></i>2.5k <small>Shares</small></div>
                                        <div class="likes"><i class="fa fa-thumbs-o-up"></i>68</div>
                                        ---->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- tab area -->
                        <div class="tab-area">
                            <ul class="tabs">
                                <li class="active"><a href="javascript:void(0);" data-toggle="#tab1"><span>Story</span></a></li>
                                <li><a href="javascript:void(0);" data-toggle="#tab2"><span>Gallery</span></a></li>
                                <li><a href="javascript:void(0);" data-toggle="#tab3"><span>Updates <b class="badge"><?= count($updates) ?></b></span></a></li>
                                <!--<li><a href="javascript:void(0);" data-toggle="#tab3"><span>Comments <b class="badge"><?= count($comments) ?></b></span></a></li>-->
                            </ul>
                            <div class="tab-data">
                                <div id="tab1" class="tab-contents">
                                    <h4><?= htmlentities($campaign->title) ?></h4>
                                    <p><?= substr(($campaign->description), 0, 110) ?></p>
                                    <div class="row">
<!---
                                        <?php
                                        //if ($campaign->video_file_name != '') {
                                            ?>
                                            <div class="col-sm-5">
                                                <?php if ($campaign->video_type == 1) { ?>
                                                    <iframe width="320" height="190"
                                                            src="https://www.youtube.com/embed/<?= $campaign->video_file_name ?>">
                                                    </iframe> 
                                                <?php } else { ?>
                                                    <iframe src="//player.vimeo.com/video/<?= $campaign->video_file_name ?>" width="320" height="190" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <?php } ?></div>
                                        <?php //} ?>
                                        <div class="col-sm-<?php
                                        if ($campaign->video_file_name == NULL)
                                            echo 12;
                                        else
                                            echo 7;
                                        ?>">
----->
                                 <div class="col-sm-12">
                                            <p><?= nl2br(substr(($campaign->description), 110, 350)) ?></p>
                                        </div>
                                    </div>
                                    <p><?= nl2br(substr(($campaign->description), 350)) ?></p>

                                    <div class="footer">
                                        <div class="row">
                                            <div class="col-sm-6 pull-right">
                                                <a href="javascript:void(0);" class="pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-mail-reply"></i> Report to Campaign</a>
                                            </div>
                                        </div>
<!---
                                        <div class="text-center">
                                            <a href="javascript:;" class="button blue subscribe"><span>Subscribe to Updates</span></a>
                                            <a href="#" class="button green"><span>Leave a Comment</span></a>
                                        </div>
---->
                                    </div>
                                </div>

<!-- tab gallery -->
                                <?php //var_dump($galleries);exit;?>
                        <div id="tab2" class="tab-contents">
                            <h4>Photos</h4>
                            <?php if (!empty($galleries)) { ?>
                                <div class="pic-slider">
                                    <?php
                                    foreach ($galleries as $gallery):
                                        ?>
                                        <div class="carousel-cell">
                                            <figure><img height="225" src="<?= base_url() ?>uploads/campaign/gallery_images/thumb/<?= $gallery->image_name ?>" alt="" /></figure>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php } else echo 'No image available!'; ?>

                                                   <h4 class="mt30">Videos</h4>
                                                   <!---
                                                        <div class="pic-slider">
                                                        	---->
                                                        	<div class="row">
                                                        	<?php if($campaign->video_url!=""){?>
                                                            <div class="carousel-cell">
                                                            	 <figure><iframe width="380" height="225" src="<?php echo $campaign->video_url;?>">
                                                                </iframe>
                                                                 </figure> 
                                                            </div>
                                                           <?php }else{?>
                                                           	<div class="carousel-cell">
                                                            	<p style="margin-left:30px;"> Not Video Yet</p>
                                                            </div>
                                                                	
                                                                <?php }?>
                                                            <!---
                                                            <div class="carousel-cell">
                                                                <figure><iframe width="380" height="225" src="https://www.youtube.com/embed/UWCdvH1FwJ0">
                                                                </iframe></figure>  
                                                            </div>
                                                            <div class="carousel-cell">
                                                                <figure><iframe width="380" height="225" src="https://www.youtube.com/embed/UWCdvH1FwJ0">
                                                                </iframe></figure>  
                                                            </div>
                                                            --->
                                                        </div>
                            <div class="footer">
                                <div class="row">
                                    <div class="col-sm-6 pull-right">
                                        <a href="javascript:void(0);" class="pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-mail-reply"></i> Report to Campaign</a>
                                    </div>
                                </div>
<!---
                                <div class="text-center">
                                    <a href="javascript:;" class="button blue subscribe"><span>Subscribe to Updates</span></a>
                                     <a href="#" class="button green"><span>Leave a Comment</span></a>
                                </div>
---->
                            </div>
                        </div>
                        <!-- /tab gallery -->

                                <div id="tab3" class="tab-contents">
                                    <div class="clearfix">
                                        <a href="#">Create Update</a>

                                        <?php
                                        $i = 0;
                                        foreach ($updates as $update):
                                            $i++;
                                            ?>
                                            <article>
                                                <h2><?= time_elapsed_string(strtotime($update->create_date)) ?></h2>
                                                <div class="row">
<!----
                                                    <?php
                                                   // if ($update->video_file_name != '') {
                                                        ?>
                                                        <div class="col-sm-5">
                                                            <?php if ($update->video_type == 1) { ?>
                                                                <iframe width="300" height="176"
                                                                        src="https://www.youtube.com/embed/<?= $update->video_file_name ?>">
                                                                </iframe> 
                                                            <?php } else { ?>
                                                                <iframe src="//player.vimeo.com/video/<?= $update->video_file_name ?>" width="300" height="176" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                            <?php } ?> </div>
                                                    <?php //} ?>

                                                    <div class="col-sm-<?php
                                                    if ($update->video_file_name == NULL)
                                                        echo 12;
                                                    else
                                                        echo 7;
                                                    ?>">
---->
                                                 <div class="col-sm-12">
                                                        <h4><?= htmlentities($update->title); ?></h4>
                                                        <p><?= nl2br(substr(($update->description), 0, 450)) ?></p></p>
                                                    </div>

                                                </div>
                                                <p><?= nl2br(substr(($update->description), 450)) ?></p>
                                                <div class="narrator">
                                                   <?php foreach($users as $user){?>
                                            <figure>
                                            	<!---
                                            	<img src="<?= base_url(); ?>assets/images/data/user-02.jpg" alt="" />
                                            	--->
                                            	<?php if($user->user_image!=""){?>
                                            	<img src="<?= base_url(); ?>uploads/campaign/gallery_images/thumb/<?php echo $user->user_image;?> " alt="" />
											   <?php }else{?>
											   	<img src="<?= base_url(); ?>uploads/campaign/gallery_images/thumb/no_image.png" alt="" />
											  <?php }?>
                                            	
                                            	</figure>
                                           <?php }?>
                                                    <h3><a href="#"><?= $update->user_name; ?></a></h3>
                                                    <div class="likes"><a href="javascript:;"><i class="fa fa-heart"></i> [ <span id="love<?= $update->id ?>"><?= $update->total_love; ?></span> ]</a></div>
                                                </div>
                                            </article>                                
                                        <?php endforeach; ?>
                                    </div>

                                    <div class="footer">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="paging">
                                                    <span>Showing  <?= $i ?> out of <?= $i ?> updates</span>
                                                    <a href="#">See More</a>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0);" class="pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-mail-reply"></i> Report to Campaign</a>
                                            </div>
                                        </div>
<!---
                                        <div class="text-center">
                                            <a href="javascript:;" class="button blue subscribe"><span>Subscribe to Updates</span></a>

                                        </div>
----->
                                    </div>
                                </div>

                                <div id="tab3" class="tab-contents">

                                    <?php
                                    if ($this->session->userdata('is_logged_in') == '') {
                                        ?>
                                        <div class="alert alert-info">You must be a logged in contributor to comment. <a href="#">Log In</a></div>
                                    <?php } ?>

                                    <div class="clearfix">
                                        <?php
                                        foreach ($comments as $comment):
                                            ?>
                                            <div class="media">
                                                <div class="media-left">
                                                    <div class="user-pic"><figure><img src="<?= base_url(); ?>assets/images/data/user-02.jpg" alt="" /></figure></div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><?= $comment->user_name ?> <small><?= htmlentities(time_elapsed_string(strtotime($comment->create_date))) ?></small> <a href="#" class="likes"><i class="fa fa-heart"></i> [ 12 ]</a></h4>
                                                    <p><?= nl2br(htmlentities($comment->comments)) ?></p>
                                                    <?php
                                                    foreach ($comment->replies as $reply):
                                                        ?>
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <div class="user-pic"><figure><img src="<?= base_url(); ?>assets/images/data/user-03.jpg" alt="" /></figure></div>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading"><?= $reply->user_name ?> <small><?= htmlentities(time_elapsed_string(strtotime($reply->reply_date))) ?></small></h4>
                                                                <p><?= nl2br(htmlentities($reply->reply_text)) ?></p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                    <?php if ($comment->total_reply > 2) { ?>
                                                        <p class="more-replies"><a href="#">See 2 more replies...</a></p>
                                                    <?php } ?>

                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                    <div class="footer">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="paging">
                                                    <span>Showing  <?= count($comments) ?> out of <?= count($comments) ?> Comments</span>
                                                    <a href="#">See More</a>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0);" class="pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-mail-reply"></i> Report to Campaign</a>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <a href="javascript:;" class="button blue subscribe"><span>Subscribe to Updates</span></a>
                                            <a href="#" class="button green"><span>Leave a Comment</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab area -->
                    </div>
                    <!-- /left panel -->

                    <!-- right panel -->
                    <div class="col-sm-4">
                        <div class="profile-right">
                            <h3>Community</h3>
                            <h2><?= $campaign->title ?></h2>
                            <p class="metadata"><span class="highlight"><?= $campaign->donor ?></span>Backers</p>
                            <p class="metadata">
                                <span class="highlight">$<?= $campaign->collected_fund ?> <small>USD</small></span>Pledged of $<?= format_num($campaign->fund_needed, 0) ?> goal
                                <span class="percent-circle">
                                    <span class="c100 p<?php
                                    $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                                    if ($percentage >= 100)
                                        echo 100;
                                    else
                                        echo $percentage;
                                    ?>">
                                        <span><?php
                                            $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                                            if ($percentage >= 100)
                                                echo '100';
                                            else
                                                echo $percentage;
                                            ?><small>%</small></span>
                                        <b class="slice"><span class="bar"></span><span class="fill"></span></b>
                                    </span>
                                </span>
                            </p>
                            <p class="metadata"><span class="highlight">
                            	  <?php
                                    $now = time(); // or your date as well
                                    $your_date = strtotime(date('Y-m-d', strtotime($campaign->end_date_time)));
                                    $datediff = $your_date - $now;
                                    echo floor($datediff / (60 * 60 * 24) + 1);
                                    ?></span>Days to go</p>

                            <div class="buttons clearfix">
                                <a href="#" class="donate-button"><span>Donate Now!</span></a>
                                <a href="https://www.facebook.com/dialog/share?app_id=579509698900395&display=popup&href=<?php echo current_url(); ?>&redirect_uri=<?php echo current_url(); ?>" class="fb-share-button"><span>Facebook Share</span></a>
                                <input type="hidden" name="share_twitter" id="share_twitter" value="<?= $campaign->id ?>">
                                <a href="javascript:;" class="follow-tweets-button" id="shareB" onclick="shareTwitterlink()"><span><!--Follow Tweets-->Twitter Share</span></a>
                                <a href="http://twitter.com/intent/tweet?original_referer=<?php echo current_url(); ?>&status=<?php echo current_url(); ?>&<?= $campaign->title ?>+<?php echo current_url(); ?>" class="follow-tweets-button" style="display:none" id="shareTw"><span><!--Follow Tweets-->Twitter Share</span></a>
                            </div>
                        </div>

                        <!-- recent donations -->
                        <div class="recent-donations">
                            <h2>Recent Donations <i class="fa fa-angle-down"></i></h2>

                            <section class="donation-box">
                                <div class="char"></div>
                                <div class="content">
                                    <span class="bar"></span><span class="bar"></span><span class="bar"></span>
                                </div>
                            </section>
<!---
                            <section class="donation-box">
                                <div class="char"></div>
                                <div class="content">
                                    <span class="bar"></span><span class="bar"></span><span class="bar"></span>
                                </div>
                            </section>

                            <section class="donation-box">
                                <div class="char"></div>
                                <div class="content">
                                    <span class="bar"></span><span class="bar"></span><span class="bar"></span>
                                </div>
                            </section>

                            <section class="donation-box">
                                <div class="char"></div>
                                <div class="content">
                                    <span class="bar"></span><span class="bar"></span><span class="bar"></span>
                                </div>
                            </section>
---->
                        </div>
                        <!-- /recent donations -->
                    </div>
                    <!-- /right panel -->
                </div>
            </div>

            <div class="footer mt40">
                <div class="text-center">
                    <a href="<?= base_url() . 'CampaignController/makelive/' . $campaign->id ?>" class="button blue"><span>Confirm Preview</span></a>
                    <a href="<?= base_url().'create-campaign/'.$campaign->slug ;?>" onclick="return onfirm('Are sure to discard ?')" class="button"><span>Discard Preview</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /content -->
<script>
	function shareTwitterlink(){
		//alert('sgggsdgdf');
		var twitterid = $('#share_twitter').val();
		//var check = '<?php echo base_url(); ?>'+"PublicCampaign/shareCount";
		//alert(check);
		//alert(twitterid);
		jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "PublicCampaign/shareCount",
            data: {
                twitterid: twitterid,
                
             },
             success: function (res) {
                	console.log(res);
                	var twUrl = $("#shareTw").attr('href');
                	window.location.assign(twUrl);
                	
              	}
        });

	}
</script>



