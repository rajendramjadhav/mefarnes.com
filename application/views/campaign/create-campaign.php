<style>
	.blue:hover{
		background: #0064d2!important;
		color : white;
		
	}
</style>
<?php 
	$campaignID = $this->uri->segment(2);
	//var_dump($getsinglecampaign);exit;
	
	if($campaignID != NULL && $campaignID >0)
	{
		if(isset($getsinglecampaign))
		{
			foreach($getsinglecampaign as $singlecampaign)
			{
				$categoryID=$singlecampaign[0]->campaign_categoriy_id;
				$title=$singlecampaign[0]->title;
				$campaign_story=$singlecampaign[0]->description;
				$video_link=$singlecampaign[0]->video_url;
				$location=$singlecampaign[0]->campaign_location_id;
				$fund_needed=$singlecampaign[0]->fund_needed;
				$campaign_duration=$singlecampaign[0]->end_date_time;
			}
		}
	}
?>
<!-- content -->
<div id="content" class="page create-campaign">
    <div class="container">
        <div class="row">
            <h2>Create your campaign</h2>
            <h3>Make a great first impression with your campaign’s title and image,<br class="hidden-xs" />and set your funding goal, campaign duration, and  category.</h3>
            
            <div class="tab-area">
                <ul class="tabs">
                    <li class="active one">Basic Info</li>
                    <li class="two">Images/Video</li>
                    <!-- <li class="three">Preview</li> -->
                </ul>

                <div class="tab-data">
                    <section class="step-1">
                        <div class="row">
                            <!-- left panel -->
                            <div class="col-sm-7 col-md-8">

                                <?php //echo form_open_multipart(base_url('create-campaign'), array('id' => 'crete-campaign')); ?> 
                                
                                <form name="crete-campaign" id="crete-campaign" method="post" action="<?php echo base_url(); ?>savecampaign">
                                <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />                   
                                <input type="hidden" name="campaignID" value="<?php echo @$campaignID; ?>" />
                                <div class="form">
                                    <div class="form-row one-third">
                                        <h4>Category</h4>
                                        <label>Help people discover your compaign on our site.</label>
                                        <select class="fancy-selectbox" name="category" id="category" required>
                                            <option value="">Please select category</option>
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?= $category->id ?>" <?php if(@$categoryID==$category->id){?> selected="selected" <?php } ?>><?= $category->name ?></option>;
                                            <?php endforeach;
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-row">
                                        <h4>Title</h4>
                                        <label>What do you want to raise money for?</label>
                                        <div class="input-group">
                                            <input type="text" id="title" name="title" class="form-control" value="<?php echo @$title; ?>" placeholder="My Campaign Title" required="">
                                            <span class="input-group-addon"></span>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <h4>Story</h4>
                                        <label>What’s this campaign for?</label>
                                        <div class="input-box">                                       
                                            <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                            <script>tinymce.init({ selector:'textarea' });</script>
                                            <textarea name="campaign_story" cols="108" rows="5" id="campaign_story" placeholder="Why this campign is for?"><?php echo @$campaign_story; ?></textarea>
                                            
                                            
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <h4>Video <small>(optional)</small></h4>
                                        <label>Personalize your campaign by adding a Video.</label>
                                        <p><label>Use External video links</label></p>
                                        <input type="text" id="video_link" onChange="nextField(this);" value="<?php echo @$video_link; ?>" name="video_link" placeholder="Youtube or Vimeo URL" >
                                    </div>

                                    <div class="form-row">
                                        <h4>Location</h4>
                                        <label>Get support from your local community.</label>
                                        <input type="text" name="location" id="location" autocomplete="on" placeholder="Enter Location" value="<?php echo @$location; ?>"> 
                                    </div>

                                    <div class="form-row">
                                        <h4>Goal</h4>
                                        <label>How much do you want to raise?</label>
                                        <div class="input-group left">
                                            <span class="input-group-addon">$</span>
                                            <input type="number" name="fund_needed" id="fund_needed" value="<?php echo @$fund_needed; ?>" required="" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <h4>Duration</h4>
                                        <label>How long do you want to raise fund?</label>
                                        <div class="input-box">
                                            <div class="days-container"><span class="days">30</span></div>
                                            <div class="slider-bar">
                                                <b>1 Day</b>
                                                <div id="day-slider" class="norange"></div>
                                                <b>60 Days</b>
                                            </div>
                                        </div>
                                        <input type="hidden" name="campaign_duration" id="campaign_duration" value="<?php if(@$campaign_duration > 0){ echo @$campaign_duration; } else { echo "30"; }?>">
                                    </div>
                                    <?php if ($error != '') { ?>
                                        <div class="alert alert-danger"><?php echo $error; ?></div>
                                    <?php } ?>
                                    <div class="alert alert-danger" style="display:none;">test</div>
                                    <div class="form-row">
                                        <div class="buttons">
                                            <!--<input type="button" style="background: #fff;" onclick="formValidation();" class="button blue continue-section-1" name="create-campaign" value="Save &amp; Next"> -->
                                              <input type="button" style="background: #fff;" onclick="formValidation();" class="button blue" name="create-campaign" value="Save &amp; Next">
<!--                                            <input type="reset"  onclick="fun_backbutton();" class="button" value="Discard Changes">-->
                                            <a href="javascript:void(0);" onclick="fun_backbutton();" class="button"><span>Discard Changes</span></a>
                                        </div>
                                    </div>
                                </div>	

                                <?php echo form_close(); ?>
                            </div>
                            <!-- /left panel -->

                            <!-- right panel -->
                            <!--<div class="col-sm-5 col-md-4">
                                <div class="right-panel">
                                    <h2>Preview</h2>
                                    <h3>Your profile will look like this.</h3>
                                    <div class="campaign-preview-box">
                                        <img src="<?php echo base_url(); ?>assets/images/data/campaign-01.jpg" alt="" />
                                        <img src="" alt="" />
                                        <span class="data">
                                            <span class="category-name">Community</span>
                                            <span class="category-title">My Campaign Title</span>
                                            <span class="category-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ...</span>
                                            <span class="user-info clearfix">
                                                <img src="<?php echo base_url(); ?>assets/images/data/user-04.jpg" alt="" />
                                                <span class="content">By <a href="#">Mary Lee</a><br />Los Angeles, CA</span>
                                            </span>
                                            <span class="category-meta">
                                                <span>Raised<b>$0</b></span>
                                                <span>Backers<b>0</b></span>
                                                <span>Days Left<b>30</b></span>
                                            </span>
                                        </span>
                                    </div>
                                    <p>
                                        <a href="<?php echo base_url(); ?>contact-us" class="support-btn">
                                            <span class="left"><i class="fa fa-lightbulb-o"></i></span>
                                            <span class="right">
                                                <span class="header">Need Help!</span>
                                                <span class="content">Let’s our expert help you to create your best campaign ever!</span>
                                            </span>
                                        </a>
                                    </p>
                                </div>
                            </div>-->
                            <!-- /right panel -->
                        </div>
                        
                    </section>

                </div>

            </div>
        </div>
    </div>
</div>

<!-- /content -->
<script>
    $(document).ready(function () {
        $('#OpenImgUpload').click(function () {
            $('#campaignPics').trigger('click');
        });
    });

    function formValidation()
    { 
        var desc=tinyMCE.get('campaign_story').getContent();
        if ($("#category").val() == '')
        {
           
            var txt = "Please select campaign category!";
            $(".alert-danger").html(txt);
            $(".alert-danger").slideDown('slow');
            $("#category").focus();
            return false;
        }
        else if ($("#title").val() == '')
        {
            var txt = "Please input campaign name!";
            $(".alert-danger").html(txt);
            $(".alert-danger").slideDown('slow');
            $("#title").focus();
            return false;
        }else if (desc == '')
        {
            var txt = "Please input campaign description!";
            $(".alert-danger").html(txt);
            $(".alert-danger").slideDown('slow');
            $("#campaign_story").focus();
            return false;
        }else if ($("#location").val() == '')
        {
            var txt = "Please input campaign location!";
            $(".alert-danger").html(txt);
            $(".alert-danger").slideDown('slow');
            $("#location").focus();
            return false;
        }
        else if ($("#fund_needed").val() == '')
        {
            var txt = "Please input amount you want to raise!";
            $(".alert-danger").html(txt);
            $(".alert-danger").slideDown('slow');
            $("#fund_needed").focus();
            return false;
        }
        $("#campaign_duration").val($(".days").html());

        $("#crete-campaign").submit();
    }

    function nextField(current) {
        //alert('tab pressed');
    }
</script>
<script type="text/javascript">
	function fun_backbutton()
	{
		window.location.href='<?php echo base_url(); ?>dashboard';	
	}
</script>	
<style type="text/css">
    body {
        font-family: sans-serif;
        font-size: 14px;
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js??v=3.13&libraries=places&key=AIzaSyBgS4A2KbRY159HA-HDIRxaoanaYQscQy8" type="text/javascript"></script>
<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>