
<!-- content -->
<?php ///var_dump($users);exit;?>
<div id="content" class="page profile">
    <div class="container">
        <div class="row">
            <!-- left panel -->
            <div class="col-sm-8">
                <div class="profile-banner">
                    <?php $image_file = (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/cover_images/preview/' . $campaign->image)) ? base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image : base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image; ?>
                    <figure><img src="<?= $image_file ?>" alt="" width="770" height="450" /></figure>
                    <div class="content">
                        <figure>
                            <span class="char d"><?php echo ($campaign->user_name != '') ? substr(ucfirst($campaign->user_name), 0, 1) : ""; ?></span>
                        </figure>
                        <div class="data">
                            <div class="left">  	
                                <h3><?= $campaign->user_name ?> <a href="#"><i class="fa fa-envelope-o"></i></a></h3>
                                <div class="clearfix">
                                    <span class="location"><i class="fa fa-map-marker"></i><?= substr($campaign->location, 0, 10) . " ..." ?></span>
                                    <span class="time-stamp"><i class="fa fa-history"></i>Created  <?= date('M d, Y', strtotime($campaign->create_date)) ?></span>
                                </div>
                            </div>
                            <div class="right">
                                <div class="shares"><i class="fa fa-line-chart"></i><?php echo $shareCount; ?> <small>Shares</small></div>
                                <!-- <div class="likes"><i class="fa fa-thumbs-o-up"></i>68</div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab area -->
                <?php //var_dump($updates);exit; ?>

                <div class="tab-area">
                    <ul class="tabs">
                        <li class="active"><a href="javascript:void(0);" data-toggle="#tab1"><span>Story</span></a></li>
                        <li><a href="javascript:void(0);" data-toggle="#tab2"><span>Gallery</span></a></li>
                        <li><a href="javascript:void(0);" data-toggle="#tab3"><span>Updates <b class="badge"><?= count($updates) ?></b></span></a></li>
                        <li><a href="javascript:void(0);" data-toggle="#tab4"><span>Comments <b class="badge"></b></span></a></li>
                    </ul>
                    <div class="tab-data">
                        <div id="tab1" class="tab-contents">
                            <h4><?= htmlentities($campaign->title) ?></h4>
                            <p><?= substr(($campaign->description), 0, 110) ?></p>
                            <div class="row">
                                <?php
                                if ($campaign->video_file_name != '') {
                                    ?>
                                    <div class="col-sm-5">
                                        <?php if ($campaign->video_type == 1) { ?>
                                            <iframe width="300" height="190"
                                                    src="https://www.youtube.com/embed/<?= $campaign->video_file_name ?>">
                                            </iframe> 
                                        <?php } else { ?>
                                            <iframe src="//player.vimeo.com/video/<?= $campaign->video_file_name ?>" width="300" height="190" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                        <?php } ?></div>
                                <?php } ?>
                                <div class="col-sm-<?php
                                if ($campaign->video_file_name == NULL)
                                    echo 12;
                                else
                                    echo 7;
                                ?>">
                                    <p><?= nl2br(substr(($campaign->description), 110, 350)) ?></p>
                                </div>
                            </div>
                            <p><?= nl2br(substr(($campaign->description), 350)) ?></p>

                            <div class="footer">
                                <div class="row">
                                    <div class="col-sm-6 pull-right">
                                        <a href="javascript:void(0);" class="pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-mail-reply"></i> Report to Campaign</a>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <a href="javascript:;" class="button blue subscribe"><span>Subscribe to Updates</span></a>
                                <!--- <a href="#" class="button green"><span>Leave a Comment</span></a>--->
                                </div>
                            </div>
                        </div>
                        <!-- tab gallery -->
                        <div id="tab2" class="tab-contents">
                            <h4>Photos</h4>
                            <?php if (!empty($galleries)) { ?>
                                <div class="pic-slider">
                                    <?php
                                    foreach ($galleries as $gallery):
                                        ?>
                                        <div class="carousel-cell">
                                            <figure><img height="225" src="<?= base_url() ?>uploads/campaign/gallery_images/thumb/<?= $gallery->image_name ?>" alt="" /></figure>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php } else echo 'No image available!'; ?>

                            <!--                            <h4 class="mt30">Videos</h4>
                                                        <div class="pic-slider">
                                                            <div class="carousel-cell">
                                                                <figure><img src="images/data/profile-pic-04.jpg" alt="" /></figure>
                                                            </div>
                                                            <div class="carousel-cell">
                                                                <figure><img src="images/data/profile-pic-05.jpg" alt="" /></figure>
                                                            </div>
                                                            <div class="carousel-cell">
                                                                <figure><img src="images/data/profile-pic-04.jpg" alt="" /></figure>
                                                            </div>
                                                            <div class="carousel-cell">
                                                                <figure><img src="images/data/profile-pic-05.jpg" alt="" /></figure>
                                                            </div>
                                                        </div>-->
                            <div class="footer">
                                <div class="row">
                                    <div class="col-sm-6 pull-right">
                                        <a href="javascript:void(0);" class="pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-mail-reply"></i> Report to Campaign</a>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <a href="javascript:;" class="button blue subscribe"><span>Subscribe to Updates</span></a>
                                <!--- <a href="#" class="button green"><span>Leave a Comment</span></a>--->
                                </div>
                            </div>
                        </div>
                        <!-- /tab gallery -->
                        <?php //var_dump($users);exit;?>
                        <div id="tab3" class="tab-contents">
                            <div class="clearfix">
                            	<!---
                                <a href="<?= base_url() . 'create-update/' . $campaign->id ?>">Create Update</a>
                                ----->

                                <?php
                                $i = 0;
                                foreach ($updates as $update):
                                    $i++;
                                    ?>
                                    <article>
                                        <h2><?= time_elapsed_string(strtotime($update->create_date)) ?></h2>
                                        <div class="row">

                                            <?php
                                            if ($update->video_file_name != '') {
                                                ?>
                                                <div class="col-sm-5">
                                                    <?php if ($update->video_type == 1) { ?>
                                                        <iframe width="320" height="190"
                                                                src="https://www.youtube.com/embed/<?= $update->video_file_name ?>">
                                                        </iframe> 
                                                    <?php } else { ?>
                                                        <iframe src="//player.vimeo.com/video/<?= $update->video_file_name ?>" width="320" height="190" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                    <?php } ?> </div>
                                            <?php } ?>

                                            <div class="col-sm-<?php
                                            if ($update->video_file_name == NULL)
                                                echo 12;
                                            else
                                                echo 7;
                                            ?>">
                                                <h4><?= htmlentities($update->title); ?></h4>
                                                <p><?= nl2br(substr(($update->description), 0, 450)) ?></p></p>
                                            </div>

                                        </div>
                                        <p><?= nl2br(substr(($update->description), 450)) ?></p>
                                        <div class="narrator">
                                        	<?php foreach($users as $user){?>
                                            <figure>
                                            	<!---
                                            	<img src="<?= base_url(); ?>assets/images/data/user-02.jpg" alt="" />
                                            	--->
                                            	<img src="<?= base_url(); ?>uploads/campaign/gallery_images/thumb/<?php echo $user->image;?> " alt="" />
                                            	
                                            	</figure>
                                           <?php }?>
                                            
                                            <h3><a href="#"><?= $update->user_name; ?></a></h3>
                                            <div class="likes"><a href="javascript:;" onclick="markAsLove(<?= $update->id ?>);"><i class="fa fa-heart"></i> [ <span id="love<?= $update->id ?>"><?= $update->total_love; ?></span> ]</a></div>
                                        </div>
                                    </article>                                
                                <?php endforeach; ?>
                            </div>

                            <div class="footer">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="paging">
                                            <span>Showing  <?= $i ?> out of <?= $i ?> updates</span>
                                            <a href="#">See More</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="javascript:void(0);" class="pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-mail-reply"></i> Report to Campaign</a>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <a href="javascript:;" class="button blue subscribe"><span>Subscribe to Updates</span></a>

                                </div>
                            </div>
                        </div>

                        <div id="tab4" class="tab-contents">
                            <div id="fb-root"></div>


                            <div class="fb-comments" data-href="<?php echo base_url() . 'campaign/' . $cam_id; ?>" data-width="750"></div>

                            <?php
                            //if ($this->session->userdata('is_logged') == '') {
                                ?>
                                <!---
                                <div class="alert alert-info">You must be a logged in contributor to comment. <a href="<?= $login_url ?>">Log In</a></div>
                                ---->
                            <?php //} ?>

                            <div class="clearfix">
                                <?php //var_dump($comments);exit;?>
                                <?php
                                foreach ($comments as $comment):
                                    ?>
                                    <div class="media">
                                        <div class="media-left">
                                            <div class="user-pic"><figure><img src="<?= base_url(); ?>assets/images/data/user-02.jpg" alt="" /></figure></div>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><?= $comment->user_name ?> <small><?= htmlentities(time_elapsed_string(strtotime($comment->create_date))) ?></small> <a href="#" class="likes"><i class="fa fa-heart"></i> [ 12 ]</a></h4>
                                            <p><?= nl2br(htmlentities($comment->comments)) ?></p>
                                            <?php
                                            foreach ($comment->replies as $reply):
                                                ?>
                                                <div class="media">
                                                    <div class="media-left">
                                                        <div class="user-pic"><figure><img src="<?= base_url(); ?>assets/images/data/user-03.jpg" alt="" /></figure></div>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><?= $reply->user_name ?> <small><?= htmlentities(time_elapsed_string(strtotime($reply->reply_date))) ?></small></h4>
                                                        <p><?= nl2br(htmlentities($reply->reply_text)) ?></p>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                            <?php if ($comment->total_reply > 2) { ?>
                                                <p class="more-replies"><a href="#">See 2 more replies...</a></p>
                                            <?php } ?>

                                        </div>
                                    </div>

                                <?php endforeach; ?>

                            </div>

                            <div class="footer">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!---
                                        <div class="paging">
                                            <span>Showing  <?= count($comments) ?> out of <?= count($comments) ?> Comments</span>
                                            <a href="#">See More</a>
                                        </div>
                                        ---->
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="javascript:void(0);" class="pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-mail-reply"></i> Report to Campaign</a>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <a href="javascript:;" class="button blue subscribe"><span>Subscribe to Updates</span></a>
                                    <!---
                                    <a href="#" class="button green"><span>Leave a Comment</span></a>
                                    ---->
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab area -->
            </div>
            <!-- /left panel -->

            <!-- right panel -->
            <div class="col-sm-4">
                <div class="profile-right">
                    <h3><?= $campaign -> category_name?></h3>
                    <h2><?= $campaign->title ?></h2>
                    <p class="metadata"><span class="highlight"><?= $campaign->donor ?></span>Backers</p>
                    <p class="metadata">
                        <span class="highlight">$<?= $campaign->collected_fund ?> <small>USD</small></span>Pledged of $<?= format_num($campaign->fund_needed, 0) ?> goal
                        <span class="percent-circle">
                            <span class="c100 p<?php
                            $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                            if ($percentage >= 100)
                                echo 100;
                            else
                                echo $percentage;
                            ?>">
                                <span><?php
                                    $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                                    if ($percentage >= 100)
                                        echo '100';
                                    else
                                        echo $percentage;
                                    ?><small>%</small></span>
                                <b class="slice"><span class="bar"></span><span class="fill"></span></b>
                            </span>
                        </span>
                    </p>
                    <p class="metadata"><span class="highlight"><?php
                            $now = time(); // or your date as well
                            $your_date = strtotime(date('Y-m-d', strtotime($campaign->end_date_time)));
                            $datediff = $your_date - $now;
                            echo floor($datediff / (60 * 60 * 24) + 1);
                            ?></span>Days to go</p>

                    <div class="buttons clearfix">
                        <a href="<?= base_url() . 'donate/' . $campaign->slug ?>" class="donate-button"><span>Donate Now!</span></a>
                        <a href="https://www.facebook.com/dialog/share?app_id=<?php echo FB_APP_ID ?>&display=popup&href=<?php echo current_url(); ?>&redirect_uri=<?php echo current_url(); ?>" class="fb-share-button"><span>Facebook Share</span></a>
                        <form action="" method="post">
                            <input type="hidden" name="share_twitter" id="share_twitter" value="<?= $campaign->id ?>">
                            <a href="javascript:;" class="follow-tweets-button" id="shareB" onclick="shareTwitterlink()"><span>Follow Tweets</span></a>
                            <a style="display: none;" href="http://twitter.com/intent/tweet?original_referer=<?php echo current_url(); ?>&status=<?php echo current_url(); ?>&<?= $campaign->title ?>+<?php echo current_url(); ?>" class="follow-tweets-button" id="shareTw">
                                <span>Follow Tweets</span></a>
                        </form> 
                    </div>
<?php //var_dump($donations);exit;?>
                    <!-- recent donations -->
                    <div class="recent-donations">
                        <h2>Recent Donations <i class="fa fa-angle-down"></i></h2>
                        <?php
                        if (count($donations) > 0) {
                            foreach ($donations as $donation):
                                ?>
                                <section class="donation-box">
                                    <div class="char <?= substr(htmlentities($donation->user_name), 0, 1) ?>"><?= ucfirst(substr(htmlentities($donation->user_name), 0, 1)) ?></div>
                                    <div class="content">
                                        <h3><?= htmlentities($donation->user_name) ?></h3>
                                        <div class="time-stamp"><?= htmlentities(time_elapsed_string(strtotime($donation->create_date))) ?> 
                                        	<!---
                                        	<a href="#" class="love active"><i class="fa fa-heart"></i> [ 16 ]</a>
                                        	---->
                                        	</div>
                                        <p><?= nl2br(htmlentities($donation->comment)) ?></p>
                                        <div class="price-tag"><small>$</small><?= htmlentities($donation->amount) ?></div>
                                    </div>
                                </section>
                            <?php endforeach; ?>

                            <div class="paging">
                                <span>Showing  <?= count($donations) ?> out of <?= $donationsCount ?> Donations</span>
                                <a href="#">See More</a>
                            </div>

                            <?php
                        }
                        else {
                            ?>
                            <section class="donation-box">
                                <div class="char n">N</div>
                                <div class="content">
                                    <h3>No donation available yet!</h3>

                                </div>
                            </section>
                        <?php } ?>
                    </div>
                    <!-- /recent donations -->
                </div>
                <!-- /right panel -->
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Report to campaign</h4>
                </div>
                <div class="modal-body">
                    <div id="successMsg" class="alert"></div>
                    <form class="form-horizontal" role="form" onsubmit="return validation();">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="inputEmail3">Reason</label>
                            <div class="col-sm-10">
                                <textarea style="width:75%; height:30%;" name="reason" id="reason" placeholder="Write some reason" required=""></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="button blue">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- /content -->
    <input type="hidden" id="campaign-id" value="<?= $campaign->id ?>">
    <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <script>
        function validation()
        {
            var campaign_id = $("#campaign-id").val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "PublicCampaign/report",
                dataType: 'json',
                data: {
                    campaign_id: campaign_id,
                    reason: $("#reason").val(),
                    '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        //console.log(res);
                        if (res.code == 200)
                        {
                            $('#successMsg').html('Campaign reported successfully');
                            $('#successMsg').addClass('alert-success');
                            $("#reason").val('');

                        } else if (res.code == 201)
                        {
                            $('#successMsg').html('You have already reported this campaign.');
                            $('#successMsg').addClass('alert-info');
                        } else {
                            $('#successMsg').html('Login to report to campaign.');
                            $('#successMsg').addClass('alert-danger');
                        }
                        $("#csrf_token").val(res.token);

                    }
                }
            });

            return false;

        }

        function markAsLove(id)
        {

            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "CampaignController/markLove",
                dataType: 'json',
                data: {
                    campaign_update_id: id,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        //console.log(res);
                        if (res.code == 200)
                        {
                            $("#love" + id).html(parseInt($("#love" + id).html()) + parseInt(1));

                        } else if (res.code == 201)
                        {
                            $('#successMsg').html('You have already reported this campaign.');
                            $('#successMsg').addClass('alert-info');
                        } else {
                            $('#successMsg').html('Login to mark this update as loved.');
                            $('#successMsg').addClass('alert-danger');
                        }
                        $("#csrf_token").val(res.token);

                    }
                }
            });

            return false;
        }

        $(".subscribe").click(function () {
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "CampaignController/subscribe",
                dataType: 'json',
                data: {
                    campaign_id: $("#campaign-id").val(),
                    '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                	//alert(res);
                	//return false;
                    if (res)
                    {
                        console.log(res);
                        if (res.code == 200)
                        {
                            $(".subscribe").html("<span>Subscribed</span>");

                        } else if (res.code == 201)
                        {
                            $(".subscribe").html("<span>Already Subscribed</span>");
                        } else {
                            $(".subscribe").html("<span>Login to subscribe to thsi campaign</span>");

                        }
                        $("#csrf_token").val(res.token);

                    }
                }
            });
        });

        // $("#shareB").click(function () {
        // event.preventDefault();
        // event.stopPropagation();
        //     	
        // var data = $("id").val();
        //     	
        // $.ajax({
        // url: '<?php echo base_url(); ?> preview/:any',
        // type: 'POST',
        // data: {data:'id','<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
        //     				
        // sucess: function(data){
        // window.location.reload();
        // }
        // });
        // // });
    </script>
    <script>
        function shareTwitterlink() {
            //alert('sgggsdgdf');
            var twitterid = $('#share_twitter').val();
            //var check = '<?php echo base_url(); ?>'+"PublicCampaign/shareCount";
            //alert(check);
            //alert(twitterid);
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "PublicCampaign/shareCount",
                data: {
                    twitterid: twitterid,

                },
                success: function (res) {
                    console.log(res);
                    //alert(res);
                    var twUrl = $("#shareTw").attr('href');
                    //alert(twUrl);
                    window.location.assign(twUrl);

                }
            });

        }
    </script>
    <style>
        ._5lm5 _2pi3{display:none;}
    </style>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=490531827700527";
            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));


    </script>