<!-- content -->
<div id="content" class="page donate">
    <div class="container">
        <div class="appreciate-donation">
            <h3>Thank you!</h3>
            <h4>We appreciate your generous contribution!</h4>
            <p class="buttons"><a href="<?= base_url().'search'?>" class="button green"><span>Donate More!</span></a></p>
           <!-- <hr />
            <h4>Create an account to keep track of all your future donations</h4>
            <p class="buttons"><a href="<?= base_url().'dashboard'?>" class="button blue"><span>Create My Account</span></a></p>
            -->
        </div>
    </div>
</div>
<!-- /content -->