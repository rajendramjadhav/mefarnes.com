<!-- content -->
<?php //var_dump($campaign);exit;?>
<div id="content" class="page donate">
    <div class="donate-banner">
        <div class="container">
           <!-- <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-4"><img src="<?= base_url(); ?>uploads/campaign/gallery_images/thumb/<?= $campaign->image ?>" alt="" width="770" height="300" /></div>
                <div class="col-sm-6 col-md-7 col-lg-8">
                    <div class="donate-banner-right">
                        <h3>Community</h3>
                        <h2><?= $campaign->title ?></h2>
                        <div class="profile-data">
                            <figure><img src="<?= base_url() ?>uploads/campaign/gallery_images/thumb/<?= $campaign->user_image?>"/></figure>
                            <div class="content">
                                <div class="left">
                                    <h3><?= $campaign->user_name ?> <a href="#"><i class="fa fa-envelope-o"></i></a></h3>
                                    <span class="location"><i class="fa fa-map-marker"></i><?= $campaign->location ?></span>
                                </div>
                                <div class="right">

                                    <a href="#" class="likes"><i class="fa fa-thumbs-o-up"></i>68</a>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="metadata"><span class="highlight"><?= $campaign->donor ?></span>Backers</div>
                            <div class="metadata"><span class="highlight"><?php
                                    $now = time(); // or your date as well
                                    $your_date = strtotime(date('Y-m-d', strtotime($campaign->end_date_time)));
                                    $datediff = $your_date - $now;
                                    echo floor($datediff / (60 * 60 * 24) + 1);
                                    ?></span>Days to go</div>
                            <div class="metadata"><span class="highlight">$<?= $campaign->collected_fund ?>  <small>USD</small></span>Pledged of $<?= number_format($campaign->fund_needed, 0) ?> goal</div>
                            <div class="percent-circle">
                                <span class="c100 p<?php $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                                    if ($percentage >= 100) echo '100';
                                    else echo $percentage; ?>">
                                    <span><?php $percentage = intval(($campaign->collected_fund * 100) / $campaign->fund_needed);
                                    if ($percentage >= 100) echo 100;
                                    else echo $percentage; ?><small>%</small></span>
                                    <b class="slice"><span class="bar"></span><span class="fill"></span></b>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
<?php if ($this->session->flashdata('message') != ''): ?>
        <div class="alert alert-success"> <?= $this->session->flashdata('message') ?> </div>
<?php endif;
?>

    <div class="alert alert-danger" style="display: none;"></div>
    <div class="container">
        <div class="tab-area">
            <ul class="tabs">
                <li class="one active">Contribution <span><i class="icon-check"></i></span></li>
                <li class="two">Personal Details <span><i class="icon-check"></i></span></li>
                <li class="three">Payment Option <span><i class="icon-check"></i></span></li>
            </ul>
<?php //echo form_open(base_url('create-donation'), array('id' => 'crete-donation'));   ?> 
            <div class="tab-data">
                <div id="tab1" class="tab-contents">
                    <div class="row">
                        <div class="col-sm-5 col-md-4"><img src="<?= base_url(); ?>assets/images/data/donate-contribute.jpg" alt="" /></div>
                        <div class="col-sm-7 col-md-8">
                            <div class="form">
                                <div class="info">&nbsp;</div>
                                <h2>Your Contribution</h2>
                                <div class="textbox">
                                    <span>$</span>
                                    <input type="text" value="5.00" name="amount" autocomplete="off" id="amount" required="" onchange="setVal();" />
                                    <br/><span id="min_amount_error" style="display:none;color:red;font-size:13px;width:180px;float:right;">Minimun Donate Amount is $5</span>
                                </div>
                                <div class="buttons">
                                    <a href="javascript:void(0);" id="continue_step_1" class="button blue continue-step-1 btn"><span>Continue</span></a>
                                </div>
                                <div class="text-right"><!---By continuing, you agree with the Mafernas terms and privacy policy.--->
You are agreeing with all <a target="_blank" href="<?= base_url() . 'terms' ?>" class="text-default">terms</a> and <a target="_blank" href="<?= base_url() . 'privacy' ?>" class="text-default">privacy policy</a></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tab2" class="tab-contents">
                    <div class="row">
                        <div class="col-sm-5 col-md-4"><img src="<?= base_url(); ?>assets/images/data/donate-personal.jpg" alt="" /></div>
                        <div class="col-sm-7 col-md-8">
                            <div class="form">
                                <div class="info">Your Contribution : $<span class="valueToSet">5.00</span></div>
                                <h2>Personal Details</h2>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="" id="namel" for="txtfname">First Name on Credit Card<span style="color:red;display:inline;">*</span> <span>[ Please Enter First Name ]</span></label>
                                        <input type="text" id="name" name="name" class="" onkeyup="validateFname();" />
                                    </div>
                                    <div class="col-md-6">
                                        <label for="txtlname" id="lnamel">Last name on Credit Card<span style="color:red;display:inline;">*</span> <span> <span>[ Please Enter Last Name ]</span></label>
                                        <input type="text" id="lname" name="lname" onkeyup="validateLname();" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtemail" id="emaill">Your Email Address<span style="color:red;display:inline;">*</span> <span> <span>[ Please Enter Correct Email Address ]</span></label>
                                        <input type="text" id="email" name="email" onkeyup="validateEmail();" autocomplete="off" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">                                        
                                        <label for="country" id="countryl">Country <span>[ *Please Enter country ]</span></label>
                                        <input type="text" name="country" id="country" />
                                    </div> 
                                    <div class="col-md-6">
                                        <label for="txtpostcode" id="postal_codel">Postal Code<span style="color:red;display:inline;">*</span> <span>[ Please Enter Postal Code ]</span></label>
                                        <input type="text" id="postal_code" name="postal_code" onkeyup="validatePostal_code()"/>
                                    </div>
                                </div>

                                <h2>Privacy Option</h2>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="select-box">
                                            <select class="fancy-selectbox" name="is_public" id="is_public">
                                                <option value="1">Make donation public</option>
                                                <option value="0">Make donation private</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="buttons">
                                    <a href="javascript:void(0);" class="button back-step-2 btn"><span>Back</span></a>
                                    <a href="javascript:void(0);" id="continue_step_2"  class="button blue continue-step-2 btn disabled"><span>Continue</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tab3" class="tab-contents">

                    <div class="row">
                        <div class="col-sm-5 col-md-4"><img src="<?= base_url(); ?>assets/images/data/donate-payment.jpg" alt="" /></div>
                        <div class="col-sm-7 col-md-8">
                            <div class="form">
                                <div class="info">Your Contribution : $<span class="valueToSet">5.00</span></div>
                                <h2>Payment Option</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="cardnumber" id="cc-numberl">Credit or Debit Number<span style="color:red;display:inline;">*</span> <span>[ Please Enter Card Number ]</span></label>
                                        <input type="text" id="cc-number" placeholder="XXXX XXXX XXXX XXXX" name="cc-number" onkeyup="validatecc_number();" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="cc-month" id="cc-monthl">Expiration Date<span style="color:red;display:inline;">*</span> <span>[ Please Enter Expiration Date ]</span></label>
                                        <input type="text" id="cc-expire" class="date-mask" data-inputmask="'alias': 'm/y', 'placeholder': 'MM/YYYY'" name="cc-expire" onkeyup="validatecc_month();" />                                        
                                    </div>
                                    <div class="col-md-6 relpos">
                                        <label for="ccv" id="cc-cvvl">Security Code<span style="color:red;display:inline;">*</span> <span>[ Please Enter Security Code ]</span></label>
                                        <input type="text" id="cc-cvv" placeholder="XXX" name="cc-cvv" onkeyup="validatecc_cvv();" />
                                        <span class="ccv"><i class="icon-ccv"></i></span>
                                    </div>
                                    <div class="col-md-6" style="float:right;">
                                        <span class="form-info">Enter 3-digits on back of card. AmEx uses front 4-digit code.</span>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea rows="2" cols="10" name="comment" id="comment" placeholder="Leave a comment"></textarea>
                                    </div>
                                </div>
                                <div class="loading pull-right" style="display: none; margin-left:7%; margin-top:-2%;"><img src="<?= base_url() . 'assets/images/ajax-loader.gif' ?>" /></div>
                                <div class="buttons">
                                    <a href="javascript:void(0);" class="button back-step-3 btn"><span>Back</span></a>
<!--                                    <a href="javascript:void(0);" class="button blue finalize-steps btn"><span>I’m Done</span></a>-->
                                    <input style="background-color:#FFF" type="submit" name="Submit" value="Submit" id="cc-submit" class="button blue finalize-steps btn disabled">
                                      <div style="display:none" class="alert alert-danger"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
<?php //echo form_close();    ?>
        </div>
    </div>
</div>
<!-- /content -->
<style>
#cc-submit:hover { color:#fff; background-color:#0064d2 !important; }
</style>

<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

<input type="hidden" id="campaign_id" value="<?= $campaign->id ?>">
<script>
    function setVal()
    {   var min_donate = 5;
        if ((document.getElementById("amount").value) <= (min_donate - 1))
         {
            document.getElementById("min_amount_error").style.display = "block";
            document.getElementById("amount").value = "";
            $('#continue_step_1').addClass("disabled");               
         } 
         else{
            document.getElementById("min_amount_error").style.display = "none";
            $('#continue_step_1').removeClass("disabled");
         };
        var amount = $("#amount").val();
        $(".valueToSet").html(amount);
    }

    function validateFname() { 

        var v_temp=$("#email").val();
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        var check_email=pattern.test(v_temp);
    
        if (document.getElementById("name").value) {
            $('#name').removeClass("error");
            $('#namel').removeClass("error");            
        } else{
            $('#name').addClass("error");
            $('#namel').addClass("error");
        };
        if (document.getElementById("name").value && document.getElementById("lname").value && document.getElementById("email").value && check_email && document.getElementById("postal_code").value) {
             $('#continue_step_2').removeClass("disabled");
        } else{
            $('#continue_step_2').addClass("disabled");
        };        
    }
    function validateLname() {     

        var v_temp=$("#email").val();
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        var check_email=pattern.test(v_temp);

        if (document.getElementById("lname").value) {
            $('#lname').removeClass("error");
            $('#lnamel').removeClass("error");            
        } else{
            $('#lname').addClass("error");
            $('#lnamel').addClass("error");
        };
        if (document.getElementById("name").value && document.getElementById("lname").value && document.getElementById("email").value && check_email && document.getElementById("postal_code").value) {
             $('#continue_step_2').removeClass("disabled");
        } else{
            $('#continue_step_2').addClass("disabled");
        };        
    }
    function validateEmail() {    
        var v_temp=$("#email").val();
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        var check_email=pattern.test(v_temp);

        if (document.getElementById("email").value && check_email) {
            $('#email').removeClass("error");
            $('#emaill').removeClass("error");            
        } else{
            $('#email').addClass("error");
            $('#emaill').addClass("error");
        };
        if (document.getElementById("name").value && document.getElementById("lname").value && document.getElementById("email").value && check_email && document.getElementById("postal_code").value) {
             $('#continue_step_2').removeClass("disabled");
        } else{
            $('#continue_step_2').addClass("disabled");
        };        
    }
    function validatePostal_code() {     
        if (document.getElementById("postal_code").value) {
            $('#postal_code').removeClass("error");
            $('#postal_codel').removeClass("error");            
        } else{
            $('#postal_code').addClass("error");
            $('#postal_codel').addClass("error");
        };
        if (document.getElementById("name").value && document.getElementById("lname").value && document.getElementById("email").value) {
             $('#continue_step_2').removeClass("disabled");
        } else{
            $('#continue_step_2').addClass("disabled");
        };        
    }
    function validatecc_number() {     
        if (document.getElementById("cc-number").value) {
            $('#cc-number').removeClass("error");
            $('#cc-numberl').removeClass("error");            
        } else{
            $('#cc-number').addClass("error");
            $('#cc-numberl').addClass("error");
        };
        if (document.getElementById("cc-number").value && document.getElementById("cc-expire").value && document.getElementById("cc-cvv").value) {
             $('#cc-submit').removeClass("disabled");
        } else{
            $('#cc-submit').addClass("disabled");
        };                
    }
    function validatecc_month() {     
        if (document.getElementById("cc-expire").value) {
            $('#cc-expire').removeClass("error");
            $('#cc-monthl').removeClass("error");            
        } else{
            $('#cc-expire').addClass("error");
            $('#cc-monthl').addClass("error");
        };
        if (document.getElementById("cc-number").value && document.getElementById("cc-expire").value && document.getElementById("cc-cvv").value) {
             $('#cc-submit').removeClass("disabled");
        } else{
            $('#cc-submit').addClass("disabled");
        };                 
    }
    // function validatecc_year() {     
    //     if (document.getElementById("cc-year").value) {
    //         $('#cc-year').removeClass("error");
    //         $('#cc-yearl').removeClass("error");            
    //     } else{
    //         $('#cc-year').addClass("error");
    //         $('#cc-yearl').addClass("error");
    //     };
    //     if (document.getElementById("cc-number").value && document.getElementById("cc-month").value && document.getElementById("cc-cvv").value) {
    //          $('#cc-submit').removeClass("disabled");
    //     } else{
    //         $('#cc-submit').addClass("disabled");
    //     };                 
    // }
    function validatecc_cvv() {     
        if (document.getElementById("cc-cvv").value) {
            $('#cc-cvv').removeClass("error");
            $('#cc-cvvl').removeClass("error");            
        } else{
            $('#cc-cvv').addClass("error");
            $('#cc-cvvl').addClass("error");
        };
        if (document.getElementById("cc-number").value && document.getElementById("cc-expire").value && document.getElementById("cc-cvv").value) {
            $('#cc-submit').removeClass("disabled");
        } else{
            $('#cc-submit').addClass("disabled");
        };                 
    }
</script>

<script type="text/javascript" src="https://static.wepay.com/min/js/tokenization.v2.js"></script>
<script type="text/javascript">




    (function () {
        //WePay.set_endpoint("stage"); // change to "production" when live
        WePay.set_endpoint("production");
        // Shortcuts
        var d = document;
        d.id = d.getElementById,
                valueById = function (id) {
                    return d.id(id).value;
                };

        // For those not using DOM libraries
        var addEvent = function (e, v, f) {
            if (!!window.attachEvent) {
                e.attachEvent('on' + v, f);
            }
            else {
                e.addEventListener(v, f, false);
            }
        };

        // Attach the event to the DOM
        addEvent(d.id('cc-submit'), 'click', function () {
            var userName = [valueById('name')].join(' ');
            var str = valueById('cc-expire');
            var cc_month = str.substring(0, 2);
            //var str = valueById('cc-expire');
            var cc_year = str.substring(3, 7);
            response = WePay.credit_card.create({
                "client_id": '<?= wepay_client_id ?>',
                "user_name": valueById('name') + ' ' + valueById('lname'),
                "email": valueById('email'),
                "cc_number": valueById('cc-number'),
                "cvv": valueById('cc-cvv'),
                "expiration_month": cc_month,
                "expiration_year": cc_year,
                "address": {
                    "postal_code": valueById('postal_code')
                }
            }, function (data) {
                if (data.error) {
                    //console.log(data);
                    console.log(data.error_description);
                    $(".alert-danger").show();  
                    $(".alert-danger").html(data.error_description);
                    $(".alert-danger").show();
                                                  setTimeout('$(".alert-danger").hide()',1500);

                    // handle error response
                } else {
                    //console.log(data.credit_card_id);
                    $("#cc-submit").val('Loading...');
                    $("#cc-submit").attr('disabled', 'disabled');
                    $('.loading').show();
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>" + "donation/checkout",
                        dataType: 'json',
                        data: {
                            campaign_id: $("#campaign_id").val(),
                            card_id: data.credit_card_id,
                            '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val(),
                            amount: $("#amount").val(),
                            comment: $("#comment").val(),
                            is_public: $("#is_public").val(),
                            "donor_first_name": $("#name").val(),
                            "donor_last_name": $("#lname").val(),
                            "email": $("#email").val(),
                            "country": $("#country").val(),
                            "postal_code": $("#postal_code").val(),
                            type: data.state
                        },
                        success: function (res) {                             
                            if (res) 
                            {   
                                console.log(res);
                                if (res.code == 200)
                                {
                                    window.location.href = res.redirect_uri

                                }else{
                                    $("#csrf_token").val(res.token);
                                    $("#cc-submit").val('Submit');
                                    $("#cc-submit").removeAttr('disabled');
                                    $('.loading').hide();
                                    $(".alert-danger").hide();
                                    alert("Unable to charge credit card. Wrong credit card details.");
                                } 
                            }
                            
                        }
                    });
                    // call your own app's API to save the token inside the data;
                    // show a success page
                }
            });
        });

    })();
</script>
<script type="text/javascript">
  $(function() {
    //$('#cc-expire').mask('00/0000');
    $('#cc-cvv').mask('0000');
    $('#cc-number').mask('0000-0000-0000-0000');
    $('#postal_code').mask('000000');
   
  });
</script>
<script>
$(document).ready(function(){
    $(":input").inputmask();
    Inputmask.extendDefinitions({
    M: {
        validator: "0[1-9]|1[012]",
        cardinality: 2,
        placeholder: 'm',
        prevalidator: [{
            validator: function (chrs, maskset, pos, strict, opts) {
                var isNumeric = new RegExp("[0-9]");
                if(!isNumeric.test(chrs)) return false;
                if(chrs > "1") {
                    maskset.buffer[pos] = "0";
                    return {
                        "pos": pos+1,
                        "c": chrs,
                    };
                }
                else return true;
            },
            cardinality: 1
        }]
    },
    Y: {
        validator: "\\d{2}",
        cardinality: 2,
        placeholder: 'y',
    }
});
});
</script>