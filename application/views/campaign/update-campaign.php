<!-- content -->
<div id="content" class="page create-campaign">
    <div class="container">
        <div class="row">
            <h2>Create your campaign</h2>
            <h3>Make a great first impression with your campaign’s title and image,<br class="hidden-xs" />and set your funding goal, campaign duration, and  category.</h3>
            <?php if ($error != '') { ?>
                <div class="alert alert-danger"><?php echo $error; ?></div>
            <?php } ?>
            <div class="tab-area">
                <ul class="tabs">
                    <li class="active one">Basic Info</li>
                </ul>

                <div class="tab-data">
                    <section class="step-1">
                        <div class="row">
                            <!-- left panel -->
                            <div class="col-sm-7 col-md-8">

                                <?php echo form_open_multipart(base_url('create-campaign/' . $campaign->slug), array('id' => 'crete-campaign')); ?> 
                                <div class="form">
                                    <div class="alert alert-danger" style="display:none;">test</div>
                                    <div class="form-row">
                                        <h4>Picture</h4>
                                        <label>Personalize your campaign by adding a picture.</label>
                                        <a href="javascript:;" class="upload-box" id="OpenImgUpload">Choose an image from your computer</a>
                                        <input type="file" id="campaignPics" name="campaignPics" style="display: none;">
                                    </div>

                                    <div class="form-row one-third">
                                        <h4>Category</h4>
                                        <label>Help people discover your compaign on our site.</label>
                                        <select class="fancy-selectbox" name="category" id="category" required="">
                                            <option value="">Please select category</option>
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?= $category->id ?>" <?php if ($category->id == $campaign->campaign_categoriy_id) echo 'selected'; ?>><?= $category->name ?></option>;
                                            <?php endforeach;
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-row">
                                        <h4>Title</h4>
                                        <label>What do you want to raise money for?</label>
                                        <div class="input-group">
                                            <input type="text" id="title" name="title" class="form-control" value="<?php echo $campaign->title; ?>" placeholder="My Campaign Title" required="">
                                            <span class="input-group-addon"></span>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <h4>Story</h4>
                                        <label>Why this campign is for?</label>
                                        <div class="input-box">
                                            <script src="<?php echo base_url('assets/ckeditor/ckeditor.js'); ?>"></script>
                                            <textarea name="campaign_story" cols="108" rows="5" id="campaign_story" placeholder="Why this campign is for?"><?php echo $campaign->description; ?></textarea>
                                            <script>
                                                CKEDITOR.replace( 'campaign_story' );
                                            </script>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <h4>Video <small>(optional)</small></h4>
                                        <label>Personalize your campaign by adding a Video.</label>
                                        <p><label>Use External video links</label></p>
                                        <input type="text" id="video_link" onChange="nextField(this);" value="<?php echo $campaign->video_url; ?>" name="video_link" placeholder="Youtube or Vimeo URL" >
                                    </div>

                                    <div class="form-row">
                                        <h4>Location</h4>
                                        <label>Get support from your local community.</label>
                                        <input type="text" id="location" value="<?php echo $campaign->campaign_location_id; ?>" name="location" placeholder="location" >
                                    </div>

                                    <div class="form-row">
                                        <h4>Goal</h4>
                                        <label>How much do you want to raise?</label>
                                        <div class="input-group left">
                                            <span class="input-group-addon">$</span>
                                            <input type="number" name="fund_needed" id="fund_needed" value="<?php echo $campaign->fund_needed; ?>" required="" class="form-control" placeholder="1234" >
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <h4>Duration</h4>
                                        <label>How long do you want to raise fund?</label>
                                        <div class="input-box">
                                            <div class="days-container"><span class="days"><?php
                                                    $now = time(); // or your date as well
                                                    $your_date = strtotime(date('Y-m-d', strtotime($campaign->end_date_time)));
                                                    $datediff = $your_date - $now;
                                                    echo floor($datediff / (60 * 60 * 24) + 1);
                                                    ?></span></div>
                                            <div class="slider-bar">
                                                <b>1 Day</b>
                                                <div id="day-slider" class="norange"></div>
                                                <b>60 Days</b>
                                            </div>
                                        </div>
                                        <input type="hidden" name="campaign_duration" id="campaign_duration" value="<?php
                                        $now = time(); // or your date as well
                                        $your_date = strtotime(date('Y-m-d', strtotime($campaign->end_date_time)));
                                        $datediff = $your_date - $now;
                                        echo floor($datediff / (60 * 60 * 24) + 1);
                                        ?>">
                                    </div>
                                    <div class="form-row">
                                        <div class="buttons">
                                            <input type="button" onclick="formValidation();" class="button blue" name="create-campaign" value="Save &amp; Preview">
<!--                                            <a href="javascript:void(0);" class="button"><span>Discard Changes</span></a>-->
                                            <input type="reset" class="button" value="Discard Changes">
                                        </div>
                                    </div>
                                </div>	

                                <?php echo form_close(); ?>
                            </div>
                            <!-- /left panel -->

                            <!-- right panel -->
                            <div class="col-sm-5 col-md-4">
                                <div class="right-panel">
                                    <h2>Preview</h2>
                                    <h3>Your profile will look like this.</h3>
                                    <div class="campaign-preview-box">
                                        <img src="<?php echo base_url(); ?>assets/images/data/campaign-01.jpg" alt="" />
                                        <span class="data">
                                            <span class="category-name">Community</span>
                                            <span class="category-title">My Campaign Title</span>
                                            <span class="category-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ...</span>
                                            <span class="user-info clearfix">
                                                <img src="<?php echo base_url(); ?>assets/images/data/user-04.jpg" alt="" />
                                                <span class="content">By <a href="#">Mary Lee</a><br />Los Angeles, CA</span>
                                            </span>
                                            <span class="category-meta">
                                                <span>Raised<b>$0</b></span>
                                                <span>Backers<b>0</b></span>
                                                <span>Days Left<b>30</b></span>
                                            </span>
                                        </span>
                                    </div>
                                    <p>
                                        <a href="#" class="support-btn">
                                            <span class="left"><i class="fa fa-lightbulb-o"></i></span>
                                            <span class="right">
                                                <span class="header">Need Help!</span>
                                                <span class="content">Let’s our expert help you to create your best campaign ever!</span>
                                            </span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <!-- /right panel -->
                        </div>
                    </section>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- /content -->
<script>
    $(document).ready(function () {
        $('#OpenImgUpload').click(function () {
            $('#campaignPics').trigger('click');
        });
    });

    function formValidation()
    {

        if ($("#campaignPics").val() != '')
        {
            //return false;
            var val = $("#campaignPics").val();
            // alert(val); //return false;
            if (!val.match(/(?:gif|jpg|png|bmp|jpeg)$/)) {
                // inputted file path is not an image of one of the above types
                var txt = "Please select image file only!";
                $(".alert-danger").html(txt);
                $(".alert-danger").slideDown('slow');
                $("#campaignPics").val('');
                return false;
            }
            else {
                if ($("#category").val() == '')
                {

                    var txt = "Please select campaign category!";
                    $(".alert-danger").html(txt);
                    $(".alert-danger").slideDown('slow');
                    $("#category").focus();
                    return false;
                }
                else if ($("#title").val() == '')
                {
                    var txt = "Please input campaign name!";
                    $(".alert-danger").html(txt);
                    $(".alert-danger").slideDown('slow');
                    $("#title").focus();
                    return false;
                }
                else if ($("#fund_needed").val() == '')
                {
                    var txt = "Please input amount you want to raise!";
                    $(".alert-danger").html(txt);
                    $(".alert-danger").slideDown('slow');
                    $("#fund_needed").focus();
                    return false;
                }



            }
        }
        $("#campaign_duration").val($(".days").html());
        $("#crete-campaign").submit();
    }
</script>

<style type="text/css">
    body {
        font-family: sans-serif;
        font-size: 14px;
    }
</style>

<title>Google Maps JavaScript API v3 Example: Places Autocomplete</title>
<script src="https://maps.googleapis.com/maps/api/js??v=3.13&libraries=places&key=AIzaSyBgS4A2KbRY159HA-HDIRxaoanaYQscQy8" type="text/javascript"></script>
<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>