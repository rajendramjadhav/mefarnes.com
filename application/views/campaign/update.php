<style>
	.blue:hover{
		background: #0064d2!important;
		color : white;
		
	}

</style>
<!-- content -->
<?php //var_dump($users);exit;?>
<div id="content" class="page dashboard create-updates">
    <div class="dashboard-header-sm">
        <div class="container">
            <div class="clearfix">
                <h2>Create Updates</h2>
            </div>
        </div>
    </div>

    <div class="contents">
        <div class="container">
            <div class="form">
                <div class="form-row">
                    <h4>Campaign Name</h4>
                    <div class="input-group">
                        <input type="text" class="form-control" value="<?= $campaign->title ?>" disabled="" />
                        <span class="input-group-addon"><i class="fa fa-ban"></i></span>
                    </div>
                </div>

                <div class="form-row">
                    <h4>Picture</h4>
                    <figure><img src="<?php echo base_url() . 'uploads/campaign/gallery_images/thumb/' . $campaign->image; ?>" alt="" /></figure>
                </div>

                <?php echo form_open_multipart(base_url('create-update/' . $campaign->id), array('id' => 'post-update')); ?> 
                <div class="form-row">
                    <h4>Title</h4>
                    <input type="text" name="title" required="" class="form-control title-text" value='' />
                </div>

                <div class="form-row">
                    <h4>Description</h4>
                    <div class="input-box">
                        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                        <script>
                            tinymce.init({
                                selector: 'textarea',
                                //toolbar: 'fontsizeselect',
                                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
                                font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
                                forced_root_block: 'p',
                                content_css: "<?= base_url() ?>assets/css/content.css",
                                menu: {
                                    file: false,
                                    edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
                                    insert: {title: 'Insert', items: 'link media | template hr'},
                                    view: false,
                                    //format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
                                    table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
                                    //tools: {title: 'Tools', items: 'spellchecker code'}
                                }
                            });
                        </script>
                        <textarea name="description" id="description"></textarea>
                    </div>
                </div>
<!---
                <div class="form-row">
                    <h4>Video <small>(optional)</small></h4>
                    <label>Please add an external (Youtube or Vimeo) video links</label>
                    <input type="text" id="video_link" onChange="nextField(this);" value="<?php echo set_value('video_link'); ?>" name="video_link" placeholder="Youtube or Vimeo URL" >
                </div>
------>

                <div class="form-row">
                    <div class="buttons">
<!--                        <a href="javascript:void(0);" class="button blue continue-section-1"><span>Add Update</span></a>
                        <a href="javascript:void(0);" class="button"><span>Discard Changes</span></a>
                        -->
                        <input type="submit" style="background: #fff;" class="button blue continue-section-1" value="Add Update" name="update">
                        <!--<input type="reset" class="button" value="Discard Changes">-->
                       <a href="javascript:void(0);" onclick="fun_backbutton();" class="button"><span>Discard Changes</span></a>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- /content -->
<script type="text/javascript">
	function fun_backbutton()
	{
		window.location.href='<?php echo base_url(); ?>dashboard';	
	}
</script>