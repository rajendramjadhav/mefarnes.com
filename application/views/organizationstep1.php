<div id="content" class="page signup">
    <div class="container">
        <div class="row">
        	<form name="frm_singup1" id="frm_singup1" method="post" action="<?php echo base_url(); ?>auth/saveorgsignupstep1">
            <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<section class="section_organization">
                    <h2>Signup as Organization</h2>
                    <!--<h3>Lorem ipsum eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>-->
                    <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                    <div class="signup-form-box">
                        <div class="col-left orange"><i class="fa fa-envelope"></i></div>
                        <div class="col-right">
                            <div class="signup-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtorgname">Organization name <span>[ *Please Enter Organization name ]</span></label>
                                        <input type="text" id="org_name" name="org_name" required="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtemail_org">Your Email Address <span>[ *Please Enter Email Address ]</span></label>
                                        <input type="email" id="email_org" name="email_org" required=""/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtcity_org">City <span>[ *Please Enter City ]</span></label>
                                        <input type="text" id="city" name="city" required="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtpassword_org">Set Password <span>[ *Please Enter Password ]</span></label>
                                        <input type="password" id="pssword_org" name="pssword_org" required="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="checkbox" id="chkbox" checked="checked" class="fancy-checkbox" /><label for="chkbox">Receive our weekly newsletter and other occasional updates</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--<a href="javascript:frm_submit();" class="button blue continue-step-1"><span>Continue</span></a>-->
                                        <button type="submit" name="save" class="button blue continue-step-1">
                                        	<span>Continue</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"> 
                                        <label>You are agreeing with all <a target="_blank" href="<?= base_url() . 'terms' ?>" class="text-default">terms</a> and <a target="_blank" href="<?= base_url() . 'privacy' ?>" class="text-default">privacy policy</a>.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>        
            </form>
        </div>
    </div>
</div> 
<script src="https://maps.googleapis.com/maps/api/js??v=3.13&libraries=places&key=AIzaSyBgS4A2KbRY159HA-HDIRxaoanaYQscQy8" type="text/javascript"></script>
<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('txtcity');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('city');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('full_address');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
$("body").on("blur", "#email_org", function () {
    var emailidcheck = $('#email_org').val();
		$.ajax({
		  type: "POST",
		  url: "<?php echo base_url(); ?>home/checkemailduplicate",          
		  data: {'emailidcheck':emailidcheck,'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'},
			success: function(total){
				if(total == 5){
					alert("Your email id is allready exist");
					$('#email_org').val('');
					$('#email_org').focus();
				}
			}
				

    });
  });
 
</script>
