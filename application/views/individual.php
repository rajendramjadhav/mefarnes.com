<div id="content" class="page signup">
    <div class="container">
        <div class="row">
        	<!-- individual signup panel : section 1 -->
            <form name="frm_individual" id="frm_individual" method="post" action="">
            <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                <section class="section_individual">
                    <h2>Signup as Individual</h2>
                    <h3><!---Lorem ipsum eiusmod tempor incididunt ut labore et dolore magna aliqua.-->Select your preference</h3>
                    <div class="signup-box">
                        <div class="or">OR</div>
                        <div class="col">
                            <h2>Use My Facebook</h2>
                
                            <div class="buttons"><a onclick="return commingsoon()" href="javascript:fun_redirect('facebook');" class="fb-blue"><i class="fa fa-facebook"></i></a></div>
                            <p>We'll never post anything on Facebook without your permission.</p>
                
                        </div>
                        <div class="col">
                            <h2>Use My Email Address</h2>
                            <div class="buttons"><a href="javascript:fun_redirect('useemail');" class="green individual-signup-mail"><i class="fa fa-envelope"></i></a></div>
                            <p>You are agreeing with all <a target="_blank" href="<?= base_url() . 'terms' ?>">terms</a> and <a target="_blank" href="<?= base_url() . 'privacy' ?>">privacy policy</a>.</p>
                        </div>
                    </div>
                    <p class="bbb"><i class="icon-bbb"></i></p>
                </section>
            </form>
            <!-- /individual signup panel : section 1 -->
       </div>
    </div>
</div> 
<script type="text/javascript">
        function commingsoon(){
               alert("Coming soon...");return false;
        }
	function fun_redirect(usertext)
	{
		var frm_obj = window.document.frm_individual;
		
		if(usertext == 'facebook')
		{
			frm_obj.action = "<?php echo base_url(); ?>login";
			frm_obj.submit();
		}
		if(usertext == 'useemail')
		{
			frm_obj.action = "<?php echo base_url(); ?>individualusersignup";
			frm_obj.submit();
		}
	}
</script>