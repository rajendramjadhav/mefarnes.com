<div id="content" class="page ">
    <div class="container">
        <div style="text-align:center" class="row"><br><br>
            <h2 style="color:#99cc00">Verify your information</h2>
              <h3>In order to withdraw your funds, all you need to verify some basic personal details.</h3>
              <div id="withdrawal_div"></div>
              <script type="text/javascript" src="https://www.wepay.com/min/js/iframe.wepay.js">
              </script>

              <script type="text/javascript">
                 WePay.iframe_checkout("withdrawal_div", "<?php echo $uri; ?>");
             </script>
        </div>
    </div>
</div>