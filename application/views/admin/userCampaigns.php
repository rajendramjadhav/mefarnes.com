<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Users</h4>
                                <p class="category">Registered users with this site</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>First Name</th>
                                    	<th>Last Name</th>
                                    	<th>Email</th>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;foreach($users as $row){?>
                                        <tr>
<td><?php echo $i;?></td>
<td><?php echo $row->first_name;?></td>
<td><?php echo $row->last_name;?></td>

<td><div id="getCampaigns" onClick="getCampaigns(<?php echo $row->id;?>);">View Campaigns</div></td>


                                        </tr>
                                    <?php $i++;}?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
function getCampaigns(id){
window.location.href = "<?php echo base_url();?>admin/dashboard/getUserCampaigns/"+id;

}
</script>