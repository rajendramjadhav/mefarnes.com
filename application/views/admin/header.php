<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
<meta name="author" content="Discoveronwheels">

<!-- The styles -->
<link id="bs-css" href="<?php echo base_url(); ?>assets/css/admin/css/bootstrap-cerulean.css" rel="stylesheet">
<style type="text/css">
body {
	padding-bottom: 40px;
}
.sidebar-nav {
	padding: 9px 0;
}
</style>
<link href="<?php echo base_url(); ?>assets/css/admin/css/bootstrap-classic.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/admin/css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/admin/css/charisma-app.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/admin/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
<link href='<?php echo base_url(); ?>assets/css/admin/css/fullcalendar.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/chosen.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/uniform.default.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/colorbox.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/jquery.cleditor.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/jquery.noty.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/noty_theme_default.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/elfinder.min.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/elfinder.theme.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/opa-icons.css' rel='stylesheet'>
<link href='<?php echo base_url(); ?>assets/css/admin/css/uploadify.css' rel='stylesheet'>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.png" />
</head>

<body>