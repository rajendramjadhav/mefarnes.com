<div class="row">
    <div class="col-md-4">
        <div class="box box-default box-solid">
            <div class="box-header with-border"> <i class="fa fa-rocket"></i>
                <h3 class="box-title">System snapshot </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <!-- <p>Add the classes <code>.btn.btn-app</code> to an <code>&lt;a></code> tag to achieve the following:</p>--> 
                <a class="btn btn-app"><span class="badge bg-yellow"></span> <i class="fa fa-coffee"></i> New campaign today </a>
            </div>
            <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
    </div>
    <div class="col-md-4">
        <div class="box box-default box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">New Campaigns</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin tablesaw" data-tablesaw-mode="columntoggle" >
                        <thead>
                            <tr>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Title</th>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="4">Created By</th>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Raised funds</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive --> 
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix"> <a href="<?= base_url('admin/campaigns/active') ?>" class="btn btn-sm btn-default btn-flat pull-right">View All campaigns</a> </div>
            <!-- /.box-footer --> 
        </div>
        <!-- /.box --> 
    </div>
    <div class="col-md-4">
        <div class="box box-default box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Login log</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Date</th>
                        <th>IP</th>
                    </tr>
                    
                </table>
            </div>
        </div>
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
    <div class="col-md-4">
        
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
</div>
<div class="clearfix"></div>

