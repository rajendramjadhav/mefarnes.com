<style>
    table a {
        display: inline !important;
    }
</style>  
<div class="row">

    <div class="col-xs-12">
<h3 class="box-title pull-right"><a onclick="window.history.back();" href="javascript:;">Back to Campaign List</a></h3>
        <div class="box box-default box-solid">
            <div class="alert alert-success" style="display:none;">Flag updated successfully</div>
            <div class="box-header with-border">
                <h3 class="box-title">Report List of " <?= $campaign->title ?>"</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row" >
                    <div class="table-responsive leads">
                        <table class="table">
                            <thead>
                                <tr>  
                                    <th>ID</th>                                    
                                    <th style="width:10%;">User name</th>
                                    <th style="width:50%;">Comment</th>
                                    <th>Report date</th>
                                    <th colspan="1" style="text-align: center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (count($reports) > 0) {
                                    //$page = $offset;

                                    foreach ($reports as $item) {
                                        ?>
                                        <tr id="campaign-tr-<?= $item->id ?>">
                                            <td><?= $item->id ?></td>
                                            <td><a > <?= $item->user_name ?></a></td>
                                            <td><a > <?= nl2br(htmlentities($item->reason)) ?></a></td>
                                            <td> <?= getDateFormat($item->report_date) ?></td>

                                            <td style="text-align:center;"><a title="Delete Report" href="javascript:;" onclick="deleteReport(<?= $item->id ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                            
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            No Data
                                        </td>
                                    </tr> 

                                <?php } ?>
                            </tbody>

                        </table>


                    </div> 
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
    <!-- /.col --> 
</div>
<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
    function deleteReport(id)
    {
        if (window.confirm('Are you sure to delete ?'))
        {
            //console.log($('#' + id).attr("title", 'test'));
            var campaign_id = id;
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/campaigns/deleteReport",
                dataType: 'json',
                data: {campaign_id: campaign_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        if (res.code == 200)
                        {
                            $(".alert-success").html("Report Deleted successfully");
                            $(".alert-success").slideDown('slow');
                            $('#campaign-tr-' + id).slideUp('slow');

                        }
                        $("#csrf_token").val(res.token);
                    }
                }
            });
        }

    }
</script>