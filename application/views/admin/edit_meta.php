
<div class="row"> 
    <!-- left column -->
    <div class="col-md-12"> 
        <!-- general form elements -->

        <div class="box box-default box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Mata Tag for <?= $list->slug ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row mrgn-box2-colm" >
                    <div class="col-md-6">
                        <div class="box-header">
                            <h3 class="box-title">Meta tags Details</h3>
                        </div>
                        
                            <?= form_open(current_url(),array('class' => 'form-horizontal validate-form','method' => 'post'))?>
                            <div class="box box-info">
                                <div class="box-body colum2-box">

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Page name <i class="fa fa-asterisk text-danger"></i></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="page_name" name="page_name" placeholder="Page Name" value="<?= $list->page_name ?>" 
                                                   data-rule-required="true"
                                                   data-msg-required="Page name required"
                                                   >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-5 control-label">Page Title <i class="fa fa-asterisk text-danger"></i></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Meta Title" value="<?= $list->meta_title; ?>"
                                                   data-rule-required="true"
                                                   data-msg-required="page Title required"
                                                   >
                                        </div>
                                    </div>
                                    
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-5 control-label">Page Keyword <i class="fa fa-asterisk text-danger"></i></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Meta Title" value="<?= $list->keyword; ?>"
                                                   data-rule-required="true"
                                                   data-msg-required="page keyword required"
                                                   >
                                        </div>
                                    </div>

           

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-5 control-label">Meta Description <i class="fa fa-asterisk text-danger"></i> </label>
                                        <div class="col-sm-7">

                                            <textarea id="meta_description" name="meta_description" data-rule-required="true"
                                                      data-msg-required="Landline number required" cols="42"><?= $list -> meta_description?></textarea>
                                        </div>
                                    </div>


                                </div>
                                <!-- /.box-body --> 
                            </div>
                            <div class="box-footer clearfix">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit" class="btn btn-info pull-right">Save Meta</button>
                                    </div>
                                </div>
                            </div>
                        <?= form_close()?>
                    </div>
                    <div class="col-lg-6" style="padding:25px;">
                        <h2 align="center">Help</h2>
                        <p class="well"><i class="fa fa-asterisk text-danger"></i> marked are mandatory fields</p>

                    </div>
                </div>

                <!-- /.col (left) --> 

                <!-- /.col (right) --> 
            </div>
        </div>
        <!-- /.box-body --> 

    </div>
    <!-- /.box --> 

</div>
<!--/.col (left) --> 
<!-- right column --> 
<!--/.col (right) --> 
</div>
