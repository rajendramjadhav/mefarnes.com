<?php
	$this->load->view('admin/header');
?>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="row-fluid">
      <div class="span12 center login-header">
        <h2>WELCOME TO GuardIntel ADMIN PANEL</h2>
      </div>
      <!--/span--> 
    </div>
    <!--/row-->
    
    <div class="row-fluid">
      <div class="well span5 center login-box">
        <?php if($this->session->flashdata('msg_type') and $this->session->flashdata('message')){ ?>
        <div id="message_box" class="alert alert-<?php echo $this->session->flashdata('msg_type');?>"> <?php echo $this->session->flashdata('message');?> </div>
        <?php } ?>
        <div class="alert alert-info"> Please login with your Username and Password. </div>
        <?php echo form_open_multipart('admin/Company/index', array('id' => 'loginform','name' => 'loginform','class' => 'form-horizontal'));?>
        <fieldset>
          <div class="input-prepend" title="Username" data-rel="tooltip"> <span class="add-on"><i class="icon-user"></i></span>
            <input autofocus class="input-large span10" type="text" name="email" value="<?php echo set_value('email'); ?>" placeholder="username" />
          </div>
          <div class="clearfix"></div>
          <div class="input-prepend" title="Password" data-rel="tooltip"> <span class="add-on"><i class="icon-lock"></i></span>
            <input class="input-large span10" type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="password" />
          </div>
          <div class="clearfix"></div>
          <p class="center span5">
            <button type="submit" class="btn btn-primary">Login</button>
          </p>
        </fieldset>
        <?php echo form_close();?> </div>
      <!--/span--> 
    </div>
    <!--/row--> 
  </div>
  <!--/fluid-row--> 
  
</div>
<?php
	$this->load->view('admin/footer');
?>