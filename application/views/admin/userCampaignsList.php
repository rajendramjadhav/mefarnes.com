<?php foreach($getUserDtls as $u){
$uname= $u->first_name." ".$u->last_name;
}?>
<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Campaigns by <?php echo $uname; ?></h4>
                                <p class="category">Registered users with this site</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Title</th>
                                    	<th>Created Date</th>
                                    	<th>Start Date</th>
                                    	<th>End Date</th>

                                    </thead>
                                    <tbody>
                                    <?php 
if(count($uCampaignsList)==0){?>
<td colspan="5" style="text-align: center;">No Campaigns yet</td>
<?php }
else{$i=1;foreach($uCampaignsList as $row){?>
                                        <tr>
<td><?php echo $i;?></td>
<td><?php echo $row->title;?></td>
<td><?php echo $row->create_date;?></td>
<td><?php echo $row->start_date_time;?></td>
<td><?php echo $row->end_date_time;?></td>



                                        </tr>
                                    <?php $i++;}}?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>