
<div class="row"> 
    <!-- left column -->
    <div class="col-md-12"> 
        <!-- general form elements -->

        <div class="box box-default box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Add Category</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row mrgn-box2-colm" >
                    <div class="col-md-6">
                        <div class="box-header">
                            <h3 class="box-title">Category Name</h3>
                        </div>
<!--                        <form class="form-horizontal validate-form" method="POST" action="<?= current_url() ?>">-->
                            <?= form_open(current_url(),array('method' => 'post','class' =>'form-horizontal validate-form')); ?>
                            <div class="box box-info">
                                <div class="box-body colum2-box">

                                    <div class="form-group">
                                        
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Category Name" value="" 
                                                   data-rule-required="true"
                                                   data-msg-required="Category name required"
                                                   >
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body --> 
                            </div>                           
                    </div>
                    <div class="box-footer clearfix">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" name="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="col-lg-6" style="padding:25px;">
                    <h2 align="center">Help</h2>
                    <p class="well"><i class="fa fa-asterisk text-danger"></i> marked are mandatory fields</p>

                </div>
            </div>

            <!-- /.col (left) --> 

            <!-- /.col (right) --> 
        </div>
    </div>
    <!-- /.box-body --> 

</div>
<!-- /.box --> 

</div>
<!--/.col (left) --> 
<!-- right column --> 
<!--/.col (right) --> 
</div>
