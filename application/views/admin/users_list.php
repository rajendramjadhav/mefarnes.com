<style>
    table a {
        display: inline !important;
    }
</style>  
<div class="row">

    <div class="col-xs-12">

        <div class="box box-default box-solid">
            <div class="alert alert-success" style="display:none;">Flag updated successfully</div>
            <div class="box-header with-border">
                <h3 class="box-title">Users List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row" >
                    <div class="table-responsive ">
<!--                        <form class="form-horizontal" method="POST" action="<?= current_url() ?>">
                            <table class="table">
                                <tr>
                                    <td style="padding-left:30px;">
                                        <label>
                                            First Name:
                                        </label>
                                    </td>
                                    <td>
                                        <input type="text" name="dfname" value="<?php if (!empty($search['dfname'])) echo $search['dfname'] ?>"/>

                                    </td>
                                    <td>
                                        <label>
                                            Last Name:
                                        </label>
                                    </td>
                                    <td>
                                        <input type="text" name="dlname" value="<?php if (!empty($search['dlname'])) echo $search['dlname'] ?>"/>

                                    </td>
                                    <td>
                                        <label>
                                            E-mail:
                                        </label>
                                    </td>
                                    <td>
                                        <input type="text" name="email" value="<?php if (!empty($search['email'])) echo $search['email'] ?>"/>

                                    </td>
                                    <td>
                                        <label>
                                            Phone:
                                        </label>
                                    </td>
                                    <td>
                                        <input type="text" name="phone" value="<?php if (!empty($search['phone'])) echo $search['phone'] ?>"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button type="submit" name="submit" class="btn btn-info pull-left">Search Dealer Details</button>
                                    </td>
                                </tr>
                            </table>
                        </form>-->
                    </div>
                    <div class="table-responsive leads">
                        <table class="table">
                            <thead>
                                <tr>           

                                    <th>SL No.</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>No. of Created Campaign </th>
                                    <th>No. of Campaign Donated</th>
                                    <th>Registered At</th>
                                    <th colspan="2">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (count($list) > 0) {
                                    $page = $offset;

                                    foreach ((array) $list as $item) {
                                        ?>
                                        <tr id="user-tr-<?= $item->id ?>">
                                            <td><?= $item->id ?></td>
                                            <td><?= $item->first_name ?></td>
                                            <td><?= $item->last_name ?></td>
                                            <td><?= $item->email ?></td>
                                            <td><?= $item->creation_count ?></td>
                                            <td><?= $item->donation_count ?></td>
                                            <td><?= getDateFormat($item->create_date) ?></td>
                                            <td><a id="<?= $item->id ?>" onclick="changeStatus(<?= $item->id ?>);" href="javascript:;" title="<?php
                                                if ($item->status == 1)
                                                    echo 'Flag as Inactive';
                                                elseif ($item->status == 0)
                                                    echo 'Flag as Active';
                                                ?>"><i class="fa fa-flag" aria-hidden="true"></i></a></td>
                                            <td><a title="Delete Permanent" href="javascript:;" onclick="deleteUser(<?= $item->id ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            No Data
                                        </td>
                                    </tr> 

                                <?php } ?>
                            </tbody>

                        </table>
                        <?= $pagination ?>

                    </div> 
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
    <!-- /.col --> 
</div>

<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
    function changeStatus(id)
    {

        var user_id = id;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "admin/users/changeStatus",
            dataType: 'json',
            data: {user_id: user_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                if (res)
                {
                    if (res.code == 200)
                    {
                        $(".alert-success").html("Campaign Updated successfully");
                        $(".alert-success").slideDown('slow');
                        $('#' + id).attr("title", 'Make Active')

                    }
                    $("#csrf_token").val(res.token);
                }
            }
        });

    }

    function deleteUser(id)
    {
        if (window.confirm('Are you sure to delete ?'))
        {
            //console.log($('#' + id).attr("title", 'test'));
            var user_id = id;
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/users/deleteUser",
                dataType: 'json',
                data: {user_id: user_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        if (res.code == 200)
                        {
                            $(".alert-success").html("User Deleted successfully");
                            $(".alert-success").slideDown('slow');
                            $('#user-tr-' + id).slideUp('slow');

                        }
                        $("#csrf_token").val(res.token);
                    }
                }
            });
        }

    }
</script>