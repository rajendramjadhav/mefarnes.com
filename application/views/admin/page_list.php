<style>
    table a {
        display: inline !important;
    }
</style>  
<div class="row">

    <div class="col-xs-12">

        <div class="box box-default box-solid">
            <div class="alert alert-success" style="display:none;">Flag updated successfully</div>
            <div class="box-header with-border">
                <h3 class="box-title">Page List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row" >
                    <div class="table-responsive leads">
                        <table class="table">
                            <thead>
                                <tr>           

                                    <th>SL No.</th>
                                    <th>Page Unique Name</th>
                                    <th>Page Name</th>
                                    <th colspan="3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (count($list) > 0) {
                                    foreach ((array) $list as $item) {
                                        ?>
                                        <tr>
                                            <td><?= $item->id ?></td>
                                            <td><?= $item->slug ?></td>
                                            <td><?= $item->page_name ?></td>
                                            
                                            <td><a title="Edit" href="<?= base_url().'admin/pages/edit_meta/'.$item->id?>"><i class="fa fa-edit" aria-hidden="true"></i></a></td>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            No Data
                                        </td>
                                    </tr> 

                                <?php } ?>
                            </tbody>

                        </table>
                       

                    </div> 
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
    <!-- /.col --> 
</div>

<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
    function changeStatus(id)
    {

        var user_id = id;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "admin/users/changeStatus",
            dataType: 'json',
            data: {user_id: user_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                if (res)
                {
                    if (res.code == 200)
                    {
                        $(".alert-success").html("Campaign Updated successfully");
                        $(".alert-success").slideDown('slow');
                        $('#' + id).attr("title", 'Make Active')

                    }
                    $("#csrf_token").val(res.token);
                }
            }
        });

    }

    function deleteUser(id)
    {
        if (window.confirm('Are you sure to delete ?'))
        {
            //console.log($('#' + id).attr("title", 'test'));
            var user_id = id;
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/users/deleteUser",
                dataType: 'json',
                data: {user_id: user_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        if (res.code == 200)
                        {
                            $(".alert-success").html("User Deleted successfully");
                            $(".alert-success").slideDown('slow');
                            $('#user-tr-' + id).slideUp('slow');

                        }
                        $("#csrf_token").val(res.token);
                    }
                }
            });
        }

    }
</script>