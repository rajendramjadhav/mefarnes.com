
<!-- Main content -->
<section class="content">
    <div class="row"> 
        <!-- left column -->
        <div class="col-md-12"> 
            <!-- general form elements -->

            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Change Password </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-lg-6">
                        <div class="box-body colum2-box">
                            <form class="form-horizontal validate-form" method="POST" action="<?= base_url('admin/change-password') ?>">
                    
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">&nbsp;</label>
                                <div class="col-sm-7">
								<div class="form-group">
                                    <label for="exampleInputName2">Old Password</label>
                                    <input type="password" name="opass" class="form-control" id="opass" placeholder=""
                                    data-rule-required="true"
                                    >
                                </div>  
                                <div class="form-group">
                                    <label for="exampleInputName2">New Passowrd</label>
                                    <input type="password" name="npass" class="form-control" id="npass" placeholder=""
                                    data-rule-required="true"
                                    >
                                </div>  
                                <div class="form-group">
                                    <label for="exampleInputName2">Confirm Password</label>
                                    <input type="password" name="cpass" class="form-control" id="cpass" placeholder=""
                                    data-rule-required="true"
                                    data-rule-equalto="#npass"
                                    >
                                </div>	
                                      
                                </div>
                            </div>

                         
                            <div class="box-footer clearfix">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="change-password" value="1" class="btn btn-info pull-right">Save</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6" style="padding:25px;">
                        <h2 align="center">Help</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                    </div>
                    <!-- /.box-body --> 

                </div>

                <!-- /.box --> 

            </div>
            <!--/.col (left) --> 
            <!-- right column --> 
            <!--/.col (right) --> 
        </div>
</section>
<!-- Main content end --> 