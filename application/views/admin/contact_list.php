<style>
    table a {
        display: inline !important;
    }
</style>  
<div class="row">

    <div class="col-xs-12">

        <div class="box box-default box-solid">
            <div class="alert alert-success" style="display:none;">Flag updated successfully</div>
            <div class="box-header with-border">
                <h3 class="box-title">Users List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row" >
                    <div class="table-responsive leads">
                        <table class="table">
                            <thead>
                                <tr>           

                                    <th>SL No.</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone </th>
                                    <th>Message</th>
                                    <th>Contacted  At</th>
                                    <th colspan="3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (count($list) > 0) {
                                    $page = $offset;

                                    foreach ((array) $list as $item) {
                                        ?>
                                        <tr id="user-tr-<?= $item->id ?>">
                                            <td><?= $item->id ?></td>
                                            <td><?= $item->first_name ?></td>
                                            <td><?= $item->last_name ?></td>
                                            <td><?= $item->email ?></td>
                                            <td><?= $item->phone ?></td>
                                            <td><?= nl2br(htmlentities($item->message)) ?></td>
                                            <td><?= getDateFormat($item->create_date) ?></td>
                                            <td><?php
                                                if ($item->replied == 1)
                                                    echo '<i class="fa fa-check" title="Replied" aria-hidden="true"></i>';
                                                elseif ($item->replied == 0)
                                                    echo '<i class="fa fa-times" title="Not replied" aria-hidden="true"></i>';
                                                ?></td>
                                            <td><a href="<?= base_url() . 'admin/pages/reply/' . $item->id ?>" title="<?php
                                                if ($item->replied == 1)
                                                    echo 'Replied';
                                                elseif ($item->replied == 0)
                                                    echo 'Not replied';
                                                ?>">
                                                       <?php
                                                       if ($item->replied == 1)
                                                           echo '<i class="fa fa-reply" aria-hidden="true"></i>';
                                                       elseif ($item->replied == 0)
                                                           echo '<i class="fa fa-reply" aria-hidden="true"></i>';
                                                       ?> 
                                                </a></td>
                                            <td><a title="Delete Permanent" href="javascript:;" onclick="deleteUser(<?= $item->id ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            No Data
                                        </td>
                                    </tr> 

                                <?php } ?>
                            </tbody>

                        </table>
                        <?= $pagination ?>

                    </div> 
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
    <!-- /.col --> 
</div>

<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
    function changeStatus(id)
    {

        var user_id = id;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "admin/users/changeStatus",
            dataType: 'json',
            data: {user_id: user_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                if (res)
                {
                    if (res.code == 200)
                    {
                        $(".alert-success").html("Campaign Updated successfully");
                        $(".alert-success").slideDown('slow');
                        $('#' + id).attr("title", 'Make Active')

                    }
                    $("#csrf_token").val(res.token);
                }
            }
        });

    }

    function deleteUser(id)
    {
        if (window.confirm('Are you sure to delete ?'))
        {
            //console.log($('#' + id).attr("title", 'test'));
            var user_id = id;
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/users/deleteUser",
                dataType: 'json',
                data: {user_id: user_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        if (res.code == 200)
                        {
                            $(".alert-success").html("User Deleted successfully");
                            $(".alert-success").slideDown('slow');
                            $('#user-tr-' + id).slideUp('slow');

                        }
                        $("#csrf_token").val(res.token);
                    }
                }
            });
        }

    }
</script>