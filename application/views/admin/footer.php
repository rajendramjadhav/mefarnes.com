<footer>
        <p class="pull-left">Copyright &copy; <a href="http://localhost/guardintel/" target="_blank">GuardIntel</a>&nbsp;2015</p>
        <p class="pull-right">Powered by: <a href="http://codelogicx.com/" target="_blank">Codelogicx Technologies Pvt. Ltd.</a></p>
    </footer>
</div>
<!--/.fluid-container--> 

<!-- external javascript
	================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 

<!-- jQuery --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery-1.7.2.min.js"></script> 
<!-- jQuery UI --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery-ui-1.8.21.custom.min.js"></script> 
<!-- transition / effect library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-transition.js"></script> 
<!-- alert enhancer library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-alert.js"></script> 
<!-- modal / dialog library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-modal.js"></script> 
<!-- custom dropdown library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-dropdown.js"></script> 
<!-- scrolspy library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-scrollspy.js"></script> 
<!-- library for creating tabs --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-tab.js"></script> 
<!-- library for advanced tooltip --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-tooltip.js"></script> 
<!-- popover effect library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-popover.js"></script> 
<!-- button enhancer library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-button.js"></script> 
<!-- accordion library (optional, not used in demo) --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-collapse.js"></script> 
<!-- carousel slideshow library (optional, not used in demo) --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-carousel.js"></script> 
<!-- autocomplete library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-typeahead.js"></script> 
<!-- tour library --> 
<script src="<?php echo base_url();?>assets/js/admin/bootstrap-tour.js"></script> 
<!-- library for cookie management --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.cookie.js"></script> 
<!-- calander plugin --> 
<script src='<?php echo base_url();?>assets/js/admin/fullcalendar.min.js'></script> 
<!-- data table plugin --> 
<script src='<?php echo base_url();?>assets/js/admin/jquery.dataTables.min.js'></script> 

<!-- chart libraries start --> 
<script src="<?php echo base_url();?>assets/js/admin/excanvas.js"></script> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.flot.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.flot.pie.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.flot.stack.js"></script> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.flot.resize.min.js"></script> 
<!-- chart libraries end --> 

<!-- select or dropdown enhancer --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.chosen.min.js"></script> 
<!-- checkbox, radio, and file input styler --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.uniform.min.js"></script> 
<!-- plugin for gallery image view --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.colorbox.min.js"></script> 
<!-- rich text editor library --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.cleditor.min.js"></script> 
<!-- notification plugin --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.noty.js"></script> 
<!-- file manager library --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.elfinder.min.js"></script> 
<!-- for iOS style toggle switch --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.iphone.toggle.js"></script> 
<!-- autogrowing textarea plugin --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.autogrow-textarea.js"></script> 

<!-- multiple file upload plugin --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.uploadify-3.1.min.js"></script> 
<!-- history.js for cross-browser state change on ajax --> 
<script src="<?php echo base_url();?>assets/js/admin/jquery.history.js"></script> 
<!-- application script for Charisma demo --> 
<script src="<?php echo base_url();?>assets/js/admin/charisma.js"></script>
<script>
    <?php if($this->session->flashdata('msg_type') and $this->session->flashdata('message')){ ?>
	  $('#message_box').delay(5000).fadeOut(400);
	<?php } ?>
</script>
</body>
</html>
