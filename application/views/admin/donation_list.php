<style>
    table a {
        display: inline !important;
    }
</style>  
<div class="row">
    <div class="col-xs-12">
        <div class="box-header with-border">
                
            <h3 class="box-title pull-right"><a onclick="window.history.back();" href="javascript:;">Back to Campaign List</a></h3>
            </div>
        <div class="box box-default box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Donation List of " <?= $campaign->title ?>"</h3>
                <h3 class="box-title pull-right">Total Fund Raised $<?= $total_donation->total_donation ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row" >
                    <div class="table-responsive leads">
                        <table class="table">
                            <thead>
                                <tr>  
                                    <th>ID</th>                                    
                                    <th style="width:10%;">Donor Name</th>
                                    <th style="width:10%;">Email</th>
                                    <th style="width:10%;">Postal Code</th>
                                    <th style="width:10%;">Country</th>
                                    <th style="width:10%;">Amount</th>
                                    <th style="width:10%;">Comment</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
//                                echo '<pre>';
//                                print_r($donation); exit;
                                if (count($donation) > 0) {
                                    //$page = $offset;
                                    foreach ($donation as $item) {
                                        ?>
                                        <tr>
                                            <td><?= $item->id ?></td>
                                            <td><?= $item->donor_first_name . ' ' . $item->donor_last_name ?></td>
                                            <td><?= $item->email ?></td>
                                            <td> <?= $item->postal_code ?></td>
                                            <td> <?= $item->country ?></td>
                                            <td> $<?= $item->amount ?></td>
                                            <td> <?= nl2br(htmlentities($item->comment)) ?></td>
                                            <td> <?= getDateFormat($item->create_date) ?></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="8">
                                            No Data
                                        </td>
                                    </tr> 

                                <?php } ?>
                            </tbody>

                        </table>


                    </div> 
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
    <!-- /.col --> 
</div>
<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
    function changeStatus(id)
    {
        //console.log($('#' + id).attr("title", 'test'));
        var campaign_id = id;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "admin/campaigns/changeStatus",
            dataType: 'json',
            data: {campaign_id: campaign_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                if (res)
                {
                    if (res.code == 200)
                    {
                        $(".alert-success").html("Campaign Updated successfully");
                        $(".alert-success").slideDown('slow');
                        $('#' + id).attr("title", 'Make Visible')

                    }
                    $("#csrf_token").val(res.token);
                }
            }
        });

    }

    function deleteCampaign(id)
    {
        if (window.confirm('Are you sure to delete ?'))
        {
            //console.log($('#' + id).attr("title", 'test'));
            var campaign_id = id;
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/campaigns/deleteCampaign",
                dataType: 'json',
                data: {campaign_id: campaign_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        if (res.code == 200)
                        {
                            $(".alert-success").html("Campaign Deleted successfully");
                            $(".alert-success").slideDown('slow');
                            $('#campaign-tr-' + id).slideUp('slow');

                        }
                        $("#csrf_token").val(res.token);
                    }
                }
            });
        }

    }
</script>