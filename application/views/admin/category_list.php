<style>
    table a {
        display: inline !important;
    }
</style>  
<div class="row">

    <div class="col-xs-12">

        <div class="box box-default box-solid">
            <div class="alert alert-success" style="display:none;">Flag updated successfully</div>
            <div class="box-header with-border">
                <h3 class="box-title">Category List
                </h3>
                <a href="<?= base_url().'admin/categories/add_category';?>"><span class="pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Category</span></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row" >

                    <div class="table-responsive leads">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th>Category Name</th>
                                    <th>No of Campaign At this category</th>
                                    <th colspan="3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (count($list) > 0) {
                                    $page = $offset;

                                    foreach ((array) $list as $item) {
                                        ?>
                                        <tr id="user-tr-<?= $item->id ?>">
                                            <td><?= $item->id ?></td>
                                            <td><?= $item->name ?></td>
                                            <td><?= $item->toatal_campaign ?></td>
                                            <td><a id="<?= $item->id ?>" onclick="changeStatus(<?= $item->id ?>);" href="javascript:;" title="<?php
                                                if ($item->status == 1)
                                                    echo 'Flag as Inactive';
                                                elseif ($item->status == 0)
                                                    echo 'Flag as Active';
                                                ?>"><i class="fa fa-flag" aria-hidden="true"></i></a></td>
                                            <td><a title="Edit category" href="<?= base_url().'admin/categories/edit_category/'.$item ->id?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                            <td><a title="Delete Permanent" href="javascript:;" onclick="deleteUser(<?= $item->id ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            No Data
                                        </td>
                                    </tr> 

                                <?php } ?>
                            </tbody>

                        </table>
                        <?= $pagination ?>

                    </div> 
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
    <!-- /.col --> 
</div>

<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
    function changeStatus(id)
    {

        var user_id = id;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "admin/categories/changeStatus",
            dataType: 'json',
            data: {user_id: user_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                if (res)
                {
                    if (res.code == 200)
                    {
                        $(".alert-success").html("Category Updated successfully");
                        $(".alert-success").slideDown('slow');
                        $('#' + id).attr("title", 'Make Active')

                    }
                    $("#csrf_token").val(res.token);
                }
            }
        });

    }

    function deleteUser(id)
    {
        if (window.confirm('Are you sure to delete ?'))
        {
            //console.log($('#' + id).attr("title", 'test'));
            var user_id = id;
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/categories/deleteUser",
                dataType: 'json',
                data: {user_id: user_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        if (res.code == 200)
                        {
                            $(".alert-success").html("Category Deleted successfully");
                            $(".alert-success").slideDown('slow');
                            $('#user-tr-' + id).slideUp('slow');

                        }
                        $("#csrf_token").val(res.token);
                    }
                }
            });
        }

    }
</script>