<style>
    table a {
        display: inline !important;
    }
</style>  
<div class="row">

    <div class="col-xs-12">

        <div class="box box-default box-solid">
            <div class="alert alert-success" style="display:none;">Flag updated successfully</div>
            <div class="box-header with-border">
                <h3 class="box-title">Reported Campaign List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row" >
                    <div class="table-responsive leads">
                        <table class="table">
                            <thead>
                                <tr>  
                                    <th>ID</th>                                    
                                    <th style="width:10%;">Campaign name</th>
                                    <th style="width:10%;">Mame of Individual</th>
                                    <th style="width:40%;">Campaign url</th>
                                    <th>No. Of Reports</th>
                                    <th>Fund Needed</th>
                                    <th>Funded</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th colspan="3" style="text-align: center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (count($campaigns) > 0) {
                                    //$page = $offset;

                                    foreach ($campaigns as $item) {
                                        ?>
                                        <tr id="campaign-tr-<?= $item->id ?>">
                                            <td><?= $item->id ?></td>
                                            <td><a > <?= $item->title ?></a></td>

                                            <td><a > <?= $item->user_name ?></a></td>
                                            <td><a href="<?= base_url() . 'campaign/' . $item->slug ?>" target="_blank" ><?= base_url() . 'campaign/' . $item->slug ?></a></td>
                                            <td><?= $item->total_report ?></td>
                                            <td> $<?= $item->fund_needed ?></td>
                                            <td> $<?= $item->collected_fund ?></td>
                                            <td> <?= getDateFormat($item->start_date_time) ?></td>
                                            <td> <?= getDateFormat($item->end_date_time) ?></td>
                                            <td><a id="<?= $item->id ?>" onclick="changeStatus(<?= $item->id ?>);" href="javascript:;" title="<?php
                                                if ($item->status == 1)
                                                    echo 'Hide';
                                                elseif ($item->status == 0)
                                                    echo 'Make Visible';
                                                ?>"><i class="fa fa-flag" aria-hidden="true"></i></a></td>
                                            <td><a title="Delete Permanent" href="javascript:;" onclick="deleteCampaign(<?= $item->id ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                            <td><a title="Report Detail" href="<?= base_url() . 'admin/campaigns/report_detail/' . $item->id ?>"><i class="fa fa-info" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="10">
                                            No Data
                                        </td>
                                    </tr> 

                                <?php } ?>
                            </tbody>

                        </table>


                    </div> 
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box --> 

        <!-- /.box --> 
    </div>
    <!-- /.col --> 
</div>
<input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
    function changeStatus(id)
    {
        //console.log($('#' + id).attr("title", 'test'));
        var campaign_id = id;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "admin/campaigns/changeStatus",
            dataType: 'json',
            data: {campaign_id: campaign_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
            success: function (res) {
                if (res)
                {
                    if (res.code == 200)
                    {
                        $(".alert-success").html("Campaign Updated successfully");
                        $(".alert-success").slideDown('slow');
                        $('#' + id).attr("title", 'Make Visible')

                    }
                    $("#csrf_token").val(res.token);
                }
            }
        });

    }

    function deleteCampaign(id)
    {
        if (window.confirm('Are you sure to delete ?'))
        {
            //console.log($('#' + id).attr("title", 'test'));
            var campaign_id = id;
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/campaigns/deleteCampaign",
                dataType: 'json',
                data: {campaign_id: campaign_id, '<?php echo $this->security->get_csrf_token_name(); ?>': $('#csrf_token').val()},
                success: function (res) {
                    if (res)
                    {
                        if (res.code == 200)
                        {
                            $(".alert-success").html("Campaign Deleted successfully");
                            $(".alert-success").slideDown('slow');
                            $('#campaign-tr-' + id).slideUp('slow');

                        }
                        $("#csrf_token").val(res.token);
                    }
                }
            });
        }

    }
</script>