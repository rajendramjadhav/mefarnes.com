
<div class="row"> 
    <!-- left column -->
    <div class="col-md-12"> 
        <!-- general form elements -->

        <div class="box box-default box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Reply to message</h3>
            </div>

            <div class="box-body">
                <div class="row mrgn-box2-colm" >
                    <div class="col-md-6">
                        <div class="box-header">
                            <h3 class="box-title">Message Content</h3>
                        </div>
                        <div>
                            <strong>Reply To:</strong> <?= $user->email ?>
                        </div>
                        <div>
                            <strong>User Name:</strong> <?= $user->first_name . ' ' . $user->last_name ?>
                        </div>
                        <div>
                            <strong>Text:</strong> <?= $user->message ?>
                        </div>
                    </div>
                </div>

                <!-- /.col (left) --> 

                <!-- /.col (right) --> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row mrgn-box2-colm" >
                    <div class="col-md-6">
                        <div class="box-header">
                            <h3 class="box-title">Reply Content</h3>
                        </div>
<!--                        <form class="form-horizontal validate-form" method="POST" action="<?= current_url() ?>">-->
                            <?= form_open(current_url(),array('class'=> 'form-horizontal validate-form','method' =>'post'))?>
                            <div class="box box-info">
                                <div class="box-body colum2-box">

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Reply Subject <i class="fa fa-asterisk text-danger"></i></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="subject" name="subject" placeholder="First Name" value="Re: <?= substr($user->message, 0, 30); ?>" 
                                                   data-rule-required="true"
                                                   data-msg-required="Subject is required"
                                                   >

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Reply Text <i class="fa fa-asterisk text-danger"></i></label>
                                        <div class="col-sm-7">
                                            <textarea cols="45" rows="5" name="text" data-rule-required="true"
                                                      data-msg-required="Reply text required"></textarea>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body --> 
                            </div>

                            <div class="box-footer clearfix">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit" class="btn btn-info pull-right">Save Dealer Details</button>
                                    </div>
                                </div>
                            </div>
                        <?= form_close()?>
                    </div>
                    <div class="col-lg-6" style="padding:25px;">
                        <h2 align="center">Help</h2>
                        <p class="well"><i class="fa fa-asterisk text-danger"></i> marked are mandatory fields</p>

                    </div>
                </div>

                <!-- /.col (left) --> 

                <!-- /.col (right) --> 
            </div>
        </div>
        <!-- /.box-body --> 

    </div>
    <!-- /.box --> 

</div>
<!--/.col (left) --> 
<!-- right column --> 
<!--/.col (right) --> 
</div>
