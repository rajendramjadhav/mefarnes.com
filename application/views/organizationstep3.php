<div id="content" class="page signup">
    <div class="container">
        <div class="row">
        	<form name="frm_signup3" id="frm_signup3" method="post" action="<?php echo base_url(); ?>auth/saveorgsignupstep3">
            <input type="hidden" id="csrf_token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                <section class="">
                    <h2>Signup as Organization</h2>
                    <!--<h3>Lorem ipsum eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>-->
                    <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                    <div class="signup-form-box">
                        <div class="col-left orange"><i class="fa fa-envelope"></i></div>
                        <div class="col-right">
                            <div class="signup-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Your Mailing Address</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="txtstreet_org_add">Location <span>[ *Please Enter Location ]</span></label>
                                        <input type="text" id="full_address" name="full_address" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                <!--                                    <a href="javascript:void(0);" class="button blue"><span>Sign up</span></a>-->
                <!--                                    <input type="submit" class="button blue" name="signup" value="Sign up">-->
                                        <button type="submit" class="button blue" name="signup"><span>Sign up</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
        </div>
    </div>
</div> 
<script src="https://maps.googleapis.com/maps/api/js??v=3.13&libraries=places&key=AIzaSyBgS4A2KbRY159HA-HDIRxaoanaYQscQy8" type="text/javascript"></script>
<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('txtcity');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('city');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('full_address');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

