<?php
//echo '<pre>';
//print_r($datab);
?>
<!-- content -->
<div id="content" class="page dashboard transaction-history">
    <div class="dashboard-header-sm">
        <div class="container">
            <div class="clearfix">
                <h2>Transaction History</h2>
            </div>
        </div>
    </div>

    <div class="contents">
        <div class="container">
            <div class="form">
                <div class="form-row">
                    <h4>Fund Available : <?php
                        if ($datab->balances[0]->currency == 'USD')
                            echo '$';
                        echo $datab->balances[0]->balance;
                        ?>
                        <span class="pull-right">Next Withdrawal Date : <?php
                        echo date('M j, Y',$datab->balances[0]->withdrawal_next_time);
                        ?><br/> &nbsp;&nbsp;<a href="<?= base_url().'update-withdrawal-frequency'?>">Update Date</a></span>
                        </h4>
                    <div class="clearfix">
                        <label>From</label>
                        <span class="input-box">
                            <input type="text" id="date_from" class="date-input" />
                        </span>
                        <label>To</label>
                        <span class="input-box">
                            <input type="text" id="date_to" class="date-input" />
                        </span>
<!--                        <span class="input-box selectbox">
                            <select class="fancy-selectbox">
                                <option>All Transactions</option>
                                <option>Debits</option>
                                <option>Credits</option>
                            </select>
                        </span>-->
<!--                        <span class="input-box selectbox">
                            <select class="fancy-selectbox">
                                <option>Campaign</option>
                                <option>Campaign One</option>
                                <option>Campaign Two</option>
                            </select>
                        </span>-->
                        <button type="submit" class="button"><span>GO</span></button>
                        <span class="links">
                            <a href="#"><i class="fa fa-file-pdf-o"></i>Get PDF</a>
                            <a href="#"><i class="fa fa-file-excel-o"></i>Get CSV</a>
                        </span>
                    </div>
                </div>
            </div>

            <div class="datagrid">
                <div class="grid-header">
                    <div class="col-one">Date</div>
                    <div class="col-two">Type</div>
                    <div class="col-three">Description</div>
                    <div class="col-four">State</div>
                    <div class="col-five">Amount</div>
                    <div class="col-six">Ref. ID</div>
                </div>
                <?php
                if (!empty($datas)) {
                    foreach ($datas as $data):
                        ?>
                        <div class="grid-row">
                            <div class="col-one"><b>Date:</b><?= date('M j, Y', ($data->withdrawal_data->create_time)) ?></div>
                            <div class="col-two"><b>Type:</b><?php
                                if ($data->type == 'ach')
                                    echo 'Bank account ';
                                else
                                    echo 'Check';
                                ?></div>
                            <div class="col-three"><b>Description:</b>
                                <?php
                                if (!empty($data->bank_data)) {
                                    echo $data->bank_data->bank_name . ',XXXXX' . $data->bank_data->account_last_four;
                                    ?>
                                    <br /><small><?= !empty($data->bank_data->note) ? $data->bank_data->note : 'N/A' ?></small>
                                    <?php
                                } elseif (!empty($data->check_data)) {
                                    echo $data->check_data->name . ' , ' . $data->check_data->city;
                                    ?>
                                    <br /><small><?= !empty($data->check_data->note) ? $data->check_data->note : 'N/A' ?></small>
                                <?php }
                                ?>
                            </div>
                            <div class="col-four"><?php
                                if ($data->state == 'new')
                                    echo 'Withdrawal was created by the application';
                                else if ($data->state == 'started')
                                    echo 'Withdrawal has started processing';
                                else if ($data->state == 'captured')
                                    echo "Withdrawal has been credited to the payee's bank account";
                                else if ($data->state == 'failed')
                                    echo 'Withdrawal has failed';
                                else if ($data->state == 'expired')
                                    echo 'Withdrawal has expired';
                                ?></div>
                            <div class="col-five"><b>Amount:</b><?php
                                if ($data->currency == 'USD')
                                    echo '$';
                                echo $data->amount;
                                ?></div>
                            <div class="col-six"><b>Ref. ID:</b><?= $data->withdrawal_id ?></div>
                        </div>
                        <?php
                    endforeach;
                }else {
                    ?>
                    <div class="grid-row">
                        <div class="col-one">No Data Available!</div>
                    </div>
<?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- /content -->
