<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['home'] = "home";
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['create-campaign'] = "campaignController/create";
$route['edit-campaign/(:num)'] = "campaignController/create";

$route['savecampaign'] = "campaignController/savecampaign";


$route['create-campaign/step-two'] = "campaignController/add_image_video";
$route['edit-campaign/step-two'] = "campaignController/add_image_video";


$route['organization-profile/step2'] = "user/orgprofilestep2";

$route['create-campaign/:any'] = "campaignController/updateCampaign";
$route['my-campaigns'] = "campaignController/index";
$route['login'] = "auth/login";
$route['logout'] = "auth/logout";
$route['signup'] = "home/signup";
$route['gmail-contact'] = "user/gmail_contact";

$route['subscribe'] = "home/subscribe";

$route['individualuser'] = "home/individualuser";
$route['organizationuser'] = "home/organizationuser";

$route['individual'] = "home/individual";
//$route['organization'] = "home/organization";

$route['individualusersignup'] = "home/individualusersignup";
$route['individual/signup'] = "home/individualsignup";

$route['organization/signup-step1'] = "home/orgsignupstep1";
$route['organization/signup-step2'] = "home/orgsignupstep2";
$route['organization/signup-step3'] = "home/orgsignupstep3";




//$route['saveprofilebasic'] = "user/profilebasic";

$route['dashboard'] = "user/myaccount";
$route['count-sharetwitter'] = "process/twitterfriendscount";
$route['settings'] = "user/settings";
$route['mydonation'] = "user/donarAccount";
//$route['settings/reset'] = "user/resetPassword";

$route['create-update/:any'] = "campaignController/update";
$route['campaign/:any'] = "publicCampaign/index";
$route['search'] = "publicCampaign/search";
$route['search/category/:any'] = "publicCampaign/search";
$route['donate/:any'] = "donation/donate";
$route['donation-success'] = "donation/thankyou";
$route['create-donation'] = "donation/checkout";

$route['faq'] = "pages/faq";
$route['terms'] = "pages/terms";
$route['privacy'] = "pages/privacy";
$route['contact-us'] = "pages/contact";
$route['how-it-works'] = "pages/howitworks";

$route['preview/:any'] = "publicCampaign/preview";
$route['campaign-success/(:any)'] = "campaignController/campaign_success";

$route['admin'] = "admin/auth/login";
$route['auth/resetpasword/(:any)'] = 'auth/resetpasword';
$route['auth/savePassword'] = 'auth/savePassword';

$route['organization/:any'] = "organization/index";
$route['donate-organization/:any'] = "organization/donate";
$route['organization-profile'] = "user/createprofile";
$route['organization-profile/(:any)'] = "publicCampaign/organization_public_profile";

$route['transaction-history'] = "withdrawal/history";
$route['update-withdrawal-frequency'] = "withdrawal/update";
$route['home/deactivate'] = "home/deactivateAccount";
$route['home/checkemailduplicate'] = "home/checkemail_duplicate";
$route['auth/saveorgsignupstep1'] = "auth/saveorgsignupstep1";