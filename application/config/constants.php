<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

// defined('we_pay_library') OR define('we_pay_library', $_SERVER['DOCUMENT_ROOT'] . '/application/libraries/wepay.php'); //
// defined('wepay_account_id') OR define('wepay_account_id', '167445584'); //1336580099
// defined('wepay_client_id') OR define('wepay_client_id', '161966'); //74601
// defined('wepay_client_secret') OR define('wepay_client_secret', 'f67c61bcc6'); //
// defined('wepay_app_access_token') OR define('wepay_app_access_token', 'STAGE_dd03380f527259774275481377cd4e58799778e4453ae9dbfc2bf3221ee197af '); //
// defined('processing_fee') OR define('processing_fee', '.05'); //


defined('we_pay_library') OR define('we_pay_library', $_SERVER['DOCUMENT_ROOT'] . '/application/libraries/wepay.php'); //
defined('wepay_account_id') OR define('wepay_account_id', '1336580099');
defined('wepay_client_id') OR define('wepay_client_id', '74601');
defined('wepay_client_secret') OR define('wepay_client_secret', '29c26803e6');
defined('wepay_app_access_token') OR define('wepay_app_access_token', 'PRODUCTION_90d635ea8ab37335dec056ed4223c6f8d9dc7b8e54f66f245ab5c45d5719b162'); //
defined('processing_fee') OR define('processing_fee', '.05'); 


/*
defined('we_pay_library') OR define('we_pay_library', $_SERVER['DOCUMENT_ROOT'] . '/application/libraries/wepay.php'); //
defined('wepay_account_id') OR define('wepay_account_id', '1454486873');
defined('wepay_client_id') OR define('wepay_client_id', '181919');
defined('wepay_client_secret') OR define('wepay_client_secret', 'd6c323497b');
defined('wepay_app_access_token') OR define('wepay_app_access_token', 'STAGE_2c6e053cd4938dcc0b1cf41b40298383d1017d896a131b8fc34756e904bf374a '); //
defined('processing_fee') OR define('processing_fee', '.05'); */





defined('ORG_GALLERY_IMAGE') OR define('ORG_GALLERY_IMAGE', 'uploads/organization/gallery/thumb/'); //

defined('GOOGLE_CLIENT_ID') OR define('GOOGLE_CLIENT_ID', '110797472005-42n032m34ij2pqdq3b9iqhqtelp97oqn.apps.googleusercontent.com'); //
defined('GOOGLE_CLIENT_SECRET') OR define('GOOGLE_CLIENT_SECRET', 'wS8Df7vcDUEqq3lKaSUVhHwU'); //
define('FB_APP_ID','680222178695742');
define('CONSUMER_KEY', 'j8ruLTnFA89MzLOiiyniXIODV');
define('CONSUMER_SECRET', '6rjkjZFEcSbdRzvlgLH3IQl5JclnNTwbEm9bkCrcyfUECxnnpZ');

