<?php

class Admin_model extends CI_Model {
	
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
	}
	
    public function check_login($where)
    {
        return $this->db->where($where)->get('admin')->num_rows();
    }

    public function get_info($where)
    {
        return $this->db->where($where)->get('admin')->row();
    }

    public function get_login_history($where)
    {
        return $this->db->where($where)->get('login_history')->result();
    }

    public function insert_login_history($data)
    {
        return $this->db->insert('login_history',$data);
    }

}
