<?php

class User_model extends CI_Model {

    public $keyword = '';
    public $sortby = '';
    public $location = '';
    public $sorttype = '';
    public $cat_id = '';

    public function __construct() {

        // Call the CI_Model constructor

        parent::__construct();
    }

    //Backend panel
    public function get_user($args, $search) {
        $this->db->limit($args['no_of_items'], $args['offset']);
        unset($args['no_of_items']);
        unset($args['offset']);
        if (!empty($args))
            $this->db->where($args);
        $this->db->select('users.*,users.id as userid')
                ->select('(select count(id) from campaign_donations where user_id = users.id) as donation_count')
                ->select('(select count(id) from campaigns where user_id = users.id) as creation_count')
                ->where('status', '1')
                ->or_where('status', '0')
                ->from('users')
        ;
        $records = $this->db->get()->result();

        // get count of all records for paginatoin
        if (!empty($args))
            $this->db->where($args);
        $total = $this->db->from('users')->count_all_results();

        return [$records, $total];
    }

    public function updateUserStatusBackend($where) {

        $query = "update users set `status` = not `status` where id= '" . $where['id'] . "'";

        return $this->db->query($query);
    }

}
