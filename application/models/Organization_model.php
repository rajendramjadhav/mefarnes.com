<?php

class Organization_model extends CI_Model {

    public $keyword = '';
    public $sortby = '';
    public $location = '';
    public $sorttype = '';
    public $cat_id = '';

    public function __construct() {

        // Call the CI_Model constructor

        parent::__construct();
    }

    public function getOganization($where) {

        $this->db->select('users.*');
        $this->db->where($where);
        $query = $this->db->get('users');
        return $query->result();
    }
    
    public function getCampaigns($where) {

        $this->db->select('campaigns.*');
        $this->db->select('(select count(id) from campaign_donations where campaign_id = campaigns.id)  as donor');
        $this->db->where($where);
        $query = $this->db->get('campaigns');
        return $query->result();
    }
    
     public function getGallery($where) {

        $this->db->select('organization_gallery.*');
        $this->db->where($where);
        $query = $this->db->get('organization_gallery');
        return $query->result();
    }
}
