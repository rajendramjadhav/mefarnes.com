<?php

class Users_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
		$this->config = array(
		'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"])."/upload/",
		'upload_url' => base_url()."upload/",
		'allowed_types' => "gif|jpg|png|jpeg",
		'overwrite' => TRUE,
		'max_size' => "100000KB",
		'max_height' => "768",
		'max_width' => "1024"
		);
    }

    public function insert_user($data) {

        $this->db->insert('users', $data);
        return $this ->db ->insert_id();
    }

    public function check_user_with_token($data) {
        return $this->db->where($data)->get('users')->result();
    }

    public function update_user_with_token($where, $data) {
        return $this->db->where($where)->update('users', $data);
    }

    public function check_login($where) {
        return $this->db->where($where)->get('users')->num_rows();
    }

    public function get_info($where) {
        return $this->db->where($where)->get('users')->row();
    }

    public function get_users() {
        return $this->db->get('users')->result();
    }

    public function get_userCampaigns($uId) {
        $campaigns = $this->db->get_where('campaigns', array('user_id=' => $uId))->result();
        return $campaigns;
    }

    public function getUserDtls($uId) {
        $userDtl = $this->db->get_where('users', array('id=' => $uId))->result();
        return $userDtl;
    }
    
    public function get_user_info_from_campaign($campaignId) {
        $this->db->select('users.new_wepay_account_id,users.new_wepay_access_token,users.wepay_user_id')
                ->from('users')
                ->join('campaigns','campaigns.user_id=users.id')
                 ->where('campaigns.id',$campaignId)   ;
        $query = $this->db->get()->row();
        // echo $this->db->last_query();
        return $query;
    }
	public function update_organization_profile($where,$data)
	{
		$this->db->where($where);
        $this->db->update('users', $data);
        return $this->db->last_query();	
	}
	public function getOrganizationProfileDetail($userID)
	{	
		$query = "SELECT * FROM users WHERE id=".$userID."";	
		$list = $this->db->query($query);
        return $list->row();
	}
	public function add_organization_profile_gellary_image($data)
	{
		$this->db->insert('organization_gallery', $data);
        return $this ->db ->insert_id();
	}
	public function getAllGellaryImage($userID)
	{
//echo "SELECT * FROM organization_gallery WHERE organization_id=".$userID." AND isImage=1";exit;
		$query = "SELECT * FROM organization_gallery WHERE organization_id=".$userID." AND isImage=1";
		$result = $this->db->query($query);
        return $result->result();	
	}
	public function getVideosLink($userID)
	{
		$query = "SELECT * FROM organization_gallery WHERE organization_id=".$userID." AND isImage=0";
		$result = $this->db->query($query);
        return $result->row();	
	}
	public function getAllVideosLink($userID)
	{
		$query = "SELECT * FROM organization_gallery WHERE organization_id=".$userID." AND isImage=0";
		$result = $this->db->query($query);
        return $result->result();	
	}
	public function deletegelleryimage($id) {
		$data=array('id'=>$id);
		$result=$this->db->delete('organization_gallery', $data);
	}
	public function addvideolink($userID)
	{

		for($i=1;$i<=$this->input->post('counter');$i++)
		{
			$record = array(
						'organization_id'=>$userID,
						'image_name'=>$_POST['videourl'.$i],
						'isImage'=>0
						);
			$query = $this->db->insert('organization_gallery', $record);
		}
	}
	public function getOrganization($userID)
	{
		$query = "SELECT org_slug FROM users WHERE id=".$userID."";	
		$result = $this->db->query($query);
		return $result->row();
	}
	public function getOrganizationID($url)
	{
		//echo "SELECT id FROM users WHERE org_slug='".$url."'";exit;
		$query = "SELECT id FROM users WHERE org_slug='".$url."'";	
		$result = $this->db->query($query);
		return $result->row();
	}
	public function getAllCampaign($userID)
	{
		$query = "SELECT * FROM campaigns WHERE user_id=".$userID."";
		$result = $this->db->query($query);
        return $result->result();
	}
	
	public function resetPassword($Id)
	{
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('id',$Id);
		$query = $this->db->get()->result();
		return $query;
		
	}
	
	public function UpdatePass($Id, $VarPass)
	{
		$this->db->where('id', $Id);
		$this->db->update('users', $VarPass);
	}
	
	public function deactivateAccount($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('users',$status);
		$query = $this->db->affected_rows();
		return $query;
	}
	
	
	public function insert_campaign_gellary_image($data,$id)
	{
		//var_dump($data);exit;
		$this->db->where('id', $id);
		$this->db->update('users', $data);
		
        //return $this ->db ->insert_id();
        return $this->db->last_query();
	}
	 
	public function getemailid($emailid)
	{
		$this->db->select('email');
		$this->db->from('users');
		$this->db->where('email',$emailid);
		$query = $this->db->get()->result();
		return $query;
		
	}
	 
	
}
