<?php

class Campaign_model extends CI_Model {

    public $keyword = '';
    public $sortby = '';
    public $location = '';
    public $sorttype = '';
    public $cat_id = '';

    public function __construct() {

        // Call the CI_Model constructor

        parent::__construct();
    }

    public function getCampaign($where) {

        $this->db->select('campaigns.*');

        $this->db->select('(select count(id) from campaign_donations where campaign_id = campaigns.id)  as donor');
        $this->db->select('(select coalesce(sum(amount),"0") from campaign_donations where campaign_id = campaigns.id)  as donation_amount');
        $this->db->where($where);

        $this->db->order_by('create_date desc');

        $query = $this->db->get('campaigns');

        return $query->result();
    }

    public function updateCampaign($data, $where) {

        $this->db->where($where);

        $this->db->update('campaigns', $data);

        return $this->db->last_query();
    }

    public function updateCampaignStatus($where) {

        $query = "update campaigns set `donation_status` = not `donation_status` where user_id= '" . $where['user_id'] . "' and id= '" . $where['id'] . "'";

        $this->db->query($query);

        return true;
    }

    public function getCampaignBySlug($where) {

        $this->db->select('campaigns.*,campaigns.campaign_location_id as location,campaign_categories.name as category_name');

        $this->db->select('(select count(id) from campaign_donations where campaign_id = campaigns.id)  as donor');

        $this->db->select('(select coalesce(sum(amount),"0") from campaign_donations where campaign_id = campaigns.id)  as collected_fund');

        $this->db->select('CASE WHEN (users.org_name IS NULL or users.org_name="")

            THEN concat(users.first_name," ",users.last_name)

            ELSE users.org_name 

       END AS user_name');
      $this->db->select('users.user_image');

        $this->db->join('users', 'campaigns.user_id =users.id');
        $this->db->join('campaign_categories', 'campaigns.campaign_categoriy_id =campaign_categories.id');

        $this->db->where($where);
        $this->db->where('campaigns.status', '1');

        $query = $this->db->get('campaigns');

       // echo $this->db->last_query();

        return $query->row();
    }

    public function getDonationDtl($id) {

        $this->db->select('campaign_donations.*');
        $this->db->select('concat(donor_first_name," ",donor_last_name) as user_name');
//        $this->db->select('CASE WHEN (users.org_name IS NULL or users.org_name="")
//
//            THEN concat(users.first_name," ",users.last_name)
//
//            ELSE users.org_name 
//
//       END AS user_name');
//
//        $this->db->join('users', 'users.id = campaign_donations.user_id');

       // $this->db->where(array('campaign_donations.campaign_id' => $id, 'is_public' => '1'));
       $this->db->where(array('campaign_donations.campaign_id' => $id));

        $this->db->order_by('campaign_donations.create_date', 'desc');

        $this->db->limit(5);

        $query = $this->db->get('campaign_donations');
        return $query->result();
    }

    public function getDonationCount($id) {

        $this->db->select('campaign_donations.id');

        $this->db->where(array('campaign_donations.campaign_id' => $id));

        $query = $this->db->get('campaign_donations');
        return $query->num_rows();
    }

    public function getUpdates($id) {
        $this->db->select('campaings_updates.*');
        $this->db->select('CASE WHEN (users.org_name IS NULL or users.org_name="")
            THEN concat(users.first_name," ",users.last_name)
            ELSE users.org_name
        END AS user_name');
        $this->db->select('(select count(id) from campaign_update_loves where campaign_update_id = campaings_updates.id)  as total_love');
        $this->db->join('users', 'users.id = campaings_updates.user_id');
        $this->db->where(array('campaings_updates.campaign_id' => $id));
        $this->db->order_by('campaings_updates.create_date', 'desc');
        $query = $this->db->get('campaings_updates');
        return $query->result();
    }
	/*
	public function getupdatesdashboard($id){
        //echo "SELECT COUNT(*) as count FROM campaings_updates where user_id = '".$id."'";exit;
        $query = "SELECT COUNT(*) as count FROM campaings_updates where user_id = '".$id."'";	
            $list = $this->db->query($query);
            return $list->row();
   	}
*/
	public function getupdatesdashboardbycampign($id){
            $this->db->select('campaings_updates.*')
                     ->from('campaings_updates')
                     ->join('campaigns','campaigns.id = campaings_updates.campaign_id','left')
				     ->where('campaings_updates.user_id',$id)
				     ->where('campaigns.status',1)
				     ->group_by('campaings_updates.id');
             $query = $this->db->get()->result();
             return $query;
			 
			
			
   	}
  public function getupdatesdashboardcount($id){
            $this->db->select('count(campaings_updates.id) as count')
                ->from('campaings_updates')
                ->join('campaigns','campaigns.id = campaings_updates.campaign_id','left')
				->where('campaings_updates.user_id',$id)
				->where('campaigns.status',1)
				->group_by('campaigns.id');
            $query = $this->db->get()->row();
            return $query;;
	}
public function get_usersbydashboard($id)

    {
       $this->db->select('users.*')
	            ->from('users')
	            ->where('id',$id);
		$query = $this->db->get()->row();
	    return $query;
			   
	   //$query = "SELECT * FROM users WHERE id ='".$id."'";	
		//$list = $this->db->query($query);
        //return $list->row();

    }

public function get_emailsharecount($id){
	$this->db->select('count(campaign_updates_subscribers.id) as count')
	         ->from('campaign_updates_subscribers')
			 ->join('campaigns','campaigns.id=campaign_updates_subscribers.campaign_id','inner')
			 ->where('campaigns.user_id',$id)
			 ->where('subscribed',1)
			 ;
            $query = $this->db->get()->row();
	return $query;
	
}

public function inserttwitterdata($param,$Id){
	$this->db->insert('twitterdata', $param);
        return $this ->db ->insert_id();
}

public function edittwitterdata($param,$Id){
	$this->db->where('user_id',$id);
     $this->db->update('twitterdata', $param);
	 return $this->db->last_query();
}



public function get_twitteralldata($id){
	$this->db->select('twitterdata.*')
	          ->from('twitterdata')
			  ->where('user_id',$id);
			  $query = $this->db->get()->row();
			  return $query;
}




    public function getComments($id) {
        $this->db->select('campaign_comments.*');
        $this->db->select('CASE WHEN (users.org_name IS NULL or users.org_name="")
            THEN concat(users.first_name," ",users.last_name)
            ELSE users.org_name
        END AS user_name');
        $this->db->select('(select count(id) from comment_replies where comment_id = campaign_comments.id)  as total_reply');
        $this->db->join('users', 'users.id = campaign_comments.user_id');
        $this->db->where(array('campaign_comments.campaign_id' => $id));
        $this->db->order_by('campaign_comments.create_date', 'desc');
        $query = $this->db->get('campaign_comments');
        $results = $query->result();

        $comments = [];
        foreach ($results as $result):
            $this->db->select('comment_replies.*');
            $this->db->select('CASE WHEN (users.org_name IS NULL or users.org_name="")
            THEN concat(users.first_name," ",users.last_name)
            ELSE users.org_name
        END AS user_name');
            $this->db->join('users', 'users.id = comment_replies.user_id');
            $this->db->where(array('comment_replies.comment_id' => $result->id));
            $this->db->order_by('comment_replies.reply_date', 'desc');
            $this->db->limit(2);
            $query = $this->db->get('comment_replies');
            $replies = $query->result();

            $result->replies = $replies;

            $comments[] = $result;
        endforeach;

        return $comments;
    }

    public function getTotCampaign() {

        $this->db->select('count(cm.id) as Totcoures')
                ->from('campaigns' . ' as cm')
                ->join('campaign_categories as cc', 'cm.campaign_categoriy_id=cc.id')
                ->join('users as u', 'cm.user_id=u.id')
                //->join('campaign_locations as cl', 'cl.id=cm.campaign_location_id')
                ->where('cm.status', '1')
                ->where('cm.preview_mode', '0')
                ->where('cc.status', '1');
        if ($this->location != "") {
            $this->db->where("cm.campaign_location_id", $this->location);
        }
        if ($this->cat_id != "") {
            $this->db->where("cc.id", $this->cat_id);
        }
        if ($this->category != "") {
            $this->db->where("cm.campaign_categoriy_id", $this->category);
        }

        if ($this->keyword != "") {
            $this->db->where("(cm.title" . " like '%" . $this->keyword . "%' or cm.description" . " like '%" . $this->keyword . "%' or cc.name" . " like '%" . $this->keyword . "%')");
        }


        $query = $this->db->get()->result_array();

        $val = $query[0]['Totcoures'];
        return $val;
    }

    public function getSreachCampaign($limit_from = '', $total_row_display = '') {

        $this->db->select('cm.*,u.first_name,u.last_name,u.org_name,u.image as user_image,cm.campaign_location_id as location,cc.name as category_name')
                ->select('(select coalesce(sum(amount),"0") from campaign_donations where campaign_id = cm.id)  as collected_fund')
                ->select('(select count(id) from campaign_donations where campaign_id = cm.id)  as donor')
                ->from('campaigns as cm')
                ->join('campaign_categories as cc', 'cm.campaign_categoriy_id=cc.id')
                ->join('users as u', 'cm.user_id=u.id')
                ->where('cm.status', '1')
                ->where('cc.status', '1')
                ->where('cm.preview_mode', '0')
                ->where('cm.end_date_time >=', date('Y-m-d H:i:s'));

        if ($this->cat_id != "") {
            $this->db->where("cc.id", $this->cat_id);
        }
        if ($this->location != "") {
            $this->db->where("cm.campaign_location_id " . " like '%" . $this->location . "%' ");
        }
        if ($this->keyword != "") {
            $this->db->where("(cm.title" . " like '%" . $this->keyword . "%' or cm.description" . " like '%" . $this->keyword . "%' or cc.name" . " like '%" . $this->keyword . "%')");
        }
        if ($this->category != "") {
            $this->db->where("cm.campaign_categoriy_id", $this->category);
        }
        if ($this->sortby != "") {
            if ($this->sortby == 'fund') {
                $this->db->order_by('cm.fund_needed', $this->sorttype);
            }if ($this->sortby == 'date') {
                $this->db->order_by('cm.start_date_time', $this->sorttype);
            }
        } else {
            $this->db->order_by('cm.id', 'desc');
        }
        if ($limit_from != '' && $total_row_display != '') {
            $this->db->limit($total_row_display, $limit_from);
        }



        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query;
    }

    public function getTotDonor() {
        $this->db->select('cd.campaign_id,count(cd.id) as donor')
                ->from('campaign_donations as cd');
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query;
    }

    
    public function getCampaignGallery($id) {
        $this->db->select('campaign_gallery.*');
        $this->db->where(array('campaign_id' => $id));
        $this->db->order_by('create_date', 'desc');
        $query = $this->db->get('campaign_gallery');
        return $query->result();
        
        
    }
    
    public function check_report($where) {
        return $this->db->where($where)->get('campaign_reports')->num_rows();
    }

    public function check_campaign_user($where) {
        return $this->db->where($where)->get('campaigns')->num_rows();
    }

    //Backend panel
    public function get_campaigns($args, $search, $type) {
        $this->db->limit($args['no_of_items'], $args['offset']);
        unset($args['no_of_items']);
        unset($args['offset']);
        if (!empty($args))
            $this->db->where($args);

        $this->db->select('cm.*')
                ->select('CASE WHEN (u.org_name IS NULL or u.org_name="")
                             THEN concat(u.first_name," ",u.last_name)
                            ELSE u.org_name 
                            END AS user_name')
                ->select('(select coalesce(sum(amount),"0") from campaign_donations where campaign_id = cm.id)  as collected_fund')
                ->select('(select count(id) from campaign_donations where campaign_id = cm.id)  as donor')
                ->from('campaigns as cm')
                ->join('campaign_categories as cc', 'cm.campaign_categoriy_id=cc.id')
                ->join('users as u', 'cm.user_id=u.id')
                ->where('cm.status !=', '-1')
                ->where('cc.status', '1');


        if (!empty($search)) {
            if (!empty($search['title'])) {
                $this->db->like('title', $search['title']);
            }
            if (!empty($search['userid'])) {
                $this->db->like('user_id', $search['userid']);
            }
            if (!empty($search['from_date']) && (empty($search['to_date']))) {
                $this->db->where('date(cm.start_date_time)>=', date('Y-m-d', strtotime($search['from_date'])));
            }
            if (!empty($search['to_date']) && (empty($search['from_date']))) {
                $endDate = date('Y-m-d', strtotime($search['to_date']));
                $this->db->where('date(cm.end_date_time) <= "' . $endDate . '"');
            }
            if (!empty($search['to_date']) && (!empty($search['from_date']))) {
                //echo $search['to_date']; exit;
                $start_date = date('Y-m-d', strtotime($search['from_date']));
                $this->db->where('date(cm.start_date_time)>=', $start_date);
                $this->db->where('date(cm.end_date_time)<=', date('Y-m-d', strtotime($search['to_date'])));
            }
        }

        if ($type == 'active')
            $this->db->where('date(cm.end_date_time)>=', date('Y-m-d'));
        if ($type == 'expired')
            $this->db->where('date(cm.end_date_time)<', date('Y-m-d'));

        $this->db->order_by('cm.end_date_time', 'asc');
        $records = $this->db->get()->result();

        // get count of all records for paginatoin
        if (!empty($args))
            $this->db->where($args)->where('status !=', '-1');
        $total = $this->db->from('campaigns')->count_all_results();

        return [$records, $total];
    }

    public function updateCampaignStatusBackend($where) {

        $query = "update campaigns set `status` = not `status` where id= '" . $where['id'] . "'";

        return $this->db->query($query);
    }

    public function get_total_donation($id) {
        $this->db->select('coalesce(sum(campaign_donations.amount),0) as total_donation');

        $this->db->where(array('campaign_donations.campaign_id' => $id));

        $query = $this->db->get('campaign_donations');
        return $query->row();
    }

    public function reported_campaign($where) {
        $this->db->select('campaign_reports.campaign_id as campaign_id');
        $this->db->group_by('campaign_reports.campaign_id');
        $query = $this->db->get('campaign_reports');

        $campaign_reports = $query->result();
        $campaigns = [];
        foreach ($campaign_reports as $campaign):
            $this->db->select('campaigns.*');
            $this->db->select('( select count(campaign_reports.id) from campaign_reports where campaign_id ="' . $campaign->campaign_id . '") total_report ');
            $this->db->select('CASE WHEN (u.org_name IS NULL or u.org_name="")
                             THEN concat(u.first_name," ",u.last_name)
                            ELSE u.org_name 
                            END AS user_name');
            $this->db->select('(select coalesce(sum(amount),"0") from campaign_donations where campaign_id = "' . $campaign->campaign_id . '")  as collected_fund');
            $this->db->join('users u', 'u.id = campaigns.user_id');
            $this->db->where('campaigns.id', $campaign->campaign_id);
            //$this ->db ->where ('date(campaigns.create_date) <=',   date('Y-m-d', strtotime('-6 months', strtotime(date('Y-m-d')))));
            $this->db->where('campaigns.status !=', '-1');
            $query_sub = $this->db->get('campaigns');

            $campaigns[] = $query_sub->row();
        endforeach;

        return $campaigns;
    }

    public function getReportDtl($where) {

        $this->db->select('campaign_reports.*');
        $this->db->select('CASE WHEN (users.org_name IS NULL or users.org_name="")

            THEN concat(users.first_name," ",users.last_name)

            ELSE users.org_name 

       END AS user_name');

        $this->db->join('users', 'users.id = campaign_reports.user_id');

        $this->db->where(array('campaign_reports.campaign_id' => $where['campaign_reports.campaign_id']));

        $this->db->order_by('campaign_reports.report_date', 'desc');

        $query = $this->db->get('campaign_reports');
        return $query->result();
    }

    public function get_featured_campaign($where) { 
        $this->db->select('cm.*,u.first_name,u.last_name,u.org_name,u.image as user_image,cm.campaign_location_id as location,cc.name as category_name')
                ->select('CASE WHEN (u.org_name IS NULL or u.org_name="")
                             THEN concat(u.first_name," ",u.last_name)
                            ELSE u.org_name 
                            END AS user_name')
                ->select('(select coalesce(sum(amount),"0") from campaign_donations where campaign_id = cm.id)  as collected_fund')
                ->select('(select count(id) from campaign_donations where campaign_id = cm.id)  as donor')
                ->select('cc.name')
                ->from('campaigns as cm')
                ->join('campaign_categories as cc', 'cm.campaign_categoriy_id=cc.id')
                ->join('users as u', 'cm.user_id=u.id')
                ->where($where)
                ->where('cc.status', '1');

        $this->db->order_by('cm.priority', 'asc');
        $this->db->order_by('cm.end_date_time', 'asc');
        return $records = $this->db->get()->result();
    }
	public function checkcommentfbuser($oauth_uid)
	{
		$query = "SELECT * FROM comment_user WHERE oauth_uid=".$oauth_uid."";	
		$list = $this->db->query($query);
        return $list->row();	
	}
	/*
	public function getemail($userId){
		$query = "SELECT email FROM users WHERE id ='".$userId."'";	
		$list = $this->db->query($query);
        return $list->row();
	}
	 */
	public function addcommentfbuser($data) {

        $this->db->insert('comment_user', $data);
        return $this ->db ->insert_id();
    }
 
    public function insertdata($data,$table)
	{
        $this->db->insert($table, $data);
        return $this->db->insert_id();
	}
	public function checkipaddress($ipaddress)
	{
		$query = "SELECT * FROM campaign_visits WHERE ip_address ='".$ipaddress."'";	
		$list = $this->db->query($query);
        return $list->row();	
	}
   public function getuseremail($user_id)
	{
		//echo "SELECT email FROM users WHERE id ='".$user_id."'";exit;
		$query = "SELECT email FROM users WHERE id ='".$user_id."'";	
		$list = $this->db->query($query);
        return $list->row();	
	}
	
	/*
	 public function checkipaddress($where,$table)

    {

         return $this->db->where($where)->get($table)->result();

    }
	 */
	  public function totalvisit($user_id)
	{
        //$query = "SELECT COUNT(*) as count FROM campaign_visits where user_id = '".$where."'";	
            //$list = $this->db->query($query);
            //return $list->row();
             $sql = "SELECT count(cs.id) as count FROM `campaign_visits` cs "
                    . "INNER JOIN campaigns as c on c.id=cs.campaign_id where c.user_id=$user_id";
            $list = $this->db->query($sql);
            return $list->row();
    }
	
	public function getcommentfbuser($id)
	{
		$query = "SELECT * FROM comment_user WHERE userID=".$id."";	
		$list = $this->db->query($query);
        return $list->row();	
	}
        public function campaignCount($where){
            $query = "SELECT COUNT(*) as count FROM campaign_shared_information $where";	
            $list = $this->db->query($query);
            return $list->row();
        }
        public function sareInfoSave($insertData){
            $this->db->insert('campaign_shared_information',$insertData);
        }
        public function totalShareCount($user_id,$shareAt){
            $sql = "SELECT count(cs.id) as count FROM `campaign_shared_information` cs "
                    . "INNER JOIN campaigns as c on c.id=cs.campaign_id where cs.shared_at=$shareAt"
                    . " AND c.user_id=$user_id";
            $list = $this->db->query($sql);
            return $list->row();
        }
		

        public function getCampaingbydonar($value="")
        {
            $this->db->select('*,campaign_donations.create_date as d_date');
            $this->db->from('campaign_donations');
            $this->db->join('campaigns', 'campaigns.id = campaign_donations.campaign_id');
            $this->db->where(array('campaign_donations.email' => $value));
            $query = $this->db->get();
            return $query->result();
        }

        public function getTotaldonor($id)
        {
            $this->db->select('count(id) as t_donor');
            $this->db->from('campaign_donations');
            $this->db->where(array('campaign_id' => $id));
            $query = $this->db->get();
            return $query->result();
        }

}
