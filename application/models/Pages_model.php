<?php

class Pages_model extends CI_Model {

    public function __construct() {

        // Call the CI_Model constructor

        parent::__construct();
    }

    public function get_campaigns($args) {
        $this->db->limit($args['no_of_items'], $args['offset']);
        unset($args['no_of_items']);
        unset($args['offset']);
        if (!empty($args))
            $this->db->where($args);

        $this->db->select('site_contacts_log.*');
        $this->db->order_by('site_contacts_log.create_date', 'desc');
        
        
        $records = $this->db->get('site_contacts_log')->result();
        //return $this ->db ->last_query();
        // get count of all records for paginatoin
        if (!empty($args))
            $this->db->where($args);
        
        $total = $this->db->from('site_contacts_log')->count_all_results();

        return [$records, $total];
    }

}
