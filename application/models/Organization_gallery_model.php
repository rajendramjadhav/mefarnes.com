<?php

class Organization_gallery_model extends CI_Model {

    public $keyword = '';
    public $sortby = '';
    public $location = '';
    public $sorttype = '';
    public $cat_id = '';

    public function __construct() {

        // Call the CI_Model constructor

        parent::__construct();
    }

     public function getGallery($where) {

        $this->db->select('campaign_gallery.*');
        $this->db->where($where);
        $query = $this->db->get('campaign_gallery');
        return $query->result();
    }
}
