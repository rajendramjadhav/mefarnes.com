<?php

class Cron_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_reminder() {

        $sql = "select id as userId,create_date,email from users where date(create_date) >= DATE_SUB(CURDATE(), INTERVAL 3 DAY) having (select count(id) from campaigns where user_id=userId ) =0";
        $list = $this->db->query($sql);
        return $list->result();
    }

    public function get_subscriber_user() {
        $sql = "SELECT campaings_updates.campaign_id,campaings_updates.title,campaings_updates.description,campaigns.title as campaign_title
                FROM `campaings_updates` inner join campaigns on campaings_updates.campaign_id = campaigns.id
                WHERE date(campaings_updates.create_date) >= DATE_SUB( CURDATE(), INTERVAL 1 HOUR)";
        $list = $this->db->query($sql);
        $campaigns = $list->result();
        $updatedCampaigns = [];
        foreach ($campaigns as $campaign):
            $sql = "select users.id,users.email,users.first_name,users.last_name,users.org_name from
                    users inner join campaign_updates_subscribers on users.id= campaign_updates_subscribers.user_id
                    where campaign_updates_subscribers.campaign_id = '" . $campaign->campaign_id . "' and campaign_updates_subscribers.subscribed=1";
            $users = $this->db->query($sql);
            $campaign->subscribers = $users->result();
            $updatedCampaigns[] = $campaign;
        endforeach;

        return $updatedCampaigns;
    }

}
