<?php

class Category_model extends CI_Model {

    public $keyword = '';
    public $sortby = '';
    public $location = '';
    public $sorttype = '';
    public $cat_id = '';

    public function __construct() {

        // Call the CI_Model constructor

        parent::__construct();
    }

    //Backend panel
    public function get_category($args, $search) {
        $this->db->limit($args['no_of_items'], $args['offset']);
        unset($args['no_of_items']);
        unset($args['offset']);
        if (!empty($args))
            $this->db->where($args);
        $this->db->select('campaign_categories.*,campaign_categories.id as category_id')
                ->select('(select count(id) from campaigns where campaign_categoriy_id=category_id) as toatal_campaign')
                ->where('status', '1')
                ->or_where('status', '0')
                ->from('campaign_categories')
                ->order_by('name', 'asc')
        ;
        $records = $this->db->get()->result();

        // get count of all records for paginatoin
        if (!empty($args))
            $this->db->where($args);
        $total = $this->db->from('campaign_categories')->count_all_results();

        return [$records, $total];
    }

    public function updateUserStatusBackend($where) {

        $query = "update campaign_categories set `status` = not `status` where id= '" . $where['id'] . "'";

        return $this->db->query($query);
    }

}
