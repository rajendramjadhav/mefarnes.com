<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller { //Authenticate

    public function __Construct() {

        parent::__Construct();
        $this->load->model('Users_model', 'UM'); // load model
        $this->load->model('Common_model', 'CM'); // load model
        $this->load->model('Cron_model', 'CRM'); // load model
        $this->output->enable_profiler(TRUE);
    }

    public function reminder() {
        $users = $this->CRM->get_reminder();

        $emailData = new stdClass();
        foreach ($users as $key => $user):

            $hour = $this->hour_from_date(date("Y-m-d H:i:s"), $user->create_date);

            if ($hour == 20) {
                $emailData = $this->CM->get_single_row_from_id(['id' => 1], 'email_templates');
                $data['email_data'] = $emailData;
                $data['user'] = $this->CM->get_single_row_from_id(['id' => $user->userId], 'users');
                $message = $this->load->view("template/reminder-after-creating-account", $data, true);
                send_mail_to_user($emailData->subject, $message, $user->email);
            } else if ($hour == 48) {
                $emailData = $this->CM->get_single_row_from_id(['id' => 2], 'email_templates');
                $data['email_data'] = $emailData;
                $data['user'] = $this->CM->get_single_row_from_id(['id' => $user->userId], 'users');
                $message = $this->load->view("template/reminder-after-creating-account", $data, true);
                send_mail_to_user($emailData->subject, $message, $user->email);
            } else if ($hour == 72) {
                $emailData = $this->CM->get_single_row_from_id(['id' => 3], 'email_templates');
                $data['email_data'] = $emailData;
                $data['user'] = $this->CM->get_single_row_from_id(['id' => $user->userId], 'users');
                $message = $this->load->view("template/reminder-after-creating-account", $data, true);
                send_mail_to_user($emailData->subject, $message, $user->email);
            }
            //mail('bikashpal2008@gmail.com','Test','Message from controller');
        endforeach;
    }

    private function hour_from_date($date_one, $dte_two) {
        $date1 = new DateTime($date_one);
        $date2 = new DateTime($dte_two);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $hours = $hours + ($diff->days * 24);

        return intval($hours);
    }

    public function campaign_update_subscribers() {
        $users = $this->CRM->get_subscriber_user();

        foreach ($users as $user):
            $emailData = $this->CM->get_single_row_from_id(['id' => 3], 'email_templates');
            $data['email_data'] = $emailData;
            $data['user'] = $this->CM->get_single_row_from_id(['id' => $user->userId], 'users');
            $message = $this->load->view("template/reminder-after-creating-account", $data, true);
            send_mail_to_user($emailData->subject, $message, $user->email);
        endforeach;
    }

}
