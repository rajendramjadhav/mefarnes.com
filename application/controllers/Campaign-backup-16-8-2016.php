<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends Authenticate { //Authenticate

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Users_model', 'UM'); // load model
        $this->load->model('Common_model', 'CM'); // load model
        $this->load->model('Campaign_model', 'CMM');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index() {
        $this->layouts->set_title("My Campaigns");
        $data = [];
        $this->layouts->render('campaign/my-campaign', $data, 'default-signedin');
    }

    public function create() {
        $this->layouts->set_title("Create Campaign");

        $params = array(
            'status' => 1
        );
        $data['categories'] = $this->CM->get_categories($params, 'campaign_categories');
        $data['locations'] = $this->CM->get_categories($params, 'campaign_locations');
        $data['error'] = '';
        if ($this->input->post('campaign_duration')) {

            $this->form_validation->set_rules('campaignPics', 'Campaign Cover Image', 'callback_cover_image_upload');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean|is_unique[users.email]');
            $this->form_validation->set_rules('campaign_story', 'Campaign Story', 'trim|required|xss_clean');
            $this->form_validation->set_rules('category', 'Category', 'trim|required|xss_clean');
            $this->form_validation->set_rules('campaign_duration', 'Campaign Duration', 'required|trim|xss_clean');
            $this->form_validation->set_rules('video_link', 'Video Link', 'trim|xss_clean');
            $this->form_validation->set_rules('location', 'Location', 'required');
            $this->form_validation->set_rules('fund_needed', 'Fund Needed', 'trim|required|xss_clean');


            if ($this->form_validation->run() == FALSE) {
                $data['error'] = validation_errors();
            } else {
                $this->session->set_flashdata('message', 'Error');
                $datas = $this->security->xss_clean($this->input->post());
                $videoId = $this->input->post('video_link');
                if($videoId!='')
                {
                 $videoDtl = videoType($videoId);
                 if($videoDtl['videoId']=='')
                 {
                    $videoDtl['videoType']=NULL;
                    $videoDtl['videoId']=NULL;
                 }
                }
                else {
                    $videoDtl['videoType']=NULL;
                    $videoDtl['videoId']=NULL;
                }
                $startDate = $this->input->post('campaign_duration');
                $startDate = $this->input->post('campaign_duration');
                $endDate = date('Y-m-d 23:59:59', strtotime("+$startDate"));
                $slug = preg_replace('/\s+/', '-', $this->input->post('title'));
                $arr = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'campaign_categoriy_id' => $this->input->post('category'),
                    'title' => $this->input->post('title'),
                    'slug'  => strtolower($slug),
                    'image' => $this->input->post('image'),
                    'description' => $this->input->post('campaign_story'),
                    'video_type' => $videoDtl['videoType'],
                    'video_file_name' => $videoDtl['videoId'],
                    'campaign_location_id' => $this->input->post('location'),
                    'fund_needed' => $this->input->post('fund_needed'),
                    'start_date_time' => date("Y-m-d H:i:s"),
                    'end_date_time' => $endDate,
                    'status' => 1,
                    'donation_status' => 1,
                    'create_date' => date("Y-m-d H:i:s")
                );
                $campaignId = $this->CM->insert($arr, 'campaigns');

                $this->session->set_flashdata('message', 'Campaign created successfully');
                redirect('my-dashboard');
            }
        }
        $this->layouts->render('campaign/create-campaign', $data, 'default-signedin');
    }

    //cover image upload
    function cover_image_upload() {

        if ($_FILES['campaignPics']['size'] != 0) {
            $upload_dir = './uploads/campaign/cover_images/';
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir);
            }
            $config['upload_path'] = $upload_dir;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['file_name'] = 'campaign_' . substr(md5(rand()), 0, 7);
            $config['overwrite'] = false;
            $config['max_size'] = '1255120';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('campaignPics')) {
                $this->form_validation->set_message('_do_upload', $this->upload->display_errors());
                return FALSE;
            } else {
                $upload_data = $this->upload->data();
                $_POST['image'] = $upload_data['file_name'];
                return true;
            }
        } else {
            $this->form_validation->set_message('image_upload', "No file selected");
            return false;
        }
    }
    
    //make donation
    public function donate()
    {
        $slug = $this->uri->segment(2);
        $where = array('slug' => $slug);
        $campaign = $this->CMM->getCampaignBySlug($where);

        if (empty($campaign)) {
            redirect('home');
        }
        $this->layouts->set_title($campaign->title);
        $data['campaign'] = $campaign;
        
        $this->layouts->render('campaign/donation', $data, 'default');
    }

}
