<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Authenticate { //Authenticate

    public function __Construct() {

        parent::__Construct();

        $this->load->model('Users_model', 'UM'); // load model

        $this->load->model('Common_model', 'CM'); // load model
        
        $this->load->model('Organization_gallery_model', 'OGM');

        $this->load->model('Campaign_model', 'CPM'); // load model
        //$this->output->enable_profiler(TRUE);
    }

    //dashboard page

    public function myaccount() {

        //$this->output->enable_profiler(TRUE);
        //$this->getAccountStatus($this->session->userdata('user_id'));

        $meta = $this->CM->get_single_row_from_id(['slug' => 'dashboard'], 'pages');
		//var_dump($meta);exit;
		//var_dump($meta->page_name);exit;
        $this->layouts->set_title($meta->page_name);
		//var_dump($this->layouts->set_title($meta->page_name));exit;

        $this->layouts->set_meta_title($meta->meta_title);

        $this->layouts->set_meta_description($meta->meta_description);

        $this->layouts->set_meta_keyword($meta->keyword);

        //$this->layouts->set_title("Dashboard");

        $data = [];

        $userId = $this->session->userdata('user_id');
		//var_dump($userId);exit;
        $data['email_contacts'] = $this->CM->get_categories(['user_id' => $userId], 'user_email_contacts');
        $data['campaigns'] = $this->CPM->getCampaign(array('campaigns.user_id' => $userId, 'status' => 1));
		//var_dump($data['campaigns']);exit;
        $data['Fbsharecount'] = $this->CPM->totalShareCount($userId,1);
		//var_dump($data['Fbsharecount']);exit;
        $data['Twittersharecount'] = $this->CPM->totalShareCount($userId,2);
		//var_dump($data['Twittersharecount']);exit;
		//$campaignId = $data['campaigns']->id;
		//var_dump($campaignId);exit;
	$data['Totalcampaignvisit'] = $this->CPM->totalvisit($userId); 
//var_dump($data['Totalcampaignvisit']);exit;
		//$data['Totalupdate'] = $this->CPM->getupdatesdashboard($userId);
            $data['Totalupdate'] = $this->CPM->getupdatesdashboardcount($userId);
	   $data['Totalupdatesbycampaign'] = $this->CPM->getupdatesdashboardbycampign($userId);
           $data['emailsharecount'] = $this->CPM->get_emailsharecount($userId);
		$data['users'] = $this->CPM->get_usersbydashboard($userId);
$data['twitteralldata'] = $this->CPM->get_twitteralldata($userId);
		//var_dump($data['Totalcampaignvisit']);exit;
        $redirect_uri = "https://www.mefarnes.com/user/imported_google_contact";
        $data['google_redirect_url'] = "https://accounts.google.com/o/oauth2/auth?client_id=" . @GOOGLE_CLIENT_ID . "&redirect_uri=" . $redirect_uri . "&scope=https://www.google.com/m8/feeds/&response_type=code";

        $this->layouts->render('user/dashboard', $data, 'default-signedin');
    }

    public function donarAccount()
    {
      
        $meta = $this->CM->get_single_row_from_id(['slug' => 'dashboard'], 'pages');
        //var_dump($meta);exit;
        //var_dump($meta->page_name);exit;
        $this->layouts->set_title($meta->page_name);
        //var_dump($this->layouts->set_title($meta->page_name));exit;

        $this->layouts->set_meta_title($meta->meta_title);

        $this->layouts->set_meta_description($meta->meta_description);

        $this->layouts->set_meta_keyword($meta->keyword);

        //$this->layouts->set_title("Dashboard");

        $data = [];
        $userId = $this->session->userdata('user_id');
    
        $email = $this->session->userdata('userdata')->email;
        $data['donarcampaigns'] = $this->CPM->getCampaingbydonar($email);
        //echo "<div style='margin-top:200px;'></div>";
        $data['total_donation'] = count($this->CPM->getCampaingbydonar($email));

        $this->layouts->render('user/dashboard_donor', $data, 'default-signedin');
    
    }

    //Delete campaign

    public function deleteCampaign() {

        $campaignId = $this->input->post('campaign_id', true);
        $userId = $this->session->userdata('user_id');
        $where = array(
            'user_id' => $userId,
            'id' => $campaignId
        );
        $campaings = $this->CPM->getCampaign($where);
        if (count($campaings) > 0) {

            $data = array('status' => '-1');
            $campaings = $this->CPM->updateCampaign($data, $where);
            $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash());
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );
        echo json_encode($return);
    }

    //turn off donation

    public function turnoffCampaign() {

        $campaignId = $this->input->post('campaign_id', true);

        $userId = $this->session->userdata('user_id');

        $where = array(
            'user_id' => $userId,
            'id' => $campaignId
        );

        $campaings = $this->CPM->getCampaign($where);



        if (count($campaings) > 0) {



            $campaings = $this->CPM->updateCampaignStatus($where);

            $return = array('message' => 'Success', 'code' => 200, 'campaings' => $campaings, 'token' => $this->security->get_csrf_hash());
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );



        echo json_encode($return);
    }

    public function settings() {
        $this->layouts->set_title("Account Settings");
        $data = [];
		$data['type'] = 'account';
		$userId = $this->session->userdata('user_id');
		$uId = $userId;
		$dataUser = $this->UM->getUserDtls($uId);
		$data['data'] = $dataUser[0];
        $this->layouts->render('user/account-settings', $data, 'default-signedin');
    }
	
	public function resetPassword(){
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('confpass', 'confirmation password', 'required|matches[password]');
		
		if($this->form_validation->run() == FALSE)
		{
			$pwd = $this->input->post('password');
			$newPass = $this->check_password($pwd);
			if($newPass == TRUE){
			$encryptPass = sha1($pwd);
			$VarPass = array('password' => $encryptPass);
			if(!empty($VarPass)){
				$currtPass = $this->input->post('currtpass');
				$Id = $this->session->userdata('user_id');
				$oldPass = $this->UM->resetPassword($Id);
				if($oldPass[0]->password == sha1($currtPass)){
					$updatePass = $this->UM->UpdatePass($Id, $VarPass);
					$this->session->set_flashdata('success','Password changed successfully');
				}else{
					$this->session->set_flashdata('success','Password does not match');
				}
			}
			}else{
				$this->session->set_flashdata('success','Password must include at least one number,letter');
			}
			
		}
		
		redirect(base_url('user/settings'));
		
	}

	public function deactivateAccount(){
		$id = $this->session->userdata('user_id');
		$status = array('status' => '0');
		// $timeOut = set_time_limit(5);
		$data = $this->UM->deactivateAccount($id,$status);
		if($data == 1){
			redirect(base_url('logout'));
		}
		// // if(!($data) == 1 ){
			// $this->session->set_flashdata('success','Sorry, Please try again after sometime!!');
		// }else{
			// $this->session->set_flashdata('success','Your account will be deactivated within 5 second!!');
			// set_time_limit($timeOut)<now();
		// }
		
		// redirect(base_url('admin/auth'));
	}
	
	function add_image_video() {
        $meta = $this->CM->get_single_row_from_id(['slug' => 'campaign-creation'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $data = [];
        $campaignId = $this->session->userdata('campaign_id_step_one');
        $where = ['campaign_id' => $campaignId];
        $data['gallery_images'] = $this->OGM->getGallery($where);
        $data['campaign'] = $this->CM->get_single_row_from_id(['id' => $campaignId], 'campaigns');
		
        $this->layouts->render('user/account-settings', $data, 'default-signedin');
    }

    function file_upload_parser() 
	{
		
		$uid = uniqid();
		//$user_id = $this->session->userdata('user_id');
		$id = $this->session->userdata('user_id');
		define('UPLOAD_DIR', 'uploads/campaign/gallery_images/thumb/');
		$img = $_POST['imgdata'];
		//var_dump($img);exit;
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = UPLOAD_DIR . $id.'_'.$uid.'.png';
		$filenameonly =  $id.'_'.$uid.'.png';
		$success = file_put_contents($file, $data);
		//var_dump($filenameonly);exit;
		
		$data = array(		  
			'user_image' => $filenameonly,
		);
		//var_dump($data);exit;
		$this->UM->insert_campaign_gellary_image($data,$id);
	}
	
	
	
	// to check the pass
	private function check_password($pwd) {
        if (strlen($pwd) < 8) {

            $this->form_validation->set_message('check_password', 'Password is too short');

            return FALSE;
        } elseif (!preg_match("#[0-9]+#", $pwd)) {

            $this->form_validation->set_message('check_password', 'Password must include at least one number!');

            return FALSE;
        } elseif (!preg_match("#[a-zA-Z]+#", $pwd)) {

            $this->form_validation->set_message('check_password', 'Password must include at least one letter!');

            return FALSE;
        } else
            return true;
    }
	
	
    public function success() {

        $userid = $this->session->userdata('user_id');

        $where = array(
            'id' => $userid,
        );

        $data = array(
            'wepay_confirm' => 1,
        );

        $this->UM->update_user_with_token($where, $data);
        $user = $this->CM->get_single_row_from_id(['id' => $userid], 'users');
        $data['user'] = $user;
        $message = $this->load->view("template/thanks-signup-wepay", $data, true);
        send_mail_to_user('Campaign Invitation', $message, $user->email);

        redirect(base_url() . 'dashboard');
    }

    public function createprofile() {
        $meta = $this->CM->get_single_row_from_id(['slug' => 'organization-profile'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);

        $user_id = $this->session->userdata('user_id');

        $data['profiledetail'] = $this->UM->getOrganizationProfileDetail($user_id);
        //$data = [];

        $this->layouts->render('user/profile', $data, 'default-signedin');
    }

    public function profilebasic() {

        $userId = $this->session->userdata('user_id');
        ;

        $where = array(
            'id' => $userId,
        );
		
		$slug = preg_replace('/\s+/', '-', $this->input->post('org_name', true));
		
        $data = array(
            'org_name' => $this->input->post('org_name'),
            'org_tag' => $this->input->post('org_tag'),
            'about_me' => $this->input->post('about_me'),
			'org_slug' => strtolower($slug),
			'isProfile' => 1
        );
		$this->session->set_userdata('isProfile',1);
        $this->UM->update_organization_profile($where, $data);

        redirect(base_url() . 'organization-profile/step2');
    }

    public function orgprofilestep2() {

        $meta = $this->CM->get_single_row_from_id(['slug' => 'organization-profile'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);


        $user_id = $this->session->userdata('user_id');
        $data['profiledetail'] = $this->UM->getOrganizationProfileDetail($user_id);
        $data['gellary_image'] = $this->UM->getAllGellaryImage($user_id);
		$data['allVideosLink'] = $this->UM->getAllVideosLink($user_id);
		
        $this->layouts->render('user/profilestep2', $data, 'default-signedin');
    }

    function file_cover_image() {
		
		$user_id = $this->session->userdata('user_id');
		define('UPLOAD_DIR', 'uploads/organization_profile/cover_images/');
		$img = $_POST['imgdata'];
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = UPLOAD_DIR . $user_id.'_cover' . '.png';
		$filenameonly =  $user_id.'_cover' . '.png';
		$success = file_put_contents($file, $data);
		
		$where = array(
			'id' => $user_id,
		);

		$data = array(
			'image' => $filenameonly
		);
		$this->UM->update_organization_profile($where, $data);
		
        /*$file = (isset($_FILES["orgcoverpics"]) ? $_FILES["orgcoverpics"] : 0);
        if ($file) {
            $dir = './uploads/organization_profile/cover_images';
            $userId = $this->session->userdata('user_id');

            $filenameonly = $userId . '_' . rand(11111, 99999) . $file["name"];
            $fileName = $dir . '/' . $filenameonly;

            if (move_uploaded_file($file["tmp_name"], $fileName)) {

                $where = array(
                    'id' => $userId,
                );

                $data = array(
                    'image' => $filenameonly
                );
                $this->UM->update_organization_profile($where, $data);

                redirect(base_url() . 'organization-profile/step2');
            } else {
                echo "ERROR: Couldn't move the file to the final location";
            }
        }*/
    }

    public function savegellaeyimage() {
        $file = (isset($_FILES["image_name"]) ? $_FILES["image_name"] : 0);
        if ($file) {
            $dir = './uploads/organization_profile/gellary_image';
            $userId = $this->session->userdata('user_id');

            $filenameonly = $userId . '_' . rand(11111, 99999) . $file["name"];
            $fileName = $dir . '/' . $filenameonly;

            if (move_uploaded_file($file["tmp_name"], $fileName)) {

                $data = array(
                    'organization_id' => $userId,
                    'title' => $this->input->post('title'),
                    'image_name' => $filenameonly,
					'isImage' =>1
                );
                $this->UM->add_organization_profile_gellary_image($data);

                redirect(base_url() . 'organization-profile/step2');
            } else {
                echo "ERROR: Couldn't move the file to the final location";
            }
        }
    }

    public function delete_cover_image() {
        $userID = $this->session->userdata('user_id');

        $where = array(
            'id' => $userID,
        );
        $data = array(
            'image' => ''
        );
        $this->UM->update_organization_profile($where, $data);
    }

    public function delete_gellery_image() {
        $id = $this->input->post('id');

        $this->UM->deletegelleryimage($id);
    }

    public function savevideos() {
		
		$user_id = $this->session->userdata('user_id');
		$this->UM->addvideolink($user_id);
		
		$url = $this->UM->getOrganization($user_id);
		$orgUrl = $url->org_slug;
        redirect(base_url() . 'organization-profile/'.$orgUrl);
    }

    

    public function imported_google_contact() {

        $client_id = GOOGLE_CLIENT_ID;
        $client_secret = GOOGLE_CLIENT_SECRET;
        $redirect_uri = "https://www.mefarnes.com/user/imported_google_contact";
        $max_results = 250;

        $auth_code = $_GET["code"];

        $fields = array(
            'code' => urlencode($auth_code),
            'client_id' => urlencode($client_id),
            'client_secret' => urlencode($client_secret),
            'redirect_uri' => urlencode($redirect_uri),
            'grant_type' => urlencode('authorization_code')
        );
        $post = '';

        foreach ($fields as $key => $value) {
            $post .= $key . '=' . $value . '&';
        }
        $post = rtrim($post, '&');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
        curl_setopt($curl, CURLOPT_POST, 5);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($result);
        $accesstoken = $response->access_token;

        $url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results=' . $max_results . '&oauth_token=' . $accesstoken;
        $xmlresponse = curl_file_get_contents($url);
        if ((strlen(stristr($xmlresponse, 'Authorization required')) > 0) && (strlen(stristr($xmlresponse, 'Error ')) > 0)) {
            echo "<h2>OOPS !! Something went wrong. Please try reloading the page.</h2>";
            // exit();
        }
        //echo "<h3>Email Addresses:</h3>";
        $xml = new SimpleXMLElement($xmlresponse);
        $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
        $result = $xml->xpath('//gd:email');
        $emails = [];
        foreach ($result as $title) {
            $emails[] = ( (string) $title->attributes()->address);
        }

        $this->session->set_userdata('gmail_contacts', $emails);

        redirect(base_url() . 'gmail-contact');
    }

    public function gmail_contact() {
        if (
                $this->session->userdata('gmail_contacts')) {
            $emails = $this->session->userdata('gmail_contacts');
            $meta = $this->CM->get_single_row_from_id(['slug' => 'gmail-contact'], 'pages');
            $this->layouts->set_title($meta->page_name);
            $this->layouts->set_meta_title($meta->meta_title);
            $this->layouts->set_meta_description($meta->meta_description);
            $this->layouts->set_meta_keyword($meta->keyword);
            $array = [];

            $this->CM->delete(['user_id' => $this->session->userdata('user_id'), 'contact_type' => 1], 'user_email_contacts');
            foreach ($emails as $email):
                $array[] = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'email' => $email,
                    'create_date' => date("Y-m-d H:i:s"),
                    'contact_type' => 1,
                );
            endforeach;

            $this->db->insert_batch('user_email_contacts', $array);
            $data = null;
            $this->layouts->render('user/import-contact', $data, 'default-signedin');
            $this->session->set_flashdata('sucess', 'Contact Imported Successfully!');
            redirect(base_url() . 'dashboard');
        }
        else {
            redirect(base_url() . 'dashboard');
        }
    }

    public function microsoft() {
        echo '<pre>';
        print_r($this->input->post('campaign_id'));
    }

}
