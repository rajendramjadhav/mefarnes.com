<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawal extends Authenticate {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Common_model', 'CM'); // load model
    }

    public function history() {

        $userId = $this->session->userdata('user_id');
        $user = $this->CM->get_single_row_from_id(['id' => $userId], 'users');
        require (we_pay_library);

        // application settings
        $account_id = $user->we_pay_account_id;
        $client_id = wepay_client_id;
        $client_secret = wepay_client_secret;
        $access_token = $user->wepay_access_token;

        // change to useProduction for live environments
        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);

        // create the withdrawal
        $response = $wepay->request('/withdrawal/find', array(//account
            'account_id' => $account_id
        ));

        $responseb = $wepay->request('/account', array(//account
            'account_id' => $account_id
        ));
        $data['datas'] = $response;
        $data['datab'] = $responseb;
        $this->layouts->render('withdrawal/transaction-history', $data, 'Default');
    }

    public function update() {
        $userId = $this->session->userdata('user_id');
        $user = $this->CM->get_single_row_from_id(['id' => $userId], 'users');
        require (we_pay_library);

        // application settings
        $account_id = wepay_account_id;
        $client_id = wepay_client_id;
        $client_secret = wepay_client_secret;
        $access_token = wepay_app_access_token;

        // change to useProduction for live environments
        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);

        // create the withdrawal
        $response = $wepay->request('/account/get_update_uri', array(//account
            'account_id' => $account_id,
            'mode' => 'iframe',
            'redirect_uri' => base_url() . 'transaction-history',
        ));
        $data['data'] = $response;
        $this->layouts->render('withdrawal/update', $data, 'Default');
    
        
    }

}
