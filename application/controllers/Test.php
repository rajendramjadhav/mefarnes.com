<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Users_model', 'UM'); // model alias 
        //$this->load->library('Wepay');
    }

    public function index() {
       $this->load->library('facebook');
       
       $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('login'),
                'scope' => array("email") // permissions here
            ));
       $this->layouts->render('test/login', $data, 'default');
    }

   

}
