<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __Construct() {

        parent::__Construct();

        $this->load->model('Users_model', 'UM'); // load model

        $this->load->model('Common_model', 'CMM');

        $this->load->model('Campaign_model', 'CM');
    }

    public function index() {

        $this->home();
    }

    public function home() {

        $meta = $this->CMM->get_single_row_from_id(['id' => 1], 'pages');

        $this->layouts->set_title($meta->page_name);

        $this->layouts->set_meta_title($meta->meta_title);

        $this->layouts->set_meta_description($meta->meta_description);

        $this->layouts->set_meta_keyword($meta->keyword);



        $data = array();

        $params = array(
            'status' => 1
        );

        $data['categories'] = $this->CMM->get_categories($params, 'campaign_categories');



        $data['locations'] = $this->CMM->get_categories($params, 'campaign_locations');



        $data['featured_campaigns'] = $this->CM->get_featured_campaign(['home_page_visible' => 1]);

        $data['home_campaigns'] = $this->CM->get_featured_campaign(['home_page_not_featured' => 1]);



        if($this->session->userdata('is_logged_in')){
            $this->layouts->render('home', $data, 'default-signedin');
        }else{
        $this->layouts->render('home', $data, 'default');
      }
    }

    public function signup() {

        $meta = $this->CMM->get_single_row_from_id(['slug' => 'signup'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $data = array();

        if ($this->session->userdata('is_logged_in')) {
            redirect('dashboard');
        }

        /* if ($this->input->post('signup')) {
          $this->load->helper(array('form', 'url'));

          $this->form_validation->set_rules('txtfname', 'First Name', 'required|trim|required|callback_username_check');
          $this->form_validation->set_rules('txtlname', 'Last Name', 'required|trim|required|callback_username_check');
          $this->form_validation->set_rules('txtcity', 'City', 'required', 'required|trim|required');
          $this->form_validation->set_rules('txtpassword', 'Password', 'required', 'required|trim|required|callback_check_password');
          $this->form_validation->set_rules('txtemail', 'Email', 'required|trim|required|is_unique[users.email]');


          if ($this->form_validation->run() == FALSE) {
          $this->layouts->render('signup', $data, 'Default');
          } else {
          $token = token();

          $user_arr = array(
          'first_name' => $this->input->post('txtfname', true),
          'last_name' => $this->input->post('txtlname', true),
          'email' => $this->input->post('txtemail', true),
          'password' => sha1($this->input->post('txtpassword', true)),
          'city'     => addslashes($this->input->post('txtcity', true)),
          'profile_type' => 1,
          'create_date' => date('Y-m-d H:i:s'),
          'newsletter' => 1,
          'user_token' => $token,
          'is_token_valid' => 'Y',
          );

          $user_id = $this->UM->insert_user($user_arr);
          $this->activation($user_id);
          redirect('home/thankyou');

          }

          } */

        $this->layouts->render('signup', $data, 'Default');
    }

    public function individualuser() {
        redirect(base_url() . 'individual');
    }

    public function individual() {
        $this->layouts->set_title("Individual Sign Up");
        $data = array();
        $this->layouts->render('individual', $data, 'Default');
    }

    public function individualusersignup() {
        redirect(base_url() . 'individual/signup');
    }

    public function individualsignup() {
        $this->layouts->set_title("Individual Sign Up");
        $data = array();
        $this->layouts->render('individualsignup', $data, 'Default');
    }

    public function saveindividualuser() {
        $this->load->helper(array('form', 'url'));

        $data = array();

        $this->form_validation->set_rules('txtfname', 'First Name', 'required|trim|required|callback_username_check');
        $this->form_validation->set_rules('txtlname', 'Last Name', 'required|trim|required|callback_username_check');
        $this->form_validation->set_rules('txtcity', 'City', 'required', 'required|trim|required');
        $this->form_validation->set_rules('txtpassword', 'Password', 'required', 'required|trim|required|callback_check_password');
        $this->form_validation->set_rules('txtemail', 'Email', 'required|trim|required|is_unique[users.email]');


        if ($this->form_validation->run() == FALSE) {
            $this->layouts->render('individualsignup', $data, 'Default');
        } else {
            $token = token();

            $user_arr = array(
                'first_name' => $this->input->post('txtfname', true),
                'last_name' => $this->input->post('txtlname', true),
                'email' => $this->input->post('txtemail', true),
                'password' => sha1($this->input->post('txtpassword', true)),
                'city' => addslashes($this->input->post('txtcity', true)),
                'profile_type' => 1,
                'create_date' => date('Y-m-d H:i:s'),
                'newsletter' => 1,
                'status'=>1, 
                'user_token' => $token,
                'is_token_valid' => 'Y',
            );

            $user_id = $this->UM->insert_user($user_arr);
            //$this->activation($user_id);

            $params = ['id' => $user_id];
            $user = $this->UM->get_info($params);

            $username = $this->input->post('txtemail', true);
            $password = sha1($this->input->post('txtpassword', true));
            $params = ['email' => $username, 'password' => $password, 'status' => 1];

                if ($this->UM->check_login($params) == 1) {
                        $usr = $this->UM->get_info($params);
                        $user_id = $this->session->set_userdata('user_id', $usr->id);
                        $this->session->set_userdata('userdata', $usr);
                        $this->session->set_userdata('is_logged_in', true);
                        $this->session->set_userdata('isProfile', $usr->isProfile);
                        $this->session->set_userdata('profile_type', $usr->profile_type);
                        $this->session->set_userdata('org_slug', $usr->org_slug);
                        Authenticate::setData($datauser = array());
                        if (($this->session->userdata('last_page'))) {
                            $redirect = $this->session->userdata('last_page');
                            $this->session->unset_userdata('last_page');
                        } else {
                            $redirect = 'dashboard';
                        }
               
               
                }

            redirect('home/thankyou'); 
        }
    }

    public function withdraw(){
         $this->layouts->set_title("Withdraw");

        require_once(APPPATH.'libraries/wepay.php');

                $userId = $this->session->userdata('user_id');
		$uId = $userId;
		$dataUser = $this->UM->getUserDtls($uId);

        // application settings
       $account_id = $dataUser['0']->new_wepay_account_id;
       $client_id = 74601;
       $client_secret = "29c26803e6";
       $access_token = $dataUser['0']->new_wepay_access_token;

      // change to useProduction for live environments
       Wepay::useProduction($client_id, $client_secret);

       $wepay = new WePay($access_token);

       // create the withdrawal
       $response = $wepay->request('account/get_update_uri', array(
        'account_id'    => $account_id,
        'redirect_uri'  => 'https://mefarnes.com/home/success',
        'mode'          => 'iframe'
        )); 
       $data['data']=array();
       $data['uri'] = $response->uri;
       $this->layouts->render('withdraw', $data, 'Default');
          
    }
    public function paymentthank() {

        $this->layouts->set_title("Thanks");


        $wepay_account_name='';
        $account_type='personal'; 
        if(isset($_SESSION['userdata']->org_name) && $_SESSION['userdata']->org_name!=''){
              $dataUser=$this->UM->getUserDtls($this->session->userdata('user_id'));
              if($dataUser['0']->org_account_type==3 || $dataUser['0']->org_account_type==4){
                  $wepay_account_name=$_SESSION['userdata']->org_name;
                  $account_type='nonprofit';
              }
              if($dataUser['0']->org_account_type==2){
                  $wepay_account_name=$_SESSION['userdata']->org_name;
                  $account_type='business';
              }
        }

        if(isset($_SESSION['userdata']->first_name) && $_SESSION['userdata']->first_name!=''){
              $wepay_account_name=$_SESSION['userdata']->first_name;
              $account_type='personal';
        }

        require_once(APPPATH.'libraries/wepay.php');
        $code = $this->uri->segment(3);

        $redirect_uri = "https://mefarnes.com/home/success"; 

        $client_id = 74601;
        $client_secret = "29c26803e6";

        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay(NULL); // we don't have an access_token yet so we can pass NULL here

        // create an account for a user
        $response = WePay::getToken($code, $redirect_uri);
        $access_token=$response->access_token;

        //Wepay::useStaging($client_id, $client_secret);

         $wepay = new WePay($access_token);

        // create an account for a user
        $account_response = $wepay->request('account/create/', array(
            'name'          => $wepay_account_name,
            'description'   => $wepay_account_name.': Wepay Account',
            'type'          => $account_type
        ));

        $acount_id=$account_response->account_id;

         $where = array(
            'id' => $_SESSION['userdata']->id
        );
         $data = array(
                'new_wepay_account_id' => $acount_id,
                'new_wepay_access_token' => $access_token
            );
         $a= $this->UM->update_user_with_token($where, $data);
        
     
        $arr = array('' => '' );  

        $data = array();
        redirect('dashboard');

        /* $this->layouts->render('payment-thankyou', $data, 'Default'); */
    }

    public function organizationuser() {
        redirect(base_url() . 'organization/signup-step1');
    }

    public function orgsignupstep1() {
        $this->layouts->set_title("Organization Sign Up");
        $data = array();
        $this->layouts->render('organizationstep1', $data, 'Default');
    }

    public function orgsignupstep2() {
        $this->layouts->set_title("Organization Sign Up");
        $data = array();
        $this->layouts->render('organizationstep2', $data, 'Default');
    }

    public function orgsignupstep3() {
        $this->layouts->set_title("Organization Sign Up");
        $data = array();
        $this->layouts->render('organizationstep3', $data, 'Default');
    }

    public function username_check($str) {

        if (!preg_match('/^[a-z .,\-]+$/i', $str)) {

            $this->form_validation->set_message('username_check', 'The {field} field must be alphanumeric');

            return FALSE;
        } else
            return true;
    }

    public function check_password($pwd) {

        $errors_init = $errors;



        if (strlen($pwd) < 8) {

            $this->form_validation->set_message('check_password', 'Password is too short');

            return FALSE;
        } elseif (!preg_match("#[0-9]+#", $pwd)) {

            $this->form_validation->set_message('check_password', 'Password must include at least one number!');

            return FALSE;
        } elseif (!preg_match("#[a-zA-Z]+#", $pwd)) {

            $this->form_validation->set_message('check_password', 'Password must include at least one letter!');

            return FALSE;
        } else
            return true;
    }

    public function thankyou() {

        $this->layouts->set_title("Thanks");

        $data = array();

        $this->layouts->render('signup-thanku', $data, 'Default');
    }

    public function activation($userid) {

        $params = ['id' => $userid];

        $user = $this->UM->get_info($params);
        
        $response = $this->test($user);
    
       // echo '<pre>';
       // print_r($response);
       // exit;
        $response1 = $this->create($response, $user);


        $dataLast = array(
            'we_pay_account_id' => $response1->account_id,
        );

        $whereLast = ['id' => $user->id,];

        $this->UM->update_user_with_token($whereLast, $dataLast);
    }

    public function test($user) {

        // require(we_pay_library);
        require(we_pay_library);
        // application settings

        $account_id = wepay_account_id; // your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = wepay_app_access_token; // your app's access_token
        // change to useProduction for live environments

        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);

        // register new merchant

        $response = $wepay->request('user/register/', array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'email' => $user->email,
            'scope' => 'manage_accounts,collect_payments,view_user,preapprove_payments,send_money',
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'original_ip' => $_SERVER['REMOTE_ADDR'],
            'original_device' => 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6;

                             en-US) AppleWebKit/534.13 (KHTML, like Gecko)

                             Chrome/9.0.597.102 Safari/534.13',
            "tos_acceptance_time" => time(),
            "callback_uri" => base_url() . 'auth/callback_from_register',
            "redirect_uri" => base_url() . 'auth/getAccountStatus/' . $user->id,
        ));



        $data = array(
            'wepay_access_token' => $response->access_token,
            'wepay_user_id' => $response->user_id,
            'wepay_token_type' => $response->token_type,
            'wepay_token_expires_in' => $response->expires_in,
        );

        $where = ['id' => $user->id];

        $this->UM->update_user_with_token($where, $data);



        return $response;
    }

    public function create($responsea, $user) {

        //require(we_pay_library);
        // application settings

        $account_id = wepay_account_id; // your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = $responsea->access_token;

        // change to useProduction for live environments
        //Wepay::useProduction($client_id, $client_secret);



        $wepay = new WePay($access_token);



        // create an account for a user

        $checkUser = $wepay->request('account/find/', array(
            'name' => 'Mefarnes App',
        ));

        if (empty($checkUser)) {
            // create an account for a user

            $response = $wepay->request('account/create/', array(
                'name' => 'Mefarnes App',
                'description' => 'This is mefarnes test account.'
            ));


            $user = $this->CMM->get_single_row_from_id(['id' => $user->id], 'users');
            $data['user'] = $user;
            $message = $this->load->view("template/thanks-signup-wepay", $data, true);

            $responseConfirm = $wepay->request('user/send_confirmation/', array(
                "email_message" => $message,
                "email_subject" => "Mefarnes Confirmation from Wepay",
                "email_button_text" => "Confirm"
            ));

            $return = $response;
        } else {

            $return = $checkUser[0];
        }



        return $return;
    }

    public function callback_from_register() {



        require(we_pay_library);

        $account_id = wepay_account_id; //your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = wepay_app_access_token; // your app's access_token
        // change to useProduction for live environments

        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);



        // create the withdrawal

        $response = $wepay->request('account/get_update_uri', array(
            'account_id' => $account_id,
            'redirect_uri' => base_url() . 'user/getAccountStatus',
            'mode' => 'iframe'
        ));



        // display the response
//        echo '<pre>';
//        print_r($response -> uri); exit;

        $arr['uri'] = $response->uri;

        //$response['data'] = (($response)); //exit;

        $this->layouts->render('campaign/redirect', $data = $arr, 'default');
    }
    
    public function deactivateAccount(){
        $id = $this->session->userdata('user_id');
        $status = array('status' => '0');
        // $timeOut = set_time_limit(5);
        $data = $this->UM->deactivateAccount($id,$status);
        if($data == 1){
            $this->load->library('facebook');

            // Logs off session from website
            $this->facebook->destroySession();
    
            $this->session->unset_userdata('userdata');
    
            $this->session->unset_userdata('is_logged');
            $this->session->unset_userdata('is_logged_in');
    
            $this->session->unset_userdata('user_id');
            
            $this->session->set_flashdata('success','Your account has been deactivated!!');
                redirect(base_url('logout'));
            }
    }
public function checkemail_duplicate(){
        //echo 'emailidemailid';
        $emailId = strtolower($_POST['emailidcheck']);
        //var_dump($emailId);
        $checkemail = $this->UM->getemailid($emailId);
        if($checkemail[0]->email == $emailId){
            $errorcode = 5;
            echo $errorcode;
        }
        
    }

}
