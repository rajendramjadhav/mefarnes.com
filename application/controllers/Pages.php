<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Common_model', 'CM'); // load model
    }

    public function faq() {
        //$this->layouts->set_title("FAQs");
        $meta = $this->CM->get_single_row_from_id(['slug' => 'faq'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $data = [];
       if($this->session->userdata('is_logged_in')){
           $this->layouts->render('pages/faq', $data, 'default-signedin');
		}else{
		  $this->layouts->render('pages/faq', $data, 'Default');
		}
        //$this->layouts->render('pages/faq', $data, 'Default');
    }

    public function terms() {
        //$this->layouts->set_title("Terms of Use");
        $meta = $this->CM->get_single_row_from_id(['slug' => 'terms'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $data = [];
      if($this->session->userdata('is_logged_in')){
            $this->layouts->render('pages/terms', $data, 'default-signedin');
		}else{
			$this->layouts->render('pages/terms', $data, 'Default');
		}
       // $this->layouts->render('pages/terms', $data, 'Default');
    }

    public function privacy() { 
        $meta = $this->CM->get_single_row_from_id(['slug' => 'privacy-policy'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->page_name);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $this->layouts->render('pages/privacy', $data=array(), 'Default');
		
      
    }

    public function contact() {
        //$this->layouts->set_title("Contact Us");
        $meta = $this->CM->get_single_row_from_id(['slug' => 'contact-us'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $data[] = [];
        $data['error'] = '';
        $data['categories'] = $this->CM->get_categories([], 'contact_category');

        if ($this->input->post('fname')) {
            $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
            $this->form_validation->set_rules('message', 'Message Text', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['error'] = validation_errors();
            } else {
                $arr = array(
                    'first_name' => $this->input->post('fname', true),
                    'last_name' => $this->input->post('lname', true),
                    'email' => $this->input->post('email', true),
                    'phone' => $this->input->post('phone', true),
                    'catgory' => $this->input->post('category', true),
                    'message' => $this->input->post('message', true),
                    'create_date' => date('Y-m-d H:i:s'),
                );
                $emailHtml = $this->load->view('template/thanks', '', true);

                $params = array(
                    'to' => $this->input->post('email', true),
                    'subject' => 'Thank you for contacting us.',
                    'html' => $emailHtml,
                    'from' => 'admin@mefarnes.com',
                );
                send_mail_through_sendgrid($params);

                $this->CM->insert($arr, 'site_contacts_log');
                $this->session->set_flashdata('message', 'Your message sent successfully');
                redirect(base_url() . 'contact-us');
            }
        }
		$data['type'] = "contact";
       if($this->session->userdata('is_logged_in')){
        $this->layouts->render('pages/contact', $data, 'default-signedin');
        }else{
        $this->layouts->render('pages/contact', $data, 'Default');	
        }

        //$this->layouts->render('pages/contact', $data, 'Default');
    }

    public function howitworks() {
        //$this->layouts->set_title("How It Works");
        $meta = $this->CM->get_single_row_from_id(['slug' => 'how-it-works'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $data = [];
        $campaignId = $this->input->post('campaign_id', true);
        $userId = $this->session->userdata('user_id');
        if($this->session->userdata('is_logged_in')){
          $this->layouts->render('pages/how-it-works', $data, 'default-signedin');
		}else{
		  $this->layouts->render('pages/how-it-works', $data, 'Default');
		}
        //$this->layouts->render('pages/how-it-works', $data, 'Default');
    }

    public function subscription() {
        $subscriptionEmail = $this->input->post('subscription_email', true);

        if (!filter_var($subscriptionEmail, FILTER_VALIDATE_EMAIL) === false) {
            $arr = ['email' => strip_tags(trim($subscriptionEmail)), 'subscription_date' => date('Y-m-d H:i:s')];
            $count = $this->CM->get_single_row_from_id(['email' => strip_tags(trim($subscriptionEmail))], 'news_letter_subscriptions');
            if (empty($count)) {
                $this->CM->insert($arr, 'news_letter_subscriptions');
                $return = array(
                    'message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash()
                );
            } else {
                $return = array(
                    'message' => 'Error', 'code' => 202, 'token' => $this->security->get_csrf_hash()
                );
            }
        } else {

            $return = array(
                'message' => 'Error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );
        }

        echo json_encode($return);
    }

}
