<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PublicCampaign extends CI_Controller { //Authenticate

    public function __Construct() {

        parent::__Construct();

        $this->load->model('Campaign_model', 'CM'); // load model

        $this->load->model('Common_model', 'CMM');
        $this->load->model('Users_model', 'UM');
    }

    public function index() {
        //$this->output->enable_profiler(TRUE);
        $meta = $this->CMM->get_single_row_from_id(['slug' => 'campaign-profile'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        //$this->output->enable_profiler(TRUE);

        $slug = $this->uri->segment(2);
        $slug =urldecode($slug);
        // var_dump($slug);exit;
        $where = array('slug' => $slug, 'preview_mode' => 0);
        //var_dump($where);exit;
        $campaign = $this->CM->getCampaignBySlug($where);
        //var_dump($campaign);exit;

        if (empty($campaign)) {

            redirect('home');
        }
        $camp_id = $campaign->id;
        $data['data'] = $camp_id;
        $where = "where campaign_id=$camp_id";
        $countCamp = $this->CM->campaignCount($where);
        //echo $this->get_the_current_url();
        $expUrl = explode("?", $this->get_the_current_url());
        //var_dump($expUrl);exit;

        if (count($expUrl) == 2) {

            //$where = "where id='$camp_id' AND shared_at=1";
            //$countCampFb = $this->CM->campaignCount($where);
            $arrDataInsert = array('campaign_id' => $campaign->id, 'shared_at' => 1);
            $this->CM->sareInfoSave($arrDataInsert);
            redirect(base_url("campaign/$slug"));
        }
        $this->layouts->set_title($campaign->title);

        $donationDtl = $this->CM->getDonationDtl($campaign->id);
        $galleries = $this->CM->getCampaignGallery($campaign->id);

        $data['campaign'] = $campaign;
        //var_dump($campaign);exit;
        $data['donations'] = $donationDtl;
        $data['donationsCount'] = $this->CM->getDonationCount($campaign->id);
        $data['updates'] = $this->CM->getUpdates($campaign->id);
        $where = array('id' => $campaign->user_id);
        //print_r($where) ;exit;
        $data['users'] = $this->CMM->get_users($where, 'users');
        //var_dump($data['users']);exit;
        $data['comments'] = $this->CM->getComments($campaign->id);
        //var_dump($data['comments']);exit;
        $data['shareCount'] = $countCamp->count;
        $data['galleries'] = $galleries;
        $this->load->library('facebook');
        $campaignId = $campaign->id;
        //$userId = $campaign->user_id;
        if ($this->session->userdata('user_id') != '') {
            $userId = $this->session->userdata('user_id');
        } else {
            $userId = 0;
        }
//var_dump($userId);exit;
        $ip_addres = $this->input->ip_address();
        $add_date = date('Y-m-d H:i:s');
        $arrInsert = array(
            'campaign_id' => $campaignId,
            'ip_address' => $ip_addres,
            'user_id' => $userId,
            'add_date' => $add_date,
        );
        $data['checkipaddressdbl'] = $this->CM->checkipaddress($ip_addres);
        //var_dump($data['checkipaddressdbl']->ip_address);exit;
        //var_dump($data['checkipaddressdbl']->ip_address);
        //var_dump($ip_addres);exit;
        if (empty($data['checkipaddressdbl']) || $data['checkipaddressdbl']->ip_address != $ip_addres) {
            $data['campaignview'] = $this->CM->insertdata($arrInsert, 'campaign_visits');
//var_dump($data['campaignview']);exit;
        }
//var_dump($data['campaignview']);exit;
        // $this->load->library('facebook', array('appId' => '579509698900395', 'secret' => 'c176323efad8e02433201b1f9be62e32'));

        $user = $this->facebook->getUser();

        //var_dump($user);exit;

        if ($user) {
            try {
                $user_profile = $this->facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
                //var_dump($user_profile);exit;


                $userdetail = $this->CM->checkcommentfbuser($user_profile['id']);

                if (isset($userdetail)) {
                    $user_id = $this->session->set_userdata('user_id', $userdetail->userID);

                    $this->session->set_userdata('is_logged', true);

                    redirect(base_url() . 'campaign/' . $slug);
                } else {
                    $facebookdata = array(
                        'oauth_provider' => 'facebook',
                        'status' => 1,
                        'oauth_uid' => $user_profile['id'],
                        'first_name' => $user_profile['first_name'],
                        'last_name' => $user_profile['last_name'],
                        'email' => @$user_profile['email'],
                        'gender' => $user_profile['gender'],
                        'locale' => $user_profile['locale'],
                        'profile_url' => 'https://www.facebook.com/' . $user_profile['id'],
                        'picture_url' => $user_profile['picture']['data']['url']
                    );
                    $insert_id = $this->CM->addcommentfbuser($facebookdata);
                    $fbuserlogin = $this->CM->getcommentfbuser($insert_id);

                    $user_id = $this->session->set_userdata('user_id', $fbuserlogin->userID);

                    $this->session->set_userdata('is_logged', true);

                    redirect(base_url() . 'campaign/' . $slug);
                }
            } catch (FacebookApiException $e) {
                $user = null;
            }
        } else {
            // Solves first time login issue. (Issue: #10)
            //$this->facebook->destroySession();
        }

        if ($user) {

            $data['logout_url'] = site_url('logout'); // Logs off application
            // OR 
            // Logs off FB!
            // $data['logout_url'] = $this->facebook->getLogoutUrl();
        } else {
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('campaign/' . $slug),
                'scope' => array("email") // permissions here
            ));
        }
        // method of layout library | autoloaded
        $data['cam_id'] = $slug;
        if ($this->session->userdata('is_logged_in')) {
            $this->layouts->render('campaign/profile', $data, 'default-signedin');
        } else {
            $this->layouts->render('campaign/profile', $data, 'default');
        }
    }

    public function search($cat_id = "") {
        $meta = $this->CMM->get_single_row_from_id(['slug' => 'search'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        //$this->output->enable_profiler(TRUE);
        if ($this->input->get('keyword')) {
            $keyword = $this->input->get('keyword');
            $data_msg['keyword'] = $keyword;
            $this->CM->keyword = strip_tags(trim($keyword));
        }
        if (isset($cat_id)) {
            $this->CM->cat_id = strip_tags(trim($cat_id));
        }
        if ($this->input->get('sortby')) {
            $sortby = $this->input->get('sortby');
            $data_msg['sortby'] = $sortby;
            $sortType = explode('-', $sortby);
            $this->CM->sortby = strip_tags(trim($sortType[0]));
            $this->CM->sorttype = strip_tags(trim($sortType[1]));
        }
        if ($this->input->get('location')) {
            $location = $this->input->get('location');
            $data_msg['location'] = $location;
            $this->CM->location = strip_tags(trim($location));
        }
        if ($this->input->get('category')) {
            $category = $this->input->get('category');
            $data_msg['category'] = $category;
            $this->CM->category = $category;
        } else {
            $this->CM->category = '';
            $data_msg['category'] = '';
            $this->CM->category = '';
        }

        if ($this->input->get('search')) {
            $data_msg['search'] = strip_tags(trim($this->input->get('search')));
        }
        if ($this->input->post('src_btn')) {
            $keyword = strip_tags(trim($this->input->post('keyword', true)));
            $sortby = strip_tags(trim($this->input->post('sortby', true)));
            $location = strip_tags(trim($this->input->post('location', true)));
            $category = strip_tags(trim($this->input->post('category', true)));


            $parameter = array(
                'keyword' => $keyword,
                'sortby' => $sortby,
                'location' => $location,
                'category' => $category,
                'search' => '1'
            );

            $redirect_pg = self::url_in_get($parameter);

            redirect($redirect_pg);
        }

        $params = array(
            'status' => 1
        );

        $data_msg['categories'] = $this->CMM->get_categories($params, 'campaign_categories');

        $data_msg['locations'] = $this->CMM->get_categories($params, 'campaign_locations');


        $data_msg['tot_rec'] = $this->CM->getTotCampaign();
        //var_dump($data_msg['tot_rec']);exit;
        $per_page = 10;
        $start = 0;
        $data = $this->CM->getSreachCampaign($start, $per_page);
        $la_donor = $this->CM->getTotDonor()->result_array();
        //var_dump($la_donor);exit;
        $donor_arr = array();
        if (!empty($la_donor)) {
            foreach ($la_donor as $key => $value) {
                $donor_arr[$value['campaign_id']] = $value['donor'];
            }
        }
        //print_r($donor_arr);exit;
        $data_msg['donor'] = $donor_arr;
        $data_msg['all_campainin'] = $data->result();
        $data_msg['per_page'] = $per_page;

        $this->layouts->set_title('Search');
        if ($this->session->userdata('is_logged_in')) {
            $this->layouts->render('campaign/search', $data_msg, 'default-signedin');
        } else {
            $this->layouts->render('campaign/search', $data_msg, 'default');
        }
        //$this->layouts->render('campaign/search', $data_msg, 'default');
    }

    function url_in_get($data = array()) {
        $get = array();


        if (count($data) > 0) {
            if (count($_GET) > 0) {
                $result = array_merge($_GET, $data);
            } else {
                $result = $data;
            }
        } else {
            if (count($_GET) > 0) {
                $result = $_GET;
            } else {
                $result = array();
            }
        }
        if (count($result) > 0) {
            $_data = array();
            foreach ($result as $key => $val) {
                if (!array_key_exists($key, $_data)) {
                    $_data[$key] = $val;
                }
            }
            foreach ($_data as $key => $val)
                $get[] = $key . '=' . urlencode($val);
            $return = base_url() . uri_string() . '?' . implode('&', $get);
        } else {
            $return = base_url() . uri_string();
        }

        return $return;
    }

    function report() {
        if ($this->session->userdata('is_logged_in')) {
            $campaign_id = $this->input->post('campaign_id');
            $reason = $this->input->post('reason', true);
            $user_id = $this->session->userdata('user_id');

            $params = array(
                'user_id' => $user_id,
                'campaign_id' => $campaign_id,
            );
            if ($this->CM->check_report($params) == 1) {
                $return = array(
                    'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
                );
            } else {
                $params = ['user_id' => $user_id, 'campaign_id' => $campaign_id, 'reason' => $reason];
                $this->CMM->insert($params, 'campaign_reports');
                $return = array(
                    'message' => 'success', 'code' => 200, 'token' => $this->security->get_csrf_hash()
                );
            }
        } else {
            $return = array(
                'message' => 'error', 'code' => 202, 'token' => $this->security->get_csrf_hash()
            );
        }
        echo json_encode($return);
    }

    function subscribe() {

        $campaign_id = $this->input->post('campaign_id');
        $user_id = $this->session->userdata('user_id');
        $user_email = $this->input->post('subscriber_email');
        //$email = $this->CMW->getemail($user_id);
        //var_dump($email);exit;

        $params = array(
            'email' => $user_email,
            'campaign_id' => $campaign_id,
            'subscribed' => 1,
        );
        if (count($this->CMM->get_single_row_from_id($params, 'campaign_updates_subscribers')) > 0) {
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );
        } else {
            $params = ['email' => $user_email, 'campaign_id' => $campaign_id];
            $this->CMM->insert($params, 'campaign_updates_subscribers');
            $return = array(
                'message' => 'success', 'code' => 200, 'token' => $this->security->get_csrf_hash()
            );
        }

        echo json_encode($return);
    }

    function preview() {
        //$this->output->enable_profiler(TRUE);
        $currentUrl = $this->get_the_current_url();
        $slug = $this->uri->segment(2);
        $where = array('slug' => $slug, 'preview_mode' => 1);
        $campaign = $this->CM->getCampaignBySlug($where);
        if (empty($campaign)) {

            redirect('home');
        }
        $camp_id = $campaign->id;
        $where = "where campaign_id=$camp_id";
        $countCamp = $this->CM->campaignCount($where);
        $expUrl = explode("?", $this->get_the_current_url());
        //var_dump($expUrl);exit;
        if (count($expUrl) == 2) {

            //$where = "where id='$camp_id' AND shared_at=1";
            //$countCampFb = $this->CM->campaignCount($where);
            $arrDataInsert = array('campaign_id' => $campaign->id, 'shared_at' => 1);
            $this->CM->sareInfoSave($arrDataInsert);
            redirect(base_url("preview/$slug"));
        }

        $this->layouts->set_title($campaign->title);
        $donationDtl = $this->CM->getDonationDtl($campaign->id);
        $galleries = $this->CM->getCampaignGallery($campaign->id);
        $where = array('id' => $campaign->user_id);
        //print_r($where) ;exit;
        $data['users'] = $this->CMM->get_users($where, 'users');

        $data['campaign'] = $campaign;
        $data['donations'] = $donationDtl;
        $data['donationsCount'] = $this->CM->getDonationCount($campaign->id);
        $data['updates'] = $this->CM->getUpdates($campaign->id);
        $data['galleries'] = $galleries;
        $data['comments'] = $this->CM->getComments($campaign->id);
        $data['shareCount'] = $countCamp->count;
        if ($this->session->userdata('is_logged_in')) {
            $this->layouts->render('campaign/preview', $data, 'default-signedin');
        } else {
            $this->layouts->render('campaign/preview', $data, 'default');
        }
    }

    public function organization_public_profile() {
        $url = $this->uri->segment(2);
        //var_dump($url);exit;
        $orgid = $this->UM->getOrganizationID($url);
//var_dump($orgid);exit;
        //count($orgid);exit;

        if (empty($orgid)) {

            redirect('home');
        }

        $expUrl = explode("?", $this->get_the_current_url());
        //var_dump($expUrl);exit;
        $this->layouts->set_title("Organization Public Profile");

        $user_id = $orgid->id;
//var_dump($user_id);exit;
        $data['profiledetail'] = $this->UM->getOrganizationProfileDetail($user_id);
        //var_dump($data['profiledetail']);exit;
        $data['gellary_image'] = $this->UM->getAllGellaryImage($user_id);
//var_dump($data['gellary_image'] );exit;
        $data['videos_link'] = $this->UM->getVideosLink($user_id);
        $data['user_campagin'] = $this->UM->getAllCampaign($user_id);
        if ($this->session->userdata('is_logged_in')) {
            $this->layouts->render('user/org_public_profile', $data, 'default-signedin');
        } else {
            $this->layouts->render('user/org_public_profile', $data, 'default');
        }
    }

    function get_the_current_url() {

        $protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
        $base_url = $protocol . "://" . $_SERVER['HTTP_HOST'];
        $complete_url = $base_url . $_SERVER["REQUEST_URI"];

        return $complete_url;
    }

    public function shareCount() { 
        $post = $this->input->post();
        $campaign_id=$_POST['campaign_id']; 
         
            $arrDataInsert = array('total_share' => '1', 'campaign_id' => $campaign_id);
            $this->CM->sareInfoSave($arrDataInsert);
            echo "yes";
       
    }

    public function inbox() {
        //var_dump($_POST);exit;
        $post = $this->input->post();
        if (!empty($post['campaign_id'])) {
            $campaign_id = $this->input->post('campaign_id');
        }
        if (!empty($post['user_id'])) {
            $user_id = $this->input->post('user_id');
        }
        if (!empty($post['your_name'])) {
            $your_name = $this->input->post('your_name');
        }
        if (!empty($post['your_email'])) {
            $your_email = $this->input->post('your_email');
        }
        if (!empty($post['your_message'])) {
            $your_message = $this->input->post('your_message');
        }

        $checkuseremail = $this->CM->getuseremail($user_id);

        $data11 = array(
            'name' => $your_name,
            'email' => $your_email,
            'message' => $your_message,
        );

        $emailHtml = $this->load->view('template/thanksinbox', $data11, true);
        //echo $emailHtml;
        //exit;

        $params = array(
            'to' => $checkuseremail->email,
            'subject' => 'Thank you for contacting us.',
            'html' => $emailHtml,
            'from' => $your_email,
        );
        send_mail_through_sendgrid($params);
        if ($checkuseremail->email != "") {
            $return = array(
                'message' => 'success', 'code' => 200
            );
        }
    }

}
