<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CampaignController extends Authenticate { //Authenticate

    public function __Construct() {

        parent::__Construct();

        $this->load->model('Users_model', 'UM'); // load model

        $this->load->model('Common_model', 'CM'); // load model

        $this->load->model('Campaign_model', 'CMM');
        $this->load->model('Organization_gallery_model', 'OGM');

        $this->load->helper('form');

        $this->load->library('form_validation');
		$this->load->library("session");
    }

    public function index() {
        $this->layouts->set_title("My Campaigns");
        $data = [];
        $this->layouts->render('campaign/my-campaign', $data, 'default-signedin');
    }

    public function create() {
		$campaignID = $this->uri->segment(2);
        //$this->layouts->set_title("Create Campaign");
        $meta = $this->CM->get_single_row_from_id(['slug' => 'campaign-creation'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $params = array(
            'status' => 1
        );

        $data['categories'] = $this->CM->get_categories($params, 'campaign_categories');
        $data['locations'] = $this->CM->get_categories($params, 'campaign_locations');
		$data['getsinglecampaign'] = $this->CM->getBySingleID($campaignID);
        $data['error'] = '';
		
        /*if ($this->input->post('campaign_duration')) {

            //$this->form_validation->set_rules('campaignPics', 'Campaign Cover Image', 'callback_cover_image_upload');

            $this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[users.email]');

            $this->form_validation->set_rules('campaign_story', 'Campaign Story', 'trim|required');

            $this->form_validation->set_rules('category', 'Category', 'trim|required');

            $this->form_validation->set_rules('campaign_duration', 'Campaign Duration', 'required|trim');

            $this->form_validation->set_rules('video_link', 'Video Link', 'trim');

            $this->form_validation->set_rules('location', 'Location', 'required');

            $this->form_validation->set_rules('fund_needed', 'Fund Needed', 'trim|required');

            if ($this->form_validation->run() == FALSE) {

                $data['error'] = validation_errors();
            } else {

                $this->session->set_flashdata('message', 'Error');

                $datas = $this->security->xss_clean($this->input->post());

                $videoId = $this->input->post('video_link', true);

                if ($videoId != '') {

                    $videoDtl = videoType($videoId);

                    if ($videoDtl['videoId'] == '') {

                        $videoDtl['videoType'] = NULL;

                        $videoDtl['videoId'] = NULL;
                    }
                } else {
                    $videoDtl['videoType'] = NULL;

                    $videoDtl['videoId'] = NULL;
                }

                $startDate = $this->input->post('campaign_duration', true);

                $startDate = $this->input->post('campaign_duration', true);

                $endDate = date('Y-m-d 23:59:59', strtotime("+$startDate"));

                $slug = preg_replace('/\s+/', '-', $this->input->post('title', true));
                $slug=html_entity_decode($slug);

                $arr = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'campaign_categoriy_id' => $this->input->post('category', true),
                    'title' => $this->input->post('title', true),
                    'slug' => strtolower($slug),
                    //'image' => $this->input->post('image'),
                    'description' => $this->input->post('campaign_story'),
                    'video_type' => $videoDtl['videoType'],
                    'video_file_name' => $videoDtl['videoId'],
                    'video_url' => $this->input->post('video_link', true),
                    'campaign_location_id' => $this->input->post('location', true),
                    'fund_needed' => $this->input->post('fund_needed', true),
                    'start_date_time' => date("Y-m-d H:i:s"),
                    'end_date_time' => $endDate,
                    'status' => 1,
                    'donation_status' => 1,
                    'create_date' => date("Y-m-d H:i:s"),
                    'preview_mode' => '1',
                );

                $campaignId = $this->CM->insert($arr, 'campaigns');
                $this->session->set_flashdata('message', 'Campaign created successfully');

                $this->session->set_userdata('campaign_id_step_one', $campaignId);
                redirect(base_url() . 'create-campaign/step-two');
                //redirect(base_url() . 'preview/' . strtolower($slug));
                //redirect('dashboard');
            }
        }*/

        $this->layouts->render('campaign/create-campaign', $data, 'default-signedin');
    }
	
	function savecampaign()
	{
		$this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[users.email]');
		$this->form_validation->set_rules('campaign_story', 'Campaign Story', 'trim|required');
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		$this->form_validation->set_rules('campaign_duration', 'Campaign Duration', 'required|trim');
		$this->form_validation->set_rules('video_link', 'Video Link', 'trim');
		$this->form_validation->set_rules('location', 'Location', 'required');
		$this->form_validation->set_rules('fund_needed', 'Fund Needed', 'trim|required');	
		
		if ($this->form_validation->run() == FALSE) {

                $data['error'] = validation_errors();
            } else {

                $this->session->set_flashdata('message', 'Error');

                $datas = $this->security->xss_clean($this->input->post());

                $videoId = $this->input->post('video_link', true);
				//var_dump($videoId);exit;
/***

                if ($videoId != '') {

                    $videoDtl = videoType($videoId);
//var_dump($videoDtl);exit;

                    if ($videoDtl['videoId'] == '') {
                        $videoDtl['videoType'] = NULL;
                        $videoDtl['videoId'] = NULL;
                    }
                } else {
                    $videoDtl['videoType'] = NULL;
                    $videoDtl['videoId'] = NULL;
                }
***/

                $startDate = $this->input->post('campaign_duration', true);
                $startDate = $this->input->post('campaign_duration', true);
                $endDate = date('Y-m-d 23:59:59', strtotime("+$startDate"));
                $slug = preg_replace('/\s+/', '-', $this->input->post('title', true));
                require_once(APPPATH.'libraries/lib.php');
                $slug=sanitize($slug);
                

              /*  $slug=strip_tags($slug);
   

     $slug=trim($slug);
     $slug=str_replace("nbsp", '', $slug);
     $slug=trim($slug);

      $slug = preg_replace('~[^\\pL\d]+~u', '-', $slug);



   

    $slug = trim($slug, '-');



    

  
    $enc = mb_detect_encoding($slug, "UTF-8,ISO-8859-1");
    $text = iconv($enc, 'UTF-8', $slug);



    // lowercase

    $slug = strtolower($slug);



    // remove unwanted characters

    $slug = preg_replace('~[^-\w]+~', '', $slug);



    if (empty($slug))

    {

        $slug='n-a';

    } */


                $arr = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'campaign_categoriy_id' => $this->input->post('category', true),
                    'title' => $this->input->post('title', true),
                    'slug' => strtolower($slug),
                    'description' => $this->input->post('campaign_story'),
                    'video_url' => $this->input->post('video_link', true),
                    'campaign_location_id' => $this->input->post('location', true),
                    'fund_needed' => $this->input->post('fund_needed', true),
                    'start_date_time' => date("Y-m-d H:i:s"),
                    'end_date_time' => $endDate,
                    'status' => 1,
                    'donation_status' => 1,
                    'preview_mode' => '1',
                );

               $campaignId = $this->CM->addeditcampaigns($arr);
			
                $this->session->set_flashdata('message', 'Campaign created successfully');

                $this->session->set_userdata('campaign_id_step_one', $campaignId);
				
				
				if ($this->input->post('campaignID') != NULL && $this->input->post('campaignID') > 0)
				{
					redirect(base_url() . 'edit-campaign/step-two');
				}
				else 
				{
                	redirect(base_url() . 'create-campaign/step-two');
				}
                //redirect(base_url() . 'preview/' . strtolower($slug));
                //redirect('dashboard');
            }
		
		
	}


    //cover image upload

    function cover_image_upload() {
        if ($_FILES['campaignPics']['size'] != 0) {

            $upload_dir = './uploads/campaign/cover_images/';

            if (!is_dir($upload_dir)) {

                mkdir($upload_dir);
            }

            $config['upload_path'] = $upload_dir;

            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|GIF';

            $config['file_name'] = $image_name = 'campaign_' . substr(md5(rand()), 0, 7);

            $config['overwrite'] = false;
            $config['min_width'] = '450';
            $config['min_height'] = '450';

            $config['max_size'] = '1255120';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('campaignPics')) {

                $this->form_validation->set_message('cover_image_upload', $this->upload->display_errors());
                echo $this->upload->display_errors();
                return FALSE;
            } else {

                $upload_data = $this->upload->data();
                $_POST['image'] = $image_name = $upload_data['file_name'];

                $tmp_path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/cover_images/';
                $previw_path = $tmp_path . "preview/";
                $thumb_path = $tmp_path . "thumb/";
                $this->load->library('resize_image');
                $image_resize = $this->resize_image;
                //--------------------- Thumb image ------------------------------------//
                $image_resize->new_width = 270;
                $image_resize->new_height = 160;
                $image_resize->image_to_resize = $tmp_path . $image_name;
                $image_resize->ratio = true; // Keep Aspect Ratio?
                $image_resize->dynamic_ratio = false; // Keep Aspect Ratio?

                $image_resize->new_image_name = $image_name;
                $image_resize->save_folder = $thumb_path;
                $process = $image_resize->resize();
                //--------------------- Preview image ------------------------------------//
                $image_resize->new_width = 770;
                $image_resize->new_height = 450;
                $image_resize->image_to_resize = $tmp_path . $image_name;
                $image_resize->ratio = true; // Keep Aspect Ratio?
                $image_resize->dynamic_ratio = false; // Keep Aspect Ratio?

                $image_resize->new_image_name = $image_name;
                $image_resize->save_folder = $previw_path;
                $process = $image_resize->resize();
                //unlink($tmp_path.$image_name);
                return true;
            }
        } else {
            $this->form_validation->set_message('image_upload', "No file selected");

            return false;
        }
    }

    public function update() {
        $campaignId = $slug = $this->uri->segment(2);
        $userId = $this->session->userdata('user_id');

        $where = array(
            'user_id' => $userId,
            'id' => $campaignId
        );
        $count = $this->CMM->check_campaign_user($where);
        $campaign = $this->CM->get_single_row_from_id($where, 'campaigns');
        if ($count > 0) {

            $this->layouts->set_title("Create Update");

            if ($this->input->post('update')) {

                $this->form_validation->set_rules('title', 'Update Title', 'trim|required');
                $this->form_validation->set_rules('description', 'Update Title', 'trim');

                $videoId = $this->input->post('video_link', true);
                if ($videoId != '') {
                    $videoDtl = videoType($videoId);
                    if ($videoDtl['videoId'] == '') {
                        $videoDtl['videoType'] = NULL;
                        $videoDtl['videoId'] = NULL;
                    }
                } else {
                    $videoDtl['videoType'] = NULL;
                    $videoDtl['videoId'] = NULL;
                }

                $arr = array(
                    'campaign_id' => $campaignId,
                    'user_id' => $this->session->userdata('user_id'),
                    'title' => $this->input->post('title', true),
                    'video_type' => $videoDtl['videoType'],
                    'video_file_name' => $videoDtl['videoId'],
                    'description' => $this->input->post('description'),
                    'create_date' => date('Y-m-d H:i:s')
                );

                $this->CM->insert($arr, 'campaings_updates');
                $this->session->set_flashdata('message', 'Campaign update created successfully');
                //redirect(base_url() . 'create-update/' . $campaignId);
                redirect(base_url() . 'campaign/' . $campaign->slug);
            }

            $data['campaign'] = $campaign;
            $this->layouts->render('campaign/update', $data, 'default-signedin');
        } else {
            $where = ['id' => $campaignId];
            redirect(base_url() . 'campaign/' . $campaign->slug);
        }
    }

    function markLove() {
        if ($this->session->userdata('is_logged_in')) {
            $campaign_update_id = $this->input->post('campaign_update_id');
            $user_id = $this->session->userdata('user_id');

            $params = array(
                'user_id' => $user_id,
                'campaign_update_id' => $campaign_update_id,
            );
            if (count($this->CM->get_single_row_from_id($params, 'campaign_update_loves')) > 0) {
                $return = array(
                    'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
                );
            } else {
                $params = ['user_id' => $user_id, 'campaign_update_id' => $campaign_update_id];
                $this->CM->insert($params, 'campaign_update_loves');
                $return = array(
                    'message' => 'success', 'code' => 200, 'token' => $this->security->get_csrf_hash()
                );
            }
        } else {
            $return = array(
                'message' => 'error', 'code' => 202, 'token' => $this->security->get_csrf_hash()
            );
        }
        echo json_encode($return);
    }

    function subscribe() {
        if ($this->session->userdata('is_logged_in')) {
            $campaign_id = $this->input->post('campaign_id');
            $user_id = $this->session->userdata('user_id');
			//$email = $this->CMW->getemail($user_id);
			//var_dump($email);exit;

            $params = array(
                'user_id' => $user_id,
                'campaign_id' => $campaign_id,
                'subscribed' => 1,
            );
            if (count($this->CM->get_single_row_from_id($params, 'campaign_updates_subscribers')) > 0) {
                $return = array(
                    'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
                );
            } else {
                $params = ['user_id' => $user_id, 'campaign_id' => $campaign_id];
                $this->CM->insert($params, 'campaign_updates_subscribers');
                $return = array(
                    'message' => 'success', 'code' => 200, 'token' => $this->security->get_csrf_hash()
                );
            }
        } else {
            $return = array(
                'message' => 'error', 'code' => 202, 'token' => $this->security->get_csrf_hash()
            );
        }
        echo json_encode($return);
    }

    function makelive() {
        $campaignId = $slug = $this->uri->segment(3);

        $userId = $this->session->userdata('user_id');
        $where = array(
            'user_id' => $userId,
            'id' => $campaignId
        );
        $data = array('preview_mode' => '0');
        $campaings = $this->CMM->updateCampaign($data, $where);
        $this->session->set_flashdata('message', 'Campaign created successfully');
        redirect('dashboard');
    }

    public function updateCampaign() {
        $slug = $this->uri->segment(2);
        $where = array('slug' => $slug);
        $campaign = $this->CMM->getCampaignBySlug($where);

        $this->layouts->set_title("Create Campaign");
        $params = array(
            'status' => 1
        );
        $data['campaign'] = $campaign;
        $data['categories'] = $this->CM->get_categories($params, 'campaign_categories');

        $data['locations'] = $this->CM->get_categories($params, 'campaign_locations');

        $data['error'] = '';

        if ($this->input->post('campaign_duration')) {

            $this->form_validation->set_rules('campaignPics', 'Campaign Cover Image', 'update_cover_image_upload');

            $this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[users.email]');

            $this->form_validation->set_rules('campaign_story', 'Campaign Story', 'trim|required');

            $this->form_validation->set_rules('category', 'Category', 'trim|required');

            $this->form_validation->set_rules('campaign_duration', 'Campaign Duration', 'required|trim');

            $this->form_validation->set_rules('video_link', 'Video Link', 'trim');

            $this->form_validation->set_rules('location', 'Location', 'required');

            $this->form_validation->set_rules('fund_needed', 'Fund Needed', 'trim|required');

            if ($this->form_validation->run() == FALSE) {

                $data['error'] = validation_errors();
            } else {
                $this->session->set_flashdata('message', 'Error');

                $datas = $this->security->xss_clean($this->input->post());

                $videoId = $this->input->post('video_link', true);

                if ($videoId != '') {

                    $videoDtl = videoType($videoId);

                    if ($videoDtl['videoId'] == '') {

                        $videoDtl['videoType'] = NULL;

                        $videoDtl['videoId'] = NULL;
                    }
                } else {

                    $videoDtl['videoType'] = NULL;

                    $videoDtl['videoId'] = NULL;
                }

                $startDate = $this->input->post('campaign_duration', true);

                $endDate = date('Y-m-d 23:59:59', strtotime("+$startDate"));

                $slug = preg_replace('/\s+/', '-', $this->input->post('title', true));


                $data = array(
                    'campaign_categoriy_id' => $this->input->post('category', true),
                    'title' => $this->input->post('title', true),
                    'slug' => strtolower($slug),
                    'description' => $this->input->post('campaign_story'),
                    'video_type' => $videoDtl['videoType'],
                    'video_file_name' => $videoDtl['videoId'],
                    'video_url' => $this->input->post('video_link', true),
                    'campaign_location_id' => addslashes($this->input->post('location', true)),
                    'fund_needed' => $this->input->post('fund_needed', true),
                    'start_date_time' => date("Y-m-d H:i:s"),
                    'end_date_time' => $endDate,
                    'status' => 1,
                    'donation_status' => 1,
                    'create_date' => date("Y-m-d H:i:s"),
                    'preview_mode' => '1',
                );

                //$campaignId = $this->CM->insert($arr, 'campaigns');

                $where = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'id' => $campaign->id
                );
                $campaings = $this->CMM->updateCampaign($data, $where);

                $this->session->set_flashdata('message', 'Campaign created successfully');

                redirect(base_url() . 'preview/' . strtolower($slug));
                //redirect('dashboard');
            }
        }

        $this->layouts->render('campaign/update-campaign', $data, 'default-signedin');
    }

    function update_cover_image_upload() {
        if ($_FILES['campaignPics']['size'] != 0) {

            $upload_dir = './uploads/campaign/cover_images/';

            if (!is_dir($upload_dir)) {

                mkdir($upload_dir);
            }

            $config['upload_path'] = $upload_dir;

            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|GIF';

            $config['file_name'] = $image_name = 'campaign_' . substr(md5(rand()), 0, 7);

            $config['overwrite'] = false;
            $config['min_width'] = '450';
            $config['min_height'] = '450';

            $config['max_size'] = '1255120';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('campaignPics')) {

                $this->form_validation->set_message('cover_image_upload', $this->upload->display_errors());
                echo $this->upload->display_errors();
                return FALSE;
            } else {

                $upload_data = $this->upload->data();
                $_POST['image'] = $image_name = $upload_data['file_name'];


                $tmp_path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/campaign/cover_images/';
                $previw_path = $tmp_path . "preview/";
                $thumb_path = $tmp_path . "thumb/";
                //$move_bucket_pv = $this->all_function->get_absolute_path(COURSE_DEFAULT_IMAGE_PATH, 0, 'PV');
//                       $temporary_image= $_FILES['campaignPics']['tmp_name'];
//                        move_uploaded_file($temporary_image, $tmp_path . $image_name);

                $this->load->library('resize_image');
                $image_resize = $this->resize_image;
                //--------------------- Thumb image ------------------------------------//
                $image_resize->new_width = 270;
                $image_resize->new_height = 160;
                $image_resize->image_to_resize = $tmp_path . $image_name;
                $image_resize->ratio = true; // Keep Aspect Ratio?
                $image_resize->dynamic_ratio = false; // Keep Aspect Ratio?

                $image_resize->new_image_name = $image_name;
                $image_resize->save_folder = $thumb_path;
                $process = $image_resize->resize();
                //--------------------- Preview image ------------------------------------//
                $image_resize->new_width = 770;
                $image_resize->new_height = 450;
                $image_resize->image_to_resize = $tmp_path . $image_name;
                $image_resize->ratio = true; // Keep Aspect Ratio?
                $image_resize->dynamic_ratio = false; // Keep Aspect Ratio?

                $image_resize->new_image_name = $image_name;
                $image_resize->save_folder = $previw_path;
                $process = $image_resize->resize();
                //unlink($tmp_path.$image_name);
                return true;
            }
        } else {

            return true;
        }
    }

    function add_image_video() {
        $meta = $this->CM->get_single_row_from_id(['slug' => 'campaign-creation'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        $data = [];
        $campaignId = $this->session->userdata('campaign_id_step_one');
        $where = ['campaign_id' => $campaignId];
        $data['gallery_images'] = $this->OGM->getGallery($where);
        $data['campaign'] = $this->CM->get_single_row_from_id(['id' => $campaignId], 'campaigns');
		
        $this->layouts->render('campaign/create-campaign-step-two', $data, 'default-signedin');
    }

    function file_upload_parser() 
	{
		
		$uid = uniqid();
		//$user_id = $this->session->userdata('user_id');
		$campaignId = $this->session->userdata('campaign_id_step_one');
		define('UPLOAD_DIR', 'uploads/campaign/gallery_images/thumb/');
		$img = $_POST['imgdata'];
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = UPLOAD_DIR . $campaignId.'_'.$uid.'.png';
		$filenameonly =  $campaignId.'_'.$uid.'.png';
		$success = file_put_contents($file, $data);
		
		$data = array(
			'campaign_id' => $campaignId,		  
			'image_name' => $filenameonly,
			'status' =>1
		);
		$this->CM->insert_campaign_gellary_image($data,$campaignId);
		
        /*$file = (isset($_FILES["campaignPics"]) ? $_FILES["campaignPics"] : 0);
        if ($file) {
            $dir = 'uploads/campaign/gallery_images/';
            $campaignId = $this->session->userdata('campaign_id_step_one');
            //$$file = str_replace('', '-', $file["name"]);
            $filenameonly = $campaignId . '_' . rand(11111, 99999) . $file["name"];
            $fileName = $dir . '/' . $filenameonly;
            if (move_uploaded_file($file["tmp_name"], $fileName)) {

                $arr = array(
                    'campaign_id' => $campaignId,
                    'image_name' => $filenameonly,
                    'create_date' => date('Y-m-d H:i:s'),
                );
                $campaignId = $this->CM->insert($arr, 'campaign_gallery');
                echo base_url() . 'uploads/campaign/gallery_images/' . $filenameonly;
            } else {
                echo "ERROR: Couldn't move the file to the final location";
            }
        }*/
    }

    function crop_image() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $targ_w = 350;
            $targ_h = 150;
            $jpeg_quality = 100;

            $src = $this->input->post('imgSrc');
            //$targ_w = $this->input->post('w');
            //$targ_h = $this->input->post('h');

            $imagArr = explode('campaign/gallery_images/', $src);
            $img_r = imagecreatefromjpeg($src);
            $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

            imagecopyresampled($dst_r, $img_r, 0, 0, $this->input->post('x'), $this->input->post('y'), $targ_w, $targ_h, $this->input->post('w'), $this->input->post('h'));
            $dir = 'uploads/campaign/gallery_images/thumb/';
            header('Content-type: image/jpeg');
            imagejpeg($dst_r, $dir . $imagArr[1], $jpeg_quality);

            echo json_encode(array('img_url' => $imagArr[0] . 'campaign/gallery_images/thumb/' . $imagArr[1], 'code' => 200, 'token' => $this->security->get_csrf_hash()));

            exit;
        }
    }

    public function save_image() {
        $src = $this->input->post('imgSrc');
        $srcArr = explode('gallery_images/thumb/', $src);
        $campaignId = $srcArr[0];
        $imagNameArr = explode('?', $srcArr[1]);
        //echo $imagNameArr[0]; exit;
        $where = ['image_name' => $imagNameArr[0]];
        $data = ['status' => 1, 'create_date' => date("y-m-d H:i:s")];

        $this->CM->common_update($data, $where, 'campaign_gallery');
        echo json_encode(array('message' => 'success', 'code' => 200, 'token' => $this->security->get_csrf_hash()));
        exit;
    }

    public function delete_image() {
        $id = $this->input->post('id', true);        

        $where = ['id' => $id];
        $gallery_image = $meta = $this->CM->get_single_row_from_id(['id' => $id], 'campaign_gallery');

        if (file_exists('uploads/campaign/gallery_images/' . $gallery_image->image_name)) {
            unlink('uploads/campaign/gallery_images/' . $gallery_image->image_name);
        }

        if (file_exists('uploads/campaign/gallery_images/thumb/' . $gallery_image->image_name)) {
            unlink('uploads/campaign/gallery_images/thumb/' . $gallery_image->image_name);
        }

        $this->CM->delete(['id' => $id], 'campaign_gallery');
        $c_id = $this->session->userdata('campaign_id_step_one');
        $res = $this->CM->get_single_row_from_id(['campaign_id' => $c_id],'campaign_gallery');
        
        if ($res) {            
         $a =   $this->CM->common_update(array('status' => '1'), array('id' => $res->id),'campaign_gallery');
        }
        
        echo json_encode(array('message' => 'success', 'code' => 200, 'token' => $this->security->get_csrf_hash()));
        exit;
    }

    public function set_profile() {
        $profile_picture = $this->input->post('profile_picture', true);
        $campaignId = $this->session->userdata('campaign_id_step_one');

        $gallery_image = $meta = $this->CM->get_single_row_from_id(['id' => $profile_picture], 'campaign_gallery');

        $this->CM->common_update(array('image' => $gallery_image->image_name, 'preview_mode' => '0'), ['id' => $gallery_image->campaign_id], 'campaigns');
        $campaign = $this->CM->get_single_row_from_id(array('id' => $campaignId), 'campaigns');
        
        echo $campaign->slug; exit;
    }
    public function campaign_success()
    {
        $this->layouts->set_title("Congratulation !");
     
        $arr = array('' => '' );        
        $this->layouts->render('campaign/campaign_success', $data = $arr, 'default');   
        
    }

}
