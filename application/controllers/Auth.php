<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __Construct() {

        parent::__Construct();

        $this->load->model('Users_model', 'UM'); // model alias 

        $this->load->model('Common_model', 'CM');

        //$this->load->library('Wepay');
    }

    public function index() {

        $this->login();
    }

    public function login() {

        if ($this->session->userdata('is_logged_in')) {
            redirect('dashboard');
        }
        //$this->layouts->set_title("Login");

        $meta = $this->CM->get_single_row_from_id(['slug' => 'signin'], 'pages');
        $this->layouts->set_title($meta->page_name);
        $this->layouts->set_meta_title($meta->meta_title);
        $this->layouts->set_meta_description($meta->meta_description);
        $this->layouts->set_meta_keyword($meta->keyword);
        
        $data = array();
        // if it has post data then process login check

        if ($this->input->post()) {
            $username = $this->input->post('email', true);
            $password = sha1($this->input->post('password', true));
            $params = ['email' => $username, 'password' => $password, 'status' => 1];

            if ($this->UM->check_login($params) == 1) {
                $usr = $this->UM->get_info($params);
                $user_id = $this->session->set_userdata('user_id', $usr->id);
                $this->session->set_userdata('userdata', $usr);
                $this->session->set_userdata('is_logged_in', true);
                $this->session->set_userdata('isProfile', $usr->isProfile);
                $this->session->set_userdata('profile_type', $usr->profile_type);
                $this->session->set_userdata('org_slug', $usr->org_slug);
                Authenticate::setData($datauser = array());
                if (($this->session->userdata('last_page'))) {
                    $redirect = $this->session->userdata('last_page');
                    $this->session->unset_userdata('last_page');
                } else {
                    $redirect = 'dashboard';
                }
                redirect($redirect);
                //redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->session->set_flashdata('message', 'Invalid username/password');
            }
        }
        $this->load->library('facebook');
        //$this->load->library('facebook', array('appId' => '579509698900395', 'secret' => 'c176323efad8e02433201b1f9be62e32'));

        $user = $this->facebook->getUser();


        if ($user) {
            try {
                $user_profile = $this->facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');


                $userdetail = $this->CM->checkfbuser($user_profile['id']);

                if (isset($userdetail)) {
                    $user_id = $this->session->set_userdata('user_id', $userdetail->id);
                    $this->session->set_userdata('userdata', $userdetail);
                    $this->session->set_userdata('is_logged_in', true);
                    Authenticate::setData($datauser = array());
                    if (($this->session->userdata('last_page'))) {
                        $redirect = $this->session->userdata('last_page');
                        $this->session->unset_userdata('last_page');
                    } else {
                        $redirect = 'dashboard';
                    }
                    redirect($redirect);
                } else {
                    $facebookdata = array(
                        'profile_type' => 1,
                        'org_account_type' => 1,
                        'oauth_provider' => 'facebook',
                        'status' => 1,
                        'oauth_uid' => $user_profile['id'],
                        'first_name' => $user_profile['first_name'],
                        'last_name' => $user_profile['last_name'],
                        'email' => @$user_profile['email'],
                        'gender' => $user_profile['gender'],
                        'locale' => $user_profile['locale'],
                        'profile_url' => 'https://www.facebook.com/' . $user_profile['id'],
                        'picture_url' => $user_profile['picture']['data']['url']
                    );
                    $insert_id = $this->CM->addfbuser($facebookdata);
                    $fbuserlogin = $this->CM->getfbuser($insert_id);

                    $user_id = $this->session->set_userdata('user_id', $fbuserlogin->id);
                    $this->session->set_userdata('userdata', $fbuserlogin);
                    $this->session->set_userdata('is_logged_in', true);
                    Authenticate::setData($datauser = array());
                    if (($this->session->userdata('last_page'))) {
                        $redirect = $this->session->userdata('last_page');
                        $this->session->unset_userdata('last_page');
                    } else {
                        $redirect = 'dashboard';
                    }
                    redirect($redirect);
                }
            } catch (FacebookApiException $e) {
                $user = null;
            }
        } else {
            // Solves first time login issue. (Issue: #10)
            //$this->facebook->destroySession();
        }

        if ($user) {

            $data['logout_url'] = site_url('logout'); // Logs off application
            // OR 
            // Logs off FB!
            // $data['logout_url'] = $this->facebook->getLogoutUrl();
        } else {
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('login'),
                'scope' => array("email") // permissions here
            ));
        }
        // method of layout library | autoloaded
        $msg = $this->layouts->loadMessage('message','error');
        $data['msg'] = $msg;
        $this->layouts->render('auth/login', $data, 'default');
    }

    public function activation() {

        if ($this->session->userdata('is_logged_in')) {

            redirect('dashboard');
        }

        $this->layouts->set_title("User Activation");

        $activationCode = $this->uri->segment(3);



        $where = array(
            'user_token' => $activationCode,
            'is_token_valid' => 'Y'
        );

        $user = $this->UM->check_user_with_token($where);



        if (count($user) > 0) {

            $data = array(
                'user_token' => NULL,
                'is_token_valid' => 'N',
                'status' => 1
            );

            $this->UM->update_user_with_token($where, $data);

            $this->session->set_flashdata('success', 'Your email id now confirmed.');

            $params = $params = ['email' => $user[0]->email, 'password' => $user[0]->password, 'status' => 1];



            $usr = $this->UM->get_info($params);



            $user_id = $this->session->set_userdata('user_id', $usr->id);



            $this->session->set_userdata('userdata', $usr);

            $this->session->set_userdata('is_logged_in', true);

            Authenticate::setData($datauser = array());



            $response = $this->test($user[0]);



            $response1 = $this->create($response);



            $dataLast = array(
                'we_pay_account_id' => $response1->account_id,
            );

            $whereLast = ['id' => $user[0]->id];

            $this->UM->update_user_with_token($whereLast, $dataLast);



            redirect(base_url() . 'dashboard');

            //redirect('login');
        } else {

            redirect('home');
        }
    }

    public function logout() {


        $this->load->library('facebook');

        // Logs off session from website
        $this->facebook->destroySession();

        $this->session->unset_userdata('userdata');

		$this->session->unset_userdata('is_logged');
        $this->session->unset_userdata('is_logged_in');

        $this->session->unset_userdata('user_id');

        redirect('home');
    }

    //Delete campaign

    public function forgotPassword() {

        $email = $this->input->post('email', true);

        $where = array(
            'email' => $email,
            'status' => 1
        );

        $campaings = $this->UM->get_info($where);

        $token = token();

        if (count($campaings) > 0) {



            $token = token();

            $data = array(
                'user_token' => $token,
                'is_token_valid' => 'Y'
            );

            $this->UM->update_user_with_token($where, $data);



            $link = base_url() . 'auth/resetpasword/' . $token;



            $message = "Password reset link.<br/><a href=" . $link . ">Click here</a><br/>If link does not work ,Please copy and paste this link.<br/>" . $link;



            //$email = $this->input->post('txtemail');

            send_mail_to_user('Password reset link', $message, $email);
            $return = array(
                'message' => 'success', 'code' => 200
            );
        } else
            $return = array(
                'message' => 'error', 'code' => 201
            );



        echo json_encode($return);
    }

    public function test($user) {

        require(we_pay_library);

        // application settings

        $account_id = wepay_account_id; // your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = wepay_app_access_token; // your app's access_token
        // change to useProduction for live environments

        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);

        // register new merchant

        $response = $wepay->request('user/register/', array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'email' => $user->email,
            'scope' => 'manage_accounts,collect_payments,view_user,preapprove_payments,send_money',
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'original_ip' => $_SERVER['REMOTE_ADDR'],
            'original_device' => 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6;

                             en-US) AppleWebKit/534.13 (KHTML, like Gecko)

                             Chrome/9.0.597.102 Safari/534.13',
            "tos_acceptance_time" => time(),
            "callback_uri" => base_url() . 'auth/callback_from_register',
            "redirect_uri" => base_url() . 'user/getAccountStatus/' . $user->id,
        ));



        $data = array(
            'wepay_access_token' => $response->access_token,
            'wepay_user_id' => $response->user_id,
            'wepay_token_type' => $response->token_type,
            'wepay_token_expires_in' => $response->expires_in,
        );

        $where = ['id' => $user->id];

        $this->UM->update_user_with_token($where, $data);



        return $response;
    }

    public function create($responsea) {

        //require(we_pay_library);
        // application settings

        $account_id = wepay_account_id; // your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = $responsea->access_token;

        // change to useProduction for live environments
        //Wepay::useStaging($client_id, $client_secret);



        $wepay = new WePay($access_token);



        // create an account for a user

        $response = $wepay->request('account/create/', array(
            'name' => 'Mefarnes App',
            'description' => 'This is mefarnes test account.'
        ));



        $responseConfirm = $wepay->request('user/send_confirmation/', array(
            "email_message" => "Welcome to <strong>mefarnes application</strong>"
        ));



        return $response;
    }

    public function callback_from_register() {



        require(we_pay_library);

        $account_id = wepay_account_id; //your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = wepay_app_access_token; // your app's access_token
        // change to useProduction for live environments

        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);



        // create the withdrawal

        $response = $wepay->request('account/get_update_uri', array(
            'account_id' => $account_id,
            'redirect_uri' => base_url() . 'user/getAccountStatus',
            'mode' => 'iframe'
        ));



        // display the response
//        echo '<pre>';
//        print_r($response -> uri); exit;

        $arr['uri'] = $response->uri;

        //$response['data'] = (($response)); //exit;

        $this->layouts->render('campaign/redirect', $data = $arr, 'default');
    }

    public function getAccountStatus($userid) {



        $where = array(
            'id' => $userid,
        );

        $user = $this->UM->check_user_with_token($where);



        //if (count($user) > 0) {

        $data = array(
            'user_token' => NULL,
            'is_token_valid' => 'N',
            'status' => 1
        );

        //}

        $this->UM->update_user_with_token($where, $data);

        $this->session->set_flashdata('success', 'Your email id now confirmed.');

        $params = $params = ['email' => $user[0]->email, 'password' => $user[0]->password, 'status' => 1];



        $usr = $this->UM->get_info($params);



        $user_id = $this->session->set_userdata('user_id', $usr->id);



        $this->session->set_userdata('userdata', $usr);

        $this->session->set_userdata('is_logged_in', true);

        Authenticate::setData($datauser = array());



        $userinfo = $this->UM->get_info(array('id' => $userid));



        $weAccountId = $userinfo->we_pay_account_id;

        require(we_pay_library);

        $account_id = $userinfo->we_pay_account_id; //your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = $userinfo->wepay_access_token; // your app's access_token
        // change to useProduction for live environments

        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);



        // create the withdrawal

        $response = $wepay->request('account/get_update_uri', array(
            'account_id' => $account_id,
            'redirect_uri' => base_url() . 'user/success',
            'mode' => 'iframe'
        ));

        $arr['uri'] = $response->uri;

        redirect(base_url() . 'user/success');
        //$this->layouts->render('campaign/redirect', $data = $arr, 'default');
    }

    public function saveorgsignupstep1() {
        $data = array();

        $this->load->helper(array('form', 'url'));

        $this->form_validation->set_rules('org_name', 'Organization Name', 'required|trim|required');
        $this->form_validation->set_rules('email_org', 'Email', 'required|trim|is_unique[users.email]'); 
        $this->form_validation->set_rules('city', 'City', 'required|trim');
        $this->form_validation->set_rules('pssword_org', 'Password', 'required|trim|required');

        //if ($this->form_validation->run() == FALSE) {
            //$this->layouts->render('organization/signup-step1', $data, 'Default');
        //} else {
         if(!empty($_POST)){
            $token = token();

            $slug = preg_replace('/\s+/', '-', $this->input->post('org_name', true));
            $user_arr = array(
                'org_name' => $this->input->post('org_name', true),
                'email' => $this->input->post('email_org', true),
                'password' => sha1($this->input->post('pssword_org', true)),
                'city' => addslashes($this->input->post('city', true)),
                'profile_type' => 2,
                'create_date' => date('Y-m-d H:i:s'),
                'newsletter' => 1,
                'user_token' => $token,
                'is_token_valid' => 'Y',
                'status'=>1, 
                'org_slug' => strtolower($slug),
            );
//var_dump($user_arr);exit;
            $user_id = $this->UM->insert_user($user_arr);
            $orguser = $this->session->set_userdata('orguser', $user_id);

            $params = ['id' => $user_id];
            $user = $this->UM->get_info($params);

            $username = $this->input->post('email_org', true);
            $password = sha1($this->input->post('pssword_org', true));
            $params = ['email' => $username, 'password' => $password, 'status' => 1];

            if ($this->UM->check_login($params) == 1) {
                        $usr = $this->UM->get_info($params);
                        $user_id = $this->session->set_userdata('user_id', $usr->id);
                        $this->session->set_userdata('userdata', $usr);
                        $this->session->set_userdata('is_logged_in', true);
                        $this->session->set_userdata('isProfile', $usr->isProfile);
                        $this->session->set_userdata('profile_type', $usr->profile_type);
                        $this->session->set_userdata('org_slug', $usr->org_slug);
                        Authenticate::setData($datauser = array());
                        if (($this->session->userdata('last_page'))) {
                            $redirect = $this->session->userdata('last_page');
                            $this->session->unset_userdata('last_page');
                        } else {
                            $redirect = 'dashboard';
                        }
               
               
                }

            //$this->activationFromSignUp($user_id,'load'); 


            redirect(base_url() . 'organization/signup-step2');
            }
       // }
    }

    public function saveorgsignupstep2() {
    	
		$data = array();
		$post = $this->input->post();
		$this->load->helper(array('form', 'url'));

        $this->form_validation->set_rules('org_account_type', 'Account type', 'required');
		if(!empty($post['org_account_type']) && $post['org_account_type']==3){
        $this->form_validation->set_rules('ein_no', 'EIN No', 'required');
		}
        //alert('kgskdgjdkfjg');
        //var_dump($this->form_validation->run());exit;
       if ($this->form_validation->run() == FALSE) {
        	
            $this->layouts->render('organization/signup-step2', $data, 'Default');
        } else {
        	//var_dump($token);exit;
            $token = token();
            $slug = preg_replace('/\s+/', '-', $this->input->post('title', true));
            $user_arr = array(
                'org_account_type' => $this->input->post('org_account_type', true),
                'ein_no' => $this->input->post('ein_no', true)
            );

            $userID = $this->session->userdata('orguser');
            $where = array(
                'id' => $userID,
            );


            $this->UM->update_organization_profile($where, $user_arr);

            redirect(base_url() . 'organization/signup-step3');
        }
    }

    public function saveorgsignupstep3() {
   // exit;
        $data = array();

        $this->load->helper(array('form', 'url'));

        $this->form_validation->set_rules('full_address', 'Address', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->layouts->render('organization/signup-step3', $data, 'Default');
        } else {
            $token = token();

            $slug = preg_replace('/\s+/', '-', $this->input->post('title', true));
            $user_arr = array(
                'street_name' => $this->input->post('full_address', true),
            );
            $userID = $this->session->userdata('orguser');
//var_dump($userID);exit;
            $where = array(
                'id' => $userID,
            );


            $this->UM->update_organization_profile($where, $user_arr);

            $this->session->unset_userdata('orguser');

            //$this->activationFromSignUp($userID,'load');
//var_dump($this->activationFromSignUp($userID));exit;

            //redirect('home/thankyou');print_r($this->layouts);
            $this->layouts->render('auth/signup-thanku', $data = array(), 'default');
            
        }
    }

    /* public function signOrganization() {
      $data = array();

      $this->load->helper(array('form', 'url'));

      $this->form_validation->set_rules('org_name', 'Organization Name', 'required|trim|required');
      $this->form_validation->set_rules('email_org', 'Email', 'required|trim|is_unique[users.email]');
      $this->form_validation->set_rules('city', 'City', 'required|trim');
      $this->form_validation->set_rules('pssword_org', 'Password', 'required|trim|required');
      $this->form_validation->set_rules('org_account_type', 'Account type', 'required');
      $this->form_validation->set_rules('ein_no', 'EIN No', 'required');
      $this->form_validation->set_rules('full_address', 'Address', 'required|trim');

      if ($this->form_validation->run() == FALSE) {
      $this->layouts->render('signup', $data, 'Default');
      } else {
      $token = token();

      $slug = preg_replace('/\s+/', '-', $this->input->post('title', true));
      $user_arr = array(

      'org_name' => $this->input->post('org_name', true),
      'email' => $this->input->post('email_org', true),
      'password' => sha1($this->input->post('pssword_org', true)),
      'city' => addslashes($this->input->post('city', true)),
      'org_account_type' => $this->input->post('org_account_type', true),
      'ein_no' => $this->input->post('ein_no', true),
      'street_name' => $this->input->post('full_address', true),
      'profile_type' => 2,
      'create_date' => date('Y-m-d H:i:s'),
      'newsletter' => 1,
      'user_token' => $token,
      'is_token_valid' => 'Y',
      'org_slug'  => strtolower($slug),
      );

      $user_id = $this->UM->insert_user($user_arr);
      $this->activationFromSignUp($user_id);
      redirect('home/thankyou');

      }

      } */

    public function username_check($str) {





        if (!preg_match('/^[a-z .,\-]+$/i', $str)) {

            $this->form_validation->set_message('username_check', 'The {field} field must be alphanumeric');

            return FALSE;
        } else
            return true;
    }

    public function activationFromSignUp($userid,$load='') {



        $params = ['id' => $userid];



        $user = $this->UM->get_info($params);

        $response = $this->testSignUp($user);

        $response1 = $this->createSignUp($response,$user,$load);



//        echo '<pre>';
//        print_r($response1);
//        exit;



        $dataLast = array(
            'we_pay_account_id' => $response1->account_id,
        );

        $whereLast = ['id' => $user->id,];
        
        //var_dump($dataLast);
       //var_dump($whereLast);exit;

        $this->UM->update_user_with_token($whereLast, $dataLast);
    }

    public function testSignUp($user) {

        require(we_pay_library);

        // application settings

        $account_id = wepay_account_id; // your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = wepay_app_access_token; // your app's access_token
        // change to useProduction for live environments

        WePay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);

        // register new merchant

        $response = $wepay->request('user/register/', array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'email' => $user->email,
            'scope' => 'manage_accounts,collect_payments,view_user,preapprove_payments,send_money',
            'first_name' => $user->org_name,
            'last_name' => $user->org_name,
            'original_ip' => $_SERVER['REMOTE_ADDR'],
            'original_device' => 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6;

                             en-US) AppleWebKit/534.13 (KHTML, like Gecko)

                             Chrome/9.0.597.102 Safari/534.13',
            "tos_acceptance_time" => time(),
            "callback_uri" => base_url() . 'auth/callback_from_register',
            "redirect_uri" => base_url() . 'auth/getAccountStatus/' . $user->id,
        ));



        $data = array(
            'wepay_access_token' => $response->access_token,
            'wepay_user_id' => $response->user_id,
            'wepay_token_type' => $response->token_type,
            'wepay_token_expires_in' => $response->expires_in,
        );

        $where = ['id' => $user->id];

        $this->UM->update_user_with_token($where, $data);



        return $response;
    }

    public function createSignUp($responsea,$user,$load='') {

       // require(we_pay_library);
        // application settings
        if(empty($load)){
        require(we_pay_library);
        }

        $account_id = wepay_account_id; // your app's account_id

        $client_id = wepay_client_id;

        $client_secret = wepay_client_secret;

        $access_token = $responsea->access_token;

        // change to useProduction for live environments
        if(empty($load)){
        Wepay::useProduction($client_id, $client_secret);
        }



        $wepay = new WePay($access_token);



        // create an account for a user

        $checkUser = $wepay->request('account/find/', array(
            'name' => 'Mefarnes App',
        ));

        if (empty($checkUser)) {



            // create an account for a user

            $response = $wepay->request('account/create/', array(
                'name' => 'Mefarnes App',
                'description' => 'This is mefarnes test account.'
            ));


            $user = $this->CM->get_single_row_from_id(['id' => $user->id], 'users');
            $data['user'] = $user;
            $message = $this->load->view("template/thanks-signup-wepay", $data, true);
       

            $responseConfirm = $wepay->request('user/send_confirmation/', array(
                "email_message" => $message,
                "email_subject" => "Mefarnes Confirmation from Wepay",
                "email_button_text" => "Confirm"
            ));

            $return = $response;
        } else {

            $return = $checkUser[0];
        }



        return $return;
    }
    
    public function resetpasword()
    {
        if ($this->session->userdata('is_logged_in')) {

            redirect('dashboard');
        }

        $this->layouts->set_title("Reset Password");

        $resetCode = $this->uri->segment(3);



        $where = array(
            'user_token' => $resetCode,
            'is_token_valid' => 'Y'
        );

        $user = $this->UM->check_user_with_token($where);
        if (count($user) > 0) {
            $arr = array('resetCode' => $resetCode );
            $this->layouts->render('auth/resetpasword', $data = $arr, 'default');
        }
        else{
            $arr = array('' => '' );
            $this->session->set_userdata('error', '<strong>Sorry!</strong>  Your reset password link has been expired.');
            $this->layouts->render('auth/resetpaswordfail', $data = $arr, 'default');   
        }

    }
    public function savePassword()
    {
        if ($this->session->userdata('is_logged_in')) {

            redirect('dashboard');
        }

        $password = sha1($this->input->post('new_password', true));
        $resetCode = $this->input->post('resetCode', true);

        $this->layouts->set_title("Reset Password");

        $where = array(
            'user_token' => $resetCode,
            'is_token_valid' => 'Y'
        );

        $user = $this->UM->check_user_with_token($where);
        
        if (count($user) > 0) {
            
            foreach ($user as $key => $value) {
                $user_id = $value->id;
            }
            $data = array('password' => $password, 'is_token_valid' => 'N');
            $where = array('id' => $user_id );
            $this->CM->common_update($data, $where, 'users');

            $this->session->set_userdata('success', '<strong>Congrats!</strong> Your password successfully changed. plz login here <a href="login">Login</a>');
            $arr = array('resetCode' => $resetCode );
            $this->layouts->render('auth/resetpaswordfail', $data = $arr, 'default');
        }

    }
}
