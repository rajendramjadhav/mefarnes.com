<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends CI_Controller { //Authenticate

    public function __Construct() {

        parent::__Construct();
		//$this->load->library("OAuth");
		$arrParam[] = CONSUMER_KEY;
		$arrParam[] = CONSUMER_SECRET;
		$arrParam[] = $_SESSION['token'];
		$arrParam[] = $_SESSION['token_secret'];
		//var_dump($arrParam);exit;
		$this->load->library("TwitterOAuth");
		$this->load->model('Campaign_model', 'CPM'); // load model

       
    }

    //dashboard page

  public function twitterfriendscount() {
    	//echo "asdf";
   if(isset($_REQUEST['oauth_token']) && $_SESSION['token']  !== $_REQUEST['oauth_token']) {

	//If token is old, distroy session and redirect user to index.php
	session_destroy();
	header('Location: index.php');
	
}elseif(isset($_REQUEST['oauth_token']) && $_SESSION['token'] == $_REQUEST['oauth_token']) {

	//Successful response returns oauth_token, oauth_token_secret, user_id, and screen_name
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['token'] , $_SESSION['token_secret']);
	$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
	if($connection->http_code == '200')
	{
		//Redirect user to twitter
		$_SESSION['status'] = 'verified';
		$_SESSION['request_vars'] = $access_token;
		
		//Insert user into the database
		$user_info = $connection->get('account/verify_credentials'); 
		$userId = $this->session->userdata('user_id');
		$param = array(	
		    'user_id' => $userId, 	  
			'name' => $user_info->name,
			'followers_count' => $user_info->followers_count,
			'friends_count' => $user_info->friends_count,
			'favourites_count' => $user_info->favourites_count,
			'profile_image_url' => $user_info->profile_image_url,
			'token' => $_SESSION['token'],
			'oauth_token' => $_SESSION['token_secret'],
		);
		$get_user = $this->CPM->get_twitteralldata($userId);
		//var_dump($get_user);exit;
		if($get_user->user_id != $userId){
		$alltwitterdata = $this->CPM->inserttwitterdata($param,$userId);
		}else{
		$alltwitterdata = $this->CPM->edittwitterdata($param,$userId);	
		}
		//var_dump($alltwitterdata);exit;
		redirect(base_url() . 'dashboard');
		
		//$name = explode(" ",$user_info->name);
		//var_dump($name);exit;
		//$fname = isset($name[0])?$name[0]:'';
		//$lname = isset($name[1])?$name[1]:'';
		//$db_user = new Users();
		//$db_user->checkUser('twitter',$user_info->id,$user_info->screen_name,$fname,$lname,$user_info->lang,$access_token['oauth_token'],$access_token['oauth_token_secret'],$user_info->profile_image_url);
		
		//Unset no longer needed request tokens
		unset($_SESSION['token']);
		unset($_SESSION['token_secret']);
		header('Location: index.php');
	}else{
		die("error, try again later!");
	}
		
}else{

	if(isset($_GET["denied"]))
	{
		header('Location: index.php');
		die();
	}

	//Fresh authentication
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
	$request_token = $connection->getRequestToken(OAUTH_CALLBACK);

	//Received token info from twitter
	$_SESSION['token'] 			= $request_token['oauth_token'];
	$_SESSION['token_secret'] 	= $request_token['oauth_token_secret'];
	
	//Any value other than 200 is failure, so continue only if http code is 200
	if($connection->http_code == '200')
	{
		//redirect user to twitter
		$twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
		header('Location: ' . $twitter_url); 
	}else{
		die("error connecting to twitter! try again later!");
	}
}
        
    }

    
}
