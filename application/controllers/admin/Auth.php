<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Admin_model', 'AM'); // model alias 
    }

    public function index() {
        $this->login();
    }

    public function login() {
        $data = array();
        $this->layouts->set_title("Admin Login");
        // if it has post data then process login check
        if ($this->input->post()) {

            $username = $this->input->post('email');
            $password = sha1($this->input->post('password'));

            $params = ['email' => $username, 'password' => $password];

            if ($this->AM->check_login($params) == 1) {

                $usr = $this->AM->get_info($params);

                $logHis['uid'] = $usr->admin_id;
                $logHis['ip'] = $this->input->ip_address();
                $logHis['other_data'] = $this->input->user_agent();
                $logHis['created_date'] = date('Y-m-d H:i:s');

                $this->AM->insert_login_history($logHis);

                $user_id = $this->session->set_userdata('user_id', $usr->admin_id);

                $this->session->set_userdata('userdata', $usr);
                $this->session->set_userdata('is_logged_in', true);
                Authenticate::setData($datauser=array());
                setMessage([
                    'title' => 'Login',
                    'text' => 'Login successful !!',
                    'timer' => 2000,
                    'type' => 'success',
                    'showConfirmButton' => false
                ]);
                redirect('admin/dashboard');
            } else {
                setMessage([
                    'title' => 'Invalid username/password',
                    'text' => '',
                    'timer' => 2000,
                    'type' => 'error',
                    'showConfirmButton' => false
                ]);
                
            }
        }
        // method of layout library | autoloaded
        $this->layouts->render('admin/login', $data, 'admin_login');
    }

    public function logout() {
        setMessage([
            'title' => 'Logout',
            'text' => 'You have logout !!',
            'timer' => 2000,
            'type' => 'success',
            'showConfirmButton' => false
        ]);
        $this->session->unset_userdata('userdata');
        $this->session->unset_userdata('is_logged_in');
        $this->session->unset_userdata('user_id');
        redirect('admin/auth');
    }

}
