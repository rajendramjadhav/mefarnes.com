<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns extends Authenticate {

    var $csrf_protection = FALSE; // on by default

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Campaign_model', 'CM'); // load model
        $this->load->model('Common_model', 'COMMON'); // load model
        //$this->load->model('Notification_model','NM');
        $this->layouts->set_title("Merfanes :: BACK PANEL");
    }

    public function active() {
        //$this->output->enable_profiler(TRUE);
        $data = array();

        $config['per_page'] = 20;

        $config['base_url'] = base_url('admin/campaigns/active/');
        $config['uri_segment'] = 4;
        $offset = $this->uri->segment($config['uri_segment']);

        if (empty($offset))
            $offset = 0;
        //$offset = !empty($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
        $no_of_items = $config['per_page'];
        $where = [
            'no_of_items' => $no_of_items,
            'offset' => $offset,
        ];

        $data = $where;

        $search_condition = $this->input->post();
        if (empty($search_condition))
            $search_condition = '';
        $dealer_list = $this->CM->get_campaigns($where, $search_condition, 'active'); // returns an array [(array)records, (number)totalcount]

        $data['list'] = $dealer_list[0];
        if (!empty($data['list'])) {
            $data['total'] = $dealer_list[1];
            $config['total_rows'] = $data['total'];
        }
        $data['pagination'] = generate_pagination($config); // generate pagination boilerplate function
        $data['search'] = '';
        if (!empty($search_condition)) {
            foreach ($search_condition as $key => $val) {
                $data['search'][$key] = $val;
            }
        }
        $data['users'] = $this->COMMON->get_categories(array('status!=-1'), 'users');
        $this->layouts->render('admin/campaign_list', $data, 'admin');
    }

    public function expired() {
        $data = array();
//$this->output->enable_profiler(TRUE);
        $config['per_page'] = 20;

        $config['base_url'] = base_url('admin/campaigns/expired/');
        $config['uri_segment'] = 4;
        $offset = $this->uri->segment($config['uri_segment']);

        if (empty($offset))
            $offset = 0;
        $no_of_items = $config['per_page'];
        $where = [
            'no_of_items' => $no_of_items,
            'offset' => $offset,
        ];

        $data = $where;

        $search_condition = $this->input->post();
        if (empty($search_condition))
            $search_condition = '';
        $dealer_list = $this->CM->get_campaigns($where, $search_condition, 'expired'); // returns an array [(array)records, (number)totalcount]

        $data['list'] = $dealer_list[0];
        if (!empty($data['list'])) {
            $data['total'] = $dealer_list[1];
            $config['total_rows'] = $data['total'];
        }
        $data['pagination'] = generate_pagination($config); // generate pagination boilerplate function
        $data['search'] = '';
        if (!empty($search_condition)) {
            foreach ($search_condition as $key => $val) {
                $data['search'][$key] = $val;
            }
        }
        $data['users'] = $this->COMMON->get_categories(array('status!=-1'), 'users');
        $this->layouts->render('admin/campaign_list', $data, 'admin');
    }

    //Update campaign
    public function changeStatus() {
        $campaignId = $this->input->post('campaign_id', true);
        $where = array(
            'id' => $campaignId
        );
        $campaings = $this->CM->getCampaign($where);

        if (count($campaings) > 0) {

            $campaings = $this->CM->updateCampaignStatusBackend($where);
            $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash(), 'query' => $campaings);
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );

        echo json_encode($return);
    }

    //campaign delete
    public function deleteCampaign() {
        $campaignId = $this->input->post('campaign_id', true);
        $type = $this->input->post('type', true);
        $where = array(
            'id' => $campaignId
        );
        $campaings = $this->CM->getCampaign($where);

        if (count($campaings) > 0) {
            if ($type == 'del') {
                $data = array('status' => '-1');
                $campaings = $this->CM->updateCampaign($data, $where);
            } elseif ($type == 'update') {
                $status = $this->input->post('status', true);
                if ($status == 'feature')
                    $field = 'home_page_visible';
                if ($status == 'home')
                    $field = 'home_page_not_featured';
                $this->COMMON->updateStatusFeature($where, 'campaigns', $field);
            }
            $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash());
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );

        echo json_encode($return);
    }

    public function donation_info($id) {
        $data['campaign'] = $this->COMMON->get_single_row_from_id(array('id' => $id), 'campaigns');
        $data['total_donation'] = $this->CM->get_total_donation($id);
        $donationDtl = $this->CM->getDonationDtl($id);
        $data['donation'] = $donationDtl;
        $this->layouts->render('admin/donation_list', $data, 'admin');
    }

    public function reported() {
        //$this->output->enable_profiler(TRUE);
        $data = array();
        $where = [
        ];
        $data['campaigns'] = $this->CM->reported_campaign($where);
        $this->layouts->render('admin/reported_campaign_list', $data, 'admin');
    }

    public function report_detail($id) {
        $data = [];
        $data['campaign'] = $this->COMMON->get_single_row_from_id(array('id' => $id), 'campaigns');
        $where = ['campaign_reports.campaign_id' => $id];
        $data['reports'] = $this->CM->getReportDtl($where);
//        echo '<pre>';
//        print_r($data); exit;

        $this->layouts->render('admin/report_list', $data, 'admin');
    }

    public function deleteReport() {
        $campaignId = $this->input->post('campaign_id', true);
        $where = array(
            'id' => $campaignId
        );

        $campaings = $this->COMMON->delete($where, 'campaign_reports');
        $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash());

        echo json_encode($return);
    }

    public function campaign_home_page() {
        //$this->output->enable_profiler(TRUE);
        $data = array();
        $where = [
            'home_page_not_featured' => 1
        ];
        $data['campaigns'] = $this->CM->get_featured_campaign($where);
        $this->layouts->render('admin/homepage_campaign_list', $data, 'admin');
    }

    public function priority() {
        $campaignId = $this->input->post('campaign_id', true);
        $priority = $this->input->post('priority', true);
        $where = array(
            'id' => $campaignId
        );
        $campaings = $this->CM->getCampaign($where);

        if (count($campaings) > 0) {
            
            $this->COMMON->common_update(['priority' =>$priority ],$where, 'campaigns');
            $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash());
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );

        echo json_encode($return);
    }

}
