<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Authenticate {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('User_model', 'UM'); // load model
        $this->load->model('Common_model', 'CM'); // load model
        //$this->load->model('Notification_model','NM');
        $this->layouts->set_title("Merfanes :: BACK PANEL");
    }

    public function active() {
        //$this->output->enable_profiler(TRUE);
        $data = array();

        $config['per_page'] = 25;

        $config['base_url'] = base_url('admin/users/active/');
        $config['uri_segment'] = 4;
        $offset = $this->uri->segment($config['uri_segment']);
        //var_dump($offset);exit;
        if (empty($offset))
            $offset = 0;
        //$offset = !empty($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
        $no_of_items = $config['per_page'];
        $where = [
            'no_of_items' => $no_of_items,
            'offset' => $offset,
            //'status' => '1',
            //'profile_type' => '1',
             //'profile_type' => '2',
            
            
        ];

        $data = $where;
         //var_dump($data);exit;
        $search_condition = $this->input->post();
        if (empty($search_condition))
            $search_condition = '';
        $dealer_list = $this->UM->get_user($where, $search_condition); // returns an array [(array)records, (number)totalcount]
       // var_dump($dealer_list);exit;
        $data['list'] = $dealer_list[0];
        if (!empty($data['list'])) {
            $data['total'] = $dealer_list[1];
            $config['total_rows'] = $data['total'];
         }
        $data['pagination'] = generate_pagination($config); // generate pagination boilerplate function
        //var_dump($data['pagination']);exit;
        $data['search'] = '';
        if (!empty($search_condition)) {
            foreach ($search_condition as $key => $val) {
                $data['search'][$key] = $val;
            }
        }
        //var_dump($data);exit;

        $this->layouts->render('admin/users_list', $data, 'admin');
    }

    //Update campaign
    public function changeStatus() {
        $user_id = $this->input->post('user_id', true);
        $where = array(
            'id' => $user_id
        );
        $campaings = $this->CM->get_single_row_from_id($where, 'users');

        if (count($campaings) > 0) {

            $campaings = $this->UM->updateUserStatusBackend($where);
            $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash(), 'query' => $campaings);
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );

        echo json_encode($return);
    }

    //campaign delete
    public function deleteUser() {
        $user_id = $this->input->post('user_id', true);
        $where = array(
            'id' => $user_id
        );
        $campaings = $this->CM->get_single_row_from_id($where,'users');

        if (count($campaings) > 0) {
            //$data = array('status' => '-1');
            //$campaings = $this->CM->common_update($data, $where,'users');
            $campaings = $this->CM->common_deleteuser($where,'users');
            $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash());
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );

        echo json_encode($return);
    }
    
    //Pending activation User
    
    public function pending()
    {
        //$this->output->enable_profiler(TRUE);
        $data = array();

        $config['per_page'] = 25;

        $config['base_url'] = base_url('admin/users/pending/');
        $config['uri_segment'] = 4;
        $offset = $this->uri->segment($config['uri_segment']);

        if (empty($offset))
            $offset = 0;
        //$offset = !empty($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
        $no_of_items = $config['per_page'];
        $where = [
            'no_of_items' => $no_of_items,
            'offset' => $offset,
            'status' => '0',
            //'profile_type' => '1',
            
        ];

        $data = $where;

        $search_condition = $this->input->post();
        if (empty($search_condition))
            $search_condition = '';
        $dealer_list = $this->UM->get_user($where, $search_condition); // returns an array [(array)records, (number)totalcount]

        $data['list'] = $dealer_list[0];
        if (!empty($data['list'])) {
            $data['total'] = $dealer_list[1];
            $config['total_rows'] = $data['total'];
        }
        $data['pagination'] = generate_pagination($config); // generate pagination boilerplate function
        $data['search'] = '';
        if (!empty($search_condition)) {
            foreach ($search_condition as $key => $val) {
                $data['search'][$key] = $val;
            }
        }

        $this->layouts->render('admin/users_list', $data, 'admin');
    }

}
