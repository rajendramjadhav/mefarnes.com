<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Authenticate {

    public function __Construct() {
        parent::__Construct();
        $this->layouts->set_title("Merfanes :: BACK PANEL");
    }

    public function profile() {
        $data = array();

        $this->layouts->render('admin/profile', $data, 'admin');
    }

    public function index() {
        $this->dashboard();
    }

    public function dashboard() {
        $data = array();
        $this->layouts->render('admin/dashboard', $data, 'admin');
    }

}
