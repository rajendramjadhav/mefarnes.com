<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Authenticate {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Common_model', 'CM'); // load model
        $this->load->model('Pages_model', 'PM'); // load model
        $this->layouts->set_title("Merfanes :: BACK PANEL");
    }

    public function contacted() {
        $data = array();

        $config['per_page'] = 20;

        $config['base_url'] = base_url('admin/pages/contacted/');
        $config['uri_segment'] = 4;
        $offset = $this->uri->segment($config['uri_segment']);

        if (empty($offset))
            $offset = 0;
        //$offset = !empty($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
        $no_of_items = $config['per_page'];
        $where = [
            'no_of_items' => $no_of_items,
            'offset' => $offset,
        ];
        $data = $where;
        $dealer_list = $this->PM->get_campaigns($data); // returns an array [(array)records, (number)totalcount]


        $data['list'] = $dealer_list[0];
        if (!empty($data['list'])) {
            $data['total'] = $dealer_list[1];
            $config['total_rows'] = $data['total'];
        }
        $data['pagination'] = generate_pagination($config); //generate pagination boilerplate function
        $where_one = [];
        $data['users'] = $this->CM->get_categories($where_one, 'site_contacts_log');

        $this->layouts->render('admin/contact_list', $data, 'admin');
    }

    public function reply($id) {
        $data['user'] = $contactInfo = $this->CM->get_single_row_from_id(['id' => $id], 'site_contacts_log');
        if ($this->input->post()) {
            $data = $this->input->post();
            unset($data['submit']);
            $dataToUpdate = ['replied' => 1];
            $r = $this->CM->common_update($dataToUpdate, ['id' => $id], 'site_contacts_log');
            if ($r) {

                //$data['text'] = $data['text'];
                $emailHtml = $this->load->view('template/contact-reply', $data, true);

                $params = array(
                    'to' => $contactInfo->email,
                    'subject' => $data['subject'],
                    'html' => $emailHtml,
                    'from' => 'admin@mefarnes.com',
                );
                send_mail_through_sendgrid($params);

                setMessageSuccess('Email sent sucessfully!!');
            } else {
                setMessageError('Somthing went wrong, please try again.');
            }
            redirect('admin/pages/reply/' . $id);
        }

        $this->layouts->render('admin/reply', $data, 'admin');
    }

    public function pagelist() {
        $data = [];
        $data['list'] = $this->CM->get_categories([], 'pages');
        $this->layouts->render('admin/page_list', $data, 'admin');
    }

    public function edit_meta($id) {
        $data = [];
        $data['list'] = $this->CM->get_single_row_from_id(['id' => $id], 'pages');

        if ($this->input->post()) {
            $data = $this->input->post();
            unset($data['submit']);
            $dataToUpdate = [
                'page_name' => $data['page_name'],
                'meta_title' => $data['meta_title'],
                'keyword' => $data['keyword'],
                'meta_description' => $data['meta_description'],
            ];
            $r = $this->CM->common_update($dataToUpdate, ['id' => $id], 'pages');
            if ($r) {
                setMessageSuccess('meta saved sucessfully!!');
            } else {
                setMessageError('Somthing went wrong, please try again.');
            }
            redirect('admin/pages/pagelist/');
        }



        $this->layouts->render('admin/edit_meta', $data, 'admin');
    }

}
