<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends Authenticate {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Category_model', 'CTM'); // load model
        $this->load->model('Common_model', 'CM'); // load model
        //$this->load->model('Notification_model','NM');
        $this->layouts->set_title("Merfanes :: BACK PANEL");
    }

    public function index() {
        $this->output->enable_profiler(TRUE);
        $data = array();

        $config['per_page'] = 20;

        $config['base_url'] = base_url('admin/categories/index/');
        $config['uri_segment'] = 4;
        $offset = $this->uri->segment($config['uri_segment']);

        if (empty($offset))
            $offset = 0;
        //$offset = !empty($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
        $no_of_items = $config['per_page'];
        $where = [
            'no_of_items' => $no_of_items,
            'offset' => $offset,
        ];

        $data = $where;

        $search_condition = $this->input->post();
        if (empty($search_condition))
            $search_condition = '';
        $dealer_list = $this->CTM->get_category($where, $search_condition); // returns an array [(array)records, (number)totalcount]

        $data['list'] = $dealer_list[0];
        if (!empty($data['list'])) {
            $data['total'] = $dealer_list[1];
            $config['total_rows'] = $data['total'];
        }
        $data['pagination'] = generate_pagination($config); // generate pagination boilerplate function
        $data['search'] = '';
        if (!empty($search_condition)) {
            foreach ($search_condition as $key => $val) {
                $data['search'][$key] = $val;
            }
        }

        $this->layouts->render('admin/category_list', $data, 'admin');
    }

    //Update campaign
    public function changeStatus() {
        $user_id = $this->input->post('user_id', true);
        $where = array(
            'id' => $user_id
        );
        $campaings = $this->CM->get_single_row_from_id($where, 'campaign_categories');

        if (count($campaings) > 0) {

            $campaings = $this->CM->updateStatus($where, 'campaign_categories');
            $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash(), 'query' => $campaings);
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );

        echo json_encode($return);
    }

    //campaign delete
    public function deleteUser() {
        $user_id = $this->input->post('user_id', true);
        $where = array(
            'id' => $user_id
        );
        $campaings = $this->CM->get_single_row_from_id($where, 'campaign_categories');

        if (count($campaings) > 0) {
            $data = array('status' => '-1');
            $campaings = $this->CM->common_update($data, $where, 'campaign_categories');
            $return = array('message' => 'Success', 'code' => 200, 'token' => $this->security->get_csrf_hash());
        } else
            $return = array(
                'message' => 'error', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );

        echo json_encode($return);
    }

    public function add_category() {

        if ($this->input->post()) {
            $data = $this->input->post();
            unset($data['submit']);
            $r = $this->CM->insert($data,'campaign_categories');
            if ($r) {
                setMessageSuccess('Category added successful !!');
            } else {
                setMessageError('Somthing went wrong, please try again.');
            }
            redirect('admin/categories/add_category');
        }
        $data = array();
        $this->layouts->render('admin/add-category', $data, 'admin');
    }
    
    public function edit_category($id) {
		if(empty($id))
		{
			setMessageError('Somthing went wrong, please try again.');
			redirect('admin/categories');
		}

		if($this->input->post())
		{
			$data = $this->input->post();
			unset($data['submit']);
			$where['id'] = $id;
			$r = $this->CM->common_update($data,$where,'campaign_categories');
			if($r)
			{ 
				
				setMessageSuccess('Category data updated successful !!');				
			}
			else
			{
				setMessageError('Somthing went wrong, please try again.');
			}
			redirect('admin/categories');			
		}
		$data = array();
		$data['data']  = $this->CM->get_single_row_from_id(['id' => $id],'campaign_categories');
        $this->layouts->render('admin/edit-category',$data,'admin');
	}

}
