<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Donation extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('Campaign_model', 'CMM'); // load model
        $this->load->model('Users_model', 'UM'); // load model
        $this->load->model('Common_model', 'CM'); // load model
        //$this->layouts->set_title("Home");
    }

    //make donation

    public function donate() {
        $meta = $this ->CM-> get_single_row_from_id(['slug' => 'campaign-profile'],'pages');
        $this->layouts->set_title($meta ->page_name);
        $this->layouts->set_meta_title($meta ->meta_title);
        $this->layouts->set_meta_description($meta ->meta_description);
        $this->layouts->set_meta_keyword($meta ->keyword);
//echo we_pay_library;
        $slug = $this->uri->segment(2);

        $where = array('slug' => $slug,'preview_mode' => 0);

        $campaign = $this->CMM->getCampaignBySlug($where);

        if (empty($campaign)) {

            redirect('home');
        }

        $this->layouts->set_title($campaign->title);

        $data['campaign'] = $campaign;

        if($this->session->userdata('is_logged_in')){
                $this->layouts->render('campaign/donation', $data, 'default-signedin');
          }else{
                $this->layouts->render('campaign/donation', $data, 'default');
          }
    }

    public function checkout() {
        $campaignId = $this->input->post('campaign_id', true);

        //if (!empty($this->session->userdata('user_id')))
        // $userId = $this->session->userdata('user_id');
        $cardId = $this->input->post('card_id', true);
        $amounts = $this->input->post('amount', true);
        $comment = $this->input->post('comment', true);
        $is_public = $this->input->post('is_public', true);

        // WePay PHP SDK - http://git.io/mY7iQQ
        require(we_pay_library);

        $campaignerAccount = $this->UM->get_user_info_from_campaign($campaignId);

        // application settings
        $account_id = $campaignerAccount->new_wepay_account_id; // your app's account_id
        $client_id = wepay_client_id;
        $client_secret = wepay_client_secret;
        $access_token = $campaignerAccount->new_wepay_access_token;

        //credit card id to charge
        $credit_card_id = $cardId;

// change to useProduction for live environments - useStaging
        Wepay::useProduction($client_id, $client_secret);

        $wepay = new WePay($access_token);

// charge the credit card
   try {
        $response = $wepay->request('checkout/create', array(
            'account_id' => $account_id,
            'amount' => $amounts,
            'currency' => 'USD',
            'short_description' => $comment,
            'type' => 'donation',
            'payment_method' => array(
                'type' => 'credit_card',
                'credit_card' => array(
                    'id' => $credit_card_id
                )
            ),
            "fee" => array(
                "app_fee" => ($amounts * processing_fee),
                "fee_payer" => "payee"
            ),
            "email_message" => array(
                "to_payer" => "Please contact us at 555-555-555 if you need assistance.",
                "to_payee" => "Please note that we cover all shipping costs."
            ),
            //"auto_release" => false,
        ));
  }
catch(Exception $e) {
  //echo 'Message: ' .$e->getMessage();
           $return = array(
                'message' => 'success', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );
}
        if (!empty($response)) {
            if ($this->session->userdata('user_id')) {
                $userid = $this->session->userdata('user_id');
            } else
                $userid = 0;
            $donation = array(
                'campaign_id' => $campaignId,
                'user_id' => $userid,
                'amount' => $amounts,
                'create_date' => date('Y-m-d H:i:s'),
                'donor_first_name' => $this->input->post('donor_first_name', true),
                'donor_last_name' => $this->input->post('donor_last_name', true),
                'email' => $this->input->post('email', true),
                'country' => $this->input->post('country', true),
                'postal_code' => $this->input->post('postal_code', true),
                'comment' => $comment,
                'is_public' => $is_public,
            );

            $this->CM->insert($donation, 'campaign_donations');
            $where = array(
                'id' => $campaignId
            );
            $campaign = $this->CM->get_single_row_from_id($where, 'campaigns');

            $this->session->set_flashdata('message', 'Thank you for your support');
            $redirect_uri = base_url() . 'donation-success';
            $return = array(
                'message' => 'success', 'code' => 200, 'redirect_uri' => $redirect_uri
            );
        }
        else {
            $return = array(
                'message' => 'success', 'code' => 201, 'token' => $this->security->get_csrf_hash()
            );
        }
        echo json_encode($return);
        //display the response
        // echo '<pre>';
        // print_r($response);
        // exit;
    }

    public function thankyou() {
        $meta = $this ->CM-> get_single_row_from_id(['slug' => 'appriciate-donation'],'pages');
        $this->layouts->set_title($meta ->page_name);
        $this->layouts->set_meta_title($meta ->meta_title);
        $this->layouts->set_meta_description($meta ->meta_description);
        $this->layouts->set_meta_keyword($meta ->keyword);
        
        //$this->layouts->set_title('Appreciate Donation');
        $this->layouts->render('campaign/appreciate-donation', $data = array(), 'default');
    }

}
