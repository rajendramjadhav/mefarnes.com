<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Layouts {

    private $ci;
    private $title = NULL;
    private $meta_title = NULL;
    private $meta_description =NULL;
    private $keyword =NULL;

    public function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->library('session');
    }

    public function set_title($title) {
        $this->title = $title;
    }

    public function set_meta_title($meta_title) {
        $this->meta_title = $meta_title;
    }
    
    public function set_meta_description($meta_description) {
        $this->meta_description = $meta_description;
    }
    public function set_meta_keyword($keyword) {
        $this->keyword = $keyword;
    }

    public function render($view_name, $params = array(), $layout = 'site') {

        $view_content = $this->ci->load->view($view_name, $params, TRUE);

        $this->ci->load->view('Layouts/' . $layout, array(
            'content' => $view_content,
            'title' => $this->title,
            'meta_title'=> $this->meta_title,
            'meta_description' => $this->meta_description,
            'keyword'   => $this->keyword,
        ));
    }
    
    public function loadMessage($key,$type=''){
        $data = '';
        if(!empty($key)){
            $data = $this->ci->session->flashdata($key);
            if(!empty($data)){
            if(!empty($type) && $type=="error"){
                $data = "<div class='error' style='margin-bottom: 2%; text-align: center; background-color: rgb(255, 44, 44); border: 1px solid rgb(255, 44, 44);'>
                            <span style='color: white; text-align: center;'>$data</span>
                        </div>";
            } else if(!empty($type) && $type=="success"){
              $data = "<div class='error' style='margin-bottom: 2%; text-align: center; background-color: green; border: 1px solid green;'>
                            <span style='color: white; text-align: center;'>$data</span>
                        </div>";  
            }
            }
            
        }
        return $data;
    }

}
