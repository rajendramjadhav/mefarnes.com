<?php

class Authenticate extends CI_Controller
{
    public static $userSessionData;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        if (!$this->session->userdata('is_logged_in'))
        {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect("login");
        }
        else
        {
            self::setdata($this->session->userdata('userdata'));
        }
    }
    
    /**
     * set authencited user session data
     * @param [array] $data [[Description]]
     */
    public static function setdata($data)
    {
        self::$userSessionData = $data;
    }
    
    
    /**
     * get authencited user session data
     * @param  [string] [$data = NULL] [[Description]]
     * @return [string || array] [[Description]]
     */
/***
    public static function getData($data = NULL)
    {
        return empty($data) ? self::$userSessionData : (is_array(self::$userSessionData) ? self::$userSessionData[$data] : self::$userSessionData->$data);
    }
*/
}