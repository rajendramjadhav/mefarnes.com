<?php
require_once __DIR__ . '/php-sdk-v4/src/Facebook/autoload.php';
if (!session_id()) {
    session_start();
}

# login.php
$fb = new Facebook\Facebook([
  'app_id' => '864814753651368',
  'app_secret' => '80ef39731be86e28be7e9c91c0134a5c',
  'default_graph_version' => 'v2.2',
  ]);

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes']; // optional
$loginUrl = $helper->getLoginUrl('http://mefarnes.com/fb-share/login-callback.php', $permissions);

echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
